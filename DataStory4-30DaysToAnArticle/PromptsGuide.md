---
title: "30 Days to an Article: Archaeological Inspiration for Your Writing"
author: "Paulina F. Przystupa and L. Meghan Dennis"
date: "28 July 2023 - Version 1.0"
output:
  html_document: default
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Guide Overview

1. Introduction
2. How to get started
3. Prompts calendar
4. Sharing your work safely on social media
5. Acknowledgements
6. References for further reading

Estimated time to read this guide: 15 minutes

# Introduction

Welcome (back) to the Digital Data Stories of the Alexandria Archive Institute and Open Context! This guide includes everything that you'll need to use archaeological data to inspire your next writing project, be it creative, critique, or academic. Whether this is for your own work in archaeological science writing, for using archaeology to write a fantasy setting for your gaming group, or for critiquing the use of material culture in a particular media for a class project, we can help guide you through it. Of course, feel free to use whatever parts of this guide provide inspiration and get the creative juices flowing, or the gears turning, or whatever stuff you associate with starting to try and get things done, creatively.

This Data Story cultivates data literacy by inspiring a daily writing practice through the use of data-driven prompts. In this guide we start with an introduction to the basics of this Data Story and its purpose. If you want more guidance, we include some examples of how to use the data-driven prompts and an example fictional narrative generated from our prompt list. In addition, we have a short summary of various online-accessible archaeological data and projects you can use to expand your pool of inspiration. This is followed by our data-driven prompts calendar. We conclude by acknowledging the awesome folks who helped make this Data Story a reality, and offer suggestions for further reading.

The guide can be used to celebrate or explore other inspirational writing projects. It can be used to work within the bounds of [Artober or ArchInk](https://doi.org/10.6078/M7WW7FSR)---which often coincide with [National Arts & Humanities Month](https://www.americansforthearts.org/events/national-arts-and-humanities-month)---to write about archaeology, during [Asian American Native Hawaiian Pacific Islander (AANHPI) Heritage Month](https://asianpacificheritage.gov/) to explore archaeological themes in AANHPI heritage, or through [National Novel Writing Month (NaNoWriMo)](https://nanowrimo.org/) or [Archaeology Writing Month (ArchWriMo)](https://doi.org/10.6078/M7HQ3X11) to fuel your novel or academic writing. In addition, feel free to integrate the prompts and our other search tutorials into a class on any related subject, or to use them for personal enjoyment.

Accompanying this guide, we're also linking to [*It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration - Part Two: The Project Search Tutorial*](https://doi.org/10.6078/M7ZP4478), [*Of Mycenaean Men: Public Archaeology Book Club - Part Two: The Open Context Keyword Search Tutorial*](https://doi.org/10.6078/M7TX3CHJ), and the [*Digging Digital Museum Collections*](https://doi.org/10.6078/M7NC5ZB1) article series from the Alexandria Archive Institute's News. These resources are all places and ways you can explore the prompts using existing archaeology related data to supplement the inspiration provided by this guide. With that, write like the wind!

# How to get started

As we mentioned, you can do anything you'd like with the prompts. But if that's too general, we'll outline how to get started and provide an example of what we did with the prompts. If you don't need guidance and already have a good idea of where you'd like to go, no problem, be inspired by data and embark on your journey.

Otherwise, to develop a new creative project, look at the first word in the list and consider what it means to you. Consider if it's a noun or a verb and what experience you've had with it. Consider what you already know about the word and your own experiences. You can even consider your experience with the word, as the attributes or observations about it you can make---as data.

Once something interesting inspires you, though, go with it! Your writing doesn't have to be long. The output can be short and simple to allow you to grow your practice. This guide is here to support your goals, and sometimes restricting a project with rules helps us flourish!

## An archaeological narrative example

What if that's not enough and you're afraid of bad writing? Well, so are a lot of people, but you can't write anything well if you don't write at all. Below, we've provided an example narrative, based on the entire prompt list, to give you an idea of what you can do if you use some of the tips from this guide.

Using all the prompts in one piece is also a possibility for solo practice or to utilize this guide within the bounds of a single educational session. It is also a nice writing practice to get you working with words, on your way to your next draft, whatever it is.

For reference, this was not a narrative cultivated over a long period of time. The authors of this guide and of the narrative below, although they have edited it for clarity, did exactly what we suggest you do in the next few sections. We picked a focus (“We need an example narrative that includes all the prompts...”), found a guide (the prompts list), then set a timer of sorts (“...we need this done today”). It may have even been a 25 minute timer, we've now forgotten...as we write a LOT. Then we wrote the narrative, and what you see below is that product. More importantly, though, is that the byproduct was that we were excited to keep going! So try it, get writing and see where it takes you.

### A Moment in the Life of an Exhausted Graduate Student

![](Images/30DaystoanArticle_ALifeOfATiredGradStudent.jpg)

> I stared into the middle distance of my computer screen. Trying to remember what a ***hypothesis*** even was. It was the sort of  ***feature*** of graduate school that I was just supposed to know. But as I looked again at my ***models*** section. I realized I had no clue.

> So I stared.

> If I didn't remember what my hypothesis was, how I was supposed to leverage my ***theory*** to argue that my ***evidence*** meant anything? Maybe that wasn't what I needed to work on today. So I closed that document and took a second to look at the ***photograph*** that was my wallpaper. It was me, smiling next to a ***posthole*** I had revealed a few summers ago, that happened to have a nail still in it. 

> Why the wood had decayed but that nail hadn't, I have no idea. Maybe that site was a ***palimpsest*** and the nail had come from a different ***deposit***. Oh dear, I was supposed to understand the ***significance*** of that. I had turned that into a ***figure*** in my dissertation. It was even a call out in one of the ***maps*** I had. I had ***drawing*** after ***drawing*** of that ***belonging*** in my notebook somewhere.

> Wow, I had lost my ***orientation***. ***Statistics***, maybe statistics was what would get me back on track. I searched my computer for my data table. Ah, sweet spreadsheets. Where the entire ***sample*** could be searched at once. Maybe this was where I had ***deposited*** my knowledge. In and amongst the ***lithic*** remains from Locus B. Or maybe it was in the separate tab for ***bone***. Probably lots of ***feasts*** there. I remembered we had no ***textiles*** at the site, so I definitely hadn't gotten lost in those threads. Ha, good joke, me!.

> Opening that up reminded me of the ***deep history*** of the site. The different ***populations*** that had lived in the area throughout time were really clear in the vast ***archive*** of what years of excavation had revealed.

> I decided to look at the ***pottery*** data again, especially that from the ***midden***. The ***descendant*** community was particularly interested in those. The local community had a ***contemporary*** practice of ceramics and wanted to see connections between the present and the past.

> When I clicked on that tab I was assaulted though by another p-value. Is that number something a ***stakeholder*** would care about? It made me think about all those discussions I'd had years ago, before getting lost and not remembering how hypotheses worked. Those early grad school arguments are part of my own ***oral history***.

> Oh, no. Maybe I just need some sleep. I can't start thinking of my own life as ***provenance*** research. Thinking that graduate school gets its own  ***provenience*** number signals a good time to just stop and head to bed.

## Setting your writing intention 

An important part of building any writing practice and attaining your goals is determining your intention. What do you want to write? How developed is your idea? Is it in a particular format? Is there a specific journal you want to submit to? Is there a particular website that you are looking to pitch?

Considering questions like these can help you figure out what your focus is right now. In addition, you may have multiple goals that you're working toward. That's cool too! Once you've figured out your focus (or foci), it's much easier to figure out how to get there.

Another suggestion we've gleaned from works like [*The Anti-Racist Writing Workshop*](https://www.antiracistworkshop.com/) is to write where and when you write best. Creating a good writing environment, or changing it up to match when you need to change tasks, will help support you in reaching your intentions. So, figure out what your writing habits are before trying to change them.

Do you like cafes? Do you like libraries? Or, do you do your best writing on napkins on the back of the seat of a moving bus? If you don't know, take some time to change your environment. If you're not feeling like your desk is working well for today, try writing at your local library and seeing what books on archaeology they have that can inspire you. Take some time to think back on days of “good writing” or “productivity” and consider what kind of environment you were in. Place can also be things like time of day, mood, smells, sounds, and other senses, so take note of those as well!

You may wonder though, what happens if you get behind, or forget one day? You're in the wrong spot for your writing, either to meet your goals or when someone has taken *your* spot at the cafe? Don't panic, it will be ok!

While we wrote these as daily inspirational prompts, there's nothing stopping you from using all the prompts at once, or building on what you did on a previous day, or combining the prompts from consecutive days. All of that is okay because we're interested in you developing your data literacy skills. Working with what you know about the prompt terms over longer periods of time will help you build writing as a habit. And, your focus and best places to write can shift, so just approach your writing with intention and work with habits that cultivate practice, rather than an ideal.

## How to get writing

Once you have a sense of your foci and when and where you work best, make sure to be in those places and hold yourself accountable. And once you are there, just write. Don't think, ponder, or rearrange your desk. Put your pen to paper and start writing, or put your fingers on keys and start typing. Many books on writing will tell you that waiting for inspiration is like waiting for the past. It will never arrive. So what's left is that you just gotta write. And, once you start, it's usually easier to keep going.

If going without a plan is too much, you can try variations on timing your writing. These are sometimes called [*writing sprints*](https://thewritepractice.com/writing-sprints/). They are typically short duration timed exercises where you just write. You don't think, or stare out the window, you just get your hand moving with the pen or fingers typing. And they can be about whatever you want, you could even write the prompt word over and over until something happens. This is similar to the [Pomodoro Technique](https://en.wikipedia.org/wiki/Pomodoro_Technique).

Using these kinds of timers acts in lieu of the old scaffolds for finishing or starting writing---aka paper deadlines, tests, or free writes like we did in school. For many of us, that's the place we really did the bulk of our writing, so a timer can act like the dreaded 20 minutes left written on the board. However, we never need to stay with that, it's just one way to start. In addition, when just sitting down to write doesn't work well, it's often because we're not committed to our focus. Setting writing foci can be difficult and that's where prompt guides like this can help. Use these as a way to focus your first sprint, then see where that takes you.

## Searching the terms for inspiration

If you need to jump-start your creativity, you can use [*It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration - Part Two: The Project Search Tutorial*](https://doi.org/10.6078/M7ZP4478) to search one of Open Context's open data publications. This will bring you to the project that inspired our other Creative Series Data Story, [*It's All in the Wrist (Bones)*](https://doi.org/10.6078/M7N877XP).

There you can look for more information and photos to spark your interest. Searching around for more information is an important part of cultivating our ability to read and work with data, which are important elements of archaeological data literacy. So give yourself a good amount of time to do this. You'll want to read more about specific entries that come up when you search the prompt term and then click through various photos, and possibly read some documents to provide more context, or introduce you to new information.

If that doesn't work, or nothing comes up within the project search, try widening your search using [*Of Mycenaean Men: Public Archaeology Book Club - Part Two: The Open Context Keyword Search Tutorial*](https://doi.org/10.6078/M7TX3CHJ). Hopefully with these guides an idea will spark after looking and comparing the various data that relate to the prompt term. Or you can draw from [*How to Search Online Museum Collections*](https://doi.org/10.6078/M7S46Q2R), a video guide available through the Alexandria Archive Institute / Open Context for searching other collections.

If these still aren't quite working, or you're looking to work with a particular kind of data, here are links to some other archaeological data platforms. This is a short list of some places you can get archaeological data online, for free. Many of these have their own resources for how to search, so we'll help you navigate to those pages: 

+ [ARIADNE PLUS](https://ariadne-infrastructure.eu/wp-content/uploads/2019/02/AriadnePortalGuide_def.pdf)
+ [Digital Archaeological Archive of Comparative Slavery (DAACS)](https://www.daacs.org/query-the-database/)
+ [Chaco Research Archive](http://www.chacoarchive.org/cra/)
+ [NOMISMA](http://nomisma.org/browse)
+ [Neotoma](https://www.neotomadb.org/data)
+ [Kerameikos](http://kerameikos.org/browse)
+ [Pleiades](https://pleiades.stoa.org/places)
+ [The Digital Archaeological Record (tDAR)](https://www.tdar.org/using-tdar/searching-tdar/)
+ [Archaeology Data Service (ADS)](https://archaeologydataservice.ac.uk/search.xhtml)

We don't know what results you'll find on these platforms but we feel that it's important to highlight the many places archaeological data are available. In general, we hope that your results help get you in the mood to keep working on whatever it is that you are working on, and maybe inspire you to make some of your archaeological data open!

# The Prompt Calendar 

![]( Images/30Days_GraphicPages_2023Jul27_Calendar.jpg)

| Dates | Prompt |
|-------|--------|
|1|Photograph|
|2|Textile|
|3|Orientation|
|4|Map|
|5|Evidence|
|6|Statistics|
|7|Belonging|
|8|Hypothesis|
|9|Model|
|10|Theory|
|11|Figure|
|12|Population|
|13|Sample|
|14|Significance|
|15|Palimpsest|
|16|Feature|
|17|Deposit|
|18|Stakeholder|
|19|Drawing|
|20|Contemporary|
|21|Deep History|
|22|Pottery|
|23|Bone|
|24|Feast|
|25|Oral History|
|26|Posthole|
|27|Descendant|
|28|Midden|
|29|Archive|
|30|Provenance/Provenience|
|31|Lithic|

# Sharing your work safely on social media

One way you might choose to use this guide is as a way to participate in various online creative communities or thematic months via social media. These can be during thematic months inspired by particular heritages, such as [AANHPI Heritage Month](https://asianpacificheritage.gov/), or through particular kinds of creative works, such as [NaNoWriMo](https://nanowrimo.org/) or the archaeology-themed NaNoWriMo variant, ArchWriMo, that runs in November. You can read more about that [here](https://doi.org/10.6078/M7HQ3X11).

ArchWriMo's goals partially inspired this Data Story and we're grateful for it. Beyond those months, though, many people inside and outside of archaeology benefit from and enjoy writing about archaeology. In classrooms, students write about sites; in fiction, writers research archaeological cultures to bring authenticity to their work; and in media, critics use archaeology to point out issues like [racism in media](https://web.archive.org/web/20200401222754/https://womenwriteaboutcomics.com/2020/03/pretty-deadly-pretty-cautious/). And, sometimes, projects get shared online.

So, you might be thinking, what should I do with my writing once it's written? Well, once you've created whatever piece the day inspired you to make, there are plenty of options. The first is always to enjoy what you wrote for yourself and relish the fact that you wrote something based on archaeological data that furthers your writing goals! You stretched your archaeological data literacy skills by taking a word and turning it into some new analysis, working with data, arguing something, or even communicating something about your work. Or you searched for more information about it and used that to play with your data and ideas even more. That's super cool all on its own.

Another option is to share what you've worked on in some capacity, and that's where various hashtags and protections to your work might come in. Depending on whether you started this writing in association with a specific month or not, there may be established hashtags that you'll want to use to associate your work with a specific community online and on social media.

For example, the DLP (the “we” in this guide!) occasionally uses this guide to post an inspirational prompt each day during various months, encouraging interested participants to share their creations. We do this through sharing images (with [alt text](https://web.archive.org/web/20230706143056/https://accessibility.huit.harvard.edu/describe-content-images)) of pages we've written and inspirational text. For NaNoWriMo shares, these might include the prompt word as a hashtag, the day of the month, and the specific NaNoWriMo, AcWriMo, or ArchWriMo tag you're participating in, so it's associated with other writing by folks focusing on archaeology-inspired work. Exploring such tags is also a great way to see what other folks are creating and celebrate everyone's pieces.

If you choose to share your work, which is awesome and not mandatory, don't forget to include your signature, a watermark, or even a photo of yourself with your work. That way, should anyone find and share it, you get credited for your work. This helps us all enjoy the things we create while respecting people's rights to own their work.

# Acknowledgements

The DLP would like to acknowledge the work that goes into the documentation and tutorials that explain how to search the specific sites we list in this resource. Thanks also to the ArchWriMo organizers who inspired this guide. In addition, we'd like to thank Dr. Kate Ellenberger for thoughtful insights on the formalization of grassroots collaborations by existing institutional entities, which guided this Data Story into its current form. Finally, thanks to Drs. Donna Yates and Laura Heath-Stout for responding to our inquiries and facilitating ArchWriMo in the past.

# References

For more information about NaNoWriMo check out the website: [https://nanowrimo.org/](https://nanowrimo.org/)

On building healthy writing environments in the classroom, check out *The Anti-Racist Writing Workshop: How to Decolonize the Creative Classroom* by Felicia Rose Chavez: [https://www.antiracistworkshop.com/](https://www.antiracistworkshop.com/)

The Data Literacy Program is made possible in part by the National Endowment for the Humanities, and by the Mellon Foundation. Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.

License: [Creative Commons CC-BY](https://creativecommons.org/licenses/by/4.0/)