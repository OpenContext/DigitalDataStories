---
title: "30 Days to an Article: Archaeological Inspiration for Your Writing - Educator resource"
author: "L. Meghan Dennis, PhD. Postdoctoral Researcher for Data Interpretation and Public Engagement"
date: "28 July 2023 - Version 1.0"
output:
  word_document: default
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

![](Images/30Days_EducatorHeader.jpg)

## Contact the Team

If you or your students have questions, comments, or concerns, please get in touch. We love feedback, especially from educators!

L. Meghan Dennis  
@gingerygamer (she/her)

Paulina F. Przystupa  
@punuckish (she/their/none)

<datastories@opencontext.org>

# Exercise Description and Aims

The [Digital Data Stories Project](https://doi.org/10.6078/M70P0X5P) promotes an increased focus in archaeological education on digital data literacy. Through the use of open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis. These practicals illustrate the confluence of science and humanities-based investigations in collected data on the past.

This approach promotes multiple levels of engagement with archaeological datasets, linking data-driven narratives with the key analytical and interpretive steps used to ethically analyze, visualize, and present research data.

In this [Digital Data Story](https://doi.org/10.6078/M7HH6H6C), participants will learn to:

> Use lists of terms to prompt creative outputs

> Participate in online sharing

> Set writing intentions

> Meet writing goals

> Share work safely on social media   

## additional texts

Neha Gupta, Andrew Martindale, Kisha Supernant, and Michael Elvidge. (2023). “The CARE Principles and the Reuse, Sharing, and Curation of Indigenous Data in Canadian Archaeology.” Advances in Archaeological Practice 11 (1). Cambridge University Press: 76–89. [DOI:10.1017/aap.2022.33](https://doi.org/10.1017/aap.2022.33).

Sara Perry. (2019). The enchantment of the archaeological record. European Journal of Archaeology, 22(3), 354-371. DOI:10.1017/eaa.2019.24. Also available at: <https://emotiveproject.eu/wp-content/uploads/2019/07/Perry_2019_Enchantment_of_the_archaeological_record.pdf>

## the dataset

In lieu of a specifc data set, in this Data Story we encourage participants to use the whole of [Open Context](opencontext.org), as well as other data publishers and data repositories listed.

This breadth of options allows researchers to select site data in the form of tables, texts, media, and maps. However, it may also be used for many other purposes relating to the study of archaeology and heritage in general.

## assessment and scaffolding options

For those students who require (or desire!) a more challenging data exercise, tutorials offered at [GLAM Workbench](https://glam-workbench.net/) can be utilized as scaffolding.

For those students who require (or desire!) more support,  splitting into pairs to complete the [Digital Data Story](https://doi.org/10.6078/M7HH6H6C) offers the opportunity to work together, switching off tasks and comparing findings.

As there are opportunities during the [Digital Data Story](https://doi.org/10.6078/M7HH6H6C) for students to select data for investigation based on their own interests, the end results produced may vary. One potential assessment option is to ask students to re-attempt the tutorial with different search goals.

## Technical Requirements

If engaging independently via their own computers, students should have a basic knowledge of how to operate a computer and how to visit a website.

If engaging via lab computers, students should have a basic knowledge of how to operate a computer and how to visit a website. Lab computers should have the following programs installed:

> A browser with internet access

> A text editor or word processor (like TextEdit, Notepad, or Word)

The website that students will be using for this exercise is available at:

<https://opencontext.org/>

## Duration

The Digital Data Story should take approximately 15 minutes to read, and a variable time to complete. It can be completed separately, or used along with other tutorials.

If students are completing a portion of the tutorial in more than one session, please direct them to pay close attention to where they left off, and the specific internet address related to the step they finished on.

### get in touch!

Please reach out if you or your students have questions, comments, or concerns. We love feedback, especially from educators!

License: [Creative Commons CC-BY](https://creativecommons.org/licenses/by/4.0/)