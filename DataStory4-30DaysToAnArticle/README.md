![](Images/30Days_Header-2871x1521.jpg)

This exercise is best suited to those with an interest in public archaeology, writing as practice, or using archaeological data as inspiration. Users should have a basic understanding of archaeological data types to understand what platforms to search, but little previous experience with archaeology is required.

This page provides access to the resource in two ways. The first is through a series of PDFs that represent the completed Data Story. The PDFs include:

1. [Creative Prompt Guide](https://doi.org/10.6078/M79Z931X) 
1. [Teaching Guide](https://doi.org/10.6078/M7CR5RGN)

This repository is secondary access to the above listed materials. These markdown files represent the source material for the text and primary images within the PDFs, minus additional formatting and with only a general placement of images. This code represents our commitment to open science and transparency in our process.

These materials, either through the PDFs or the code, are designed to be used synergistically. However, any piece of this Data Story may be used separately or re-ordered according to the requirements of the individual or to specific educational goals. In addition, this Data Story references [*Of Mycenaean Men - Part Two: The Open Context Keyword Search Tutorial*](https://doi.org/10.6078/M7TX3CHJ) and [*It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration*](https://doi.org/10.6078/M7S180ND). Furthermore, the markdown files may be altered and forked from the Codeberg repository for custom use with different data sets. These resources are available free to use under a Creative Commons Attribution ([CC BY](https://creativecommons.org/licenses/by/4.0/)) license.

Thank you so much for utilizing our educational resources! If participants have the time, please consider contributing thoughts to our [ongoing survey](https://berkeley.qualtrics.com/jfe/form/SV_5mBm1RbFtQqTjZc), whose data we will use to periodically update the resource. Such updates will be noted here, and will be first available through the code repository prior to PDF updating.

*First published: 28 July 2023*

This work has been made possible in part by the National Endowment for the Humanities and The Mellon Foundation. Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or The Mellon Foundation. 