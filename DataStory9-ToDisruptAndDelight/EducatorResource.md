---
title: "To Delight and Disrupt:  Teaching Archaeology Through Narrative"
subtitle: "Educator Resource"
author: "Paulina F. Przystupa Postdoctoral Researcher in Archaeological Data Literacy"
date: "`r Sys.Date()`"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

![](Images/AAI-OC_DataStoryPublished_Header-ToDelightAndDisrupt-Educators.jpg)

# Exercise Description and Aims

The [Data Stories Project](https://doi.org/10.6078/M70P0X5P) promotes an increased focus in archaeological education on digital data literacy. Through the use of open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis and pedagogy. These practicals illustrate the confluence of science and humanities-based investigations in collected data on the past.

This approach promotes multiple levels of engagement with archaeological data sets, linking data-driven narratives with the key analytical and interpretive steps used to ethically analyze, visualize, and present research data.

![](Images/ToDelight_wordcloud.png)

In this [Digital Data Story](https://doi.org/10.6078/M7X928FR), participants will learn to:

+ Critically read academic articles
+ Summarize the value of data stories
+ Analyze pedagogical approaches
+ Evaluate the relevance of archaeological data literacy
+ Develop an understanding of education through narrative

## additional texts   

Graham, Shawn, Neha Gupta, Jolene Smith, Andreas Angourakis, Andrew Reinhard, Kate Ellenberger, Zack Batist, Joel Rivard, Ben Marwick, Michael Carter, Beth Compton, Rob Blades, Cristina Wood, and Gary Nobles (2020) *The Open Digital Archaeology Textbook*. [https://o-date.github.io/](https://o-date.github.io/)

Walter, Maggie, Tahu Kukutai, Stephanie Russo Carroll, and Desi Rodriguez-Lonebear (editors) (2020) *Indigenous Data Sovereignty and Policy* Routledge: London. [https://doi.org/10.4324/97804292739](https://doi.org/10.4324/9780429273957) 

## the data set

In lieu of a specific data set, in this [Digital Data Story](https://doi.org/10.6078/M7X928FR) we encourage participants to consider how publications act as data sets that bridge understanding through grammar, syntax, and content. We also focus on our final data literacy component—communication—to encourage participants to consider their reflections, summaries, and responses as data about their own learning.

For Part Two, learners may use open data sets available on [Open Context](https://opencontext.org/) in their data story. In addition, the Data Publications on Open Context may be used for a variety of other purposes relating to the study of archaeology and heritage in general. For example, they can be used as quality sources of archaeological data to develop skills in filtering data in spreadsheets or R or to learn more about belongings and artifacts from around the world.

## assessment and scaffolding options

For those students who require (or desire!) additional support, splitting into pairs to complete the [Digital Data Story](https://doi.org/10.6078/M7X928FR) offers the opportunity to work together to answer the initial response questions and discuss any challenging parts of the assigned publication(s).

The suggested publication(s) may also be read in smaller segments or responses limited to only the questions relevant to pedagogical goals of the specific cohort.

As there are opportunities during the [Digital Data Story](https://doi.org/10.6078/M7X928FR) for personalization, the end results produced may vary. One potential assessment option is to ask students to assess a classmates' responses to the publications or to swap narrative products created by their fellow students to explore their preliminary teaching-with-narrative products or notes for such a creation

## Technical Requirements

If engaging independently via their own computers or devices, students should know how to: visit and navigate a website and access online text. Participants should have a notebook, journal, or some other method of recording their responses, summaries, and reflections. Phones, voice recorders, or laptops are fine, as long as their technical operation doesn’t distract from the process of critical reading.

If engaging via lab computers, students should have a basic knowledge of how to operate a computer and how to visit a website.  Lab computers should have the following programs installed:

+ A browser with internet access 
+ Optional: a PDF reader
+ Optional: A way for users to record their responses, summaries, and reflections

Optional data sets for use with Part Two are available at: 

[https://opencontext.org/](https://opencontext.org/)

## Duration

This [Digital Data Story](https://doi.org/10.6078/M7X928FR) is given in two parts, with part one broken up by the reading of an article partway through the guide. Each part can be completed separately, though they are additive when completed together.

The first part is a response and reflection guide that should be paired with a recommended reading from the list provided with that piece. This may take multiple hours depending on the level of detail students give to their responses, summaries, and reflections. The second part will also vary in duration as each learner will approach responding to and playing the work of interactive fiction in their own way. If completing this Digital Data Story in more than one session, please direct users to pay close attention to where they stopped and keep track of their notes. 

### get in touch by contacting the team

If you or your students have questions, comments, or concerns, please get in touch. We love feedback, especially from educators!

Paulina F. Przystupa
@punuckish (she/their/none)

<datastories@opencontext.org>

**Licensing:** The text from this educator's resource text and the image "To Disrupt and Delight Word Cloud" are both by Paulina F. Przystupa from the Data Literacy Program (DLP) /  [CC BY](https://creativecommons.org/licenses/by/4.0/). The main image "To Disrupt and Delight Educator's header" by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) and includes an adaptation of "[Figure 014 from Africa/Egypt/Abydos/Operation 203](https://n2t.net/ark:/28722/k23r17177)" by Matthew Douglas Adams from [Open Context](https://opencontext.org/) licensed [CC BY](https://creativecommons.org/licenses/by/4.0/).