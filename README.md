# Digital Data Stories

![](https://alexandriaarchive.org/wp-content/uploads/2021/02/data-stories-logo.jpg)

Welcome to the Digital Data Stories (DDS) Project from the Alexandria Archive Institute! We’re happy to provide these educational resources, which can be used for personal practice or integrated into a course.

The DDS Project promotes a focus in archaeological education on digital data literacy. These exercises, guides, and tutorials teach principles of digital data literacy alongside methods in archaeological analysis and combine different ways of learning and literacy components to illustrate the confluence of science and humanities-based investigations in data collected about the past.

This approach promotes multiple levels of engagement through reading, working with, analyzing, arguing, and communicating with archaeological data to ethically explore the data we have about the past. In these resources, users will utilize a variety of data, from published open-access data sets to popular books to their own belongings to cultivate and practice archaeological data literacy. 

We’re happy to provide you with these educational resources, which can be used for personal practice or integrated into a course. PDF versions of this are available at [https://alexandriaarchive.org/digital-data-stories/](https://doi.org/10.6078/M74F1NW0) or <https://doi.org/10.6078/M74F1NW0>.