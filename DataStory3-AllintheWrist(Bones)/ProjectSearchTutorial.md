---
title: "It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration"
subtitle: 'Part Two - The Project Search Tutorial'
author: "Paulina F. Przystupa"
date: "20 July 2023 - Version 1.0"
output:
  html_document: default
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Tutorial overview

In Part Two of this Data Story we will learn to:

1. Navigate to Open Context on the web
2. Search or navigate to a particular project on Open Context
3. Look at data records from that project
4. Search those records using keywords
5. Examine digital objects associated with those keywords

## Key concepts used in this tutorial

- browse for a project
- browse a data set
- data structures
- columns, attributes, headings
- entries, rows, observations
- objects
- keyword search

Estimated time to complete this tutorial: 60 minutes

# Introduction

Welcome (back) to our Digital Data Stories! In this tutorial, we're going to focus on reading data. To do this we're going to walk through how to search for and access one specific data set on [Open Context](https://opencontext.org) (OC). After that, we'll look at using images and textual information in that data set as artistic inspiration for something like Artober. If we're unfamiliar with Artober, check out [*It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration Part One - The Prompt List*](https://doi.org/10.6078/M7KH0KG4). But, why would we do this?

Well, we may be students assigned to use a specific data set on OC, or we may be interested in finding out about a specific project whose data OC holds. Or, maybe we thought the project sounded cool and we wanted to learn more about it as it seemed to fit with some ideas we're already working on. Regardless, we know what data set we want to explore, we just haven't had to find it on our own before. In this tutorial, we'll be navigating to and searching the data set called *Oracle Bones in East Asia*.

# Finding Open Context on the Web

Likely, if we've found this data story we are probably familiar with searching on the web in some capacity. Or at least, we know which buttons to click to get to the sites we're familiar with. Regardless of how comfortable we are with getting to a website, we'll explore some options for how to navigate to Open Context (OC), where the data set we're interested in searching is located.

Once we've used one of the three options below for navigating to OC's website we can move on to the next section called **Finding our project**. Remember, our goal is to get to a page that looks like the one in the following image (though the color in the search bars might be different).

![This is what the main page of Open Context usually looks like, and is where we'd like to go.](Images/OpenContextHomePage.jpg)

## Clicking a hyperlink

The easiest way to get to the OC website will be, assuming we're reading this on a computer that is already connected to the internet, by going to [https://opencontext.org](https://opencontext.org). You can do this by by clicking on the words, “[https://opencontext.org](https://opencontext.org)”. This should bring up our default browser, and navigate directly to the main page of OC, which should look like the image we have above.

## Copying and pasting a web address

The second way we can get to the website is by copying that website address or URL (which stands for Uniform Resource Locator) and pasting it into the box at the top of our browser window. To do this, we'll highlight the website listed in the **Clicking a hyperlink** section, [https://opencontext.org](https://opencontext.org), by clicking just to the left of the h and holding that click down while we use our mouse to scroll over to just past the **g** in “org”. We will then use the opposite, typically the right, click button on our mouse to bring up a menu.

![After opposite-clicking, these options will show up within a Microsoft OS environment.](Images/OppositeClickExample.JPG)

We'll scroll down to “Copy” and click on that. We'll then open up our favorite browser. Then we'll navigate to the bar where the web address goes, typically an empty box near the top of the screen, and then click into that box to bring up the flashing cursor. We'll then opposite-click in that area bringing up that same menu. This time we'll scroll down to “Paste”. That should put that URL into the box. If we didn't quite capture the URL correctly, we can also just type “[https://opencontext.org](https://opencontext.org)” into that box.

Once the web address is in the box, we can usually hit enter, or click a variation of a “go” button and the browser will navigate to the OC main page as seen above.
## Searching for Open Context within our browser

One additional way we can navigate to the page is by directly searching for the OC website. We can do this by navigating to our preferred search engine or by typing “Open Context” into our web address box.

It's a lot more common now for browsers, the thing we use to interact with the internet, to have the box where we put in the website address or URL also double as a place to search, without having to navigate to a specific search engine. Which website will do the searching depends on the defaults within our preferred browser.

Typically, when we search we tend to toss the words or sentences we're interested in into the box. This is especially helpful if we don't remember exactly what we're looking for. So, if we didn't remember the whole website address for Open Context, we can just put “Open Context”, on its own, into the box.

Once we have our favorite browser open, or have navigated to our preferred search engine, we'll just type “Open Context” into the search box and hit enter or “search” or whatever variant of that is appropriate for our online environment to navigate to the website.

![After searching for “Open Context” we'll get a list that might look something like this (if we're using Google Chrome to search with Google though possibly in a different color).](Images/SearchForOpenContext.jpg)

Once we've hit that button, we'll probably have a long list of potential sites to go to. We'll be looking for the Open Context site that also has something about “Publisher of Research Data” associated with it. When we click on that link, we should get to a page that looks like the one at the beginning of this section.

# Finding our project

Now that we've located the Open Context website on the internet and navigated to it, we'll need to explore OC for the particular project we're interested in. We'll do this in a way that gets to the project directly. To start, we'll locate the “Data Publications” navigation option in the upper left portion of the main page, also known as the navigation bar and it should look like this:

![This is where we can start looking for projects on the OC website.](Images/OpenContextBrowseMenu.jpg)

Once we've clicked on “Data Publications” we'll be brought to a new page.

![This is what the “Data Publications” page should look like after we click on it.](Images/OpenContextBrowseProjectsResult.jpg)

This is a collection of all the projects that OC currently publishes sorted by “Info Content, descending” as the default. We can change how projects are sorted by clicking on the little triangles next to each column heading.

Since we already know that *Oracle Bones in East Asia* is a project published on OC, the fastest way to get to the project is to type in “Oracle Bones in East Asia” in the search box above the Projects table that says “Type search term(s)..”. A slower way is to scroll to see if the project is in the first 50 entries and then keep going to the next page if it's not there.

![This is the Oracle Bones entry we're looking for.](Images/ScrollForOracle.jpg)

After finding the *Oracle Bones in East Asia* project, we'll click on the hyperlinked name of the project to bring us to the project specific page.

![This is what the project page for *Oracle Bones in East Asia* looks like.](Images/ProjectOracleBones.jpg)

This landing page gives us a description of the project overall and some background about who collected the data as well. If we're interested, we can spend some time reading this page to better understand the project.

Also, we've completed the first part of our search tutorial, we found the project! If we're feeling comfortable clicking around and seeing where this page takes us, great! Please feel free, that's what Open Context is for.

If we're using this guide in tandem with the [*It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration Part One - The Prompt List*](https://doi.org/10.6078/M7KH0KG4), we can take a look at the prompt for today and see where that takes us.

However, sometimes a little more guidance on how to search is important, and if that's the case, let's continue to the next section.

# Searching and browsing the project data

Now that we've done all the hard work of locating the *Oracle Bones in East Asia* project, we can take a breather. Shake off any residual tension that comes from clicking, scrolling, and searching, and start playing around with the data from the project. We'd like to note that right now we can only access some of the data for this project, so if you search in the future you might find more data!

This means that as we're clicking through and exploring the data, we might not be able to open everything, and that's OK. Data take time to clean and get the right permissions to use, so it's important to have patience when using open data sets. To start exploring we'll begin by finding the “Explore Project Data” box on the Project page.

## Browsing all data records

![Once we click on the “Subjects of Observation” triangle we'll get a drop down menu that looks like this.](Images/ProjectOracleBonesAllDataRecords.jpg)

If we know we're looking for a particular kind of data we can click a specific option under the 
“Subjects of Observation” triangle menu. Since we're browsing for now, we'll want to scroll down to “Subjects of Observations **(All)**” and click there. This will bring us, once again, to a new page.

![This is approximately the page we should have navigated to once we've clicked on “Subjects of Observations (All)” on the project page.](Images/OracleBonesAllDataRecords.jpg)

This brings us to a web page that lists all the records for this particular project. OC uses projects to organize the data it publishes. Each project represents the result of data creation work for research, conservation, and/or management by some entity. When data is systematically organized according to specific criteria (like a project), we call that data “structured”.

Projects are just one way to structure data and each database, although similar in spirit, will have its own structures. This is important to remember if we explore other databases that might have their data organized into “galleries'' or “collections”. These are just different ways to define specific aggregations of what they have available and different choices for how to organize what they have available. However, the structure of data isn't the first thing we'll notice visually. Most likely, the first thing will be a map with different colored squares.

These show us the approximate location of where these data came from in geographic space and an approximation of how many records are associated with that area. If we'd like, we can just stay here and play around with the map. If not, we can look at another interesting graphics generated from the data by exploring the different tabs.

![This is a temporal distribution of the belongings from this project, with higher peaks corresponding to more belongings coming from that period in time. ](Images/OracleBonesAllDataRecords_Scroll1.jpg)

After clicking on the **Time Ranges** tab, we'll see an image that looks like the one above. It looks like  a couple of superimposed hills, with each having a different height and slightly different color. High hills mean a lot of returned results associated with that time range and low hills mean that there aren't very many.

These hills also vary in width. Width indicates how sure we are of the date for a particular result and/or how long a period was. Wider hills indicate less surety, or larger range of potential dates, and narrower hills suggest better surety, or a smaller range of potential dates to associate with that result. The hills represent both how long something endured and an (educated) guess about the general time-frame associated with these results.

If we'd like, we can play around with some of the pieces on this graphic to see the range of times digital objects associated with the *Oracle Bones in East Asia* project appeared in the past. However, that can take a lot of time! So for those of us interested in other kinds of data, we can keep going through this tutorial and move to look at the the **Value Ranges** tab.

![Value ranges are data driven options for simple visualizations of various measured attributes within the data set.](Images/OracleBonesAllDataRecord_ValueRanges.jpg)

After clicking on the **Time Ranges** tab, we'll see an image that looks like the one above. It looks like a couple of superimposed hills, with each having a different height and slightly different color. High hills mean a lot of returned results associated with that time range and low hills mean that there aren't very many.

For *Oracle Bones in East Asia* the default Value Ranges that OC suggest are: Integer Descriptions, Decimal Descriptions and Calendar Descriptions. Under each categorization will be a list of specific attributes. We can either click directly on the attribute hyperlink or click the vertical ellipsis, select multiple attributes, and then the magnifying glass to visualize the data.

If we just clicked one attribute to visualize we'll get one bar chart (which we can learn more about by reading the [*Cow-culating Your Data in Spreadsheets and R*](https://doi.org/10.6078/M73N21HR) Data Story). If wanted to look at multiple attributes we'll get multiple bar charts.

We can alter these bar charts by filtering the values using the “Filter Ranges” boxes on the bottom. We can do this by typing or clicking the up and down arrows to increase or decrease the values and then clicking the inverted triangle button to filter by these options.

While we've been playing around with these different projects and attributes we may notice that more filters got added to the Applied Filters box. These identify what specifically we're looking at and what records returned based on those filters. That's pretty handy to double-check exactly what we're looking at! But if we want to return to our original search, we'll need to click on the little circles with “X”s in them to get back to searching for information about ‘bone'.

![The last section of the “all data records” site includes a table of all the data records for this project.](Images/OracleBonesAllDataRecords_Scroll2.jpg)

After clicking through those other tabs, we'll click on the last one labeled “Item Records”. It produces a structured arrangement of observations on the page. This is the actual list of data records for the project and it's organized into a table. If we aren't familiar with data tables we can take a look at the **Parts of a table** section. Otherwise, we can scroll through the existing list to examine what's there.

However, if we're feeling a little fatigued by all this scrolling and clicking we might choose to click on the first bone in the list. Clicking on your first entry will open a new tab rather than transform our current page.

![This is the unique page just for Bone 229 that we navigate to after clicking it's entry.](Images/Bone254-ObjectPage.jpg)

Examining the record for this specific bone brings up all the information for it in it's own cool page. Looking at its photo and related information might be enough for inspiration, so we can stop here if we'd like.

But, if we'd rather get more detail about the objects, with the additional observations gathered for belongings from the *Oracle Bones in East Asia* data set, we can skip to the **Searching by keyword** section and start exploring the data using a specific criteria.

However, if we're still wondering how to read the data in the table we can take a look at the next section.

### Parts of a table

So, tables aren't just things with four legs that support your laptop, tables are also a way to organize observations. In this kind of table, there are typically two axes of information that we use when structuring data into this form.

These are the horizontal headings, fields, columns, or attributes that list the specific observations that were taken for each object. For the list table on this page, these attributes are: Info Content, Category, Item, Project, Context, Chronology, and Updated. These are usually the first row, or line, in a data table.

![The data records in the “Item Records” tab are organized into a table. We've circled the headings and identified an individual row with a bounding box.](Images/OracleBonesAllDataRecords_Scroll2-CirclesAdded.jpg)

The next axis of information are the things listed vertically –-- entries, rows, or observations. These refer to the specific belonging we are observing, in this case a particular oracle bone, and the specificities of the attributes it had. These are listed so that everything in that line refers to the same object. When we searched this time, this page had the first entry as follows:

> Info Content:

> ![](Images/Bone229Thumbnail.JPG)

> Category: Animal Bone

> Item: Bone 229

> Project: Oracle Bones in East Asia

> Context: Asia/China/Shanxi/Taosi

> Chronology: 2300 BCE to 1900 BCE

> Updated: 2022-10-26

It's important to note that the preceding digital object is just an example. That's because Open Context is always changing, adding new data and adjusting to accommodate dynamic search trends. That means the next time we search, we might get a different digital object as our first entry and that entry will have the same kinds of observations noted about it. Regardless, if we're inspired we can find out more about that digital object by clicking the entry! However, the observations in this table are really interesting on their own and we can probably use them to come up with a cool data-driven creation.

### A short note on the word “object” (for those who are technically inclined)

The word “object” can mean a lot of things when we're talking about archaeological data. It can refer to a specific belonging that an archaeologist, descendant, or layperson found or analyzed during an archaeological investigation, but it can also be more than that.

In the realm of archaeological data, we can consider the records we produce, which list the things we observe about a belonging, as “objects”. In this case, these might be things like site forms (explored in more detail in our [*Gabbing about Gabii*](https://doi.org/10.6078/M7DV1H1R) Data Story), or things like photos, like the entry from Photo ID-054 in the next section.

Warning, very technical bit ahead! The hierarchy for the linked open data on Open Context relies on something called [CIDOC-CRM](https://www.cidoc-crm.org/) This is one (but not the only!) international standard for sharing cultural heritage information in an interoperable way. While we call these things objects, as sometimes we might pull up a PDF rather than a belonging when searching Open Context, the CIDOC-CRM would refer to these as a “Physical thing”.

In Open Context, photos, PDFs, and data about individual belongings are “objects” because they refer to a single specific thing that has information about it stored in OC. In OC, each “thing” has a unique web address that acts as both a location and an identifier. This enables us to link to and cite specific objects, whether they're observations about belongings themselves or information about the observations made about belongings or sites.

In fancy terminology, these unique web addresses are called URIs. This stands for Universal Resource Identifier, giving a thing a name as well as a location. This is a little different than the URL, which is just the address or where a thing is located and is not the name of that thing.

# Searching by keyword

If we know that tables are much more than places to put our plates or we've decided we just want to know about search, we'll start here. If after casually browsing all data we don't find what we're looking for, or if we know exactly what we'd like (perhaps because we just want to see what today's creative prompt brings up), we can approach the data in a different way. Instead, we'll do what is called a *keyword search*.

Similar to how we might have searched the internet for Open Context at the beginning of the tutorial, we can search within this project to find records related to a particular word. To do this though, we won't use the browser search bar, like to find a website. Instead, we'll use that box that we saw under the “Data Records” triangle in the “Browse, Search Project” box on the Project page.

![The “Explore Project Data” box is found on the left side of the page.](Images/SearchWithinProject.JPG)

If we've navigated away from the project page we'll need to click back to return to it or use this handy DOI, <https://doi.org/10.6078/M74B2Z7J>, to navigate directly there.

Once we've relocated the “Browse, Search Project” box on the Project page we'll find a bar within 
that box, below the options, that says “Search within Project”. In that box we'll want to type a keyword. For this tutorial, we're going to go with the first word from the *It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration Part One* calendar and that word is “bone”. We'll type that into the box, then click on the magnifying glass icon or hit the enter key on our keyboard.

![Typing “bone” into the search box will look something like this.](Images/ProjectOracleBonesSearch.jpg)

If we completed the **Browsing all data records** section of this tutorial, the resulting page we're led to might seem very familiar. That's because it has the same structure as the “All Data Records” results page. However, rather than getting a map and a temporal distribution based on information from all of the data records, we get versions based on only the records that mention the word “bone” somewhere in their available information.

![This is approximately the page we should get when we search for “bone” in the data.](Images/ProjectOracleBonesSearchBoneResults.jpg)

As these two pages look very similar, we might be worried we didn't do the right thing. However, we can easily check this. We can do this by finding a box called “Applied Filters”.

![The "Applied Filters" box should look like this.](Images/AppliedFilters.JPG)

Rather than just have the project name listed, this also has a filter called:

**Current Text Search Filter**

Search Term(s): 'bone'

This was the term we used to search. So we've definitely done the right thing! If we don't have this in that box, we'll want to go back to the tab for “View of Oracle Bones in Ea…” and type “bone” in again and see what comes up. Or we picked a different word to search so a different word is there, and that's totally cool.

By now, we're probably ready to see what information we got when we searched for “bone” in this project. To do that, we'll scroll past the map and the chronological distribution to the records list. If we aren't familiar with tables and skipped **Browsing all data records** we should go back to the **Parts of a Table** section within the** Browsing all records** section. If we're comfortable with tables we can start looking at what our search provided us with.

![This is a slightly different list than the “all records list” as it only includes entries with “bone” in the record.](Images/ProjectOracleBonesSearchBoneResults_List.jpg)

We'll see that in comparison to the table created when we looked at all records, this table has the same headings but a different object is listed first. Depending on when we search to try out this tutorial, our first entry might also be different so we'll just search for the same kinds of things in our results. In this table, the first non-project entry we get is for “Item Label Photo ID-054” and under ”Category” we might notice that the word “Bone” is highlighted in the phrase,

> “China/Henan/Wangjinglou/Bone 177 …Photo ID-054 from China/Henan/Wangjinglou/Bone 177 Image Type Photograph Neolithic period…”

This identifies where the word “bone” was found in relation to this entry and specifically that it had to do with the category. Additionally, if we click the hyperlink for this record we go to a photo and page that has more information about that object. And that's pretty cool.

![This is the page for Drawing ID-004 and near the top we'll notice a <-> symbol that associates this photo with Mark 2.](Images/OracleBonesBone177_photoClick.jpg)

Whether we click on the hyperlink for the name of the item or on the thumbnail photo associated with that entry, we'll be brought to the same page. This page is for the specific photo and if we look near the top of the page, just above the enlarged image, we'll see a <-> image, an arrow with pointers on both ends, that suggests that this image, besides being its own unique object, is associated with Mark 2.

![This is a close up of the double arrow <-> symbol that we are looking for. It indicates a relationship between the two pieces.](Images/OracleBonesBone177_photoClick_DoubleArrowSymbol.jpg)

Awesome! This means that there's more information about this photo (and the bone in it) that we can explore. If we'd like, we can click on that name and read more information about Mark 2 itself rather than just information about its portrait.

![The page for Mark 2 should look like this.](Images/Bone177-ArtifactPage.jpg)

Once we navigate to the page for Mark 2 we can see that there is a lot more information about that digital object to use as inspiration. This includes other photos associated with it as well as specific descriptions of its attributes. Congratulations! We've found one belonging 
(digital object? artifact?) that we can use as inspiration for today's search prompt, or that can teach us more about oracle bones.

When we get a new prompt for tomorrow, or if we just want to look at more oracle bones, we can use the same process to search for more belongings around that theme or around any other keyword of interest!

# Conclusion

Congratulations! We learned to navigate to the Open Context webpage, explore projects, find a specific project by name, navigate to the project page, and then browse the data in a couple of different ways. That's a lot to learn, but there is also a lot of cool stuff out there about the oracle bones just within the project's information.

Now that we know how to do that, we can use this same process to search for the other prompts in our creative list, if we're participating or utilizing the [*It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration Part One - The Prompt List*](https://doi.org/10.6078/M7KH0KG4), and see what sparks our imagination. We can keep browsing or searching and do so for as long as we'd like. Additionally, it's important to practice these skills so that navigating in this fashion becomes more comfortable. Therefore, even if we think we've got this down, we may want to come back again, particularly with a different search terms, to see what happens

What will we do if there are no records? Where will we find inspiration? Well, we might need to try something else if that's the case, but let's be happy we've found what we have so far. With that, we can enjoy exploring the rest of the prompts in our creative list, if that's why we're here, and find out more about oracle bones in general by checking out the [*It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration Part One - The Prompt List*](https://doi.org/10.6078/M7KH0KG4) and generally enjoy what the archaeological data records on OC inspire in our own work.

## Key concepts used in this tutorial

*Browse for a project*

Scrolling through a list of projects to find one we're interested in.

*Browse a data set*

Looking through a particular data set to find something that's of interest to use.

*Data structures*

Organizing data according to the desires of the institution hosting that data.

*Columns, attributes, headings*

Specific observations that were taken for a particular object.

*Entries, rows, observations*

Specific things that were observed, and the particular instance of the attributes they had.

*Objects*

Defined entities within a particular data structure (belongings, documents, and images).

*Keyword search*

Using a particular word to search the web or a project within OC or any other searchable place, looking only for entries that include that particular word.

License: [Creative Commons Attribution (CC BY-SA 2.0)](https://creativecommons.org/licenses/by-sa/2.0/)