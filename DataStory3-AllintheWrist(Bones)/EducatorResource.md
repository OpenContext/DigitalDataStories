---
title: "It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration - Educator Resource"
author: "L. Meghan Dennis, PhD. Postdoctoral Researcher for Data Interpretation and Public Engagement"
date: "20 July 2023 - Version 1.0"
output:
 html_document: default
 word_document: default
 pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

![](Images/AllinTheWrist-EducatorHeader.jpg)

## Contact the Team

If you or your students have questions, comments, or concerns, please get in touch. We love feedback, especially from educators!

L. Meghan Dennis 
@gingerygamer (she/her)

Paulina F. Przystupa 
@punuckish (she/their/none)

datastories@opencontext.org

# Exercise Description and Aims

The [Digital Data Stories Project](https://doi.org/10.6078/M70P0X5P) promotes an increased focus in archaeological education on digital data literacy. Through the use of open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis. These practicals illustrate the confluence of science and humanities-based investigations in collected data on the past.

This approach promotes multiple levels of engagement with archaeological data sets, linking data-driven narratives with the key analytical and interpretive steps used to ethically analyze, visualize, and present research data.

In this [Digital Data Story](https://doi.org/10.6078/M7N877XP), participants will learn to:

> Use lists of terms to prompt creative outputs

> Participate in online sharing

> Browse a data set

> Search a data set using keywords

> Understand data structures in open data sets

## additional texts

Jiang Yubin (2020) Joining jades and linking pearls: 120 Years of rejoining fragmented oracle bone inscriptions, Chinese Studies in History, 53:4, 331-350, DOI: 10.1080/00094633.2020.1834815

Li Anzhu. Oracle-Bone Inscriptions and Cultural Memory. Frontiers in Art Research (2020) Vol. 2 Issue 9: 63-73. https://doi.org/10.25236/FAR.2020.020913.

## the data set

This dataset allows researchers to select site data in the form of tables, texts, media, and maps. However, it may also be used for many other purposes relating to the study of divination rituals in East Asia, as well as the study of Chinese Neolithic and Bronze Age sites.

These data were collected and analysed by the [Oracle Bone Project](https://doi.org/10.6078/M74B2Z7J), an archaeological initiative led by [Katherine Brunson](https://scholar.google.com/citations?user=OftBB7EAAAAJ&hl=en) with international partners.

## assessment and scaffolding options

For those students who require (or desire!) a more challenging data exercise, tutorials offered at [GLAM Workbench](https://glam-workbench.net/) can be utilized as scaffolding.

For those students who require (or desire!) more support, splitting into pairs to complete the [Digital Data Story](https://doi.org/10.6078/M7N877XP) offers the opportunity to work together, switching off tasks and comparing hypotheses and results.

As there are opportunities during the [Digital Data Story](https://doi.org/10.6078/M7N877XP) for students to select tabular data for investigation based on their own interests, the end results produced may vary. One potential assessment option is to ask students to re-attempt the tutorial with different search goals.

## Technical Requirements

If engaging independently via their own computers, students should have a basic knowledge of how to operate a computer and how to visit a website.

If engaging via lab computers, students should have a basic knowledge of how to operate a computer and how to visit a website. Lab computers should have the following programs installed:

> A browser with internet access

> A text editor (like TextEdit or Notepad)

The dataset that students will be using for this exercise is available at: 

<https://doi.org/10.6078/M74B2Z7J>

## Duration

The Digital Data Story is given in two parts. The first part is variable in duration, and the second part should take approximately 1 hour. Each part can be completed separately, though they are additive when completed in total.

If students are completing a portion of the tutorial in more than one session, please direct them to pay close attention to where they left off, and the specific internet address related to the step they finished on.

### get in touch!

Please reach out if you or your students have questions, comments, or concerns. We love feedback, especially from educators!

License: [Creative Commons Attribution (CC BY-SA 2.0)](https://creativecommons.org/licenses/by-sa/2.0/)