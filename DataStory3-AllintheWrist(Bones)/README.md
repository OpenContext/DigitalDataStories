![](Images/AllintheWrist_Header-2896x1521.jpg)

This exercise is best suited to those with an interest in public archaeology, the dynamics of ritual behavior, or the archaeology of central China. Users should have a basic understanding of archaeological data types, but little previous experience with archaeology is required.

This page provides access to the resource in two ways. The first is through a series of PDFs that represent the completed Data Story. These PDFs include:

1. [Creative Prompt Guide](https://doi.org/10.6078/M7KH0KG4) 
1. [Project Search Tutorial](https://doi.org/10.6078/M7ZP4478) 
1. [Teaching Guide](https://doi.org/10.6078/M747480V)

A single PDF for the combined tutorials is available [here](https://doi.org/10.6078/M78050Q5). 

This repository is secondary access to the above listed materials. These markdown files represent the source material for the text and primary images within the PDFs, minus additional formatting, and with only a general placement of images. This code represents our commitment to open science and transparency in our process.

These materials, either through the PDFs or the code, are designed to be used synergistically. However, any piece of this Data Story may be used separately or re-ordered according to the requirements of the individual or for specific educational goals. This Data Story also references [*Of Mycenaean Men Part Two: The Open Context Keyword Search Tutorial*](https://doi.org/10.6078/M7TX3CHJ). Furthermore, the markdown files may be altered and forked from the Codeberg repository for custom use with different data sets. These resources are available free to use under a [Creative Commons Attribution (CC BY-SA 2.0)](https://creativecommons.org/licenses/by-sa/2.0/) license.

Thank you so much for utilizing our educational resources! If participants have the time, please consider contributing thoughts to our [ongoing survey](https://berkeley.qualtrics.com/jfe/form/SV_5mBm1RbFtQqTjZc), whose data we will use to periodically update the resource. Such updates will be noted here, and will be first available through the code repository prior to PDF updating.

*First published: 20 July 2023*

This work has been made possible in part by the National Endowment for the Humanities and The Mellon Foundation. Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or The Mellon Foundation. 