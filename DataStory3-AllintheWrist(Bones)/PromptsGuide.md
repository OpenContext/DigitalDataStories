---
title: "It's All in the Wrist (Bones): Archaeological Data as Artistic Inspiration"
subtitle: 'Part One - The Prompt List'
author: "Paulina F. Przystupa and L. Meghan Dennis"
date: "20 July 2023"
output:
  html_document: default
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Guide Overview

1. Introduction
2. How to get started
3. A Moment in the Life of Jiǎ
4. The prompt list
5. Background on the *Oracle Bones in East Asia* data set
6. Sharing your work safely on social media
7. Acknowledgements
8. References

Estimated time to read this guide: 15 minutes

> *This Data Story contains no images of human remains.*

# Introduction

Welcome (back) to the Digital Data Stories of the Alexandria Archive Institute and Open Context! This guide includes everything that you'll need to use archaeological data to inspire your next creative project. Whether this is for your own practice in archaeological illustration, to consider a theme for creating a fantasy setting set in the archaeological past for your gaming group, or to explore the material culture of a particular region for a class project, we can guide you through it. Of course, feel free to use whatever parts of this guide provide inspiration and get the creative juices flowing, or the gears turning, or whatever stuff we associate with starting to get things done, creatively.

This Data Story focuses on cultivating data literacy by creating art from data-inspired prompts using creative exploration. In this guide we include an introduction to the basics of this Data Story. We then include some examples of how to use the data-driven prompts and an example fictional narrative generated from our prompt list. This is followed by our creative prompts calendar. If you want to learn where these prompts came from, after the calendar, we provide a summary of the data set in the words of the people that published or produced the data set that inspired this prompt list. Finally, we explain how to safely share your work on social media then acknowledge the awesome folks who helped make this a reality and suggest further reading.

The guide can be used to celebrate or explore many inspirational projects. It can be used to work within the bounds of [Artober or ArchInk](https://doi.org/10.6078/M7WW7FSR) which often coincide with [National Arts & Humanities Month](https://www.americansforthearts.org/events/national-arts-and-humanities-month), [Asian American Native Hawaiian Pacific Islander (AANHPI) heritage month](https://asianpacificheritage.gov/), or even as inspiration during [National Novel Writing Month (NaNoWriMo)](https://nanowrimo.org/) or [Archaeology Writing Month (ArchWriMo)](https://doi.org/10.6078/M7HQ3X11). In addition, feel free to integrate the prompts and the associated search tutorial into a class on any related subject or use it for personal enjoyment. All of the prompts in this list, and the guide itself, have been proofed, vetted, and sensitivity read, and so we hope that this provides a list to keep you inspired.

Accompanying this guide, we're also providing [*Part Two - The Open Context Project Search Tutorial*](https://doi.org/10.6078/M7ZP4478). If the prompted word on our calendar isn't sparking joy or inspiration, you can use that term to search the project that generated the prompt list, the [*Oracle Bones in East Asia*](https://doi.org/10.6078/M74B2Z7J) project, for images and other related information. This tutorial is specifically tailored to searching within that project.

If that's still not quite enough you can try searching Open Context (or the web) for more information and examples. If you need help searching Open Context outside of the original project, you can take a look at [*The Open Context Search Tutorial*](https://doi.org/10.6078/M7TX3CHJ) for how to conduct a keyword search on Open Context. With that, may the ancestors smile upon you and grant you inspiration.

# How to get started

As we mentioned, you can do anything you'd like with the prompts. But if that's too general, in this section we outline how to get started and an example of what we did with the prompts. If you don't need guidance and have a good idea of where you'd like to go, no problem, be inspired by data and embark on this journey.

Otherwise, look at the first word in the calendar and consider what it means to you. Consider if it's a noun or a verb and what experience you have had with it. Consider what you already know about the word. While we generated this list from an existing data set, you can think of your own experiences as data too. Your experiences with the word are the attributes or observations about it you can make. If nothing sparks your creativity just from thinking, you can use the [*Part Two - The Open Context Project Search Tutorial*](https://doi.org/10.6078/M7ZP4478). This will bring you to the project that inspired this list.

There you can look for more information and photos to spark your interest. Searching around for more information is an important part of cultivating our reading and working with data literacies, so give yourself a good amount of time to do this. You'll want to read more about specific entries that came up when you searched the word, click through various photos, and possibly read some documents to provide more context. All of this will introduce you to new information.

If that doesn't work, or nothing comes up within the project search, try widening your search using [*The Open Context Search Tutorial*](https://doi.org/10.6078/M7TX3CHJ). Hopefully between both of those, an idea will spark after looking and comparing the various data that relate to the prompt word. If not, consider searching the web more generally using the keyword. Once something interesting inspires you though, go with it! Your creations don't have to be huge, they can be small and simple too. This guide is here to support whatever your creative goals are and sometimes restricting a project helps us flourish!

You may wonder though, what happens if you get behind, or forget one day? While we organized the list to inspire a new creation each day, there's nothing stopping you from trying to use all the prompts at once or build on what you did on a previous day or combine the prompts from consecutive days. All of that is okay because we're interested in you developing your data literacy skills.

If you're not sure *what* your medium should be, one idea could be making soups or other foods inspired by the list, perhaps with a focus on spices associated with Chinese cooking. Or you could illustrate what it might mean to feel or be lucky. Or compose a short song about a trash pit. Or you can change what medium you do for each prompt. All of these are great ways to explore archaeological data and learn to work with it.

Below, we've provided an example narrative, based on the entire prompt list, to give you an idea of what can be done. Using all the prompts in one piece is also a possibility for solo practice or to utilize this guide within the bounds of a single educational session.

## A Moment in the Life of Jiǎ

*Bones*, bones, bones, that's all bàbā would talk about. The *scapula* we could prepare as tribute to be used in royal *divination* *rituals*. Or that was the hope. If we were *lucky*, the local lord would choose our bones, or maybe even our *goats*, as tribute for the King, the ruler of the current *dynasty*. Or that's what bàbā said anyways.

But before such pieces could be *drilled* properly and used in the *sacred* fires, they were just *porous* pieces. We had to use *scrapers* or other surfaces to remove the meat. Only then could they be *inscribed* with *religious* requests for the ancestors, or even just *written* on with our regional names, and have the fire applied to them to be part of this important process. Prepared bones were what a *diviner* of any caliber needed, not just a hunk of meat.

And in the *present*, the failures of our *household*, which didn't make it to the point of being good enough to be included as tribute, ended up in our *trash pit*. Broken pieces, which had a *crack* or two before even getting heated, couldn't say anything about the future. Reminding us that the *past* is behind us and to not let our *emotions* get ahead of us.

I tapped the bone against my scraping surface and couldn't help but notice how *hollow* the *sound* was. Maybe this would be the one that the lord would pick. Perhaps even an *auspicious* sound though I tried to look at it with some *objectivity*. I was new to the *technology* part, just having started apprenticing with bàbā a few months ago. But I felt confident that this was the one, even though it was the *earliest* in the process I'd noticed something unique about the piece, so I set it aside for now. The *latest* in my growing pile of bones that had gone through the proper *treatment*.

![](Images/AllinTheWrist_GraphicPages_2023Jul19_Page_1.jpg)

# The Prompt Calendar

![](Images/AllinTheWrist_GraphicPages_2023Jul19_Page_2.jpg)

## The Prompt Calendar as text

| Dates | Prompt |
|-------|--------|
|1|Bone|
|2|Divination|
|3|Past|
|4|Royal|
|5|Written|
|6|Lucky|
|7|Emotions|
|8|Porous|
|9|Inscription|
|10|Religious|
|11|Crack|
|12|Technology|
|13|Scraper|
|14|Objectivity|
|15|Auspicious|
|16|Scapula|
|17|Goat|
|18|Sound|
|19|Ritual|
|20|Future|
|21|Treatment|
|22|Household|
|23|Drill|
|24|Earliest|
|25|Fire|
|26|Diviner|
|27|Hollow|
|28|Present
|29|Trash pit
|30|Latest|
|31|Sacred|

# Background on the *Oracle Bones in East Asia* data set

For this data-inspired creative project guide, we're drawing from a data set published on Open Context that is part of the [*Oracle Bones in East Asia*](https://doi.org/10.6078/M74B2Z7J) project. The following is the summary for the data set on Open Context written by the people who contributed the data set. Further suggestions for general reading about oracle bones can be found at the end of this guide. Please note, though the contributers use the term ‘East Asia' and the data set largely concerns finds within China, the research field of related [scapulimancy](https://en.wikipedia.org/wiki/Scapulimancy) extends across Asia. Within this Data Story, we have utilized similar vocabulary for consistency.

> Oracle bones --- animal bones used for pyro-osteomantic divination rituals in East Asia --- are one of the most important types of bone artifacts in Chinese Neolithic and Bronze Age sites and the source of inscriptions containing the earliest writing in ancient China. Although these inscriptions are the focus of most research, oracle bone use far pre-dates the inscribed examples and continues after they were a primary medium for writing. Uninscribed oracle bones are rarely published and there is a lack of metric data available for studying spatial and temporal trends in oracle bone manufacture and use. In the Oracle Bone Project, we are reviewing collections of oracle bones housed in institutions across China in order to collect comprehensive data on the types of animal bones used in divination, the methods of oracle bone manufacture, and the archaeological contexts in which the bones are found. Our goal is to trace the origins of oracle bone divination rituals, their spread across Asia during the Neolithic, and the ultimate development of oracle bone divination as a central part of Shang Dynasty royal religious practices. The project brings new zooarchaeological and technological perspectives to research on oracle bones and addresses Anthropological questions about the role of ritual technologies in household and state-level institutions.

> <p>Data collected as part of the Oracle Bone Project is published on Open Context in a multi-language open access format. The raw data can be used by researchers around the globe to examine temporal and spatial trends in oracle bone manufacturing and use. Our focus is on uninscribed cases that have not received as much scholarly attention, but we encourage other scholars and institutions to upload additional data from inscribed or uninscribed oracle bones in their own collections. All contributions are associated with a publication record that is fully citable, searchable, downloadable in multiple formats, and linked to data standards that facilitate interoperability. Data input forms in English, Chinese, Korean, and Japanese are coming soon!</p>

> <p>The Oracle Bone Project is an international collaboration between the Institute of Archaeology, Chinese Academy of Social Sciences (IA CASS) and Harvard University.The project co-PIs are Katherine Brunson (Wesleyan University), Rowan Flad (Harvard University Department of Anthropology), and Zhipeng Li (Institute of Archaeology, Chinese Academy of Social Sciences).</p>

# Sharing your work safely on social media

One way you might use this list is to participate in various online creative communities or thematic months via social media. These can be during thematic months inspired by particular heritages, such as [AANHPI heritage month](https://asianpacificheritage.gov/), or particular kinds of creative works, such as [NaNoWriMo](https://nanowrimo.org/). There's even an archaeology themed tag on social media called ArchInk that runs in October. You can read more about that [here](https://doi.org/10.6078/M7WW7FSR).

And while the Data Literacy Project was in part inspired by ArchInk illustration, creative works in general are part of archaeological practice. In classrooms, students commonly learn to draw artifacts, make maps, or participate in ethnoarchaeological activities where we use our lived experiences to recreate the past. And those projects sometimes get shared online.

In addition, you might have just found this list and thought it was cool but might be thinking, what should I do with my creations? Well, once you've created whatever piece the day inspired, there are plenty of options. The first is always to just enjoy the piece for yourself and relish the fact that you created something based on archaeological data! You stretched your archaeological data literacy skills by taking a word and turning it into something new. Or you searched for more information about it and used that to play with the idea even more. And that is super cool all on its own.

Another option is to share your creation in some capacity. And that's where various hashtags and protections to your work might come in. Depending on whether you started this in association with a specific month there may be established hashtags that you'll want to use to associate your work with a specific community.

For example, the DLP (the “we” in this guide!) occasionally uses this guide to post the inspirational prompt daily during various months, encouraging interested participants to share their creations. We do this through sharing images (with [alt text](https://web.archive.org/web/20230706143056/https://accessibility.huit.harvard.edu/describe-content-images)) and text. For [Artober](https://doi.org/10.6078/M7WW7FSR) shares, these might include the prompt word as a hashtag, the day of the month, and the specific Artober or ArchInk tag you're participating in to associate it with other pieces made by folks focusing on archaeology-inspired creations. Exploring such tags is also a great way to see what other folks are creating and celebrate everyone's pieces.

If you choose to share your work, which is awesome and not mandatory, don't forget to include your signature, a watermark, or even a photo of yourself with your images. That way should anyone find and share it, you can get credited for your work. That way we can all enjoy the things we create while respecting people's rights to own their work.

# Acknowledgements

This Data Story is a collaboration with the producers of this data set, co-PIs Katherine Brunson (Wesleyan University), Rowan Flad (Harvard University), and Zhipeng Li (Chinese Academy of Social Sciences), and with our wonderful sensitivity reader Stefanie.

The writers of this data story, L. Meghan Dennis and Paulina F. Przystupa are not of East Asian descent. To ensure that we created a Data Story narrative that properly highlighted and respected the materials and the cultures associated with them, we reached out to some additional collaborators to ensure that this was an appropriate data set to use and that we did so in a way that respected the content highlighted in this data set. In addition, we'd like to thank Kate Ellenberger for thoughtful insights on the formalization of grassroots collaborations by existing institutional entities, which guided this Data Story into its current form.

# References

For a quick but detailed overview of the archaeological (re)discovery of oracle bones and their purposes: <https://en.wikipedia.org/wiki/Oracle_bone>

For more information on scapulimancy:  <https://en.wikipedia.org/wiki/Scapulimancy>

For additional images related to oracle bones, check out the MET:  <https://n2t.net/ark:/28722/k2959zk0r>

For more more information on National Arts & Humanities Month, which runs in October: <https://www.americansforthearts.org/events/national-arts-and-humanities-month>

License: [Creative Commons Attribution (CC BY-SA 2.0)](https://creativecommons.org/licenses/by-sa/2.0/)