---
title: "The Road Most Traveled - An Archaeological Survey Guide"
author: "Paulina F. Przystupa, Postdoctoral Researcher in Archaeological Data Literacy and L. Meghan Dennis, Postdoctoral Researcher for Data Interpretation and Public Engagement"
date: "`r Sys.Date()`"
output:
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
#RELEASE DATE: 08/2024 updated 09/2024
#VERSION: 2.1
#LICENSE: CC BY-SA https://creativecommons.org/licenses/by-sa/4.0/deed.en
```

![](Images/TRMT_MainImage.png)

# Guide Overview
<newline>

1. Introduction
   a. What is slow-observation?
   b. How does this relate to archaeological data literacy?
1. Selecting your survey space
1. The exercise
1. Concluding *The Road Most Traveled*
1. References for further reading
1. Credits
1. Acknowledgments
1. Optional Survey form

Estimated time to read this guide: 15 minutes 
\newline
Estimated time to complete the exercise: User determined

# Introduction

Welcome (back) to the [Digital Data Stories](https://doi.org/10.6078/M74F1NW0) of the Alexandria Archive Institute and Open Context! This Data Story focuses on how learning to observe the world around you can cultivate archaeological data literacy. To get started, this guide provides an introduction to a solo-practice observation exercise that incorporates concepts from slow-looking, which is the movement that underlies what we call slow-observation. We do this to help participants understand the practice of slow-looking and use it in other contexts, such as in group exercises to teach archaeological data literacy-relevant skills.

This Data Story will help you conduct a series of data literacy-focused archaeological surveys. In addition, you can use the guide as a series of optional elements to integrate into a class exercise or for personal enjoyment. You can travel the same route multiple times but each time for the first time! All of the suggestions and exercises in this guide have been tested by the [Data Literacy Program](https://doi.org/10.6078/M70P0X5P) (DLP) team or were adapted from existing slow-observation exercises in other fields, such as the work done by [Project Zero](http://www.pz.harvard.edu/search/site/slow%20looking). As a supplement to these exercises, you can explore further slow-looking resources via [Thinking Museum](https://thinkingmuseum.com/2020/12/02/simple-ways-to-practise-slow-looking-every-day/), [Project Zero Guide](https://pz.harvard.edu/sites/default/files/Slow%20Looking%20-%207.11.2018.pdf), and [Slow Art Day](https://www.slowartday.com/about/tools/) --- some of whose works were the basis and inspiration for this exercise!

For this [*Solo Series*](https://alexandriaarchive.org/2023/05/25/solo-steady-and-structured-data-literacy/) Data Story, you will go on eight excursions along the same route of your choice. Each survey should follow the exact same path, including the same side of the street or thoroughfare and direction. However, for each excursion we provide a specific focus for your foray. To formalize this practice, at the end of each survey you can jot down your thoughts in the boxes describing the focus of each jaunt, the form included at the end of this guide, or record your observations in a personal notebook or journal or via a recording device, such as your phone.

If you don’t use the included boxes or form try to capture the same kinds of observations each time. This will help you track the different things you noticed while you were out and about. You can keep a handwritten journal, an audio journal, a vlog, prepare a poem to perform, or use illustration. You can even use different note taking styles each time! Should you vary your note-taking style, consider how different kinds of note-taking influenced what you noticed during your excursion. For example, what recording technique made you feel like you noticed the most about your surroundings?  More important than the format is that you can revisit these recollections at the end of this Data Story and compare what you noticed each time. This is a great way to see how you exercised your observational skills to perceive more of your surroundings each time you conducted the survey. 

After completing the eighth excursion, you can compare your notes to see how these practices and various foci helped you hone slow-observation skills out in the world. Slow-observation is an important skill for conducting surveys, which are a key element of archaeological practice for locating and assessing sites. Through these observations of the physical world we can say more about the past. Furthermore, the process of creating standardized observations is an archaeological data literacy skill on its own.

To expand your archaeological data literacy practice, you can turn this exercise into a structured data set using your notes as an adaptation of the [*Gabbing About Gabii: Notes to Data to Narrative*](https://doi.org/10.6078/M7DV1H1R). Use your collection of observation forms, or your notes, instead of the Gabii forms that the original Data Story instructs you to use. 

*If using this as part of a classroom exercise, the notes from multiple participants can be combined to generate the data set instead.*

## What is slow-observation?

For this Data Story, we define slow-observation as taking the time to focus on observing a particular thing and exploring various ways to observe it so that we can take in as much as possible about the focus of our attention. Similar to other calls for slowing down, such as the [slow food movement](https://www.slowfood.com/) movement, [slow science](https://en.wikipedia.org/wiki/Slow_science), and [slow journalism](https://www.nationalgeographic.org/projects/out-of-eden-walk/your-own-walk/), this guide encourages participants to slow their pace and take time noticing a particular thing. This guide relies heavily on slow-looking-related and adapted practices. However, slow-looking, and our use of its related terms, more accurately expands the idea of looking mindfully or with intention, and includes using all our senses, rather than focusing on sight alone.

Furthermore, the process does not actually need to be slow in and of itself. Instead the practice is meant to encourage people to draw on their senses and observe something *deeply*. This can take time, as in the exercise conducted by Eva Mol with Herbert the pot (Mol 2021 or watch her presentation about the activity [here](https://www.youtube.com/watch?v=ziuma1eFLf4&t=4027s)), or it can be brief and timed, like in the [Ten times Two (10 x 2) exercise](http://pzartfulthinking.org/?p=90) (Tishman 2017). Regardless of the pace, the intention is to select something and focus on specifically observing it.

In some ways, this can be considered a meditative practice, where people take the time to do something that’s good for them (Harper 2017). Being outside is often a nice break from sitting by a stuffy computer all day, and has other health benefits (Harper 2017). Such mindful approaches are an important aspect of archaeology in many ways and it can be used as a grounding exercise outside of archaeology as well.

## How does this relate to archaeological data literacy?

The ability to observe as much as we can about something is one way to cultivate archaeological [data literacy](https://doi.org/10.6078/M73F4MRM). Archaeologists regularly use mindful observation of a landscape, called survey, to locate archaeological places digitally and in the real world. This Data Story outlines how to cultivate the observational skills necessary for archaeological surveys through practicing those skills in a familiar place.

Sometimes archaeological surveys begin with looking at documents, identifying where folks have already found archaeological data or sites. When we’re talking about archaeological data literacy this is often where we begin. For these surveys, we leverage the data sources we know of to build a narrative or guide for where to go and observe next. But how do you become the first person to observe or identify that archaeological site or place?

To do this, we (archaeologists) use a different kind of [survey](https://doi.org/10.6078/M7SF2T9Q). Often referred to as “pedestrian survey” or just “survey”, this practice involves moving across the landscape in person, looking for evidence of human activities. Sometimes we walk, sometimes we take ATVs, and sometimes we look for changes in vegetation from drone imagery we captured while in an area. (We stay on the ground and the drone goes in the air!) There are lots of modes of transportation and ways to conduct this practice, so that anyone can participate in an archaeological survey!

However, archaeological surveys often take us into unfamiliar places, where terrain, color, or vegetation make it hard to see the archaeological evidence we’re looking for. In these cases it’s really important to leverage our mindful observational skills to carefully notice and care for the landscape in, often with more intentionality than we would normally in a normal hike or travels. That’s where the skills in this Data Story come in handy. Here we'll explore different foci for your observation of the landscape to practice different kinds of mindful and slow observation.

# Selecting your survey space

For this archaeological data-inspired practice, we want to take the guess-work out of where you’ll be surveying and focus on honing observational skills that you can use anywhere you might go. The first step in doing this is to select where you’d like to survey. This can be anywhere, as long as you can access it regularly over the next few days, weeks, or months.

Depending on your pace, this can be a park, your neighborhood, a local hiking spot, or a route to the store. It doesn't need to be a long route, exciting, or unique. But it should be easy to revisit and long enough to get yourself out of your head and into your body to experience the environment. These features remind us that archaeological surveys occur in places you might consider beautiful and boring and sometimes the more familiar a walk, the more fascinating the change is when one truly notices its landscape.

Once you have a route in mind, map it out and remember which turns you’ll make, how long it will take, and which side of the thoroughfare you’ll be traveling on. To hone your observational skills, try to control as many of these variables as possible. This will allow you to focus on observation, rather than the other issues of the journey itself. Of course, though, conduct this exercise safely and adapt it, and your practice, to your environment. Above the science, we at the DLP are safety-first!

*In a group setting, educators may decide routes beforehand or have participants pick routes near their meeting area.*

## Optional - Formally map your survey

To practice other relevant archaeology data skills, consider drawing a map of the route you will take to complete this Data Story. While many urban areas, likely where you’ll be surveying, have accessible maps to draw on or adapt, this is rarely the case for archaeological surveys!

Due to this, making an intelligible map without the aid of much technology is still an important skill in archaeology. Sometimes your GPS ([Global Positioning System](https://en.wikipedia.org/wiki/Global_Positioning_System)) device doesn’t work, your [topographic maps](https://en.wikipedia.org/wiki/Topographic_map) are old (either because they’re not updated OR because your state uses a specific historical map as a reference), or the folks you work for don’t have the budget for digital technologies. When these happen, pencil and paper your friends (at least for drawing maps!).

First, draw a rough map based on what you remember of your survey route. We suggest that, while you draw, keep internal consistency using a physical size reference, a ruler, or even graph paper. For example, if you know that the lots for houses in your neighborhood are roughly the same size, use that as a standard in your map. That way two blocks with the same number of houses are the same length! Then you can use the same number of squares on the grid paper or length on a ruler for that same distance.

If you want to draw a more accurate map, you can bring a tape measure with you and something that measures angles. You’ll use the tape measure to gauge the length you walked along a straight line and then the thing that measures angles to note where you move off that line, and by how much. These will create accurate vertices on your map, relative to the axis of each line. Or due north, if you decide to bring a north pointing compass with you.

Then, you’ll need to mark which direction you turned from that as you moved off your straight line. This can be done using a magnetic compass, to get the angle in terms of magnetic degrees, or by using another method to access angles, such as considering the numbers of a clock and noting if you turned to “9 o’clock” or “2 o’clock”…as long as you’re fairly careful about walking in a straight line.

If you want to avoid bringing a tape measure out with you on your route, read the next section, if not, feel free to skip to our discussion of sampling strategies!

*Educators may have learners work in pairs to create these maps or, for larger groups, see how individually made maps fit together if surveying adjacent areas.*

### Measuring using steps, pedals, pushes, or other references

You will need a tape measure for this next part. But, if you follow these instructions, you’ll only need it once. What we’re going to do is estimate our stride length, wheel push amount, or pedal counts. We’ll use this as a placeholder for measuring the distance of our path's segments.

First, lay out your tape measure to a significant distance. Stretch it waaaay out. Longer than that. After that, move along that length and note how many movements it takes to fill the distance. Count how many it takes the first time and then repeat this exercise 5-10 times. For each time, take the length of the tape measure and divide it by the number of pushes or steps or movements you took. That will give you a unit distance  (such as meters or feet) per step, push, pedal, or motion count. 

To help in your calculations you can fill out the table below.

![](Images/TRMT_TablePage7.png)

Then average those all observations together to get your average unit distance-per-motion. To get the simple average, first add up all your unit distance-per-motione and then divide them by the number of times you calculated unit distance-per-motion. You can use the spaces below to organize that calculation. Put the results from the third column in the previous table above the corresponding row number in the table below. Then add those numbers together and put that in the Total box.

![](Images/TRMT_TablePage8.png)

Then divide the total number by the number of rounds you did to get the average motion (with units).  Put the total on the first line below, the number of rounds on the second. Then in the blank space write the result of that calculation. Then put your unit of distance in the line after that and the motion on the line after the per.

![](Images/TRMT_FormulaPage8.png)
Once you have that calculated you can then note down how many pedals, pushes, or steps you took along a route and convert it to real distance!

*For an in-person group setting, have learners work in pairs with one recording and calculating while the other creates the motions per distance.*

## Optional - Sampling strategies

To incorporate even *more* archaeological data skills we suggest stopping along your route at designated intervals. These allow your specific places to compare observations between surveys. To learn even more data literacy skills you can decide, before starting, you’d like to do: random, stratified random, systematic, or stratified unaligned systematic sampling when selecting those locations. These are four kinds of real sampling strategies that archaeologists employ when surveying a new place for evidence of human activity.

### Random sampling

This sampling strategy is exactly what it describes, it’s random. To use this strategy on any or all of your surveys, just choose to stop 10 times randomly throughout the journey. Once you’re paused, take the time to fill out the survey form included at the end of this Data Story. If you use this strategy on multiple surveys you don’t need to stop at the same place because it’s meant to be a random sample selection.

### Stratified random sampling

This sampling strategy involves dividing up the survey route or area based on some natural or existing quality in the landscape, and then randomly sampling within those areas proportionally. For example, sometimes we’ll survey an environment that has a mixture of forest and meadowland, but 75% is forest. If we want to do stratified random sampling we want to ensure that our number of samples is proportional to each type of existing quality. This would mean that 75% of our samples should be in forest while the remaining 25% are in meadows. If you use this sampling strategy, consider an existing aspect of your route, for example does it switch from paved road to dirt road, and sample randomly but in proportion to how much of the route is paved versus dirt.

### Systematic or interval sampling

This kind of sampling is when you decide to stop at regular intervals to take observations. To use this sampling strategy on any or all of your excursions, divide your route equally into ten parts either using distance or time. After a certain distance or number of minutes, stop and record something in the included survey form. If you use this strategy on multiple trips make sure to stop at the same location every time.

### Stratified unaligned systematic sampling

This strategy is when you divide up the area to survey into equal areas, for example on a grid system, and then sample randomly within that particular area. It’s like stratified sampling, in that you’re making sure you get samples within a particular area, but instead of having that stratification be defined by, for example, the amount of meadowland versus desert, it’s just an arbitrary area. It’s unaligned because the locations sampled will not be in the same row or column, but it’s still systematic because within a certain area there will be another sample. To use this sampling strategy, divide your route up into equal time or distance intervals and within each pick a random place to stop.

Once you’ve picked your sampling strategy, grab your recording method (or bring along copies of the observation form) and get surveying! If you’d like to practice these sampling strategies yourself before applying it to your survey, you can check out [this activity](https://socialsci.libretexts.org/Bookshelves/Anthropology/Archaeology/Digging_into_Archaeology:_A_Brief_OER_Introduction_to_Archaeology_with_Activities_%28Paskey_and_Cisneros%29/06:_How_to_Find_Archaeological_Sites/6.04:_Activity_3_-_Sampling_the_Past) written by Jess Whalen. There you can learn even more about sampling in archaeology.

*In group settings, have learners discuss what sampling strategies might work best for the routes they're working with.*

# The exercise

We outline eight surveys to conduct for this Data Story. While it's the exact same landscape each time, it’s important to bring fresh senses to every survey so you don’t miss what’s around you. We hope that by exploring eight different foci and becoming aware of the demonstrated differences that each brings (hopefully captured in your notes!) you cultivate those observation skills through actual practice.  While eight might seem like a lot of times to traverse the same route, it's the repetition that allows you to hone the observation skills necessary for archaeological survey. Bringing that mindfulness to every survey is important because, out in the field, your one observation of the landscape might be the only recording a place gets for a very long time!

And unlike real archaeological surveys, there’s no rush for this exercise. You can take your time and go at the pace that feels comfortable to you. You can do them in a single day, within a week, within a month, or over many months or even years because it's all about practicing your skills! And remember, as we suggested in the *Selecting your survey space* section, there are many ways to use this Data Story. You might decide to go on *your primary pathway* and map the survey route accurately, stopping 10 times along the route to take structured observations. Or you'll create eight illustrations centering the foci of each excursion. Regardless of how you take *The Road Most Traveled*, we hope that it encourages you to practice your observation skills in a serious way.

*When using this Data Story as a group exercise within a single session, assign each person one of the eight recommended surveys then conduct the exercise as a single sojourn with participants using the included form. Those assigned the *primary pathway *should be at the front of the group with those in the *eighth excursion *at the back. Those assigned the *seventh sojourn *should pair with those in the *sixth study *for safety and to meet the requirements for the sixth study. All others should be dispersed between those assigned to group one and group eight.*

## Exercise alternate - A single survey, with several stops

![](Images/TheRoadMostTraveled_roadAsset_Alternate.png)

In this Data Story, we want people to practice their observation skills. For archaeologists, this usually means closely observing *and* recording what we notice. Because without recording, we have no ability to share what we observed about our world!

To cultivate this aspect, all options for this Data Story encourage participants to record their findings. Any kind of recording is great practice for cultivating good archaeological field note taking practices. These notes also mean that, once you’ve completed this Data Story, you can combine notes from the eight different surveys together. These notes will demonstrate how changing foci altered what you noticed or illustrate consistent patterns in the surveyed area. However, you, or your facilitator, may want to formalize one, or all of the surveys', observations to practice turning those notes into structured data, such as in a table.

To take this alternate route, make at least 10 stops for observations along a single survey, potentially using one of the sampling strategies introduced previously, and fill out one of the included forms at each stop along the survey.

If you choose to do this, follow the instructions for the *primary pathway* or *tertiary traversal* to guide your observation-foci and skip the rest of the suggested exercise. You may also do this for each excursion to generate even more data. Regardless, ten stops gives you plenty of chances to find patterns, record similar observations, draw miniature maps, or take photographs, without turning the practice into a rushed mess, which is the exact *opposite* of a mindful practice. 

*To use this alternative as a group exercise have all participants use the same observation foci and fill out the included form for each stop. Then combine all their observations into a single data set, either as a group exercise in structured data creation or as a facilitator task, to explore patterns that emerge from their observations.*

## 1) Your primary pathway

Once you’ve got your route planned out, go for it! For this first round, as you travel, consider what do you see, hear, feel, and smell while on your path? What pops out to you? What do you remember? Don’t focus on anything specific, besides sticking to your route.

Afterwards, record your observations to note the things that you saw. You'll do this after every survey so decide now if you'd prefer to use the form, putting notes in the spaces included with this Data Story, or noting these in another format and deciding your own criteria. Make sure to record what these criteria are so that your notes are comparable between surveys. You can record those criteria wherever you're taking notes or jos down a few thoughts in the space below. 

![](Images/TRMT_PrimaryPathway.png)

## 2) Your second survey, being on the level

For your second survey, focus on keeping your gaze at a particular level on your walk (while also following all safety guidelines for your area). Circle it below so you don't forget!

+ Look up! (Higher than your resting line of sight)
+ Look down! (Lower than your resting line of sight)
+ Look straight ahead! (At your own eye level)

If you drift from this, that’s okay! Just bring your focus back to the level you picked originally  At the end, record what you observed in a similar fashion as you did for the primary pathway.

![](Images/TRMT_SecondSurvey.png)

## 3) Your tertiary traversal, motif motivators

Now that you’re attuned to your surroundings, we’d like you to search for a particular theme, motif, belonging, or symbol on your traversal. These foci can be anything that falls into those categories, as long as there's a uniting theme.

Examples include a particular color, a specific type of item, something abstract like "symmetry", the texture of the pavement, or the sound from wind chimes. Another option is to focus on a bigger theme. For example, if you're surveying during a season that has less daylight and you live in an area where people put up lights or other decor, you could focus on the concept of “Holiday Decorations” or look for indications that people celebrate a specific one!

Regardless of your motif, focus on noticing that theme throughout the survey. At the end, fill out ye olde notes. And, so you don't forget your motif, write or drawn it below:

![](Images/TRMT_TertiaryTraversal.png)

## 4) Your fourth foray, stay sensitive to surroundings

For this round, figure out which sense you’ve used most on your last trips based on the notes you’ve been taking. You can do this by categorizing different observations based on the sense you used to observe them and noting that in your records. (Typically for archaeological surveys, we tend to use sight, but the other senses are really important for observing the environment as well.) You can also fill out the table below to tally how many observations you made using each of these senses:

![](Images/TRMT_FourthForay.png)
Once you've sorted that out, on this jaunt, focus utilize one of your less-used senses (like the one with the lowest number in the table you filled out) to observe your landscape. Smells, sounds, and the feeling of the ground underneath us can all signal the presence of an archaeological site, so it’s important to utilize all of our senses as we practice slow observation. As always, make sure to navigate your route safely with an acute awareness of your area and environment. At the end of your foray, add to your records, noting what you observed with this different sense.

## 5) The quinary questing, sunset, sunrise, or some other time

While we didn’t specify this, it’s likely you've been conducting these surveys around the same time of day. For this quinary quest, we’d like you to study your route at a different time than normal. If you're a morning mover, try going in the early evening. Been having post dinner digressions? Try going out around lunch. See how the time of day influences what you notice on your route. At the end of your study, note what you observed as another entry. Also take a second to note or draw what time of day you normally went out for your excursions and when you're going for this round in the space below: 

![](Images/TRMT_QuinaryQuesting.png)

## 6) The sixth study, pairing up is caring

Now that you’re an expert excursioner (having done these surveys for a few minutes, days, weeks, or months), ask a friend to accompany you. If you know someone who has a particular expertise, such as an artist, a physical therapist, an animal lover, a botanist, someone who cooks a lot, or anything really, consider bringing them along. Note down why you brought them along in the box below. (Bonus points if they like to observe and talk about the environment!)

This sixth study is an adaptation of Alexandra Horowitz's work in the book *On Looking: Eleven Walks with Expert Eyes* (2013). Beyond the bonus of having a buddy, real archaeological surveys are also often done in groups. On those though, you’ll usually walk within 10 meters of one another rather than next to them. For this round, listen to what your partner has to say about the surroundings. Then, once you've returned, record what you observed about the landscape through your conversation with your partner.

![](Images/TRMT_SixthStudy.png)

## 7) The seventh sojourn, now reverse

For the penultimate peregrination, we’d like you to explore your environment by taking your route backwards. There are two ways to do this. The first is to sojourn in the opposite direction than you normally do. If you usually take a right and arrive back at your starting point from the left, go left and end on your right.

The other option is to take the same route but face the opposite direction, moving backwards along that same path. Be careful if you take option two as you’ll be moving in the opposite direction of where you are facing. Because this is the case, make sure to move slowly while you observe the route. At the end of your sojourn, survey forms complete must you...to note what you observed while going backwards.

Note which choice you made in the space below:

![](Images/TRMT_SeventhSojourn.png)

## 8) The eighth excursion, the last time is the first time

After moving along the same path so many times, with different people, visual foci, times of day, and directions, go on your excursion, surveying your surroundings one last time. This time, no strings attached. Just move along it like you did during your primary pathway. At the end of it, don't forget to note what you observed and take the chance to reconsider the recording criteria you set for yourself at the beginning.

![](Images/TRMT_EigthExcursion.png)

# Concluding *The Road Most Traveled*

Now that you’ve exercised your observation skills a number of times, whether you completed this within a day, a week, a few weeks, a month, or a few months, you’ll now have data from multiple surveys exploring various elements of the environment. Review all of your excursion recordings and consider what they tell you about your landscape and the route you took. Then answer the following questions and include them with your notes.

+ Are there similarities between the different ways you surveyed your route? What did you notice that was different?

+ How did you observe the same places in different ways? Did you notice different features or attributes of your path? What perspective or survey method provided the most insight for your survey route?

+ What kind of data did you create based on your observations? Would you consider these quantitative or qualitative assessments? If you wanted someone to note the same qualities you observed on this route in another place, what would record or ask others to record? How would you structure that request to ensure similarity between your records and theirs?

After you've answered these questions, reexamine your notes and remind yourself that these are the same skills you’d use on a real archaeological survey, just in a different context. (And archaeologists are all about noticing contexts). Oh and that you probably wouldn't get to revisit your surveyed area as regularly as you did in this exercise!

If you’d like to turn the notes from *The Road Most Traveled* into structured data (expanding your archaeological data literacy skills!), you can use [*Gabbing About Gabii: Going from Notes to Data to Narrative*](https://doi.org/10.6078/M7DV1H1R) to turn your forms (like the included survey form) into a data table. If you recorded your observations differently, such as using voice, *Gabbing About Gabii* provides guidance on identifying data types, selecting appropriate attribute headings, and how to record observations for specific attributes that you can pull out from other recording styles. After doing that, you can even use *Gabbing about Gabii*'s narrative guide to write short stories about the survey based on your observations. 

Even if you don’t turn your observations from this Data Story into structured data, you can repeat these exercises to practice your survey skills and mindfully observe your surroundings whenever you travel to a new place. Or whenever you happen upon your road most traveled!

# References for further reading
These references are listed in order of appearance in the text or are suggestions for further reading on the topics presented. If any URLs navigate to a broken page, please check the [Wayback Machine](https://web.archive.org/) for an archived copy of the material.

The Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0): <https://creativecommons.org/licenses/by-sa/4.0/>

Check out other Digital Data Stories here: <https://doi.org/10.6078/M74F1NW0>

Find out about the other work that the Data Literacy Program does here:  <https://doi.org/10.6078/M70P0X5P>

Explore to explore Project Zero and it’s resources begin with:  <http://www.pz.harvard.edu/search/site/slow%20looking>

Consider how you can incorporate slow looking into your daily life through the Thinking Museum: <https://thinkingmuseum.com/2020/12/02/simple-ways-to-practise-slow-looking-every-day/>

Check out the Slow Looking Guide from Project Zero: <https://pz.harvard.edu/sites/default/files/Slow%20Looking%20-%207.11.2018.pdf>

Learn more about Slow Art day here: <https://www.slowartday.com/about/tools/>

Learn about Data Stories in the *Solo Series* here: <https://alexandriaarchive.org/2023/05/25/solo-steady-and-structured-data-literacy/>

*Gabbing about Gabii: Notes to Data to Narrative* by Paulina F. Przystupa and L. Meghan Dennis (2022) <https://doi.org/10.6078/M7DV1H1R>

Find more information about the slow food movement here: <https://www.slowfood.com/>

Consider the benefits and costs of slow science by learning more here: <https://en.wikipedia.org/wiki/Slow_science>

Read about some examples of slow journalism here: <https://www.nationalgeographic.org/projects/out-of-eden-walk/your-own-walk/>

"'Trying to Hear with the Eyes': Slow Looking and Ontological Difference in Archaeological Object Analysis" by Eva Mol (2021) <https://www.tandfonline.com/doi/full/10.1080/00293652.2021.1951830>

Check out Eva Mol's presentation on Herbert the pot here (it should be about 1:07:07 into the video): <https://www.youtube.com/watch?v=ziuma1eFLf4&t=4027s>

Find a guide for the Ten times Two (10 x 2) exercise here: <http://pzartfulthinking.org/?p=90>

*Slow Looking: The Art and Practice of Learning Through Observation* by Shari Tishman (2017) <http://www.pz.harvard.edu/resources/slow-looking-the-art-and-practice-of-learning-through-observation>

*Unfuck Your Brain: Using Science to Get Over Anxiety, Depression, Anger, Freak-outs, and Triggers* by Faith G. Harper (2017) <https://www.faithgharper.com/>

Explore our definition of data literacy here: <https://doi.org/10.6078/M73F4MRM>

Check out an example of archaeological survey here: <https://doi.org/10.6078/M7SF2T9Q>

Need more information on the Global Positioning System (GPS)? Check out there wiki: <https://en.wikipedia.org/wiki/Global_Positioning_System>

Check out more information on what topographic maps are, on their wiki: <https://en.wikipedia.org/wiki/Topographic_map>

*On Looking: Eleven Walks with Expert Eyes* by Alexandra Horowitz (2013) <https://alexandrahorowitz.net/On-Looking>

For access to the sampling strategies activity by Jess Whalen check out this link: <https://socialsci.libretexts.org/Bookshelves/Anthropology/Archaeology/Digging_into_Archaeology:_A_Brief_OER_Introduction_to_Archaeology_with_Activities_%28Paskey_and_Cisneros%29/06:_How_to_Find_Archaeological_Sites/6.04:_Activity_3_-_Sampling_the_Past>

*How to See* by Thích Nhất Hạnh (2019) <https://www.parallax.org/product/how-to-see/>

Gain access to any broken links by getting an archived copy from the Wayback Machine: <https://web.archive.org/>

# Credits

Unless otherwise specified this work and its components are shared under a [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) license. To attribute this work, please use our suggested citation: 

Paulina F. Przystupa and L. Meghan Dennis, 2024, “The Road Most Traveled: An Archaeological Survey Guide”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: <https://doi.org/10.6078/M79S1P6F>.

In order of appearance, this Data Story includes: "TRMT_MainImage" and "The Road Most Traveled short path" by L. Meghan Dennis from the Data Literacy Program (DLP) adapted from works that have image provenance and attribution information forthcoming. It also includes "TheRoadMostTraveled_roadAsset_Alternate" by Paulina F. Przystupa from the DLP / CC BY-SA and an adaptation (separated into individual icons) of "senses" by Smashing Stocks through the Noun Project (<https://thenounproject.com/icon/senses-3845210/>) under a Royalty-Free License. All other images included in this text either have their attributions cited in their captions or are logos for our sponsors. 

# Acknowledgements

This Data Story is a collaboration between the producers of the Data Story, our testing audiences, and our open peer reviewers. We are grateful to all the work that people in the existing slow looking movement have done to inform our assembly of this exercise. Eva Mol’s short presentation on the topic during the Teaching and Learning conference hosted by Karina Croucher and Hannah Cobb, Shari Tishman’s work, and Alexandra Horowitz’s foundations in slow looking on walks helped to bring new ingredients to the Data Story. We appreciate the avenues these works provided in encouraging us, and other archaeologists, to practice observation and data literacy skills.

The Data Stories are part of the overarching Data Literacy Program, with support from a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.

![](Images/TRMT_ObservationForm_Page_1.jpg)
![](Images/TRMT_ObservationForm_Page_2.jpg)