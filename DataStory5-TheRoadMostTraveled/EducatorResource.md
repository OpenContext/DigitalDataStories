---
title: 'The Road Most Traveled: An Archaeological Survey Guide'
subtitle: 'Educator Resource'
author: "L. Meghan Dennis, Postdoctoral Researcher and Paulina F. Przystupa, Postdoctoral Researcher in Archaeological Data Literacy"
date: "`r Sys.Date()`"
output:
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

![](Images/TheRoadMostTraveled_EducatorsImage.jpg)

# Exercise Description and Aims

The [Digital Data Stories Project](https://doi.org/10.6078/M74F1NW0) promotes an increased focus in archaeological education on digital data literacy. Through the use of open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis. These practicals illustrate the confluence of science and humanities-based investigations in collected data on the past.

This approach promotes multiple levels of engagement with archaeological data sets, linking data-driven narratives with the key analytical and interpretive steps used to ethically analyze, visualize, and present research data.

In this [Digital Data Story](https://doi.org/10.6078/M79S1P6F), participants will learn to:

+ Identify an appropriate survey strategy
+ Create a map of a survey route
+ Deduce distance with and without formal tools
+ Document observations while moving on the landscape
+ Report observations in a basic survey form

![](Images/RoadMostTraveled-WordCloud.png)

## additional texts

E.B. Banning,  A.L. Hawkins. and S.T. Stewart (2011) Sweep widths and the detection of artifacts in archaeological survey, *Journal of Archaeological Science*, 38:12, 3447-3458. DOI: <https://doi.org/10.1016/j.jas.2011.08.007>.

Néhémie Strupler (2021) Re-discovering Archaeological Discoveries. Experiments with reproducing archaeological survey analysis, *Internet Archaeology* 56. DOI: <https://doi.org/10.11141/ia.56.6>.

## the data set

In lieu of a specific data set, in this [Digital Data Story](https://doi.org/10.6078/M79S1P6F) we encourage participants to use their own local area to combine archaeological methods and their personal environment.

This breadth of possibilities allows participants to select survey areas that line up with their research interests. However, this choice may also be used for other purposes relating to the study of archaeology and heritage in general such as to focus study survey work in a specific environment type.

## assessment and scaffolding options

For those participants who require (or desire) more support, splitting into pairs to complete the [Digital Data Story](https://doi.org/10.6078/M79S1P6F) offers the opportunity to work (and walk!) together, switching off tasks and comparing observations and associations. Resources available via the GLAM Workbench can also provide increased rigor.

As this [Digital Data Story](https://doi.org/10.6078/M79S1P6F) relies innately on personalization, the end products will vary. One potential assessment option is to ask students to reattempt the [Digital Data Story](https://doi.org/10.6078/M79S1P6F)  with a different route, or to swap routes with a partner and attempt observations, with comparative discussion to explore different outcomes.

## Technical Requirements

Participants should have a notebook, journal, printout of the [Digital Data Story](https://doi.org/10.6078/M79S1P6F), or some other method of recording observations. Voice recorders, phones, tablets, or laptops are fine, as long as their technical operation doesn’t distract from the process of focused observation or create risk while moving around.

## Duty of Care

Because students or participants may be completing the exercise within their own neighborhoods or local areas, it is important to ensure a safe and emotionally secure environment during debrief or group sharing. Not all students are comfortable sharing the details of their lived experiences, and facilitators should be mindful of the emotional state and physical safety of students.

Being aware of how students are responding to their observations is key to maintaining the duty of care inherent as an educator. Students may wish to complete the exercise in an environment different from their own neighborhood or local area and educators can offer their school environment as an alternative.

## Duration

The Digital Data Story is variable in duration. Some participants may choose to do the exercise within a compressed window, while others may embrace the “slow” aspect of the process and take longer with some portions of the exercise.

If students are completing a portion of their observations via more than one session, please direct them to pay close attention to where they left off, and the specific guidance related to the step they ended on.

### get in touch by contacting the Team

If you or your students have questions, comments, or concerns, please get in touch. We love feedback, especially from educators!

L. Meghan Dennis 
@archaeoethicist (she/her)

Paulina F. Przystupa 
@punuckish (she/their/none)

<datastories@opencontext.org>

**Licensing:** “The Road Most Traveled Educators Sheet Text” and “RoadMostTraveled-WordCloud” by L. Meghan Dennis and Paulina F. Przystupa are original works from the Data Literacy Program (DLP) licensed [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) “TheRoadMostTraveled_EducatorsImage” by Paulina F. Przystupa and L. Meghan Dennis from the DLP licensed [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) and includes an adaptation (recolored) of “The Road Most Traveled short path” by L. Meghan Dennis from the DLP adapted from vector images with image provenance and attribution information forthcoming. 