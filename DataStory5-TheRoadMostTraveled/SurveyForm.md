---
title: 'The Road Most Traveled: An Archaeological Survey Guide - Survey Form'
author: "L. Meghan Dennis"
date: "`r Sys.Date()`"
output:
  html_document:
    df_print: paged
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
#This is a markdown copy of the survey forms included as images at the end of the survey guide
#RELEASE DATE: 08/2024
#VERSION: 2.0
#LICENSE: CC BY-SA https://creativecommons.org/licenses/by-sa/4.0/deed.en
```

|Property on Route (Public, Private, Governmental, Other) | Partner Speciality?|  
|---------------------------------------------------------|--------------------|
| | |
| | |
| | |
| | |

|Survey Method (interval, random, random stratified, other)|Observations|
|----------------------------------------------------------|------------|
|Weather||
||
|Time of Day||   
||
|\# of Stops/List of Stops||  
||
|Total Duration of Survey (5 minutes, 30 minutes, an hour?)||  
||

|Observations (What did you see? What did you hear, or smell? What was most memorable?)| 
|--------------------------------------------------------------------------------------|
||
||
||
||

|Finds|\# of Finds|Notes|
|-----|-----------|-----|
|(EXAMPLE) Dogs|2| One dog was off-leash; Another dog was afraid of a goose, I was too!|
||||
||||
||||
||||

|Sketch Plan of General Area (show route and annotate observational sites)|
|-------------------------------------------------------------------------|
||
||
||
||

|Photos (indicate cardinal direction, photo number, any notable content)|
|-----------------------------------------------------------------------|
||
||
||
||