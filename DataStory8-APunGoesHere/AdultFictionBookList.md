---
title: 'A Pun Goes Here: The AAI Reads Fiction Book Club'
subtitle: 'Part Two: The book list - Adult fiction'
author: "Paulina F. Przystupa and L. Meghan Dennis"
date: "`r Sys.Date()`"
output:
  word_document: default
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
#RELEASE DATE: 01/2025
#VERSION: 2.0
```

# Book list overview

1. Introduction   
2. Why this list of books?  
3. How to read the book entries  
4. The adult fiction book list   
5. References and further reading  
6. Credits  
7. Acknowledgements

Estimated time to read this guide: 20 minutes (including all book entries)

![](Images/APGH_Sunflowers.png)

# Introduction

Welcome to the [Digital Data Stories](https://doi.org/10.6078/M74F1NW0) of the [Alexandria Archive Institute](https://alexandriaarchive.org/) and [Open Context](https://opencontext.org)! This is the adult fiction version of *A Pun Goes Here* Part Two: The book list. It includes links to video summaries for each book, transcripts of those videos, and noted content warnings.

This part helps readers choose good reads for themselves and helps participants run their own archaeology data literacy-focused book club. It can also act as a list of popular archaeology-related fiction that can be integrated into a class or used for personal enjoyment. Readers looking for discussion questions, search terms, or related resources that accompany each book in this list can check out the adult fiction version of [Part Three: The discussion guide](https://doi.org/10.6078/M7PR7T4J). 

As a supplement to these lists and guides, you can explore the [book data table](https://doi.org/10.6078/M7PC30GS) to cultivate your data literacy skills. This spreadsheet—which includes data about the books in all of our book club Data Stories—allows you to explore the works in a more technical way. Participants can cultivate data literacy by learning how to work with data through sorting, filtering, and visualizing patterns in metadata, such page lengths and publication dates; associated search terms and questions; and creator demographics. Those unfamiliar with working in spreadsheets can use [this tutorial](https://doi.org/10.6078/M7Q81B6V) for guidance. 

# Why this list of books?

We want this list to support archaeology-interested book clubs in libraries, early career archaeologist clubs, secondary education, university departments, or any communities interested in archaeology and prepared to read works of adult fiction. This book list does this by drawing on published popular fiction to teach the principles of archaeological data literacy through reading and discussion.

*Note: All the books in this list are adult fiction and therefore do not have an established reading level. Late teen readers interested in this list, or educators working with mature high school students, should examine the content warnings included with each book blurb to see if the book is appropriate for you or your community.*

To help group the books for use in book clubs or to read a few books with similar themes, we organized this list around three questions for readers to consider. 

+ Can you tell an archaeologist wrote this?
+ How do science fiction or fantasy novels utilize archaeological information?
+ Why might authors use archaeology in mystery novels?

We associate each question with four books so that this book club can last a whole year. These questions can also guide variants of the club that focus on one question or ones that read a book related to each question. Of course, participants can also pick books from the list however they like, or as appropriate for their group!  If you'd like to learn more about how we picked these specific books check out [Part One: The book club guide](https://doi.org/10.6078/M7BR8Q9T) or our article on [how to find fun archaeology-related fiction](https://doi.org/10.6078/M7F769PS). 

# How to read the book entries

In each entry, we introduce a different book from the adult fiction list. These include links to a short video that summarizes the book—with transcript—and content warnings. Each of these sections are also marked with the data literacy component that they relate to.

To see how the different parts of these entries relate to data literacy, you'll begin by reading the book's title and author to see “what aspects of the world they represent” ([Bhargava et al. 2016:198](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/)). Look for the book spines sitting together within a dark blue circle to find that information in the entries.

From there, we consider the *Content warnings* section to be “working with” the data because those tags are part of acquiring the data from the book and managing it. Look for the planner within the light blue circle to find those in the book blurb entries.

*Note: Predominantly, the DLP team tagged the warnings themselves using trigger or warning lists from [Book Trigger Warnings](https://booktriggerwarnings.com/) (BTW) and the [Trigger Warning Database](https://triggerwarningdatabase.com/contributors/) (TWD). However, for some books we used crowdsourced content warnings to expand our noted warnings. Look for the superscripts BTW and TWD denoting when a book entry includes crowd-sourced tags and see the references section for links to their warning pages. If you identify additional content warnings, please reach out so we can update those entries and the book data table.*

![](Images/APGH_BookEntry-Blurb.jpg)

*Here's an example of a book entry. Can you spot the data literacy component symbols?*

Lastly, as part of the “communicating” literacy component, you can get a taste of each book by checking out our [*Video Book Blurbs*](https://www.youtube.com/playlist?list=PLhjqAN1yrR07iPyOZsxuJUdgJGhUXFa-2) or reading through their transcripts! You'll find these examples of *communicating* marked by a little computer with the wifi symbol on the screen that sits in a black circle. If you're interested in the data literacy components of "analyzing" and "arguing", we recommend checking out [Part Three](https://doi.org/10.6078/M7PR7T4J) for discussion questions, search terms, and related resources for each book.

![](Images/DLPdataliteracycomponents.png)

*Here's [Bhargava et al.’s (2016)](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/) original definition for the components of data literacy. We added icons for each to help locate these components throughout the Data Story.*

We hope these features create clear connections between reading and data literacy to demonstrate how book clubs help you cultivate the components of data literacy in diverse ways. If you'd like to learn more about making your own guide check out the Data Story Short [*How to Make Your Own (Archaeological) Book Club*](https://doi.org/10.6078/M7K072DH). Thank you so much for checking out our book list and happy reading!

\newpage

# The adult fiction book list

```{r}
bookData = read.delim("Data/", header=TRUE, sep = ",")

fiction = subset(bookData, bookData$InWhichSeries == "Fiction")
themes = levels(factor(fiction$AssociatedTheme))

reading = "Images/Reading_Image-01.png"
working = "Images/WorkingWith_Image-01.png"
analyzing = "Images/Analyzing_image-01.png"
arguing = "Images/Arguing_Image-01.png"
communicating = "Images/Communicating_Image-01.png"
counter = 0

counter = counter+1
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
```

This section of the R markdown version of the text is not an exact replica of the PDF or dynamic document. Instead, this section should be combined with a local CSV version of the book data table (available at https://doi.org/10.6078/M7PC30GS) and run in R. It pulls local data via R to generate the text and images from the table, assuming the related images are available. 

This allows you to modify the table, adding your own books, questions,  videos, or any content you so choose and then create a document with the same elements as the Data Story *A Pun Goes Here*. There are other modifications as well to accommodate for the data pulling elements of this document. For example, instead of superscripts there is a new section about the source for the tagged content warnings. The content warning sections were predominantly noted by the DLP and entries with crowdsourced tags include links to their [Book Trigger Warnings](https://booktriggerwarnings.com/) and/or [Trigger Warning Database](https://triggerwarningdatabase.com/contributors/) pages. We provide further references for those pages under *References and further reading*, to credit those sources for some of the noted warnings. We include image credits for each book entry image under *Credits* in order of appearance. 

We've still organized the list below by their thematic questions, as listed in the book data table. 

## Theme `r counter`: `r themes[counter]`
All these books were written by people who were archaeologists at some point in their career. 

### *`r currentTheme$Title[1]`* by `r currentTheme$FirstAuthor[1]`

### *`r currentTheme$Title[2]`* by `r currentTheme$FirstAuthor[2]`

### *`r currentTheme$Title[3]`* by `r currentTheme$FirstAuthor[3]`

### *`r currentTheme$Title[4]`* by `r currentTheme$FirstAuthor[4]`

```{r}
counter = counter+1
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
```

## Theme `r counter`: `r themes[counter]`
Nonarchaeologists incorporated archaeology elements in their world building for these novels. 

### *`r currentTheme$Title[1]`* by `r currentTheme$FirstAuthor[1]`

### *`r currentTheme$Title[2]`* by `r currentTheme$FirstAuthor[2]`

### *`r currentTheme$Title[3]`* by `r currentTheme$FirstAuthor[3]`

### *`r currentTheme$Title[4]`* by `r currentTheme$FirstAuthor[4]`

```{r}
counter = counter+1
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
```

## Theme `r counter`: `r themes[counter]`
Nonarchaeologists used archaeology or archaeological methods to mediate these mysteries.

### *`r currentTheme$Title[1]`* by `r currentTheme$FirstAuthor[1]`

### *`r currentTheme$Title[2]`* by `r currentTheme$FirstAuthor[2]`

### *`r currentTheme$Title[3]`* by `r currentTheme$FirstAuthor[3]`

### *`r currentTheme$Title[4]`* by `r currentTheme$FirstAuthor[4]`

\newpage

```{r}
counter = 1 #reset to pull the entries in the right order
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
bookCounter = 1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript [](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`
\newpage
```{r}
bookCounter = bookCounter+1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript [](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage

```{r}
bookCounter = bookCounter+1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage

```{r}
counter = counter+1
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
bookCounter = 1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage

```{r}
bookCounter = bookCounter+1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage

```{r}
counter = counter+1
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
bookCounter = 1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage

```{r}
bookCounter = bookCounter+1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` video transcript[](`r communicating`)

`r currentTheme$VideoTranscript[bookCounter]`

### Content warnings ![](`r working`)

`r currentTheme$ContentWarnings[bookCounter]`

#### Source for content warnings

`r currentTheme$ContentWarningSources[bookCounter]`

\newpage

# References and further reading

These references are listed in order of appearance in the text for this part of the Data Story or are suggestions for further reading on the topics presented. This section does not repeat full links included with individual book entries. If any links navigate to a broken page, please check the [Wayback Machine](https://web.archive.org/) for an archived copy of the material.

Check out other Digital Data Stories here: [https://doi.org/10.6078/M74F1NW0](https://doi.org/10.6078/M74F1NW0)

Find out about the work of the Alexandria Archive Institute at: [https://alexandriaarchive.org/](https://alexandriaarchive.org/)

Check out Open Context for yourself here: [https://opencontext.org](https://opencontext.org)

You'll find this book list's related discussion guide, aka Part Three, at: [https://doi.org/10.6078/M7PR7T4J](https://doi.org/10.6078/M7PR7T4J)

"*Aggregative Series* book data" by Paulina F. Przystupa and L. Meghan Dennis (2025)  [https://doi.org/10.6078/M7PC30GS](https://doi.org/10.6078/M7PC30GS)

Check out this spreadsheet tutorial if you desire or require help with sorting, filtering, or visualizing data: [https://doi.org/10.6078/M7Q81B6V](https://doi.org/10.6078/M7Q81B6V)

The Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0): [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)

You can check out *A Pun Goes Here* Part One here: [https://doi.org/10.6078/M7BR8Q9T](https://doi.org/10.6078/M7BR8Q9T) 

Learn how to find fun archaeology related fiction here: [https://doi.org/10.6078/M7F769PS](https://doi.org/10.6078/M7F769PS) 

"Data Murals: Using the Arts to Build Data Literacy" by Rahul Bhargava, Rahul, Ricardo Kadouaki, Emily Bhargava, Guilherme Castro, and Catherine D'Ignazio (2016)  
[https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/)

Explore our definition of data literacy here: [https://doi.org/10.6078/M73F4MRM](https://doi.org/10.6078/M73F4MRM)

See Book Trigger Warnings for more information about trigger warnings:   
[https://booktriggerwarnings.com/](https://booktriggerwarnings.com/) 

Or you can check the Trigger Waring Database: [https://triggerwarningdatabase.com/contributors/](https://triggerwarningdatabase.com/contributors/) 

You can check out our video book blurbs here: [https://www.youtube.com/playlist?list=PLhjqAN1yrR07iPyOZsxuJUdgJGhUXFa-2](https://www.youtube.com/playlist?list=PLhjqAN1yrR07iPyOZsxuJUdgJGhUXFa-2)

"How to Make Your Own (Archaeological) Book Club" by Paulina F. Przystupa (2025)  
[https://doi.org/10.6078/M7K072DH](https://doi.org/10.6078/M7K072DH) 

"A Master of Djinn by P. Djèlí Clark" by Albi Hayes, Jenjenreviews, Bookrobbot, and Polterghastly (last updated 2022): [https://www.booktriggerwarnings.com/A_Master_of_Djinn_by_P._Dj%C3%A8l%C3%AD_Clark](https://www.booktriggerwarnings.com/A_Master_of_Djinn_by_P._Dj%C3%A8l%C3%AD_Clark)

"A Master of Djinn by P Djèlí Clark " by rachel @ typed truths (2022): [https://triggerwarningdatabase.com/2022/05/20/a-master-of-djinn-by-p-djeli-clark/](https://triggerwarningdatabase.com/2022/05/20/a-master-of-djinn-by-p-djeli-clark/)

"Provenance by Ann Leckie" by Cerilouisereads, Jenjenreviews, and Bookrobbot (last updated 2021): [https://www.booktriggerwarnings.com/Provenance_by_Ann_Leckie](https://www.booktriggerwarnings.com/Provenance_by_Ann_Leckie)

"Black Sun by Rebecca Roanhorse" by Cerilouisereads, Jenjenreviews, Bookrobbot, DariusMortee, and Traeumenvonbuechern (last updated 2022): [https://www.booktriggerwarnings.com/Black_Sun_by_Rebecca_Roanhorse](https://www.booktriggerwarnings.com/Black_Sun_by_Rebecca_Roanhorse)

"Black Sun by Rebecca Roanhorse" by Bec @ bec\&books (2022): [https://triggerwarningdatabase.com/2022/04/30/black-sun-by-rebecca-roanhorse/](https://triggerwarningdatabase.com/2022/04/30/black-sun-by-rebecca-roanhorse/)

"Murder in Mesopotamia by Agatha Christie" by Jenjenreviews, Bookrob13, Bookrobbot, and Albi Hayes (last updated 2021): [https://www.booktriggerwarnings.com/Murder_in_Mesopotamia_by_Agatha_Christie](https://www.booktriggerwarnings.com/Murder_in_Mesopotamia_by_Agatha_Christie)

"Murder in Mesopotamia by Agatha Christie" by rachel @ typed truths (2022): [https://triggerwarningdatabase.com/2022/06/03/murder-in-mesopotamia-by-agatha-christie/](https://triggerwarningdatabase.com/2022/06/03/murder-in-mesopotamia-by-agatha-christie/)

Gain access to any broken links by getting an archived copy from the Wayback Machine: [https://web.archive.org/](https://web.archive.org/)

# Credits

Unless otherwise specified in captions, or updated when image provenance is determined, this work and its components are shared under a [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license. To attribute this work, please use our suggested citation:

Paulina F. Przystupa and L. Meghan Dennis, 2025, “A Pun Goes Here: The AAI Reads Fiction Book Club”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: [https://doi.org/10.6078/M7PZ56Z5](https://doi.org/10.6078/M7PZ56Z5).

In order of appearance, this Data Story includes:

"APGH_Sunflowers" by L. Meghan Dennis from the DLP adapted from "Sunflowers" by Vincent Van Gogh, with attribution information for the digitized image of that painting forthcoming. 

"APGH_BookEntry-Blurb" by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) which includes "APGH\_Sunflowers", and "Reading Icon-Color", "Working with Icon-Color", and "Communicating Icon-Color" by L. Meghan Dennis / [CC BY](https://creativecommons.org/licenses/by/4.0/) adapted from "Books" ([https://thenounproject.com/icon/books-4439922/](https://thenounproject.com/icon/books-4439922/)), "File" ([https://thenounproject.com/icon/file-1331311/](https://thenounproject.com/icon/file-1331311/)), and "Internet" ([https://thenounproject.com/icon/internet-1771870/](https://thenounproject.com/icon/internet-1771870/)) by Made by Made (Made x Made) from the Noun Project under a royalty free license.

"DLP data literacy components" by L. Meghan Dennis and Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/), quoted text from Bhargava et al. (2016) and icons previously attributed as well as "Analyzing Icon-Color" and "Arguing Icon-Color" by L. Meghan Dennis / CC BY which are adapted from "School laptop" ([https://thenounproject.com/icon/school-laptop-1675142/](https://thenounproject.com/icon/school-laptop-1675142/)) and "Debate" ([https://thenounproject.com/icon/debate-776656/](https://thenounproject.com/icon/debate-776656/)) by Made by Made (Made x Made) from the Noun Project under a royalty free license.

“Sunflowers graphic” by Paulina F. Przystupa from the DLP. That image includes "APGH_Sunflowers" see preceding for attribution information. 

 “A Climate of Fear graphic” by Paulina F. Przystupa from the DLP / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) includes an adaptation (cropped, recolored, resized, and reoriented) of “[A Seal at the Glacial Lagoon](https://commons.wikimedia.org/wiki/File:A_Seal_at_the_Glacial_Lagoon.jpg)” by Andrew Bowden from Flickr via Wikimedia Commons ([https://commons.wikimedia.org/wiki/File:A_Seal_at_the_Glacial_Lagoon.jpg](https://commons.wikimedia.org/wiki/File:A_Seal_at_the_Glacial_Lagoon.jpg)) under [CC BY-SA](https://creativecommons.org/licenses/by-sa/2.0/deed.en) 2.0 Generic.

“Prudence graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) includes an adaptation (recolored, resized, reoriented) of “[airship](https://thenounproject.com/icon/airship-1054374)” and an adaptation (recolored, resized, cropped) of “[Cup](https://thenounproject.com/icon/cup-921945/)” both by Alex Muravev from The Noun Project ([https://thenounproject.com/icon/airship-1054374/](https://thenounproject.com/icon/airship-1054374/) and [https://thenounproject.com/icon/cup-921945/](https://thenounproject.com/icon/cup-921945/)) under Royalty-Free Licenses.

“Braving Fate graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) includes an adaptation (cropped, recolored, resized) of “[Boudica statue, Westminster (8433726848)](https://commons.wikimedia.org/wiki/File:Boudica_statue,_Westminster_\(8433726848\).jpg)” by Paul Walter from Flickr via Wikimedia Commons ([https://commons.wikimedia.org/wiki/File:Boudica_statue,_Westminster_(8433726848).jpg](https://commons.wikimedia.org/wiki/File:Boudica_statue,_Westminster_\(8433726848\).jpg)) under CC BY 2.0 Generic.

“A Master of Djinn graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) and includes an adaptation (recolored and resized) of “[Fig. 40 from Africa/Egypt/Cairo/Sabil-kuttab of ’Ali Bey al-Dumyati](https://n2t.net/ark:/28722/k21v5x22g)” by Hans Theunissen from Open Context ([https://n2t.net/ark:/28722/k21v5x22g](https://n2t.net/ark:/28722/k21v5x22g)) under CC BY 4.0.

“Provenance graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) and includes  an adaptation (recolored) of “[singing bowl](https://thenounproject.com/icon/singing-bowl-3572270/)” by Hey Rabbit from The Noun Project ([https://thenounproject.com/icon/singing-bowl-3572270/](https://thenounproject.com/icon/singing-bowl-3572270/)) under a Royalty Free License.

“Black Sun graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/). That image includes an adaptation (cropped, recolored, resized) of “[Indians and pioneers; an historical reader for the young](https://commons.wikimedia.org/wiki/File:Indians_and_pioneers;_an_historical_reader_for_the_young_\(1897\)_\(14579358589\).jpg)” by Blanche Evans Hazard and Samuel Train Dutton from The Internet Archive Book Images contributed by The Library of Congress ([https://commons.wikimedia.org/wiki/File:Indians_and_pioneers;_an_historical_reader_for_the_young_(1897)_(14579358589).jpg](https://commons.wikimedia.org/wiki/File:Indians_and_pioneers;_an_historical_reader_for_the_young_\(1897\)_\(14579358589\).jpg)) and has no known copyright restrictions.

“The Clockwork Dynasty graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/). That image includes an adaptation (cropped, recolored, resized) of “[Chinese woodcut, Famous medical figures: The Yellow Emperor](https://commons.wikimedia.org/wiki/File:Chinese_woodcut,_Famous_medical_figures;_The_Yellow_Emperor_Wellcome_L0039314.jpg)” by Gan Bozong (Tang period, 618-907) from Wellcome Images, a website operated by Wellcome Trust, a global charitable foundation based in the United Kingdom ([https://commons.wikimedia.org/wiki/File:Chinese_woodcut,_Famous_medical_figures;_The_Yellow_Emperor_Wellcome_L0039314.jpg](https://commons.wikimedia.org/wiki/File:Chinese_woodcut,_Famous_medical_figures;_The_Yellow_Emperor_Wellcome_L0039314.jpg)) under CC BY 4.0 International.

“Artifact graphic” by Paulina F. Przystupa from the DLP / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). That image includes an adaptation (cropped, recolored, resized) of “[Necklace](https://commons.wikimedia.org/wiki/File:Khalili_Collection_Islamic_Art_jly_1261.6.jpg)” by Khalili Collections from Khalili Collections ([https://commons.wikimedia.org/wiki/File:Khalili_Collection_Islamic_Art_jly_1261.6.jpg](https://commons.wikimedia.org/wiki/File:Khalili_Collection_Islamic_Art_jly_1261.6.jpg)) under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/igo/deed.en) IGO.

“The Crossing Places graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/). That image includes an adaptation (cropped, recolored, resized) of “[At Banham, Norfolk](https://commons.wikimedia.org/wiki/File:E.T._Daniell_-_At_Banham,_Norfolk.jpg)” by Edward Thomas Daniell from Norfolk Museums Collections ([https://commons.wikimedia.org/wiki/File:E.T._Daniell_-_At_Banham,_Norfolk.jpg](https://commons.wikimedia.org/wiki/File:E.T._Daniell_-_At_Banham,_Norfolk.jpg)) under Public Domain.

“Murder in Mesopotamia graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/). That image includes an adaptation (cropped, recolored, and resized) of “[The archæology of the cuneiform inscriptions](https://commons.wikimedia.org/wiki/File:The_arch%C3%A6ology_of_the_cuneiform_inscriptions_\(1908\)_\(14780901584\).jpg)” by Archibald Henry Sayce from The Internet Archive ([https://commons.wikimedia.org/wiki/File:The_arch%C3%A6ology_of_the_cuneiform_inscriptions_(1908)_(14780901584).jpg](https://commons.wikimedia.org/wiki/File:The_arch%C3%A6ology_of_the_cuneiform_inscriptions_\(1908\)_\(14780901584\).jpg)) contributed by the University of California LIbraries and has no known copyright restrictions.

“The Golden Hairpin graphic” by Paulina F. Przystupa from the DLP / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). That image includes an adaptation (cropped, recolored, and resized) of a photograph of “[Hairpin](https://commons.wikimedia.org/wiki/File:Hairpin-IMG_0446.JPG)” by an unknown artist, the piece was photographed by Rama who shared and uploaded the work ([https://commons.wikimedia.org/wiki/File:Hairpin-IMG_0446.JPG](https://commons.wikimedia.org/wiki/File:Hairpin-IMG_0446.JPG)) under a [CC BY-SA](https://creativecommons.org/licenses/by-sa/3.0/fr/deed.en) 3.0 France.

All other images included in this text either have their attributions cited in their captions or are logos for our sponsors. We will update this credits section with forthcoming image provenance and attribution information. 

# Acknowledgements

This Data Story is a collaboration between the producers of the Data Story, our testing audiences, AIA staff, and our open peer reviewers. We are grateful to all the work that writers and readers have done to create works with archaeological themes and are indebted to the work of numerous archaeologists, librarians, and archaeology enthusiasts for their continuing contributions to the corpus of archaeology-related fiction and book lists to find more of them. We're also very grateful for the ongoing crowdsourcing efforts at Book Trigger Warnings and Trigger Warning Database that provided comprehensive warnings for some books and guidance on warnings to add to books that lacked representation on those pages.  

The Data Stories are part of the overarching Data Literacy Program, with support from  a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.