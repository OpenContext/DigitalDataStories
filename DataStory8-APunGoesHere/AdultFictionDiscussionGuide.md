---
title: 'A Pun Goes Here: The AAI Reads Fiction Book Club'
subtitle: 'Part Three: The discussion guide - Adult fiction'
author: "Paulina F. Przystupa and L. Meghan Dennis"
date: "`r Sys.Date()`"
output:
  word_document: default
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
#RELEASE DATE: 01/2025
#VERSION: 2.0
```
# Discussion guide overview

1. Introduction
2. Reading the discussion guide
   1. Discussion questions
   2. Search terms
   3. Related resources
3. The adult fiction book list
4. References and further reading 
5. Credits
6. Acknowledgements

Estimated time to read this guide: 30 minutes (including all book entries)

![](Images/APGH_AgathaChristie.png)

# Introduction

Welcome to the [Digital Data Stories](https://doi.org/10.6078/M74F1NW0) of the [Alexandria Archive Institute](https://alexandriaarchive.org/) and [Open Context](https://opencontext.org)! This is the adult fiction version of *A Pun Goes Here Part Three: The discussion guide*. This part of the Data Story includes links to the video summaries of each book and then discussion questions, search terms, and related resources to help run your own archaeological data literacy-focused book club and learn more about the archaeology incorporated into each book.

These materials can be integrated into a class for discussion and/or response purposes or used for personal enjoyment to engage more deeply with the materials on your own. If you'd like to read the transcripts for the video summaries and learn about the content warnings for each book, check out the adult fiction version of [Part Two: The book list](https://doi.org/10.6078/M7TH8JVV). Or, if you're curious about how we picked the books in this list, check out [Part One: The book club guide](https://doi.org/10.6078/M7BR8Q9T) or our article on [how to find fun archaeology-related fiction](https://doi.org/10.6078/M7F769PS).

As a supplement to these lists and guides, you can explore the [book data table](https://doi.org/10.6078/M7PC30GS) to cultivate your data literacy skills. This spreadsheet—which includes data about the books in all of our book club Data Stories—allows you to explore the works in a more technical way. Participants can cultivate data literacy by learning how to work with data through sorting, filtering, and visualizing patterns in metadata, such page lengths and publication dates; associated search terms and questions; and creator demographics. Those unfamiliar with working in spreadsheets can use [this tutorial](https://doi.org/10.6078/M7Q81B6V) for guidance. 

# Reading the discussion guide

Part Three focuses on using discussion and light research to expand the data literacy skills introduced by the books in ways that complement Part Two, which focuses on reading to teach archaeological data literacy principles. It does so by providing discussion questions, search terms, and related resources for each book. If you'd like to learn more about how we developed these aspects of the guide, please check out the Data Story Short [*How to Make Your Own (Archaeological) Book Club*](https://doi.org/10.6078/M7K072DH).

![](Images/APGH_BookEntry-Discussion.jpg)

*This is what the book entries in this discussion guide look like. Can you spot the data literacy components we tagged in this entry?*

This part of the Data Story is all about cultivating the ability to analyze, argue, and communicate with data by engaging with the books through discussion, search, and even some research. This means that in this guide we assume familiarity with the books on the list so only provide a link to the video summary. 

*Note: Readers interested in knowing if the books included in this guide are appropriate for their community should watch the book blurb videos or examine the book summaries and content warnings included in [Part Two](https://doi.org/10.6078/M7TH8JVV).*

To read these books, and their entries, to help cultivate archaeological data literacy, you'll read the works themselves to see “what aspects of the world they represent” ([Bhargava et al. 2016:198](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/)) and have the book title and author as data. Like in Part Two they're tagged with the books-on-a-shelf icon.

If you're interested in "working with" data you'll find the planner icon associated with the *Related resources* for each book in this part of the Data Story. While we marked *Content warnings* with that symbol in Part Two, the *Related resources* provide content-specific examples of how others have acquired and managed archaeological data related to the themes of each book.

![](Images/DLPdataliteracycomponents.png)

*Here's [Bhargava et al.’s (2016)](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/) original definition for the components of data literacy. We added icons for each to help locate these components throughout the Data Story.*

If you don't want to rely on the resources we found, you can use the *Search terms—*marked with the laptop-with-list icon—to “analyze” the data. You can use search, on your preferred platform or resource, to define, analyze, and learn more about the archaeological concepts brought up in the books. This process will help you learn more about the book, its archaeological content, and compare it with other works. 

Once you've done some analyzing, forming responses to the *Discussion questions* will help you practice “arguing” with data. Discussing with others, or reflecting on your own responses to the questions, are a chance to transfer what you read into data that can answer a question or solve a problem. We tagged these questions with the squared textbox or callout symbol on a pink background. 

Lastly, you can review our [*Video Book Blurbs*](https://www.youtube.com/playlist?list=PLhjqAN1yrR07iPyOZsxuJUdgJGhUXFa-2) to see how we communicated our thoughts about the books. They can guide how you'll communicate your answers to the discussion questions, summarize your findings from the recommended resources, or state your conclusions about the search terms to your fellow participants or through personal reflection.

We hope that by identifying these parts of *A Pun Goes Here* this Data Story helps you understand how reading and analyzing works of fiction can help you cultivate your own archaeological data literacy.  

## Discussion questions

![](Images/Arguing_Image-01.png)

While all of the books in the adult fiction list are appropriate for adult readers, we supply three kinds of questions appropriate for groups with different levels of familiarity with archaeology. The first are thematic questions. The second are broad questions relevant for all books in the guide. The third are questions tailored to each book on the list. 

These questions allow participants to engage with the data literacy aspects of each novel through discussion, argument, and communication with their community. Organizers can give the questions to participants ahead of time to ponder as they read, save them to use as prompts during the book group’s meeting, or revise them to suit the group’s needs or interests. These questions can also act as an individual reflection exercise for solo-readers. Feel free to make up your own questions and, if you're willing, share them with us so we can expand our question pool! And if you'd like to learn more about how we wrote these questions, check out the Data Story Short [*How to Make Your Own (Archaeological) Book Club*](https://doi.org/10.6078/M7K072DH).

The **thematic questions** encourage people to think about the books in relation to how we grouped them together. These are good ice-breaker questions that introduce archaeology to communities that lack familiarity with the topic. They're also useful for groups familiar with archaeology but who prefer to have discussions guide themselves. These questions work best with the books included in that theme and center on:

+ Can you tell an archaeologist wrote this?
+ How do science fiction or fantasy novels utilize archaeological information?
+ Why might authors use archaeology in mystery novels?

![](Images/APGH_BookCase.png)

The **broad questions** are best suited to communities with some experience with archaeology or data. They also work well for communities that prefer more structure to their discussions or are in the process of learning more about archaeology. These questions can be introduced as follow ups to the thematic questions for appropriate groups. Facilitators can use these for all books included in this list—regardless of theme—and include the questions:

* What was your biggest takeaway from reading this book? What did you like or dislike about it?

* How do you feel the book fits the purpose of the book club or the discussion theme that it’s associated with? What examples would you give to support that conclusion?  
    
* How diverse are the characters referenced within this work? How do those characters reflect different approaches to understanding the major conflicts in the work? (For example, how many pages or chapters does it take for the book to have a woman or gender-minoritized character speak and do those characters have similar or varying perspectives on the world?)   
* How does the background of the author influence how you understand the work? (For example, what do participants think the author looks like? How might that influence the characters, the plot, and the author’s use of archaeology?)  
    
* Storytelling is one of the first ways people interact with data, so how does considering these books as data sources for the places, events, or peoples they encounter—whether entirely fictional or based on facts—influence your understanding of the book? What are some structured observations you can make about the work?  
    
* If we consider these books as data sources, what data analysis approaches would you use to aid (or thwart!) a character in one of these books? What observations or data would you need to make to do this?

The **tailored questions** are best suited to communities familiar with archaeology because they focus explicitly on archaeology and data. We recommend hosts or discussion leaders read through the tailored questions ahead of time to see if they're appropriate for your community and select, modify, or create ones that fit their group. These, alongside the search terms and related resources, are found with each book entry.  

## Search terms

![](Images/Analyzing_image-01.png)

Each entry includes search terms relevant to the content of its book. Participants can use these to get a taste of archaeological research and analysis by finding more information about each term. While the DLP curated this vocabulary, we leave it up to participants to find definitions and more information so they can practice their online and resource search skills. By discovering for themselves what the words mean, finding more information, or locating relevant data, readers can practice their ability to work with archaeological data.

Search terms or phrases *lacking an caret (^)* can be searched on [Open Context](https://opencontext.org/) to find real archaeological data associated with that vocabulary. Participants can explore Open Context on their own or or use an Open Context [keyword search tutorial](https://doi.org/10.6078/M7TX3CHJ) to locate such items of interest. Search terms **with an** **caret (^)** won't have results in Open Context. Instead, participants should search these terms on their preferred platform or within any databases included as related resources for those specific books. Some other resources for finding archaeological information and data are available in [*30 Days to an Article*](https://doi.org/10.6078/M7HH6H6C)*.* 

## Related resources

![](Images/WorkingWith_Image-01.png)

We provide a short list of further reading, information, and related content for the books included in this guide. Each book's list of resources includes at least one additional read related to the book. These may be used to expand the book club beyond the twelve included or act in lieu of the recommended book should that work be difficult to find for your community. In the case of serieses, we included the second book in that series as the primary recommendation. For others, we prioritized books by the same author that had similar content. We recommended books by other authors when the author lacked other books with related content or when an author's works lacked English translations. However, unlike the books in the main list, the DLP has not read all of the works listed as further reading. 

Beyond another book, we include links to resources with information related to the archaeology referenced within the work. These sources are a starting point for further investigation of the book and are not endorsed by the DLP. They include things like interviews with the author of the book; real archaeological data sets or reports; and links to UNESCO sites related to the books. These provide communities who want to learn more with a starting point for their inquiries without sorting through keyword search results. If any of the links for the related resources don't work, check the Wayback Machine for an archived version. Or if you've got additional suggestions please reach out!

# The adult fiction discussion guide

```{r, include = FALSE}
bookData = read.delim("Data/", header=TRUE, sep = ",")

fiction = subset(bookData, bookData$InWhichSeries == "Fiction")
themes = levels(factor(fiction$AssociatedTheme))

reading = "Images/Reading_image-01.png"
working = "Images/WorkingWith_Image-01.png"
analyzing = "Images/Analyzing_image-01.png"
arguing = "Images/Arguing_Image-01.png"
communicating = "Images/Communicating_Image-01.png"
counter = 0

counter = counter+1
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
```

This section of the R markdown version of the text is not an exact replica of the PDF or dynamic document. Instead, this section should be combined with a local CSV version of the book data table (available at https://doi.org/10.6078/M7PC30GS) and run in R. It pulls local data via R to generate the text and images from the table, assuming the related images are available. 

This allows you to modify the table, adding your own books, questions,  videos, or any content you so choose and then create a document with the same elements as the Data Story *A Pun Goes Here*. There are other modifications as well to accommodate for the data pulling elements of this document. For example, the asterisk notice was replaced with a note about a caret (^) because the asterisk is used as part of markdown language. However, we include image credits for each book entry image under *Credits* in order of appearance like in the other documents. 

## Theme `r counter`: `r themes[counter]`
All these books were written by people who were archaeologists at some point in their career. 

### *`r currentTheme$Title[1]`* by `r currentTheme$FirstAuthor[1]`

### *`r currentTheme$Title[2]`* by `r currentTheme$FirstAuthor[2]`

### *`r currentTheme$Title[3]`* by `r currentTheme$FirstAuthor[3]`

### *`r currentTheme$Title[4]`* by `r currentTheme$FirstAuthor[4]`

```{r}
counter = counter+1
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
```

## Theme `r counter`: `r themes[counter]`
Nonarchaeologists incorporated archaeology elements in their world building for these novels. 

### *`r currentTheme$Title[1]`* by `r currentTheme$FirstAuthor[1]`

### *`r currentTheme$Title[2]`* by `r currentTheme$FirstAuthor[2]`

### *`r currentTheme$Title[3]`* by `r currentTheme$FirstAuthor[3]`

### *`r currentTheme$Title[4]`* by `r currentTheme$FirstAuthor[4]`

```{r}
counter = counter+1
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
```

## Theme `r counter`: `r themes[counter]`
Nonarchaeologists used archaeology or archaeological methods to mediate these mysteries.

### *`r currentTheme$Title[1]`* by `r currentTheme$FirstAuthor[1]`

### *`r currentTheme$Title[2]`* by `r currentTheme$FirstAuthor[2]`

### *`r currentTheme$Title[3]`* by `r currentTheme$FirstAuthor[3]`

### *`r currentTheme$Title[4]`* by `r currentTheme$FirstAuthor[4]`

\newpage

```{r}
counter = 1 #reset to pull the entries in the right order
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
bookCounter = 1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage

```{r}
bookCounter = bookCounter+1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage

```{r}
counter = counter+1 #reset to pull the entries in the right order
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
bookCounter = 1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage

```{r}
bookCounter = bookCounter+1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage

```{r}
counter = counter+1 #reset to pull the entries in the right order
currentTheme = subset(fiction, fiction$AssociatedTheme == themes[counter])
bookCounter = 1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage
```{r}
bookCounter = bookCounter+1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage

```{r}
bookCounter = bookCounter+1
```
![](`r reading`)

![](Images/`r currentTheme$BookGraphic[bookCounter]`)

You can find our book blurb at:  [`r currentTheme$AAIReadsVideo[bookCounter]`](`r currentTheme$AAIReadsVideo[bookCounter]`)

### Discussion questions for *`r currentTheme$Title[bookCounter]`* by `r currentTheme$FirstAuthor[bookCounter]` ![](`r arguing`)

`r currentTheme$GuidingQuestion1[bookCounter]`

`r currentTheme$GuidingQuestion2[bookCounter]`

`r currentTheme$GuidingQuestion3[bookCounter]`

`r currentTheme$GuidingQuestion4[bookCounter]`

`r currentTheme$GuidingQuestion5[bookCounter]`

### Search Terms ![](`r analyzing`)

`r currentTheme$SearchTerm1[bookCounter]`, `r currentTheme$SearchTerm2[bookCounter]`, `r currentTheme$SearchTerm3[bookCounter]`, `r currentTheme$SearchTerm4[bookCounter]`, `r currentTheme$SearchTerm5[bookCounter]`

### Further Resources ![](`r working`)

`r currentTheme$Recommendation1[bookCounter]`

`r currentTheme$Recommendation2[bookCounter]`

`r currentTheme$Recommendation3[bookCounter]`

\newpage

# References and further reading

These references are listed in order of appearance in the text for this part of the Data Story or are suggestions for further reading on the topics presented. We do not repeat the suggested links or citations included with each individual book entry. If any links navigate to a broken page, please check the [Wayback Machine](https://web.archive.org/) for an archived copy of the material.

Check out other Digital Data Stories here: [https://doi.org/10.6078/M74F1NW0](https://doi.org/10.6078/M74F1NW0)

Find out about the work of the Alexandria Archive Institute at: [https://alexandriaarchive.org/](https://alexandriaarchive.org/)

Check out Open Context for yourself here: [https://opencontext.org](https://opencontext.org)

See the book list, also known as Part Two, at: [https://doi.org/10.6078/M7TH8JVV](https://doi.org/10.6078/M7TH8JVV)

You can check out *A Pun Goes Here* Part One here: [https://doi.org/10.6078/M7BR8Q9T](https://doi.org/10.6078/M7BR8Q9T) 

Learn how to find fun archaeology related fiction here: [https://doi.org/10.6078/M7F769PS](https://doi.org/10.6078/M7F769PS) 

"*Aggregative Series* book data" by Paulina F. Przystupa and L. Meghan Dennis (2024)  [https://doi.org/10.6078/M7PC30GS](https://doi.org/10.6078/M7PC30GS)

The Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0): [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)

Check out this spreadsheet tutorial if you desire or require help with sorting, filtering, or visualizing data: [https://doi.org/10.6078/M7Q81B6V](https://doi.org/10.6078/M7Q81B6V)

"How to Make Your Own (Archaeological) Book Club" by Paulina F. Przystupa (2025)  
[https://doi.org/10.6078/M7K072DH](https://doi.org/10.6078/M7K072DH) 

"Data Murals: Using the Arts to Build Data Literacy" by Rahul Bhargava, Rahul, Ricardo Kadouaki, Emily Bhargava, Guilherme Castro, and Catherine D'Ignazio (2016)  
[https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/)

Explore our definition of data literacy here: [https://doi.org/10.6078/M73F4MRM](https://doi.org/10.6078/M73F4MRM)

You can check out our video book blurbs here: [https://www.youtube.com/playlist?list=PLhjqAN1yrR07iPyOZsxuJUdgJGhUXFa-2](https://www.youtube.com/playlist?list=PLhjqAN1yrR07iPyOZsxuJUdgJGhUXFa-2)

If you require or desire help with keyword search on Open Context check out this tutorial: [https://doi.org/10.6078/M7TX3CHJ](https://doi.org/10.6078/M7TX3CHJ)

*30 Days to an Article: Archaeological Inspiration for Your Writing* by Paulina F. Przystupa and L. Meghan Dennis (2023)  
[https://doi.org/10.6078/M7HH6H6C](https://doi.org/10.6078/M7HH6H6C)

Gain access to any broken links by getting an archived copy from the Wayback Machine: [https://web.archive.org/](https://web.archive.org/)

# Credits

Unless otherwise specified in captions, or updated when image provenance is determined, this work and its components are shared under a [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license. To attribute this work, please use our suggested citation:

Paulina F. Przystupa and L. Meghan Dennis, 2025, “A Pun Goes Here: The AAI Reads Fiction Book Club”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: [https://doi.org/10.6078/M7PZ56Z5](https://doi.org/10.6078/M7PZ56Z5).

In order of appearance, this Data Story includes:

"DLP Agatha Christie Plaque" by L. Meghan Dennis from the Data Literacy Program (DLP) adapted from (cropped, recolored, resized) "[Dame Agatha Christie 1890-1976 author lived here](https://commons.wikimedia.org/wiki/File:Dame_Agatha_Christie_1890-1976_author_lived_here.jpg)" by Spudgun67 representing their own work via Wikimedia Commons ([https://commons.wikimedia.org/wiki/File:Dame_Agatha_Christie_1890-1976_author_lived_here.jpg](https://commons.wikimedia.org/wiki/File:Dame_Agatha_Christie_1890-1976_author_lived_here.jpg)) under [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) 4.0 International

"APGH_BookEntry-Discussion" by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) which includes "APGH_Sunflowers" by L. Meghan Dennis from the DLP adapted from "Sunflowers" by Vincent Van Gogh, with attribution information for the digitized image of that painting forthcoming; "Reading Icon-Color", "Working with Icon-Color", "Analyzing Icon-Color" and "Arguing Icon-Color" by L. Meghan Dennis / [CC BY](https://creativecommons.org/licenses/by/4.0/) which are adapted from "Books" ([https://thenounproject.com/icon/books-4439922/](https://thenounproject.com/icon/books-4439922/)), "File" ([https://thenounproject.com/icon/file-1331311/](https://thenounproject.com/icon/file-1331311/)), "School laptop" ([https://thenounproject.com/icon/school-laptop-1675142/](https://thenounproject.com/icon/school-laptop-1675142/)) and "Debate" ([https://thenounproject.com/icon/debate-776656/](https://thenounproject.com/icon/debate-776656/)) by Made by Made (Made x Made) from the Noun Project under a royalty free license.

 "DLP data literacy components" by L. Meghan Dennis and Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/), quoted text from Bhargava et al. (2016) and icons previously attributed as well as "Communicating Icon-Color" by L. Meghan Dennis Internet from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) "Internet" ([https://thenounproject.com/icon/internet-1771870/](https://thenounproject.com/icon/internet-1771870/)) by Made by Made (Made x Made) from the Noun Project under a royalty free license.

"dark blue bookcase" by L. Meghan Dennis from the DLP adapted from an Adobe stock image 

“Sunflowers graphic” by Paulina F. Przystupa from the DLP. That image includes "APGH_Sunflowers" see preceding for attribution information. 

 “A Climate of Fear graphic” by Paulina F. Przystupa from the DLP / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) includes an adaptation (cropped, recolored, resized, and reoriented) of “[A Seal at the Glacial Lagoon](https://commons.wikimedia.org/wiki/File:A_Seal_at_the_Glacial_Lagoon.jpg)” by Andrew Bowden from Flickr via Wikimedia Commons ([https://commons.wikimedia.org/wiki/File:A_Seal_at_the_Glacial_Lagoon.jpg](https://commons.wikimedia.org/wiki/File:A_Seal_at_the_Glacial_Lagoon.jpg)) under [CC BY-SA](https://creativecommons.org/licenses/by-sa/2.0/deed.en) 2.0 Generic.

“Prudence graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) includes an adaptation (recolored, resized, reoriented) of “[airship](https://thenounproject.com/icon/airship-1054374)” and an adaptation (recolored, resized, cropped) of “[Cup](https://thenounproject.com/icon/cup-921945/)” both by Alex Muravev from The Noun Project ([https://thenounproject.com/icon/airship-1054374/](https://thenounproject.com/icon/airship-1054374/) and [https://thenounproject.com/icon/cup-921945/](https://thenounproject.com/icon/cup-921945/)) under Royalty-Free Licenses.

“Braving Fate graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) includes an adaptation (cropped, recolored, resized) of “[Boudica statue, Westminster (8433726848)](https://commons.wikimedia.org/wiki/File:Boudica_statue,_Westminster_\(8433726848\).jpg)” by Paul Walter from Flickr via Wikimedia Commons ([https://commons.wikimedia.org/wiki/File:Boudica_statue,_Westminster_(8433726848).jpg](https://commons.wikimedia.org/wiki/File:Boudica_statue,_Westminster_\(8433726848\).jpg)) under CC BY 2.0 Generic.

“A Master of Djinn graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) and includes an adaptation (recolored and resized) of “[Fig. 40 from Africa/Egypt/Cairo/Sabil-kuttab of ’Ali Bey al-Dumyati](https://n2t.net/ark:/28722/k21v5x22g)” by Hans Theunissen from Open Context ([https://n2t.net/ark:/28722/k21v5x22g](https://n2t.net/ark:/28722/k21v5x22g)) under CC BY 4.0.

“Provenance graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/) and includes  an adaptation (recolored) of “[singing bowl](https://thenounproject.com/icon/singing-bowl-3572270/)” by Hey Rabbit from The Noun Project ([https://thenounproject.com/icon/singing-bowl-3572270/](https://thenounproject.com/icon/singing-bowl-3572270/)) under a Royalty Free License.

“Black Sun graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/). That image includes an adaptation (cropped, recolored, resized) of “[Indians and pioneers; an historical reader for the young](https://commons.wikimedia.org/wiki/File:Indians_and_pioneers;_an_historical_reader_for_the_young_\(1897\)_\(14579358589\).jpg)” by Blanche Evans Hazard and Samuel Train Dutton from The Internet Archive Book Images contributed by The Library of Congress ([https://commons.wikimedia.org/wiki/File:Indians_and_pioneers;_an_historical_reader_for_the_young_(1897)_(14579358589).jpg](https://commons.wikimedia.org/wiki/File:Indians_and_pioneers;_an_historical_reader_for_the_young_\(1897\)_\(14579358589\).jpg)) and has no known copyright restrictions.

“The Clockwork Dynasty graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/). That image includes an adaptation (cropped, recolored, resized) of “[Chinese woodcut, Famous medical figures: The Yellow Emperor](https://commons.wikimedia.org/wiki/File:Chinese_woodcut,_Famous_medical_figures;_The_Yellow_Emperor_Wellcome_L0039314.jpg)” by Gan Bozong (Tang period, 618-907) from Wellcome Images, a website operated by Wellcome Trust, a global charitable foundation based in the United Kingdom ([https://commons.wikimedia.org/wiki/File:Chinese_woodcut,_Famous_medical_figures;_The_Yellow_Emperor_Wellcome_L0039314.jpg](https://commons.wikimedia.org/wiki/File:Chinese_woodcut,_Famous_medical_figures;_The_Yellow_Emperor_Wellcome_L0039314.jpg)) under CC BY 4.0 International.

“Artifact graphic” by Paulina F. Przystupa from the DLP / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). That image includes an adaptation (cropped, recolored, resized) of “[Necklace](https://commons.wikimedia.org/wiki/File:Khalili_Collection_Islamic_Art_jly_1261.6.jpg)” by Khalili Collections from Khalili Collections ([https://commons.wikimedia.org/wiki/File:Khalili_Collection_Islamic_Art_jly_1261.6.jpg](https://commons.wikimedia.org/wiki/File:Khalili_Collection_Islamic_Art_jly_1261.6.jpg)) under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/igo/deed.en) IGO.

“The Crossing Places graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/). That image includes an adaptation (cropped, recolored, resized) of “[At Banham, Norfolk](https://commons.wikimedia.org/wiki/File:E.T._Daniell_-_At_Banham,_Norfolk.jpg)” by Edward Thomas Daniell from Norfolk Museums Collections ([https://commons.wikimedia.org/wiki/File:E.T._Daniell_-_At_Banham,_Norfolk.jpg](https://commons.wikimedia.org/wiki/File:E.T._Daniell_-_At_Banham,_Norfolk.jpg)) under Public Domain.

“Murder in Mesopotamia graphic” by Paulina F. Przystupa from the DLP / [CC BY](https://creativecommons.org/licenses/by/4.0/). That image includes an adaptation (cropped, recolored, and resized) of “[The archæology of the cuneiform inscriptions](https://commons.wikimedia.org/wiki/File:The_arch%C3%A6ology_of_the_cuneiform_inscriptions_\(1908\)_\(14780901584\).jpg)” by Archibald Henry Sayce from The Internet Archive ([https://commons.wikimedia.org/wiki/File:The_arch%C3%A6ology_of_the_cuneiform_inscriptions_(1908)_(14780901584).jpg](https://commons.wikimedia.org/wiki/File:The_arch%C3%A6ology_of_the_cuneiform_inscriptions_\(1908\)_\(14780901584\).jpg)) contributed by the University of California LIbraries and has no known copyright restrictions.

“The Golden Hairpin graphic” by Paulina F. Przystupa from the DLP / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). That image includes an adaptation (cropped, recolored, and resized) of a photograph of “[Hairpin](https://commons.wikimedia.org/wiki/File:Hairpin-IMG_0446.JPG)” by an unknown artist, the piece was photographed by Rama who shared and uploaded the work ([https://commons.wikimedia.org/wiki/File:Hairpin-IMG_0446.JPG](https://commons.wikimedia.org/wiki/File:Hairpin-IMG_0446.JPG)) under a [CC BY-SA](https://creativecommons.org/licenses/by-sa/3.0/fr/deed.en) 3.0 France.

All other images included in this text either have their attributions cited in their captions or are logos for our sponsors. We will update this credits section with forthcoming image provenance and attribution information. 

# Acknowledgements

This Data Story is a collaboration between the producers of the Data Story, our testing audiences, AIA staff, and our open peer reviewers. We are grateful to all the work that writers and readers have done to create works with archaeological themes and are indebted to the work of numerous archaeologists, librarians, and archaeology enthusiasts for their continuing contributions to the corpus of archaeology-related fiction and book lists to find more of them. 

The Data Stories are part of the overarching Data Literacy Program, with support from  a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.