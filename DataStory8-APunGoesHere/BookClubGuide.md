---
title: 'A Pun Goes Here: The AAI Reads Fiction Book Club'
subtitle: 'Part One: The book club guide'
author: "Paulina F. Przystupa and L. Meghan Dennis"
date: "`r Sys.Date()`"
output:
  word_document: default
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
#RELEASE DATE: 01/2025
#VERSION: 2.0
```

![](Images/APunGoesHere_Hat.png)

# Book club guide overview

1.  Introduction
2.  Data Story Parts
3.  How do book clubs cultivate archaeological data literacy?
4.  Running your reading group
5.  Next steps
6.  References and further reading
7.  Credits
8.  Acknowledgements

Estimated time to read this guide: 15 minutes

# Introduction

Welcome to the [Digital Data Stories](https://doi.org/10.6078/M74F1NW0)
of the [Alexandria Archive Institute](https://alexandriaarchive.org/)
and [Open Context](https://opencontext.org)! *A Pun Goes Here: The AAI
Reads Fiction Book Club* uses archaeology-related fiction to cultivate
archaeological data literacy through reading and discussion. This Data
Story makes archaeology and data literacy approachable by highlighting
how existing fiction presents and explores archaeology-related topics.

*A Pun Goes Here* provides resources to help you run your own book club, a framework for incorporating fiction into your next class, or can just be used for personal enjoyment. As part of the materials for this Data Story we include: the [Educator's Sheet](https://doi.org/10.6078/M76W9872), this how to guide for the Data Story, a [book list](https://doi.org/10.6078/M7TH8JVV), and a [discussion guide](https://doi.org/10.6078/M7PR7T4J). Each part provides scaffolding to use the Data Story in different ways and we explain *how* in the Data Story Parts section of this guide. We hope this Data Story helps cultivate archaeological data literacy for you and your community.

# Data Story Parts

In this section, we provide more information about the different pieces included in *A Pun Goes Here*. These summaries introduce each part of the Data Story as a unique resource. While we hope participants use these synergistically, we wrote them to be adaptable so that each part can be separated and re-ordered to fit participants' desires and needs. 
## Part One: The book club guide

Part One, this document, introduces the Data Story's goals, explains how the Data Story cultivates archaeological data literacy, and provides guidance for running a book club or facilitating discussion in your community, group, or next class.

This part of the Data Story is great for educators, book club facilitators (like librarians), or members of the public interested in starting their own archaeology-fiction solo or group reading journey. Those who want more help creating an archaeology-related book club for themselves should check out: [*How to Make Your Own (Archaeological) Book Club*](https://doi.org/10.6078/M7K072DH).

## Part Two: The book lists

Part Two, the book lists, are the lists of recommended books compiled for *A Pun Goes Here* by the Data Literacy Program (DLP), aka the authors of this guide. If you're looking for recommended reads in the realm of archaeology-related fiction, go ahead and jump to a [Part Two](https://doi.org/10.6078/M7TH8JVV). 

The book lists include our recommended reads, curated for specific age audiences. We present these lists as a series of book entries and each list has a short explanation for how to read those entries. We do this because each book's entry includes additional DLP-curated information—such as a short summary and content warnings—to help our readers choose good reads for themselves from the list.

Currently, we only offer an adult fiction book list. This list includes books appropriate for 18+ readers or mature teens at their, or their educators', discretion. We hope to expand this Data Story to include lists of archaeology-related fiction for other age-groups in the future. 

## Part Three: The discussion guides

Part Three, the discussion guides, match the books in the accompanying audience-appropriate book list. If you're looking for good questions to ask your group or to help you reflect on archaeology-related fiction, go ahead and jump to a [Part Three](https://doi.org/10.6078/M7PR7T4J).

Organizers can give these guides to participants ahead of time to ponder as they read, save them to use as prompts during the book group’s meeting, or revise them to suit the group’s needs or interests. The discussion guides include thematic questions, broad questions relevant to all the books on the list, and questions tailored to each novel. While the thematic and broad questions can be used by audiences with any level of familiarity with archaeology, the tailored questions are best for archaeology-inclined groups. 

To cultivate other forms of data literacy, the discussion guides also provide recommended search terms and further resources to expand engagement with the books. Check out the next section, Cultivating data literacy beyond the book club, for more information about incorporating those sections into your group's discussion. 

Currently, we only offer a discussion guide to accompany the adult fiction book list. The questions in this discussion guide are appropriate for 18+ readers or mature teens at their, or their educators', discretion. 

![](Images/Analyzing_image-01.png)

## Part Four (optional): Cultivating data literacy skills beyond the book club

Beyond reading and discussion, this Data Story provides a few options for cultivating additional data literacies by exploring the books, and their related archaeological topics, through concrete technical skills. These include learning how to work with and analyze data by searching for real online-accessible materials and exploring real data using digital spreadsheets. 

The [book data table](https://doi.org/10.6078/M7PC30GS)'s search terms are incorporated into the discussion guides and can help participants find more information about archaeology-related topics introduced by the books in the list. To find real archaeological data about those terms, participants can search [Open Context](https://opencontext.org/) without guidance or use an Open Context [keyword search tutorial](https://doi.org/10.6078/M7TX3CHJ) for help. However, search terms with a **caret (^)** won't have results in Open Context, so you'll need to use your preferred search engine instead to learn more about those terms. 

If online searches aren't appropriate for your group, each discussion entry also includes related resources located by the DLP. These include materials like additional reads as well as potential places to learn more about the archaeology related to each book. 

Readers can also use the [book data table](https://doi.org/10.6078/M7PC30GS) to build concrete skills with spreadsheets by examining metadata; associated search terms, questions, and recommended resources; and background information about the creators of the works included in our *Aggregative Series* book clubs. Participants can sort, filter, and visualize patterns in the [book data table](https://doi.org/10.6078/M7PC30GS) without guidance or use [this DLP tutorial](https://doi.org/10.6078/M7Q81B6V) to learn foundational spreadsheet skills.

These additional resources help participants engage with the books recommended in this Data Story in a deeper way. However, the book data table, the book list, and discussion guide were all created by the DLP so if any information is incorrect or out of date, please let us know! 

# How do book clubs cultivate archaeological data literacy?

In the rush for technology-oriented skills, we sometimes forget that words and sentences are structured data too! To remind us of this, and build on existing literacies in our communities, this Data Story cultivates archaeological data literacy through a widely accessible medium, books.

Beyond reading, book clubs cultivate archaeological data literacy by engaging our ability to read critically and then discuss and communicate our findings, which are essential elements of data literacy ([Bhargava et al. 2016](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/)). *A Pun Goes Here* cultivates the *archaeological* aspect by encouraging participants to read novels that engage with archaeological content, topics, periods, skills, methods, or ethical concerns. 

We've marked different sections in each [Part Two](https://doi.org/10.6078/M7TH8JVV) and [Part Three](https://doi.org/10.6078/M7PR7T4J)—which include DLP curated information about the books—with color-coordinated symbols that relate to their associated data literacy components. We use an adapted version of [Bhargava and colleagues (2016)](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/)'s data literacy components in [our definition of data literacy](https://doi.org/10.6078/M73F4MRM).

## The components of data literacy

Bhargava and colleagues (2016) identify four components of data literacy. The first component is the ability to read data. For this Data Story, besides reading the works themselves to see “what aspects of the world it represents” ([Bhargava et al. 2016:198](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/)) participants can identify the book title and author as data. From there, we consider *Content warnings* as “working with” the data, the second component of data literacy. As the DLP team read through the works, and as you will too, you’ll see how those warnings are part of acquiring the data from the book and managing it, particularly for different audiences. Potential readers need to work with those warnings to see if the book is appropriate for them. The DLP tagged most warnings but we incorporated some crowd-sourced tags from [Book Trigger Warnings](https://booktriggerwarnings.com/) or the [Trigger Warning Database](https://triggerwarningdatabase.com/contributors/). See the [book data table](https://doi.org/10.6078/M7PC30GS) for links to those sources for individual books.

We then consider the *Search terms* a way to “analyze” the data, aka the third component of data literacy. The search terms help you see what you can learn about, or from, the book and to compare it with other works. Readers can use the search terms to guide their personal analyses of the book or analyze the book through discussion with their group mates. They can also use those terms to develop technical analysis skills, such as those suggested in the Cultivating data literacy skills beyond the book club section. 

![](Images/DLPdataliteracycomponents.png)
*Here's [Bhargava et al.’s (2016)](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/) original definition for the components of data literacy. We added icons for each to help locate these components throughout the Data Story.*

After this analysis, crafting responses to the discussion questions cultivates the ability to argue, Bhargava and colleagues's last data literacy component. To do this, readers must draw from the book's data, content, and related resources to make a particular point. However, arguing is not the *DLP's* last data literacy component.

As part of our adaptation, the DLP separated “communicating” from arguing because we believe that the skills to make an argument to “support a larger narrative” ([Bhargava et al. 2016](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/)) are distinct from *communicating* that narrative. That's because the goal of *communicating* is to ensure that the audience, aka the other members of your reading group, understand what is being argued! We provide an example of communicating in our [*Video Book Blurbs*](https://www.youtube.com/playlist?list=PLhjqAN1yrR07iPyOZsxuJUdgJGhUXFa-2). Each video communicates our thoughts about the books, including important themes and interesting features. However, if videos aren't your style, you can read through the transcripts instead. Rather than make your own video, each participant in the book club will communicate their findings by seeing how well their fellow participants understand their points and opinions about the books during discussion.

## Why fiction? Why these books?

We decided to focus on fiction for this Data Story because we wanted to highlight how novels have huge potential to help people learn about archaeology. Regardless of whether they portray the past, present, future, or an alternate reality, popular fiction is a great way to demonstrate how books we pick up for fun can help us cultivate our archaeological data literacy. 

However, we know that there are a lot of book lists out there. So, rather than recommend the same novels available on other lists, we focused on ensuring that our lists embodied how diverse archaeological data and knowledge are in fiction. This meant that we took our time putting our lists together, reading each work and considering each potential book against the project goals and values of the DLP.

![](Images/APGH_BookCase.png)

When looking for books to include, we searched for books with examples of archaeology from all over the world; works by authors of color or initially written in a language other than English; novels, when written by white authors, that had main characters of color; novels by women or gender minoritized people; and materials that were still in print and accessible through public libraries, or available for purchase, in diverse formats (physical books, ebooks, and/or audiobooks). All these elements were important because they made the books accessible in the literal sense and accessible as works that collectively include content relevant to diverse experiences and interests. 

These project goals were difficult to meet and search for, which we reflected on in the article [*Demographics and data stories*](https://alexandriaarchive.org/2022/07/08/demographics-and-data-stories/), through your average keyword search. So, in practice, after narrowing this Data Story to its focus on popular fiction, we started by reviewing books that we had read recently. Then, we compared our recent reads against our project values, explored their themes, and used those to help us find additional books. If you want the whole story (and to learn some tips on searching Wikipedia in a more complex way) check out our article on [how to find fun archaeology-related fiction](https://doi.org/10.6078/M7F769PS). It outlines the specific steps we took to find the books included in the adult fiction book list and how we incorporated our project values in this process.  

So, take your time with our recommendations and if you have additional books you think are ethical, diverse, and incorporate archaeology, let us know! We’re always looking for new reads. Or, if you've been inspired by this Data Story to put together your own book list check out the Data Story Short [*How to Make Your Own (Archaeological) Book Club*](https://doi.org/10.6078/M7F769PS) for our recommendations on creating your own book club guide. You can also check out more about our author demographics in the [book data table](https://doi.org/10.6078/M7PC30GS). There we aggregated information about author identity from information findable online, or in some cases included by the authors themselves, so that you can evaluate for yourself how well we met our project values. 

# Running your reading group

There are many reasons to start and ways to run a book club, reading group, or to facilitate discussion. To embrace that diversity, our main recommendation is that you (as the facilitator, or at least the person reading through this guide) are clear about the purpose of the group. Without a clear purpose it's easy for discussion groups—focused on any subject—to get waylaid by unrelated topics or for meetings—such as book clubs or parties—to end up less than fun. By having a clear purpose, all participants know what their role is and everyone knows that they play that role willingly.

The following are resources that can guide your reading group. However, every facilitator and participant should pick from what we present and only use what makes sense for their community. If you've never facilitated a group before, consider reading or reviewing:

- *The Art of Gathering: How We Meet and Why It Matters* by Priya Parker  
- *Liz Lerman's Critical Response Process: A Method for Getting Useful Feedback on Anything You Make, from Dance to Dessert* by Liz Lerman and John Borstel  
- *How to Write a Lot: A Practical Guide to Productive Academic Writing* by Paul J. Silvia  
- *The Anti-racist Writing Workshop: How to Decolonize the Creative Classroom* by Felicia Rose Chavez 

These explore how to define the purpose of your group, introduce ideas about constructive critique to keep discussion respectful, and then guidelines for keeping groups focused on their purposes in different ways. If you want some shorter materials, the tabletop roleplay community has some resources such as:

- *TTRPG Safety Toolkit: A Quick Reference Guide* co-curated by Kienna Shaw and Lauren Bryant-Monk  
- *How to run a Session 0 for D&D and other RPGs* by Maddie Cullen 

These works provide examples of how to organize and facilitate groups of people in different ways. Some focus more on developing a safe and equitable atmosphere, while others focus on how to keep groups of people on topic or adjusting your physical meeting space to align with the appropriate atmospheres for different kinds of gatherings. Depending on the demographics of your group, you may also consider using [Progressive Stack](https://en.wikipedia.org/wiki/Progressive_stack), or encourage participants to read about progressive stack, to ensure that everyone's perspective is heard equitably. 

# Next steps

Now that you've read through our book club guide, we hope you're excited to read some archaeology-related fiction and develop your archaeological data literacy. Whether by exploring the specific parts of this Data Story that fit the goals of you and your community, considering the variety of ways that reading fiction can cultivate archaeological data literacy, or learning new approaches for facilitating group discussion, we hope this guide helps you get the most out of the books we recommend. Thanks so much for using our Digital Data Stories. If you have any feedback or questions don't hesitate to reach out or submit an open peer review. Happy Reading!

# References and further reading

These references are listed in order of appearance in the text for this part of the Data Story or are suggestions for further reading on the topics presented. If any links navigate to a broken page, please check the [Wayback Machine](https://web.archive.org/) for an archived copy of the material.

Check out other Digital Data Stories here: [https://doi.org/10.6078/M74F1NW0](https://doi.org/10.6078/M74F1NW0)

Find out about the work of the Alexandria Archive Institute at: [https://alexandriaarchive.org/](https://alexandriaarchive.org/)

Check out Open Context for yourself here: [https://opencontext.org](https://opencontext.org)

Find the Educator's Sheet for this Data Story at: [https://doi.org/10.6078/M76W9872](https://doi.org/10.6078/M76W9872)

See the book list, also known as Part Two, at: [https://doi.org/10.6078/M7TH8JVV](https://doi.org/10.6078/M7TH8JVV)

You'll find the book list's related discussion guide, aka Part Three, at: [https://doi.org/10.6078/M7PR7T4J](https://doi.org/10.6078/M7PR7T4J)

The Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0): [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)

"How to Make Your Own (Archaeological) Book Club" by Paulina F. Przystupa (2025)  
[https://doi.org/10.6078/M7K072DH](https://doi.org/10.6078/M7K072DH) 

"*Aggregative Series* book data" by Paulina F. Przystupa and L. Meghan Dennis (2025) [https://doi.org/10.6078/M7PC30GS](https://doi.org/10.6078/M7PC30GS)

If you require or desire help with keyword search on Open Context check out this tutorial: [https://doi.org/10.6078/M7TX3CHJ](https://doi.org/10.6078/M7TX3CHJ)

Check out this spreadsheet tutorial if you desire or require help with sorting, filtering, or visualizing data: [https://doi.org/10.6078/M7Q81B6V](https://doi.org/10.6078/M7Q81B6V)

"Data Murals: Using the Arts to Build Data Literacy" by Rahul Bhargava, Rahul, Ricardo Kadouaki, Emily Bhargava, Guilherme Castro, and Catherine D'Ignazio (2016)  
[https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/](https://www.media.mit.edu/publications/data-murals-using-the-arts-to-build-data-literacy/)

Explore our definition of data literacy here: [https://doi.org/10.6078/M73F4MRM](https://doi.org/10.6078/M73F4MRM)

See Book Trigger Warnings for more information about trigger warnings: [https://booktriggerwarnings.com/](https://booktriggerwarnings.com/)

Or you can check the Trigger Warning Database: [https://triggerwarningdatabase.com/contributors/](https://triggerwarningdatabase.com/contributors/) 

Watch our video book blurbs here: [https://www.youtube.com/playlist?list=PLhjqAN1yrR07iPyOZsxuJUdgJGhUXFa-2](https://www.youtube.com/playlist?list=PLhjqAN1yrR07iPyOZsxuJUdgJGhUXFa-2)

"Demographics and data stories" by Paulina F. Przystupa (2022)  
[https://alexandriaarchive.org/2022/07/08/demographics-and-data-stories/](https://alexandriaarchive.org/2022/07/08/demographics-and-data-stories/)

Learn how to find fun archaeology related fiction here: [https://doi.org/10.6078/M7F769PS](https://doi.org/10.6078/M7F769PS)

*The Art of Gathering: How We Meet and Why It Matters* by Priya Parker (2020) 
[https://www.priyaparker.com/book-art-of-gathering](https://www.priyaparker.com/book-art-of-gathering)

*Liz Lerman's Critical Response Process: A Method for Getting Useful Feedback on Anything You Make, from Dance to Dessert* by Liz Lerman and John Borstel (2003)  
[https://www.danceexchange.org/merchandise/p/critical-response-process](https://www.danceexchange.org/merchandise/p/critical-response-process)

*How to Write a Lot: A Practical Guide to Productive Academic Writing* by Paul J. Silvia (2018)  
[https://www.apa.org/pubs/books/4441031](https://www.apa.org/pubs/books/4441031)

*The Anti-racist Writing Workshop: How to Decolonize the Creative Classroom* by Felicia Rose Chavez (2021)  
[https://www.antiracistworkshop.com/](https://www.antiracistworkshop.com/)

You can find more resources from the *TTRPG Safety Toolkit: A Quick Reference Guide* co-curated by Kienna Shaw and Lauren Bryant-Monk with and updates to the quick reference guide available here: [bit.ly/ttrpgsafetytoolkit](http://bit.ly/ttrpgsafetytoolkit)

"How to run a Session 0 for D&D and other RPGs" by Maddie Cullen (2022)   
[https://www.dicebreaker.com/categories/roleplaying-game/how-to/how-to-run-session-0-dnd-rpg](https://www.dicebreaker.com/categories/roleplaying-game/how-to/how-to-run-session-0-dnd-rpg)

Learn about Progressive Stack at: [https://en.wikipedia.org/wiki/Progressive\_stack](https://en.wikipedia.org/wiki/Progressive_stack) 

Gain access to any broken links by getting an archived copy from the Wayback Machine: [https://web.archive.org/](https://web.archive.org/)

# Credits

Unless otherwise specified in captions, or updated when image provenance is determined, this work and its components are shared under a [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license. To attribute this work, please use our suggested citation:

Paulina F. Przystupa and L. Meghan Dennis, 2025, “A Pun Goes Here: The AAI Reads Fiction Book Club”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: [https://doi.org/10.6078/M7PZ56Z5](https://doi.org/10.6078/M7PZ56Z5).

In order of appearance, this Data Story includes:

 "APGH_hat" by L. Meghan Dennis from the Data Literacy Program (DLP), with image attribution information forthcoming.

"Analyzing Icon-Color" by L. Meghan Dennis from the DLP / CC BY adapted from "School laptop" ([https://thenounproject.com/icon/school-laptop-1675142/](https://thenounproject.com/icon/school-laptop-1675142/)) by Made by Made (made x made) from the Noun Project under a royalty free license.

 "DLP data literacy components" by L. Meghan Dennis and Paulina F. Przystupa from the DLP / CC BY, quoted text from Bhargava et al. (2016) and icons are "Reading Icon-Color", "Working with Icon-Color", "Arguing Icon-Color", and "Communicating Icon-Color" by L. Meghan Dennis / CC BY which are adapted from "Books" ([https://thenounproject.com/icon/books-4439922/](https://thenounproject.com/icon/books-4439922/)), "File" ([https://thenounproject.com/icon/file-1331311/](https://thenounproject.com/icon/file-1331311/)), "Debate" ([https://thenounproject.com/icon/debate-776656/](https://thenounproject.com/icon/debate-776656/)), and "Internet" ([https://thenounproject.com/icon/internet-1771870/](https://thenounproject.com/icon/internet-1771870/)) by Made by Made (Made x Made) from the Noun Project under a royalty free license.

"dark blue bookcase" by L. Meghan Dennis from the DLP adapted from an Adobe stock image. 

All other images included in this text either have their attributions cited in their captions or are logos for our sponsors. We will update this credits section with forthcoming image provenance and attribution information. 

# Acknowledgements

This Data Story is a collaboration between the producers of the Data Story, AAI staff, and our open peer reviewers. We are grateful to all the work that writers and readers have done to create works with archaeological themes and are indebted to the work of numerous archaeologists, librarians, and archaeology enthusiasts for their continuing contributions to the corpus of archaeology-related fiction and book lists to find more of them. We'd specifically like to thank Librarian Alenka Figa for their advice regarding library-accessible databases. We're also very grateful for the ongoing crowdsourcing efforts at Book Trigger Warnings and Trigger Warning Database that provided comprehensive warnings for some books and guidance on warnings to add to books that lacked representation on those pages.

The Data Stories are part of the overarching Data Literacy Program, with support from  a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.