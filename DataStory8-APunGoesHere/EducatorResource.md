---
title: "A Pun Goes Here: The AAI Reads Fiction Public Archaeology Book Club"
subtitle: "Educator Resource"
author: "Paulina F. Przystupa - Postdoctoral Researcher in Archaeological Data Literacy"
date: "`r Sys.Date()`"
output:
  word_document: default
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

![](Images/APGH_EducatorHeader.jpg)

# Exercise Description and Aims

The [Digital Data Stories Project](https://doi.org/10.6078/M74F1NW0) promotes an increased focus in archaeological education on digital data literacy. Through the use of fiction and open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis. These practicums illustrate the confluence of science and humanities-based investigations in collective data about the past.

This approach promotes multiple levels of engagement with archaeological data sets, linking data-driven narratives with the key analytical and interpretive steps used to ethically analyze, visualize, and present research data.

![](Images/APGH_wordCloud.png)

In this [Digital Data Story](https://doi.org/10.6078/M7PZ56Z5), participants will learn to:

+ Critically read fiction books
+ Evaluate the use of archaeological knowledge in fiction
+ Locate information about archaeological vocabulary online
+ Develop an understanding of archaeological data
+ Hypothesize about future uses of archaeology in fiction

## additional texts

Myers, Kate (2022) *Excavations: A Novel.* HarperCollins: New York. 

Mickel, Allison (2012) The Novel-ty of Responsible Archaeological Site Reporting: How Writing Fictive Narrative Contributes to Ethical Archaeological Practice. *Public Archaeology* 11(3)-107-122. DOI: [https://doi.org/10.1179/1465518713Z.00000000011](https://doi.org/10.1179/1465518713Z.00000000011)

## the data set

This [Digital Data Story](https://doi.org/10.6078/M7PZ56Z5) directs participants to explore the works recommended in the *Aggregative Series* Book Club Data Stories or to use data from [Open Context](http://opencontext.org), as well as other data publishers or repositories, to explore archaeological vocabulary and information. 

The [Data Literacy Program](https://doi.org/10.6078/M70P0X5P) created the book data set as part of this Data Story. Data sets on Open Context, other data publishers, or online repositories list their primary data creators with each data publication. Participants can use these data sets for other purposes related to the study of archaeology and heritage, such as to locate quality sources of archaeological data, explore author demographics, and learn more about belongings and artifacts from around the world. 

## assessment and scaffolding options

For students who require (or desire\!) a more challenging data exercise, tutorials offered at [GLAM Workbench](https://glam-workbench.net/), the Data Story [*Cow-culating Your Data*](https://doi.org/10.6078/M73N21HR), or its adaptation [*Cow-culating Your Data: The* Of Mycenaean Men *Spreadsheet Tutorial*](https://doi.org/10.6078/M7Q81B6V) can be utilized as scaffolding.

For those who require (or desire\!) more support, the books in the [Digital Data Story](https://doi.org/10.6078/M7PZ56Z5) may also be read in smaller segments and then discussed in pairs to spend more time with the material. In addition participants may use other formats (such as audiobooks) to review material.

As there are opportunities during the [Digital Data Story](https://doi.org/10.6078/M7PZ56Z5)  for students to select data for investigation based on their own interests, the end results produced may vary. One potential assessment option is to ask students to re-attempt the search on a different database.

## Technical Requirements

The recommended books for this [Digital Data Story](https://doi.org/10.6078/M7PZ56Z5) can be found in physical and electronic text formats as well as audio formats. If engaging independently via their own computers or devices, students should know how to: visit a website and access ebooks or audiobooks, if electing to use those formats.

If engaging via lab computers, students should have a basic knowledge of how to operate a computer and how to visit a website. Lab computers should have the following programs available for students:

+ A browser with internet access  
+ A spreadsheet program such as LibreOffice Calc, Google Sheets, or Microsoft Excel  
+ A PDF reader

The website that students will be using for this exercise is available at: [https://opencontext.org/](https://opencontext.org/)

With the optional data set available at: [https://doi.org/10.6078/M7PC30GS](https://doi.org/10.6078/M7PC30GS)

## Duration

The [Digital Data Story](https://doi.org/10.6078/M7PZ56Z5) has three parts meant to be used synergistically: the book club guide, the book blurbs, and the discussion guide. We suggest a month to finish each book with discussion of the books taking place once a month. Discussion should take approximately one hour and can be scaled to the length of your class. 

To cultivate additional literacy skills, or help in preparation for discussion, students can use the search tutorial–included with *Of Mycenaean Men*—to learn about the archaeological vocabulary related to each book. Students may also use the [adapted spreadsheet tutorial](https://doi.org/10.6078/M7Q81B6V)–also included with *Of Mycenaean Men*–to cultivate skills in working with data. Each tutorial takes one hour to complete. If participants complete the tutorials over more than one session, please direct them to pay close attention to the in-text directions on how to save their work and to where they left off in the instructions. 

### get in touch by contacting the team

If you or your students have questions, comments, or concerns, please get in touch. We love feedback, especially from educators!

Paulina F. Przystupa
@punuckish (she/their/none)

L. Meghan Dennis
@archaeoethicist (she/her)

<datastories@opencontext.org>

**Licensing:**  "*A Pun Goes Here* Educators Sheet Text" and "*A Pun Goes Here* Word Cloud" by Paulina F. Przystupa are original works from the Data Literacy Program (DLP) / [CC BY](https://creativecommons.org/licenses/by/4.0/). "A Pun Goes Here Educators Image" by Paulina F. Przystupa from the DLP / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) includes "dark blue bookcase" by L. Meghan Dennis from the DLP / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) adapted from an Adobe stock image and "APunGoesHere_Hat" by  L. Meghan Dennis, with image provenance and attribution information forthcoming. 