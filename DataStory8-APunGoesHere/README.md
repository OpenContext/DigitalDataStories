![](https://alexandriaarchive.org/wp-content/uploads/2024/05/AAI-OC_DataStoryPublished_Header-APunGoesHere.jpg)

*This *Aggregative Series* Data Story is now published!*

This exercise is best suited to those with an interest in reading fiction, understanding fictional uses of archaeological data, or discussing the impacts of archaeological data on fiction novels. Users should have some familiarity with group discussion or book clubs, but previous experience with data or archaeology is not required.

This page provides access to the resource in two ways. The first is through a series of digital documents that represent the complete Data Story. These include the:

1. [Teaching guide](https://doi.org/10.6078/M76W9872)
1. [Book club guide](https://doi.org/10.6078/M7BR8Q9T)
1. [Adult fiction book list](https://doi.org/10.6078/M7TH8JVV)
1. [Adult fiction discussion guide](https://doi.org/10.6078/M7PR7T4J)

A single-PDF for the combined Data Story materials is available [here](https://doi.org/10.6078/M7ZC8109).
This repository is secondary access to the above listed materials. These R markdown files provide the text and primary images from the electronic documents, minus additional formatting, and with only a general placement of images. The R markdown files may be altered and forked from the repository for custom use with the original or different book data sets. To more accurately recreate the dynamic documents for the adult fiction book list and discussion in R markdown, users should download the [book data table](https://doi.org/10.6078/M7PC30GS) alongside the code and images so that the code pulls the correct materials. This code represents our commitment to open science, data literacy, and transparency in our process.

These materials, either through the single-PDF publication of record, dynamic digital documents, or the code, are designed to be used synergistically. However, any piece of this Data Story may be used separately or re-ordered according to the requirements of the user or according to specific educational goals. *A Pun Goes Here* and its resources are free to use under a Creative Commons Attribution-ShareAlike ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)) license unless otherwise noted. In addition, this Data Story references [*Cow-culating Your Data with Spreadsheets and R*](https://doi.org/10.6078/M73N21HR) ([CC BY](https://creativecommons.org/licenses/by/4.0/)), an adaptation to the spreadsheets section of that [tutorial tailored to the book data](https://doi.org/10.6078/M7Q81B6V), and [*Of Mycenaean Men*](https://doi.org/10.6078/M7TX3CHJ) ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)).

Thank you so much for using our educational resources! Although this Data Story is published, if you or your participants have the time, please consider contributing an [open peer review](https://doi.org/10.6078/M72B8W5G) for our ongoing review process. Such updates will be noted below and first available through the digital documents and Codeberg repository prior to PDF updating.

To attribute this Data Story or any of it's parts, please use our suggested citation:
    
    Paulina F. Przystupa and L. Meghan Dennis, 2025, “A Pun Goes Here: The AAI Reads Fiction Book Club”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: https://doi.org/10.6078/M7PZ56Z5.

***Preprint page published:*** *May 2024*

***Page updated:*** *17 January 2025 - This Digital Data Story completed peer review in 2024 and, after revision, is now published. Major updates included the separation of the book club guide into three parts, the guide, the book list, and the discussion guide; the notation of the age group the books are for; acknowledgement of the academic tone in the discussion questions through reorganizing the suggested questions; explaining of the asterisks note more clearly; references to where users can learn more about our recommended books' author demographics; the creation of separate resource on finding books (see* [Finding fun archaeology-related fiction](https://doi.org/10.6078/M7F769PS)*) and making one's own book club guide (see the Data Story Short* [How to Make Your Own (Archaeological) Book Club](https://doi.org/10.6078/M7K072DH)*); reformatting the new texts, and updated or additional images. There was also a textual revision to this page in the body and credits sections. 20 Feburary 2025 - Updates to the funding credits.*

***Image credit:*** *"[A Pun Goes Here Header](https://alexandriaarchive.org/wp-content/uploads/2024/05/AAI-OC_DataStoryPublished_Header-APunGoesHere.jpg)" by Paulina F. Przystupa from the Data Literacy Program (DLP) / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). It includes an adaptation of "APunGoesHere_Hat" by L. Meghan Dennis with attribution information forthcoming and the "AAI reads logo" by Paulina F. Przystupa, which is an adaptation of an Adobe stock image.*

The Data Stories are part of the overarching Data Literacy Program, with support from  a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.