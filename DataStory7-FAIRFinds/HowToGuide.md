---
title: 'FAIR Finds: Using Twine in Archaeological Education'
subtitle: 'Part Two: The How-To Guide'
author: 'L. Meghan Dennis and Paulina F. Przystupa Postdoctoral Researcher for Data Visualization and Reproducibility'
date: "`r Sys.Date()`"
output:
  html_document:
    df_print: paged
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
# RELEASE DATE: 10/2024 
# VERSION: 2.0
# LICENSE: CC BY-SA https://creativecommons.org/licenses/by-sa/4.0/deed.en
```

![](Images/FairFinds_FirstPage.png) 

# **Guide Overview**

1. [Introduction](#introduction)

2. [What is Twine?](#what-is-twine)

3. [How does this relate to data literacy?](#how-does-this-relate-to-data-literacy)

4. [Selecting your data set](#selecting-your-data-set)

5. [The exercise](#the-exercise)

6. [Reflections](#reflections)

7. [References and further reading](#references-and-further-reading)

8. [Credits](#credits)

9. [Acknowledgments](#acknowledgments)

Estimated time to read this guide: 25 minutes

Estimated time to complete the exercise: User determined

# **Introduction** {#introduction}

Welcome to the [Digital Data Stories](https://doi.org/10.6078/M74F1NW0) of the [Alexandria Archive Institute](https://alexandriaarchive.org/) and [Open Context](https://opencontext.org)! This is Part Two of [*FAIR Finds: Using Twine in Archaeological Education*](https://doi.org/10.6078/M7GF0RNT) and focuses on learning to use game design tools, specifically [Twine](https://en.wikipedia.org/wiki/Twine_\(software\)), to create data-driven narratives using archaeological data as an example. This guide introduces narrative concepts, explains what Twine is (and isn’t), connects narrative outreach to digital data literacy in archaeology and beyond, provides guidance for selecting an open data set to work with, and walks through the basics of building a data-driven game using [interactive fiction](https://en.wikipedia.org/wiki/Interactive_fiction) (IF), or text and hypertext-based storytelling. This guide concludes with reflective practices to consider as part of the process of using archaeology and other open data as inspiration.

Your game, or the process of creating one, can be used as an element to integrate into a class exercise, for personal enjoyment, be part of a larger program of outreach, or just help you work through your own data considerations! If you'd like to play the game associated with the examples in this guide, check out [*FAIR Finds*](https://doi.org/10.6078/M7M906SV), or its [walkthrough](https://doi.org/10.6078/M7HD7SSN), and if you want more guidance on incorporating *FAIR Finds* into your pedagogy (or need a handy glossary and key concepts reference) check out [*Part Three: Referencing and Teaching* FAIR Finds](https://doi.org/10.6078/M76Q1VC4).  All of the suggestions and exercises in these guides were created and tested by the Data Literacy Program team (DLP) or are adapted from existing resources for Twine, IF, and digital storytelling.

For this [*Interactive Series*](https://alexandriaarchive.org/2024/02/15/intersecting-interactive-ideas-for-data-literacy/) Data Story, you'll mainly be writing or typing. You *can* include graphics and sound in Twine games, but the strength and foundation of an interactive fiction piece is its writing. To help with this, you'll want to find a few different colored sticky notes and markers; a board or a flat surface to arrange (and rearrange) them on; a notebook to jot thoughts and ideas down in; and a computer where you can download software or a computer with an internet browser, such as Firefox or Opera.

# **What is Twine?** {#what-is-twine}

Twine is one of many [tools](https://www.inklestudios.com/ink/) used in the creation of [IF](https://www.ifarchive.org/). This form of storytelling relies on text over graphics-focused interfaces—creating an easier entry point for designing a game—and for use on multiple platforms. In the case of Twine specifically, the software is [open source](https://opensource.com/resources/what-open-source), free to use, and doesn't require previous experience with coding languages, or high-end computers or software. After you’ve written and built your narrative with Twine, the Twine application turns it into HTML (the basic building blocks of web pages), so that it can be housed on a specific game repository or regular webpage. 

The Twine application (or app) can be downloaded onto your computer, [here](https://github.com/klembot/twinejs/releases), or can be used entirely through your web browser [here](https://twinery.org/2/#/). If you choose to use the browser-only option, pay close attention to the guidance that Twine gives you on how to save your work, and to digital privacy for that work because you aren't saving it locally.

Twine works by writing individual ‘passages’ and then connecting those passages. It may help to think of each passage as a scene, with each scene offering different options for how you respond. When playing the game you choose a response by clicking on a hyperlinked piece of text, just like if you were clicking links on a webpage. It’s a bit like a [Wiki rabbit hole](https://en.wikipedia.org/wiki/Wiki_rabbit_hole) except a purposeful one that follows a story.

For example, in the image below you have a passage. This passage looks like a page from a physical book. At the bottom of the passage, you’ll see several options presented to you as links (the text colored blue). Within the game, clicking on one of these links directs the player to another passage, which shifts the story depending on their choice.

![](Images/FairFinds_PassageExample-01.png)

*This is what one of the passages of* FAIR Finds *looks like in the browser, with key elements pointed out with arrows.*

The interface for inputting text into Twine is divided into two sections. As shown below, the section on the right is a word processor or text editor. Words are typed in just as if they were going into a document, with a few small tweaks (mostly involving punctuation marks) that create different kinds of, or modifications to, the text. In this case, the [[ ]]  marks (double bracket marks) surrounding the blue text automatically create new passages for those choices, and transform the text into links rather than staying unlinked text.

![](Images/FairFinds_FlowChart1-01.png)

*This points out some of the terms we’ll be using later in the guide to identify key elements of storytelling through the Twine application.*

The section on the left of the image is the visual organizer for the passages. You can see how passages "connect" to one another—making the branching narrative—and in what direction the story "flows". You can give each passage a name too, to help you keep track of what’s where in your story and how they connect to each other.

# **How does this relate to data literacy?** {#how-does-this-relate-to-data-literacy}

![](Images/FairFinds_Shovel-01.png)

The ability to synthesize data into different formats, including formats intended for public outreach, is an important part of cultivating data literacy in archaeology and in other fields. This part of *FAIR Finds* explains how to create digitally formatted data-driven narratives that draw on open archaeological data sets (as an example of open data!) that encourage playfulness in engagement. Play is an important pedagogical tool in many fields, appropriate for multiple age groups and learner levels, which allows for questioning, hypotheses testing, and the creation of personal connections with data. And, importantly for us as archeologists, a personal connection to the past through relationships with [material culture](https://doi.org/10.6078/M7639MVB).

While archaeologists (and other scientists) feel comfortable working with spreadsheets, graphs, and data tables, these formats require specialist knowledge and training, often unavailable to the average non-archaeologist or non-scientist. However, this doesn’t mean that the information in those products should stay locked away! Sharing data with non-archaeologist or non-data literate publics is important, and playful engagement, such as through Twine games and narrative broadly, is one way to do it. So when we’re talking about archaeological data literacy (a content-specific type of data literacy), it’s important to remember that this literacy takes many forms, and teaching and learning it can take many forms too! This is where we can shine as archaeologists and story-oriented scientists, providing information and education in ways that the public genuinely enjoys. To do that though, we need good data to work with.

While IF focuses on communicating as its major data literacy component, the ability to communicate through data-driven narratives (archaeological or not) requires reading, working with, analyzing, and arguing with data as well. Therefore, creating an archaeology (or other data) inspired IF narrative can be used to illustrate those skills through playable games and allow learners to practice and cultivate those data literacy skills by building their own.

# **Selecting your data set** {#selecting-your-data-set}

If you’re lucky, you’re reading this and thinking, “Oh, I already have data to work with!” If so, great! Hopefully you’re familiar with your data, and you have it organized in a way that makes it easy for you (and others) to access and reference. If you don’t have a data set to draw on though, don’t despair. There are lots of open data sets available and if you're in the market for an *archaeological* data set, there are ones for many different periods and geographic areas.

When choosing a data set to work with for designing a work of IF, whether archaeological, mineral, or economical, you’ll want to consider whether you have permission to use that data in a transformative work. That’s why using open data (like from [Open Context](https://opencontext.org)) is easiest, the permissions are all laid out for you, often via a [Creative Commons License](https://creativecommons.org/) that the data set’s creator selected for the data they shared.

![](https://artiraq.org/static/opencontext/giza-sphinx/preview/Drawings/d-ss-016.jpg)

*The Sphinx materials are pretty cool but are they appropriate for the audience you're working with? This is "[Drawing d-ss-016 from Africa/Egypt/Giza/Sphinx Amphitheater/Sphinx Ditch/Sphinx Statue](https://n2t.net/ark:/28722/k2x06cb5q)" by Mark Lehner from the* [ARCE Sphinx Project 1979-1983 Archive](https://doi.org/10.6078/M7CZ356B) *via Open Context / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0).*

The next thing you want to think about is whether the data set you’re considering is appropriate for the audience you want to reach or the purpose you want the game to serve. For example, if you want to write a game to teach undergraduates about construction techniques in ancient Egypt, you might look at the [*ARCE Sphinx Project 1979-1983 Archive*](https://doi.org/10.6078/M7CZ356B), which has drawings, photos, maps, and detailed notes.

![](https://artiraq.org/static/opencontext/giza-botany/preview/Giza-Botany-Feature-Summaries-with-URIs.png)

*Plant data—sometimes referred to as archaeobotanical—might be a better fit. This image is "[Giza-Botany-Feature-Summaries-with-URIs](https://artiraq.org/static/opencontext/giza-botany/preview/Giza-Botany-Feature-Summaries-with-URIs.png)" and depicts the first lines of the "[Giza Botany Feature Summaries from Africa/Egypt/Giza/Heit el-Ghurab](https://doi.org/10.6078/M7348HG6)" by Claire Malleson from Open Context / [CC BY](https://creativecommons.org/licenses/by/4.0/).*

But if you want to teach that same group about plants in ancient Egypt (which some folks might know as archaeobotany) because your class includes a mixture of anthropology and biology majors, a better data set to use would be the [*Giza Botanical Database*](https://doi.org/10.6078/M7JH3J99). If you’re feeling ambitious though, you could create a narrative that incorporates data from *both* of these sources, or from other data sets created by similar projects that focus on different things.

The last thing to consider is whether the data set you’re looking at offers you the opportunity to tell a good story. You want your game to be based on quality data, but you also need it to be enjoyable, so folks will feel that suspension of disbelief that comes from a good story.

With those considerations in mind, if you’re looking for a data set associated with an existing Data Story we recommend [*Oracle Bones in East Asia*](https://doi.org/10.6078/M74B2Z7J), which is the data set related to [*It’s All in the Wrist (Bones): Archaeological Data as Artistic Inspiration*](https://doi.org/10.6078/M7N877XP). It’s got a [handy tutorial](https://doi.org/10.6078/M7ZP4478) that teaches you how to search for Data Publications on Open Context. That way if [*Oracle Bones in East Asia*](https://doi.org/10.6078/M74B2Z7J) isn’t for you, you know how to look for other projects. On the other hand, if you don’t have a data set in mind and desire, or require, help keyword searching Open Context we recommend the *[Of Mycenaean Men - The Open Context Search Tutorial](https://doi.org/10.6078/M7TX3CHJ)*. With those two guides helping you find a data set, let’s look at how to start writing in Twine!

# **The exercise** {#the-exercise}

Before beginning this exercise, we recommend that you play through the Twine game associated with it, *FAIR Finds*. It can be found [here](https://doi.org/10.6078/M7M906SV). And if you get confused by any terms or concepts in the game (or walkthrough or this guide) check out [*Part Three: Referencing and Teaching* FAIR Finds](https://doi.org/10.6078/M76Q1VC4) for more context. 

## **Part One: The Paper Prototype** {#part-one:-the-paper-prototype}

For this exercise, we’re first going to make what’s called a [Paper Prototype](https://www.interaction-design.org/literature/topics/paper-prototyping). Prototyping on paper lets you focus on your story flow and developing a narrative structure before inputting your work into a digital format. So get out the sticky notes, markers, and that big flat surface that we mentioned in the [Introduction](#introduction)!

Depending on how complex your story is, you may want to use multiple colors, designs, sizes, shapes, or textures of paper or sticky notes and writing implements in multiple colors, widths, textures, or types. These variations allow you to see connections or similarities and, when arranged on a large flat surface, understand the development of the story together rather than in pieces.

An erasable whiteboard is also great for this, as it gives you the chance to add more notes to yourself as you’re designing. (Note: *Online tools similar to physical whiteboards that allow for screen readers may also be a useful tool when working in groups or as an accessibility tool.)*

![](Images/FairFinds_StickyNote1.png)

When the DLP designed *FAIR Finds*, they used four colors of sticky notes. Each color corresponded to a letter or concept in the FAIR Principles to ensure we incorporated them all. They also used two colors of markers - black and purple to separate other aspects of the story. Black marker text was for the passage name (we’ll explain what that means in Twine in the next section). Purple marker text was for notes about what information that passage needed.

By using sticky notes and markers identifying the necessary components, the DLP could see a whole outline of the game. Then they could rearrange the notes to add additional narrative and change things during the design process because all the information was there on that flat surface. This system allowed the DLP to see how passages connected to each other and explore different ways to connect those materials.

Now, let’s create a sticky note for *your* first two passages! This will be the first text that your player sees, and the first choice that they make. (In *FAIR Finds* there was a bit of a cheat, as the first few passages had blue text links, aka choices, but there was only one option per passage. This was to get the player used to clicking on links to advance the story, before they had to actually make a story-changing choice.)

On your first sticky note, write the name of the passage (what do you want it to be called?), a note about how you’re going to start your data-driven game (do the players wake up at home, arrive at work, open their phone?). On the second sticky note, write the name of the passage, a note about what has to happen or what information needs to be in that passage (what does the player learn, what’s their main problem, why are they where they are right now?). Now, make a *third* sticky note, but make this one a choice point. There should be a passage name, notes to yourself, and two choices for the player to make (what are they going to do about the problem presented, or how will they react, etc.?).

![](Images/FairFinds_StickyNote2.png)

Congratulations (and assuming you're using archaeological data to inspire your narrative) you just began the fabulous life of an [archaeogamer](https://en.wikipedia.org/wiki/Archaeogaming) - an archaeologist who uses games in their practice to question, analyze, and share archaeological data!

Now that you have the start of a game on paper, let’s try putting it in Twine. For the purposes of this exercise, we’re going to assume that you’ve downloaded Twine onto your computer and can access it through the desktop application. 

(Note: *You can do everything we’re doing in the desktop application version of Twine in the browser-based version. You'll just have to think about saving your work more intentionally than when using the desktop version.*)

## **Part Two: The Twine Tale**

Open up your copy of Twine. There should be a bar across the top of the interface that looks something like this:

![](Images/FairFinds_New.png)

*This is the bar you’re looking for to start working in Twine after you’ve downloaded the application to your computer and clicked on the Twine icon.*

(Note: *All the screencaps included in this tutorial are from a Mac OS environment so your specific screen might look a little different. Just focus on the parts within the Twine application rather than the stuff outside it.*)

Go ahead and click on “New”. You’ll be asked to name your story, and after you do that and click “Create” Twine will open up a big blue grid with a square and a tiny green rocketship in the middle of it. This square is your first passage. The rocketship will always show you where your game starts.

![](Images/FairFinds_FirstPassage.jpg)

*This is what it looks like when you create your first passage but haven’t written anything yet!*

Do what the box tells you and double click on it. This will open up the text-processing content box that you use to input all your narrative and design elements. Because you can add things like HTML or CSS (or more advanced coding!) to Twine, this sidebar can be for more than text. It can hold all those extras (like images and sound) that we mentioned earlier, but let’s save those sorts of extras for another Data Story!

![](Images/FairFinds_FirstPassageTyping.jpg)

*This is what an untitled passage-content box looks like once you click on the square labeled*     Untitled Passage*.*

Ok, let’s rename our first passage! You should have the name of this passage on your sticky note. Click on “Rename”, and when you’ve typed in the new name for your passage, click “OK”.

![](Images/FairFinds_FirstPassageTypingTitle.jpg)

*This is what it looks like when you rename an untitled passage to something.*

Now, in that same content box, enter the text for your first passage based on the note you wrote. This doesn’t have to be perfect. It can just be a copy of your general ideas. You can always play with your text and tweak your words later on, just like you were writing in any other format.

Once you’ve done that, at the bottom of that first passage, type in the name of your second passage, and then surround that name with double brackets. Like this, [[Yay]]. Doing this will create a new box in the big blue grid area with a little arrow pointing to it. This creates your second passage and it's connected to your first passage by that arrow!

![](Images/FairFinds_FirstPassageTypingExample.jpg)

*Adding text within double brackets created a new passage that users can navigate to.*

Go ahead and click on that second passage and drag your mouse around. See how the passage moves, but the arrow continues to connect the two passages? Sometimes it’s useful to sort of..."swoosh" or move a few passages off to the side to help you organize your thoughts or focus on one part of the story. You can move these all over and they’ll stay connected, so you don’t lose anything. This aspect in Twine is like that big flat surface we used during the [*Paper  Prototype*](#part-one:-the-paper-prototype) section. 

If you opposite-click (typically right, but it could be left for some folks!), you grab the whole section and can pan across the story. This leaves the passages right where they are so you can go through chunks of the story without moving individual pieces. This can be useful to explore how different passages cluster together throughout the story. 

Now go ahead and do the same thing in your second passage that you did in your first (typing in your notes and ideas), except, don’t rename it. The system already named it for you, based on what you entered as the link to that passage. Once you’ve done that, you should have three passages on your grid.

![](Images/FairFinds_SecondPassage.jpg)

*You can keep adding passages in the same manner by adding double brackets around the particular phrases that identify choices you want the user to make.*

In the third passage, type up the information from your physical note, but this time, make TWO links. Each should be independently surrounded by brackets, like this: [[Proud of]] [[You]].

![](Images/FairFinds_FivePassages.jpg)

*By adding two options (both in double brackets), you created two new additional passages, one for each option.*  

And just like that, you have *five* passages and are on your way to creating an archaeology or other kind of data-driven narrative with choices to engage the player in an interactive story.

If you’re using the desktop version of Twine, you don’t need to worry about saving your game. The system automatically does this for you, so you can just exit the program when you’re done for the day. If you’re using the browser-based version make sure to read about how to save your work [here](https://twinery.org/cookbook/questions/stories_saved.html).

![](Images/FairFinds_Build.jpg)

*To make the game playable for others, even in a testing phase, you'll want to find the "Build" tab in Twine for desktop.*

If you want to share your work-in-progress with someone else, click on “Build” at the top of the interface. This will show you a command below it called, “Publish to File”. Click on this and name your file whatever you want, choose where you want it saved, and click on save. This creates an HTML file of your game, which you can send to anyone to try out. If the person you send it to has a browser, like Firefox or Opera, double-clicking on the file will open your game in that browser, and they can play! It’s as simple as that. If you wrote your whole story in a browser check out this [helpful guide](https://twinery.org/cookbook/questions/stories_saved.html) for how to save it in that format. 

After testing, or before if you want to polish your narrative more than you've done already, you can explore the many ways to tweak and customize your game. You can style your text with rich text or formatting, like making text **bold** or *italic*. You can also change the color of any part of your text or change the color of your links. Or you can alter your links to make them underlined or just [colored text](https://doi.org/10.6078/M76Q1VC4) depending on the style you're going for.

Beyond text formatting, you can also import images or add music or sound clips. Just make sure you have permission to use what you include! Like in *FAIR Finds* the game, you'll need to follow the licensing rules for any media you want to incorporate into your narrative. Or use things (images, music, sounds, etc.) that you made or own! Twine also has a robust user community, and there are lots of resources out there to help you customize the presentation and structure of your narrative as you grow more confident with the format.

# **Reflections** {#reflections}

As you work through the process of creating a data-driven narrative in Twine, it’s important to remember that this is an archaeological and scientific process too! Like any research outcome, take some time to reflect on what you’re putting into the narrative you're creating. The following questions may help you as you write and revise your work.

- What are the main data sources that I used to construct my narrative and what kind of data are they?
    
- Who is my intended audience and does my tone match my intended audience?  
    
- Would I be comfortable including the action or conclusion in this passage in a more “typical” slide presentation at an academic conference?  
    
- Have I upheld my responsibilities to any partners or related parties with how I’ve used, interpreted, or portrayed the data in this narrative?  
    
- Is my work promoting an ethical data practice of respect for stakeholders, partners, colleagues, and communities? If not, is this used to illustrate a particular ethical point?  
    
- Am I having, creating, and encouraging a joyful expression of archaeology or another kind of data in this work?

Take some time to write out answers to these questions periodically as you work through your narrative and at the end of the process. Alternatively, have them posted somewhere so you can reflect on these questions as you create a more reflexive practice for yourself.

You may also use these questions to guide discussion between yourself and others who might be making data-driven narratives to explore your collective progression. Remember that while the format is different, your writing should still be data-driven and your scholarship should still be rigorous! Returning regularly to these questions will help create a reflexive practice for your studies that you can apply anywhere.

# **References and further reading** {#references-and-further-reading}

These references provide guidance on the pieces linked to in this how to guide and are listed in order of appearance in the text. We also include related readings expanding on some of the topics introduced in this section. If any URLs navigate to a broken page please check the [Wayback Machine](https://web.archive.org/) for an archived copy of the material.

The Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0): [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)

Check out our other Digital Data Stories here: [https://doi.org/10.6078/M74F1NW0](https://doi.org/10.6078/M74F1NW0)

Learn about the Alexandria Archive Institute at their website: [https://alexandriaarchive.org/](https://alexandriaarchive.org/)

Find more sources for open archaeological data at Open Context: [https://opencontext.org](https://opencontext.org)

Check out the main page for *FAIR Finds: Using Twine in Archaeological Education*: [https://doi.org/10.6078/M7GF0RNT](https://doi.org/10.6078/M7GF0RNT)

Learn more about Twine: [https://en.wikipedia.org/wiki/Twine\_(software)](https://en.wikipedia.org/wiki/Twine_\(software\))

For a quick summary of what interactive fiction is, check out their Wikipedia page:  [https://en.wikipedia.org/wiki/Interactive\_fiction](https://en.wikipedia.org/wiki/Interactive_fiction)

*FAIR Finds* (the game) by L. Meghan Dennis (2023)  
[https://doi.org/10.6078/M7M906SV](https://doi.org/10.6078/M7M906SV)

Check out *FAIR Finds: Using Twine in Archaeological Education - Part One B: Game Walkthrough* by Paulina F. Przystupa and L. Meghan Dennis (2024)  
[https://doi.org/10.6078/M7HD7SSN](https://doi.org/10.6078/M7HD7SSN)

*FAIR Finds: Using Twine in Archaeological Education - Part Three: Referencing and Teaching FAIR Finds* by Paulina F. Przystupa and L. Meghan Dennis (2024) [https://doi.org/10.6078/M76Q1VC4](https://doi.org/10.6078/M76Q1VC4)

You can read more about our *Interactive Series* Data Stories in *Intersecting Interactive Ideas for Data Literacy* an article on the AAI News: [https://alexandriaarchive.org/2024/02/15/intersecting-interactive-ideas-for-data-literacy/](https://alexandriaarchive.org/2024/02/15/intersecting-interactive-ideas-for-data-literacy/) 

More tools for IF include other interactive fiction programs like "ink: a narrative scripting language for games". Learn about it here: [https://www.inklestudios.com/ink/](https://www.inklestudios.com/ink/)

Find more examples of interactive fiction at the Interactive Fiction Archive: [https://www.ifarchive.org/](https://www.ifarchive.org/)

Learn more about what it means for something to be open source here: [https://opensource.com/resources/what-open-source](https://opensource.com/resources/what-open-source)

You can download the Twine desktop application at this link: [https://github.com/klembot/twinejs/releases](https://github.com/klembot/twinejs/releases) 

If you want more guidance on how to use Twine in your web browser check out: [https://twinery.org/2/\#/](https://twinery.org/2/#/)

To learn more about Wiki rabbit holes check out: [https://en.wikipedia.org/wiki/Wiki\_rabbit\_hole](https://en.wikipedia.org/wiki/Wiki_rabbit_hole)

Learn more about material culture in a Data Literacy Program-related article about it here: [https://doi.org/10.6078/M7639MVB](https://doi.org/10.6078/M7639MVB)

Check out Creative Commons licenses (including access to all linked licenses, their definitions, uses and so much more) here: [https://creativecommons.org/](https://creativecommons.org/)

If you want your own copy of "Drawing d-ss-016 from Africa/Egypt/Giza/Sphinx Amphitheater/Sphinx Ditch/Sphinx Statue" navigate to: [https://n2t.net/ark:/28722/k2x06cb5q](https://n2t.net/ark:/28722/k2x06cb5q)

"ARCE Sphinx Project 1979-1983 Archive" by Mark Lehner (2017)  
[https://doi.org/10.6078/M7CZ356B](https://doi.org/10.6078/M7CZ356B)

If you'd like a copy of the super exciting image "Giza-Botany-Feature-Summaries-with-URIs" you can find one at: [https://artiraq.org/static/opencontext/giza-botany/preview/Giza-Botany-Feature-Summaries-with-URIs.png](https://artiraq.org/static/opencontext/giza-botany/preview/Giza-Botany-Feature-Summaries-with-URIs.png)

On the other hand, if you want the *data* depicted in that image you'll want to navigate to: [https://doi.org/10.6078/M7348HG6](https://doi.org/10.6078/M7348HG6) 

The Creative Commons Attribution License 4.0 (CC BY): [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)   
   
"Giza Botanical Database" by Claire Malleson & Rebekah Miracle (2018) [https://doi.org/10.6078/M7JH3J99](https://doi.org/10.6078/M7JH3J99)

"Oracle Bones in East Asia" by Katherine Brunson, Zhipeng Li, & Rowan Flad (2016)  
[https://doi.org/10.6078/M74B2Z7J](https://doi.org/10.6078/M74B2Z7J)

*It’s All in the Wrist (Bones): Archaeological Data as Artistic Inspiration* by Paulina F. Przystupa and L. Meghan Dennis (2023)  
[https://doi.org/10.6078/M7N877XP](https://doi.org/10.6078/M7N877XP)

*It’s All in the Wrist (Bones): Archaeological Data as Artistic Inspiration - Part Two: The Search Tutorial* by Paulina F. Przystupa (2023)  
[https://doi.org/10.6078/M7ZP4478](https://doi.org/10.6078/M7ZP4478)

*Of Mycenaean Men - The Open Context Search Tutorial* by Paulina F. Przystupa (2023)  
[https://doi.org/10.6078/M7TX3CHJ](https://doi.org/10.6078/M7TX3CHJ)

Learn about the benefits of the paper prototype here: [https://www.interaction-design.org/literature/topics/paper-prototyping](https://www.interaction-design.org/literature/topics/paper-prototyping)

Want to know more about archaeogamers, check out the wikipedia article on them: [https://en.wikipedia.org/wiki/Archaeogaming](https://en.wikipedia.org/wiki/Archaeogaming)

If you want more help figuring out how to save your work in the browser version of Twine explore this link: [https://twinery.org/cookbook/questions/stories\_saved.html](https://twinery.org/cookbook/questions/stories_saved.html)

*The Twine Cookbook* by Dan Cox, Chris Klimas, Thomas Michael Edwards, David Tarrant, Leon Arnott, Shawn Graham, Akjosch, Chapel, G.C. “Grim” Baccaris, Evelyn Mitchell, and James Skemp (2021)  
[https://twinery.org/cookbook/](https://twinery.org/cookbook/)

*Playful Lenses: Using Twine to Facilitate Open Social Scholarship through Game-based Inquiry, Research, and Scholarly Communication* by Rebecca Wilson, Jon Saklofske and The INKE Research Team (2019)   
[https://doi.org/10.5334/kula.11](https://doi.org/10.5334/kula.11)

*Teaching through Play: Using Video Games as a Platform to Teach about the Past* by Krijn Boom, Csilla E. Ariese, Bram van den Hout, Angus Mol, and Aris Politopoulos (2020) [http://dx.doi.org/10.5334/bch.c](http://dx.doi.org/10.5334/bch.c)

Gain access to any broken links by getting an archived copy from the Wayback Machine: [https://web.archive.org/](https://web.archive.org/)

# **Credits** {#credits}

Unless otherwise specified in captions, or updated when image provenance is determined, this work and its components are shared under a [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) 4.0 license. To attribute this work, please use our suggested citation:

L. Meghan Dennis and Paulina F. Przystupa, 2024, “FAIR Finds: Using Twine In Archaeological Education”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: [https://doi.org/10.6078/M7GF0RNT](https://doi.org/10.6078/M7GF0RNT).

For images with other attribution information, in order of appearance, this Data Story includes: "FairFinds\_FirstPage" and "FairFinds\_Shovel-01" by L. Meghan Dennis from the Data Literacy Program (DLP) with attribution information forthcoming. These are followed by "FairFinds\_StickyNote1" and "FairFinds\_StickyNote2" by Paulina F. Przystupa from the DLP, which are adaptations (resized, recolored, with new text) of "DLP Sticky Note" by L. Meghan Dennis from the DLP with attribution information forthcoming. All other images included in this text (such as screencaptures) are licensed alongside the work at large, have their attributions cited in their captions, or are logos for our sponsors.

# **Acknowledgments** {#acknowledgments}

This Data Story is a collaboration between the producers of the Data Story, our testing audiences, AAI staff, and our open peer reviewers. We are grateful for all the work that people in the existing Interactive Fiction and Twine creators community have done to inform our assembly of this exercise. We appreciate the avenues these works provided in encouraging us, and other archaeologists, to experience archaeology playfully while working within a data-driven framework.

The Data Stories are part of the overarching Data Literacy Program, with support from  a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.