![](https://alexandriaarchive.org/wp-content/uploads/2024/04/AAI-OC_DataStoryPublished_Header-FAIRfinds.jpg)

*This* Interactive Series *Data Story is now published*

This exercise is best suited to those with an interest in the archaeology of Spanish Florida; Findable, Accessible, Interoperable, and Reusable (FAIR) data; or using interactive fiction for data-driven education. Users should know how to use the internet, navigate a website, and interact with a PDF document but previous experience with archaeology or any kind of data is not required.

This page provides access to the resource in two ways. The first is through a playable online game and a series of digital documents that represent the complete Data Story. These include the:

1. Game *[FAIR Finds](https://doi.org/10.6078/M7M906SV)*
1. [Teaching Guide](https://doi.org/10.6078/M7N29V32)
1. Optional game [walkthrough](https://doi.org/10.6078/M7HD7SSN) 
1. [How-to guide for making interactive fiction](https://doi.org/10.6078/M7RV0KTM)
1. [Referencing and teaching resource](https://doi.org/10.6078/M76Q1VC4)

The game *FAIR Finds*, which is a piece of interactive fiction, is a Data Story all on its own. The optional game walkthrough explains how to play. The how-to guide explains how we created the *FAIR Finds* game and how to make one's own piece of data-inspired interactive fiction. The referencing and teaching resources acts as a reference guide for important vocabulary and major concepts in the game to help users learn more about the content and better incorporate the Digital Data Story into their pedagogy. A single PDF for the combined textual materials is available [here](https://doi.org/10.6078/M7BP00ZX). 

This repository is secondary access for the code, text, and images within the game, PDFs, and Google Documents, minus additional formatting and with only a general placement of images. This code represents our commitment to open science and transparency in our process.

These materials are designed to be used synergistically. However, any piece of this Data Story may be used separately or re-ordered according to the requirements of the individual or to specific educational goals. *FAIR Finds* and its resources are available free to use under a Creative Commons Attribution-ShareAlike ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)) license. In addition, this Data Story references *[It’s All in the Wrist (Bones): Archaeological Data as Artistic Inspiration](https://doi.org/10.6078/M7N877XP)* ([CC BY-SA](https://creativecommons.org/licenses/by-sa/2.0/)), *[Of Mycenaean Men - The Open Context Search Tutorial](https://doi.org/10.6078/M7TX3CHJ)* ([CC BY](https://creativecommons.org/licenses/by/4.0/)), and *[30 Days to An Article: Archaeological Inspiration For Your Writing](https://doi.org/10.6078/M79Z931X)* ([CC BY](https://creativecommons.org/licenses/by/4.0/)). 

Thank you so much for using our educational resources! Although this Data Story is published, if you or your participants have the time, please consider contributing an [open peer review](https://doi.org/10.6078/M72B8W5G) for our ongoing review process. Such updates will be noted below and first available through the digital documents and Codeberg repository prior to PDF updating.

To attribute this Data Story or any of it's parts, please use our suggested citation:

    Meghan Dennis and Paulina F. Przystupa, 2024, “FAIR Finds: Using Twine In Archaeological Education”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: https://doi.org/10.6078/M7GF0RNT

***First published:*** *April 2024*

***Updated:*** *May 2024 - Changed "PDF updating" to "digital document updating". 30 October 2024 - This Digital Data Story completed peer review in 2024 and, after revision, is now published. Major updates included the addition of the expanded reference and teaching guide, generalization of language to apply to disciplines beyond archaeology, reformatting the new text, and updated or additional images in. There was also a textual revision to this page in the body and credits sections. 5 November 2024 - Added suggested citation.  20 February 2025 - Updated funding acknowledgement.*

***Image Credit:*** *The “[FAIR Finds Header](https://alexandriaarchive.org/wp-content/uploads/2024/04/AAI-OC_DataStoryPublished_Header-FAIRfinds.jpg)” by Paulina F. Przystupa from the Data Literacy Program (DLP) / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). It includes an adaptation of “[FAIRFinds_FirstPage](Images/FAIRFinds_FirstPage.png)” and “[fairfindssplash2](https://archive.org/details/fairfindssplash2)” by L. Meghan Dennis also from the DLP. "[FAIRFinds_FirstPage](Images/FAIRFinds_FirstPage.png)" has additional attribution information forthcoming. "[fairfindssplash2](https://archive.org/details/fairfindssplash2)" is licensed [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) and includes a modified Adobe stock image.*

The Data Stories are part of the overarching Data Literacy Program, with support from  a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.