---
title: "FAIR Finds: Using Twine in Archaeological Education - Educator Resource"
author: "L.  Meghan Dennis, PhD. Postdoctoral Researcher for Data Interpretation and Public Engagement and Paulina F. Przystupa, MA Postdoctoral Research in Archaeological Data Literacy"
date: "29 October 2024"
output:
  html_document: default
  word_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
#RELEASE DATE: 10/2024
#VERSION: 2.0
```

![](Images/AAI-OC_DataStoryPublished_Header-FAIRfinds-Educators.jpg)

# Exercise Description and Aims

The [Digital Data Stories Project](https://doi.org/10.6078/M74F1NW0) promotes an increased focus in archaeological education on digital data literacy. Through the use of open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis. These practicums illustrate the confluence of science and humanities-based investigations in collective data about the past.

This approach promotes multiple levels of engagement with archaeological data sets, linking data-driven textual narratives with the key analytical and interpretive steps used to ethically analyze, visualize, and present research data.

![](Images/FairFinds_WordCloud.png)

In this [Digital Data Story](https://doi.org/10.6078/M7GF0RNT), participants will learn to:

+ Define the FAIR principles
+ Locate sources of open archaeological data sets
+ Determine how FAIR an archaeological data set is
+ Create a data-driven narrative using Twine that teaches a specific archaeological concept
+ Evaluate their own and others’ work through opportunities for written reflection

## additional texts

Hageneuer, Sebastian (2021) Archaeogaming: How Heaven’s Vault Changes the “Game”. In *Pearls, Politics and PistachiosL. Essays in Anthropology and Memories on the Occasion of Susan Pollock’s 65th Birthday*, edited by Herausgeber\*innenkollektiv eds. pp.631-642. *ex oriente,* Berlin. [https://doi.org/10.11588/propylaeum.837.c10771](https://doi.org/10.11588/propylaeum.837.c10771)

Copplestone, T. and D. Dunne (2017) Digital Media, Creativity, Narrative Structure and Heritage, *Internet Archaeology* 44. [https://doi.org/10.11141/ia.44.2](https://doi.org/10.11141/ia.44.2)

## the data set

The Twine game central to this [Digital Data Story](https://doi.org/10.6078/M7GF0RNT) draws on multiple data sets, including the [*Baptizing Spring Zooarchaeological Data*](https://doi.org/10.6078/M7H70CX3)—available at [Open Context](http://opencontext.org)—and the *Baptizing Spring (Mission San Juan de Guacara)* data—available from the [Comparative Mission Archaeology Portal](https://cmap.floridamuseum.ufl.edu/sites/baptizing-spring/) (CMAP). In creating games, however, participants can use any open archaeological data set, based on the desired topical focus.

## assessment and scaffolding options

For those who require (or desire) more support, splitting into pairs to complete the [Digital Data Story](https://doi.org/10.6078/M7GF0RNT) offers the opportunity to work together, switching off tasks and allowing all participants to try their hand at different areas of the game and game design. Resources available via the [Twine Cookbook](https://twinery.org/cookbook/) can also provide increased rigor for game design.

As this [Digital Data Story](https://doi.org/10.6078/M7GF0RNT) relies innately on personalization, the end products will vary. One potential assessment option is to ask students to reflect on the outcome of their playthrough of *FAIR Finds* the game and why they picked their specific ending. Another option is to ask students to playtest one another’s games, while checking off features on a list, to ensure all skill goals are met.

## **Technical Requirements**

Participants should have access to a computer with browser-based internet access for playing the *FAIR Finds* game, reading the other parts of the Data Story (if using the dynamic digital documents), inputting their new game into the Twine engine (if using the browser-based version), and accessing the *Twine Cookbook* for specialized technical examples and common problems. Participants may also need a PDF reader (if using the PDF version of the Data Story), a computer with the Twine app downloaded in areas with inconsistent internet access, and will require sticky notes and writing utensils for pre-planning narrative routes and key moments.

![](Images/FairFinds_Educators_Flowchart.jpg)

## **Duty of Care**

Because of the personal nature of gameplay and creation, it is important to ensure a safe and emotionally secure environment during play and during any debrief or group sharing. Not all students are comfortable sharing their choices, creative endeavors and influences, and facilitators should be mindful of the emotional state and safety of students. Being aware of how students are responding to feedback is key to maintaining the duty of care inherent as an educator.

## **Duration**

The Digital Data Story is variable in duration. Some participants may choose to do the exercise within a compressed window, while others may embrace the creativity inherent in gameplay and design and take longer with some portions of the exercise.

If students are completing their gameplay and game creation over more than one class or meeting session, please ensure they record their game progress or save their work in an offline format. This is why pre-planning for game design with sticky notes helps!

### get in touch by contacting the team

If you or your students have questions, comments, or concerns, please get in touch. We love feedback, especially from educators!

L. Meghan Dennis  
@archaeoethicist (she/her)

Paulina F. Przystupa  
@punuckish (she/their/none)

<datastories@opencontext.org>

**Licensing:**  "FAIR Finds Educators Sheet Text", "FAIR Finds Word Cloud", and "FairFinds_Educators_Flowchart" by L. Meghan Dennis are original works from the Data Literacy Program licensed [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). "FAIR Finds Educators Image" by L. Meghan Dennis from the DLP licensed [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) includes an adaptation (cropped, recolor, resized) of an Adobe stock image.
