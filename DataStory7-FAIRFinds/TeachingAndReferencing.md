---
title: 'FAIR Finds: Using Twine in Archaeological Education'
subtitle: 'Part Three: Teaching and Referencing FAIR Finds'
author: "Paulina F. Przystupa Postdoctoral Researcher in Archaeological Data Literacy"
date: "`r Sys.Date()`"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
# RELEASE DATE: 10/2024 
# VERSION: 1.0
# LICENSE: CC BY-SA https://creativecommons.org/licenses/by-sa/4.0/deed.en
```

![](Images/FairFinds_Teaching_MainImage.JPG)

# **Overview**

1. [Introduction](#introduction)  
2. [Key terms and glossary](#key-terms-and-glossary)  
   1. [Game-related vocabulary](#game-related-vocabulary)  
   2. [Archaeology and technical jargon](#archaeology-and-technical-jargon)  
   3. [Interactive fiction terms](#interactive-fiction-terms)  
3. [Major concepts](#major-concepts)  
   1. [*Baptizing Spring* the archaeological site](#baptizing-spring)  
   2. [The FAIR Principles](#fair-principles)  
   3. [Creative Commons licenses](#creative-commons-licenses)  
4. [Teaching *FAIR Finds*](#teaching-fair-finds)  
   1. [Example Lesson 1: FAIR Principles and archaeological research](#example-lesson-1:-fair-principles-and-archaeological-research)  
   2. [Example Lesson 2: Text-based games for outreach](#example-lesson-2:-text-based-games-for-outreach)  
5. [References and further reading](#references-and-further-reading)  
6. [Credits](#credits)  
7. [Acknowledgements](#acknowledgements)

Estimated time to read this guide (including all definitions and both lesson plans): 1 hour

# **Introduction** {#introduction}

Welcome (back) to the [Digital Data Stories](https://doi.org/10.6078/M74F1NW0) of the [Alexandria Archive Institute](https://alexandriaarchive.org/) (AAI) and [Open Context](https://opencontext.org)!  This is *Part Three: Referencing and Teaching* FAIR Finds where we pull out key terms and concepts from the [game](https://doi.org/10.6078/M7M906SV), [walkthrough](https://doi.org/10.6078/M7HD7SSN), and [how to make your own game guide](https://doi.org/10.6078/M7RV0KTM) for easy reference. This part can help you work through your own data needs and provide guidance on how to incorporate [*FAIR Finds*](https://doi.org/10.6078/M7M906SV) into your pedagogy through a class exercise or as part of a larger program of outreach! 

The main goal of [*FAIR Finds*](https://doi.org/10.6078/M7M906SV) is to use [interactive fiction](https://en.wikipedia.org/wiki/Interactive_fiction) (IF), specifically [Twine](https://en.wikipedia.org/wiki/Twine_\(software\)), to understand Findable, Accessible, Interoperable, and Reusable (FAIR) data and to introduce Creative Commons (CC) licenses for data but it can be overwhelming to remember everything you learned! To help you out, we're providing this reference and teaching guide to further explain all the cool concepts introduced by *FAIR Finds* and outlined in our [*Educator's Sheet*](https://doi.org/10.6078/M7N29V32)*.* That way, if you haven’t memorized [*FAIR Finds*](https://doi.org/10.6078/M7M906SV) on your first play through, you can refer to this document rather than play it all over again. (Though you can play it again, no stress either way!)

This part of [*FAIR Finds: Using Twine in Archaeological Education*](https://doi.org/10.6078/M7GF0RNT) includes a [*Key terms and glossary*](#key-terms-and-glossary) section, which explains important vocabulary. It's separated into vocabulary related to the game or to game creation, words related to archaeology or data, and then important terms related to the creation of IF broadly. Following that, we provide more detail on the archaeological and data literacy concepts introduced *within* the game in the [*Major concepts*](#major-concepts) section.

Specifically, we provide more context for the *Baptizing Spring* archaeological site, the FAIR Principles, and CC licenses. This guide then concludes with the [*Teaching* FAIR Finds](#teaching-fair-finds) section, which provides additional teaching suggestions and two example lesson plans to help incorporate *FAIR Finds* into two different educational scenarios. We hope this helps you get even more out of *FAIR Finds* and if you have any more suggestions don't hesitate to [reach out](mailto:datastories@opencontext.org).

# **Key terms and glossary** {#key-terms-and-glossary}

In this section, we'll focus on defining terms used in the game, walkthrough, and how-to guide for easy reference. You can use this as a reference to provide context for the game for yourself and others or incorporate these terms into your lesson plan as appropriate for the age-group you're using *FAIR Finds* with.

We've pulled some of these definitions from our descriptions of these terms in other parts of this Data Story, so they might sound familiar. Otherwise, we wrote these definitions ourselves and used quotation marks to indicate when we grabbed those definitions word for word from other places.

Alongside each DLP definition, we include links to other resources (outside the AAI ecosystem) for more information about each term. We've separated the words in this glossary based on the content they relate to. We first focus on terms important for gameplay, followed by archaeology or technical terms, and conclude with vocabulary specific to making interactive fiction. 

## **Game-related vocabulary**  {#game-related-vocabulary}

![](Images/FairFinds_controllers-DarkBlue.png)

This part of the glossary includes words related to making or playing a text-based game or work of interactive fiction. If you don't find the word you're looking for here, see if it's listed in [*Archaeology and technical jargon*](#archaeology-and-technical-jargon) or [*Interactive fiction terms*](#interactive-fiction-terms) because these sections do overlap vocabulary wise!   

| Term | Our definition and links to other resources |
| :---- | :---- |
| Double brackets | Two sets, where a set includes a left and right version, of square brackets nested within each other (rather than two consecutive sets of brackets). These are visualized using the square bracket symbols on the keyboard and without text between them double brackets look like this: [[]] and with text they look like [[double brackets]]. For more information on their use in Twine see: [https://tiiny.host/blog/build-text-game-twine/](https://tiiny.host/blog/build-text-game-twine/#:~:text=Use%20the%20%5B%5Bdouble%20square%20bracket%5D%5D%20code%20to%20tell,automatically%20create%20them%20for%20you) |
| Download | Both a verb and noun, this is the process, or product, of retaining a copy of a file by saving it locally to a computer or personal device from an online source. There's more to downloads though and Wikipedia is a great place to start learning about the process and products: [https://en.wikipedia.org/wiki/Download](https://en.wikipedia.org/wiki/Download) |
| HTML/CSS | Mentioned in other sections of this Data Story as "the basic building blocks of web pages" or "more advanced coding!" HTML and CSS are two different coding approaches or languages that are used to represent content on a webpage. Specifically, they can host the content and alter its arrangement or appearance in that digital format. HTML stands for Hypertext Markup Language and CSS refers to Cascading Style Sheet. They are both super powerful so, if you're interested, you can learn more about them at the links below: [https://en.wikipedia.org/wiki/HTML](https://en.wikipedia.org/wiki/HTML); [https://en.wikipedia.org/wiki/CSS](https://en.wikipedia.org/wiki/CSS)  |
| Hyperlink or link | A digital connection between two digital places where a click will lead you to a new digital place. They are usually identified by a change in color or formatting on a word, letter, symbol, or image. This short article from the University of Washington explains how links compare to buttons in regards to navigation and accessibility on their websites: [https://www.washington.edu/accesstech/websites/links-buttons](https://www.washington.edu/accesstech/websites/links-buttons/#:~:text=In%20HTML%2C%20links%20and%20buttons,video%2C%20or%20submitting%20a%20form) |
| Interactive fiction (IF) | The use of software to allow players to execute typed or text commands that alter the choices made within a digital narrative, either to characters or other features within a story. Similar to works—digital and physical—where you can select how the story goes by picking different options. Unique to this format is that the user is typing or clicking or using other interfaces mediated through technology as the mechanism to advance or make selections. You can learn more about IF, check out a different platform for making it, and play through other folks' works at the sites below: [https://en.wikipedia.org/wiki/Interactive_fiction](https://en.wikipedia.org/wiki/Interactive_fiction); [https://www.inklestudios.com/ink/](https://www.inklestudios.com/ink/); [https://www.ifarchive.org/](https://www.ifarchive.org/) |
| Interface | In relation to *FAIR Finds,* an interface is an interactive method for navigating digital content, often assumed to be visual but there are other ways for this to occur, including verbal commands using assistive technologies such as screen readers. You can learn more about how interface design works from Tokio School: [https://www.tokioschool.com/en/news/game-interface-design/](https://www.tokioschool.com/en/news/game-interface-design/) |
| Level | A subsection of a game that focuses on a particular goal, often associated with tasks related to a particular place or segment of the story that requires completion before advancement of the overall objective. Wikipedia has more information about how other folks define video game levels so check out their article about them here: [https://en.wikipedia.org/wiki/Level_(video_games)](https://en.wikipedia.org/wiki/Level_\(video_games\)) |
| Loop | An element within a game where a user keeps making the same, or occasionally different, choices that result in the same recurring actions or outcomes within the narrative. This can be accidental, when you don't realize a specific choice leads to the same endpoint, or intentional when you repeat a behavior. These are similar to loops in coding, where one cycles through the same action and over. You can learn about coding loops at the reference below: [https://en.wikipedia.org/wiki/Control_flow\#Loops](https://en.wikipedia.org/wiki/Control_flow#Loops) |
| Rich text / Formatting  | These alter the appearance of the content in a digital environment. Rich text (a more proprietary name) or formatting (the more generic) are features like making text look **bold**, *italic,* or underlined. They also include the ability to change the color of any part of your text or change the color and appearance of your links. For example, making hyperlinks underlined with a new color or just colored text, like the links we include alongside these definitions! Formatting is super powerful and Wikipedia can help you learn more about it: [https://en.wikipedia.org/wiki/Formatted_text](https://en.wikipedia.org/wiki/Formatted_text) |
| Open source | Open source refers to software, typically, that have transparent creations (such as code) supporting their functionality and require no cost to use. You can explore definitions of open source from this…source: [https://opensource.com/resources/what-open-source](https://opensource.com/resources/what-open-source) |
| Text-based game | A game whose primary—but not necessarily exclusive—mode of storytelling and narrative is written language or text. These can be digital or physical games, though in relation to *FAIR Finds* it refers to digitally-hosted versions of those text games. You can get more information about them here: [https://en.wikipedia.org/wiki/Text-based_game](https://en.wikipedia.org/wiki/Text-based_game) |
| Web browser | An interface used to access the internet such as Firefox, Opera, or Edge. These have developed a lot over the years so learn more about their history and use at Wikipedia: [https://en.wikipedia.org/wiki/Web_browser](https://en.wikipedia.org/wiki/Web_browser) |

## **Archaeology and technical jargon** {#archaeology-and-technical-jargon}

In this section, we provide more DLP-written definitions, this time for archaeology and technical jargon. For most definitions, we include links to sources outside the DLP/AAI ecosystem that include more information about the concept. In a few cases though, we include links to DLP or AAI content that explores the concept in more depth. (Note: *As in the [Game-related vocabulary](https://docs.google.com/document/d/1jh5KPMMksEGnr13itmHNZ1M1doJL-UXlB6JtIOt5Mc0/edit?pli=1#heading=h.bmo04pg51o0y) section, you might notice some phrases that echo things said in other parts of this Data Story.*)

![](Images/FairFinds_Shovel-DarkBlue.png)

They may also include links to the [*Major concepts*](#major-concepts) section for topics, like the FAIR Principles. That section provides even more context for the main themes and content discussed through *FAIR Finds*. Some other great sources for archaeology-related vocabulary include the New York City Archaeological Repository's [glossary](https://archaeology.cityofnewyork.us/education/glossary), Parks Canada's [archaeological glossary](https://parks.canada.ca/culture/arch/page2/doc2), and the Archaeology Data Service's [glossary](https://archaeologydataservice.ac.uk/help-guidance/glossary/) if you don't quite find what you're looking for in this guide.

| Term | Our definition and links to other resources |
| :---- | :---- |
| Archaeobotanical or archaeobotany or plant data | The archaeological investigation of plants or vegetation related to human experiences of the past. It includes both macro, micro, and fossilized remains in certain cases. Sometimes just called "plant data" colloquially amongst archaeologists. However, folks who work with that kind of data often discuss and evaluate whether archaeobotany is the same as paleoethnobotany. You can see some of that discussion at the site below: [https://habitsofatravellingarchaeologist.com/archaeobotany-vs-paleoethnobotany-vs-paleobotany](https://habitsofatravellingarchaeologist.com/archaeobotany-vs-paleoethnobotany-vs-paleobotany/#:~:text=For%20those%20who%20differentiate%20the,relationship%20between%20people%20and%20plants) |
| Archaeogamer  | These are archaeologists who use games in their practice of the discipline to question, analyze, and share archaeological data. It's become so popular in the 21st century that it has its own Wikipedia page, check out more information at: [https://en.wikipedia.org/wiki/Archaeogaming](https://en.wikipedia.org/wiki/Archaeogaming) |
| Archaeological report   | A professional technical report written by an archaeologist or archaeology-adjacent professional that explains the outcomes of an archaeological investigation. These can be written by academic, government, and private sector archaeologists or archaeology-adjacent professionals. This helpful link explains different kinds of archaeological reporting in more detail: [https://www.phillyarchaeology.net/research/archaeological-research/types-of-reports/](https://www.phillyarchaeology.net/research/archaeological-research/types-of-reports/) |
| Attributes | These are standardized features noted about a particular subject of observation that often get captured as separate headings in a spreadsheet. Wikipedia gives some more examples of this in context as well as alternate words we use to also mean attribute: [https://en.wikipedia.org/wiki/Variable_and_attribute_(research)](https://en.wikipedia.org/wiki/Variable_and_attribute_\(research\)) |
| *Baptizing Spring*  | The name of the archaeological site that provided comparative data for the fictional employee in the *FAIR Finds* game. While only one site, the information created from the evidence at the site exists in multiple places. Check out the [*Major concepts*](#major-concepts) section for more about the site and the [references](#references-and-further-reading) list for related links to archaeological investigations of the mission site. |
| CC BY-ND | This is a specific Creative Commons license that has limited reuse capabilities because of its necessity to use the work in totality. The CC part stands for Creative Commons, which is the entity that defined the license. The BY indicates that one needs to attribute the work to its original creator or owner by including who the original work was "by". This can also include stating whether changes were made to the original media before sharing and redistribution in addition to listing the author/owner in a particular way. Lastly, the ND means no derivatives or “[i]f you remix, transform, or build upon the material, you may not distribute the modified material” ([Creative Commons 2024](https://creativecommons.org/licenses/by-sa/3.0/deed.en)). You can find that quote and read the details of the whole license at: [https://creativecommons.org/licenses/by-sa/3.0/deed.en](https://creativecommons.org/licenses/by-sa/3.0/deed.en) |
| Ceramic data | Data from the archaeological record that focuses on pottery, ceramic, porcelain, and similar objects made from clay, water, and inclusions. You can learn more about how archaeologists use ceramic data at the website below:  [https://habitsofatravellingarchaeologist.com/what-do-archaeologists-love-ceramics/](https://habitsofatravellingarchaeologist.com/what-do-archaeologists-love-ceramics/) |
| Citations | Are how we trace particular ideas, data, quotes, images, and other creations back to an original source, often using names, dates, and titles. They usually have a formal structure that standardizes what and how that information is included in a citation. The publisher of the work defines their structure and the included information. Citations allow us to give credit to the originators of ideas and to trace the evolution of a particular concept or idea. Wikipedia has a pretty thorough explanation of them if you'd like to learn more: [https://en.wikipedia.org/wiki/Citation](https://en.wikipedia.org/wiki/Citation) |
| Comparative Mission Archaeology Portal (CMAP) | Is an online source for real archaeological data related to mission sites that aims to compare, or provide users the ability to compare, the materials collected at sites whose collections are housed at various repositories. It is the online location where, in the *FAIR Finds* game, you found additional data about *Baptizing Spring* that you considered incorporating into your report*.* You can check out more about them on their website: [https://cmap.floridamuseum.ufl.edu/](https://cmap.floridamuseum.ufl.edu/) |
| Compliance or private archaeology (sometimes called Cultural Resource Management or CRM) | A type of archaeology conducted by professional archaeologists that allow certain projects—often related to government-funded works or infrastructure—to comply, or align their outcomes with, the laws and regulations regarding archaeology or cultural heritage in a particular area. Wikipedia is a pretty good source for learning a more about this type of archaeology:  [https://en.wikipedia.org/wiki/Cultural_resource_management](https://en.wikipedia.org/wiki/Cultural_resource_management) |
| Data / data set | Data are structured observations made by a person of a particular set of things. A data set is a comprehensive series of data. Data and data sets are created by people, as the entities making those observations. In the DLP video about the book *Living in Data* ([Thorp 2021](https://us.macmillan.com/books/9780374720513/livingindata)), we talk a lot about different kinds of, and approaches to, data: [https://doi.org/10.6078/M7GQ6VW7](https://doi.org/10.6078/M7GQ6VW7) |
| Data usage license/ Data license | A legal outline and statement of how people who are not the data creator or owner may use the materials related to certain data. Many coding repositories include a list of potential licenses to select from before creating a repository or folder for any code you share. You can learn more about a few well-known data licenses and their use at the websites below: [https://opensource.org/license/mit](https://opensource.org/license/mit); [https://howtofair.dk/how-to-fair/data-licences/](https://howtofair.dk/how-to-fair/data-licences/) |
| Digital privacy | Concerns over the ability to control information about oneself, one's actions, or one's dependents online and/or in digital spheres that are upheld by legal and ethical practices and rights within specific nations or communities. Digital privacy is super important and Wikipedia's article on it provides a great start to learning more: [https://en.wikipedia.org/wiki/Digital_privacy](https://en.wikipedia.org/wiki/Digital_privacy) |
| Documentation | This refers to information, such as metadata (defined below), that describe how a particular data set, collection, or assemblage came into being or came into relation with each other. Such documentation comes in the form of verbal descriptions or reports, sometimes called paradata, that give the story of the creation of the documented subject. They can also include explanations for how to use something or how it has been used to aid interested parties in reuse of those materials. The following website provides some examples about documentation in data-focused contexts: [https://howtofair.dk/how-to-fair/documentation/](https://howtofair.dk/how-to-fair/documentation/) |
| FAIR data / FAIR Principles  | A series of principles that outline best practices for data creation and sharing within the mindset of reuse and synthesis by various communities beyond the original data creator or owner. See the [*Major concepts*](#major-concepts) section for an expanded explanation and contextualization of the principles or check out the following websites for more context outside of *FAIR Finds*:  [https://www.go-fair.org/](https://www.go-fair.org/), [https://www.go-fair.org/fair-principles](https://www.go-fair.org/fair-principles/) or [https://eos-rcn.github.io/web/](https://eos-rcn.github.io/web/) |
| Grey lit or grey literature    | This kind of literature typically consists of unpublished or not-traditionally-published archaeology and adjacent-discipline technical reports produced by private archaeology companies, the government, or other entities who conducted legally-mandated compliance archaeology or related work. These publications are considered grey or gray because of their difficulty in accessing the information within them when outside those professional contexts. This article from *Internet Archaeology* provides more context for grey literature in England: [https://intarch.ac.uk/journal/issue40/6/toc.html](https://intarch.ac.uk/journal/issue40/6/toc.html)   |
| Hypothesis testing | The evaluation of an "if this, then this" statement that can be disproved to reach a conclusion about a particular question. Hypotheses are often a lot more complicated than that though, so we'll point you towards some Wikipedia articles that explore the concepts in more detail: [https://en.wikipedia.org/wiki/Hypothesis](https://en.wikipedia.org/wiki/Hypothesis); [https://en.wikipedia.org/wiki/Statistical_hypothesis_test](https://en.wikipedia.org/wiki/Statistical_hypothesis_test) |
| License | Within the context of *FAIR Finds,* a license is a legal permission that governs the reuse, remixing, and amount of attribution required for a particular material's use by a non-owner of that material. Licenses are an important part of data literacy in any field so the DLP wrote their own article about licenses that provides more detail: [https://alexandriaarchive.org/2024/07/05/introduction-to-licenses-and-copyright/](https://alexandriaarchive.org/2024/07/05/introduction-to-licenses-and-copyright/) |
| Lithic data | Systematic observations about rocks whose context and/or alteration support their use by humans in some capacity. Often, lithic refers to chipped-stone tools but can also include fire modified rock or rocks used in other, non-chipped, fashions. If this rocked your socks, Wikipedia's entry about them might boulder you over: [https://en.wikipedia.org/wiki/Lithic_analysis](https://en.wikipedia.org/wiki/Lithic_analysis) |
| Metadata | Metadata are data, descriptive information, or structured observations about other data. They provide context and provenance for the data themselves. Metadata are also an important element of the FAIR Principles framework so we provide more explanation for them within that section of [*Major concepts*](#major-concepts). You can also learn more about their value in this article from *Internet Archaeology:*  [https://intarch.ac.uk/journal/issue2/wise/toc.html](https://intarch.ac.uk/journal/issue2/wise/toc.html) |
| Material culture | The physical endurances of human experiences of the past that modern people may collect to observe and create data from or about ([Hicks 2020](https://www.plutobooks.com/9780745341767/the-brutish-museums/)). The DLP has also explored the use of this term (and it's variants) in more detail for those who want to evaluate if "material culture" is the best word to use in their archaeological practice:  [https://doi.org/10.6078/M7639MVB](https://doi.org/10.6078/M7639MVB) |
| Open Context | Open Context is a free, open access resource for the Web publication of primary field research from archaeology and related disciplines. You can check the project out yourself to learn more at: [https://opencontext.org](https://opencontext.org) |
| Open data sets | Data sets whose licensing or permissions align with definitions of "open access"—or other understandings of "open"—that include the ability to use the data without direct involvement of the authors (as a contrast to "data available upon request") and with diverse reuse allowances. You can learn more about what "open" means in archaeology in this article from *The SAA Archaeological Record,* which includes some authors that might sound familiar:  [http://onlinedigeditions.com/publication/?m=16146\&i=440506\&p=10\&ver=html5](http://onlinedigeditions.com/publication/?m=16146&i=440506&p=10&ver=html5) |
| Permissions | Permissions outline what a non-owner of an archaeological data set (or other material type) is allowed to do with, or if they are allowed to access, the data set or material. These can be requested from the owner or controller, outlined through a license (such as the CC BY-ND), or be established through legal concepts such as copyright or public domain designations. You can learn more about the philosophy behind permissions at Wikipedia: [https://en.wikipedia.org/wiki/Permission_(philosophy)](https://en.wikipedia.org/wiki/Permission_\(philosophy\)) |
| Post ex / post-ex / Post excavation / Post-excavation | Post ex or post-ex is shorthand for post excavation (or post-excavation). These are all the archaeology tasks that take place *after excavation* has been done, although some of these tasks might be started during excavations (when "excavation" refers to a large ongoing project). Typically though, post ex activities continue long after the excavation part of the season or project is complete. These include turning physical paper records into manipulatable spreadsheets; cataloging artifacts and sending them off to specialists; and preparing reports of different kinds about the project. You can learn more about how one site approaches post-ex at the link below: [https://www.mustfarm.com/post-dig/post-ex-diary-2-what-is-post-excavation/](https://www.mustfarm.com/post-dig/post-ex-diary-2-what-is-post-excavation/) |
| PO / Project Officer | A PO, which stands for project officer, is a type of supervisor on an archaeological project. They manage the running of archaeological projects at different scales depending on the structure of the company and where in the world they work. You can check out the tasks a PO should do at the following job listing: [https://www.archaeologists.net/jis/jobs/archaeological-project-officer-1718285203](https://www.archaeologists.net/jis/jobs/archaeological-project-officer-1718285203) |
| Provenance | Provenance, not to be mistaken with the archaeological term provenience, is the documentation of the owners of a particular piece (like a painting) and how they came to own it. Provenance is usually used to establish authenticity or legal ownership. For data, this is the ability to connect from the person *currently using* the data all the way back to the person who originally created and/or owned the data. Another way to think about it is documenting the stewardship, development, ownership, and creation of data or a data set that is available in the present, or that existed at some point, back to its origination. You can find more detail about this concept and its practice at Wikipedia: [https://en.wikipedia.org/wiki/Provenance](https://en.wikipedia.org/wiki/Provenance) |
| Shovel Test | Also referred to as a shovel probe or a shovel test pit, this is when archaeologists use a shovel to dig a deep (often about three feet or one meter) but narrow (1 ft or less in diameter) hole in the ground to see if there are any subsurface archaeological materials. These are usually dug in a series of different places within a defined area to assess location of, test for depth, and define the extent of archaeological deposits. Shovel tests are so common they've got their own Wikipedia entry: [https://en.wikipedia.org/wiki/Shovel_test_pit](https://en.wikipedia.org/wiki/Shovel_test_pit) |
| Spreadsheets | A particular data structure that organizes data into standardized rows and columns to allow for systematic notation. These are most often digital but the term comes from a physical predecessor in ledger books. You can learn more about these beloved and dreaded data structures on Wikipedia:  [https://en.wikipedia.org/wiki/Spreadsheet\#History](https://en.wikipedia.org/wiki/Spreadsheet#History) |
| Standard vocabulary / Controlled vocabulary  | Standard or controlled vocabularies involve words that have the same meaning across different data sets and are structured, available, shared, and widely used together by the community who agreed on their usages and definitions. Such vocabularies are how we ensure that different data sets are talking about the same kinds of content. The words used in those contexts are also controlled in that there is a set list of them that people draw from that standardize things like spelling, capitalization, and punctuation. ARIADNEplus provides some great references to learn more about these vocabularies in context:  [https://ariadne-infrastructure.eu/producing-metadata-for-archaeological-datasets/](https://ariadne-infrastructure.eu/producing-metadata-for-archaeological-datasets/) |
| Zooarchaeological data | Zooarchaeological—or faunal—data are structured observations (data) about the remains of animals (fauna) at a site. You might also see this kind of analysis listed as archaeozoology but that's an argument for those subspecialists. If you're interested in this type of data, Wikipedia isn't a bad place to start learning more:  [https://en.wikipedia.org/wiki/Zooarchaeology](https://en.wikipedia.org/wiki/Zooarchaeology) |

## **Interactive fiction terms** {#interactive-fiction-terms}

![](Images/FairFinds_StickyNote4.png)

These terms are all about interactive fiction, the medium we used for the main part of this Data Story. This section of the glossary focuses on words necessary to understand how to think about and make interactive fiction.

Like in the preceding sections, some text in the explanations, definitions, or descriptions reflect how we explored this vocabulary in other parts of *FAIR Finds*. Where those weren't enough, we've added our own definitions and context. For folks who want to learn even more, we've included links that go beyond this Data Story to provide further context or additional examples of these terms out in the wild. 

| Term | Our definition and links to other resources |
| :---- | :---- |
| Audience | The people you are making a game, or another communicative piece, for. These are the groups of people (age groups, classes, communities, etc.) that you intend to be users, participants, or broadly "engagers" with your creation. People have a spent a lot of time exploring what audiences are, so learn more at Wikipedia:   [https://en.wikipedia.org/wiki/Audience](https://en.wikipedia.org/wiki/Audience) |
| Choice point  | This is a point within a playable narrative where the participant *chooses* from defined options to construct a certain path for themselves through that narrative. You'll make one in Part Two by creating multiple options that a participant can choose from within a single Twine passage. Or, you might have some options for these noted in your paper prototype. These choices reflect how the playable character could react to the presented problem at that moment in the story. GamersLearn wrote an interesting article exploring the use of choice in video games: [https://www.gamerslearn.com/design/autonomy-and-choice-in-video-games](https://www.gamerslearn.com/design/autonomy-and-choice-in-video-games) |
| Game | A playable creative work that involves interaction within a set of parameters. In the case of *FAIR Finds*, we're focusing on playable media in digital contexts through a choice-and-links-based system. Games vary widely and Wikipedia can give you an introduction to that variation: [https://en.wikipedia.org/wiki/Game](https://en.wikipedia.org/wiki/Game) |
| Link | Similar to "hyperlinks or link" in [*Archaeology and technical jargon*](#archaeology-and-technical-jargon), except these are links specifically within an interactive fiction context used to advance the story. In *FAIR Finds*, those were the words with light blue color formatting. These provide users options for navigating to the subsequent sections of the game. They're created through the use of double brackets to move from one passage to another. You can learn more about creating links in Twine from the *Twine Cookbook*: [https://twinery.org/cookbook/starting/twine2/creatinglinks.html](https://twinery.org/cookbook/starting/twine2/creatinglinks.html) |
| Interactive Fiction | In another part of this Data Story we introduced interactive fiction as "text and hypertext-based storytelling" that "relies on text over graphics-focused interfaces—creating an easier entry point for designing a game—and for use on multiple platforms". This really sums it up nicely but you can find more information and some examples at: [https://en.wikipedia.org/wiki/Interactive_fiction](https://en.wikipedia.org/wiki/Interactive_fiction) and [https://www.inklestudios.com/ink/](https://www.inklestudios.com/ink/); [https://www.ifarchive.org/](https://www.ifarchive.org/) |
| Narrative | This is the story (as defined below) mediated through the experiences of a particular perspective. Within *FAIR Finds,* the narrative is the exact way that a particular user navigated the choices presented within the game. Writers in various creative fields will debate this definition though, so we suggest you check out Beemgee (a non-open source software system for creatives) for more context:  [https://www.beemgee.com/blog/story-vs-narrative/](https://www.beemgee.com/blog/story-vs-narrative/) |
| Paper prototype | This is where you write out your major story points, desires, and ideas on paper or in another visual/textual format to outline the material before delving into creating within a particular medium. We also introduced this approach in Part Two as a way to "focus on your story flow and developing a narrative structure before inputting your work into a digital format". That's a pretty solid definition but you can learn more about how valuable it is from Interaction Design: [https://www.interaction-design.org/literature/topics/paper-prototyping](https://www.interaction-design.org/literature/topics/paper-prototyping) |
| Passages  | These are the nodes in your Twine story where content, including choices and action items, occur. They can be understood as "scenes" or "moments" within the game that offer possibilities for how the player or user could respond to the story. The *Twine Cookbook* explains a lot more about these parts of Twine: [https://twinery.org/cookbook/terms/terms_passages.html](https://twinery.org/cookbook/terms/terms_passages.html) |
| Pedagogy | Is both a field of study and an approach to teaching and learning. In the context of *FAIR Finds: Using Twine in Archaeological Education* we use it to mean incorporating the Data Story, in parts or totality, into one's educational practice. You can explore the many ways people use pedagogy over at Wikipedia: [https://en.wikipedia.org/wiki/Pedagogy](https://en.wikipedia.org/wiki/Pedagogy) |
| Play / player | The act of interaction in a non-formal sense and the person undertaking such an endeavor. For our purposes, this refers to moving through the story of *FAIR Finds* or through a game made by the user of the Data Story. You can learn more about play, players, and the alternative that we occasionally reference—gamer, specifically for a player of games—over on Wikipedia: [https://en.wikipedia.org/wiki/Play_(activity)](https://en.wikipedia.org/wiki/Play_\(activity\)) and  [https://en.wikipedia.org/wiki/Gamer](https://en.wikipedia.org/wiki/Gamer) |
| Story | This is the sequence of events that occur within the game or other recounting medium. They are the broad strokes or events that explain or illustrate what happens in the thing you are creating. Sometimes used synonymously with narrative. In Twine, this is used as the generic for anything created with the format. While the Wikipedia resource below to refers to this as narrative, the entry demonstrates the ongoing debates about which is the right term to assign these different ways of exploring or contextualizing the parts of storytelling:  [https://en.wikipedia.org/wiki/Narrative](https://en.wikipedia.org/wiki/Narrative) |
| Wiki rabbit hole | This is when you start searching and learning about something, often on Wikipedia, and then clicking on related materials within the page that leads you to a new page. You then keep following new topics by continuing to click links to new materials to read or absorb. We'll let the originator of this problem explain themselves in more detail:  [https://en.wikipedia.org/wiki/Wiki_rabbit_hole](https://en.wikipedia.org/wiki/Wiki_rabbit_hole) |

## **Not finding what you're looking for?**

If none of these sections had the words you were looking for, check [*Major concepts*](#major-concepts) next to see if the term you're looking for has more context in that section.

Otherwise, you can get some help looking on Open Context for related information and data using the [*Open Context Keyword Search Tutorial*](https://doi.org/10.6078/M7TX3CHJ) or use some of the recommended resources listed in [*30 Days to An Article: Archaeological Inspiration For Your Writing*](https://doi.org/10.6078/M79Z931X) to look for more context. FInally, don't hesitate to [reach out](mailto:datastories@opencontext.org) with suggestions for more necessary vocabulary to improve this list. 

# **Major concepts** {#major-concepts}

Now that you’ve played all the way through [*FAIR Finds*](https://doi.org/10.6078/M7M906SV) (or at least read the walkthrough) and read, or glanced at, some of the vocabulary used in this Data Story, you know a bit about FAIR data and how Creative Commons (CC) licenses work. In this section, we provide more information about the archaeological data literacy-related content explored in [*FAIR Finds*](https://doi.org/10.6078/M7M906SV), which you might want to reference without replaying the game. 
We focus on providing references and additional information for the site under investigation in the game (*Baptizing Spring*), the FAIR Principles, and Creative Commons licenses. These concepts are useful to explore and reference when incorporating the game into your pedagogy, provide important background on course-relevant topics, and give students who desire or require it more context for the lesson or game. 

## ***Baptizing Spring***  {#baptizing-spring}

The first data set that you find on the internet in the game is the [*Baptizing Spring Zooarchaeological Data*](https://doi.org/10.6078/M7H70CX3) on Open Context. Open Context mainly publishes open access archaeological data so most of the data there is FAIR in many ways. However, if you plan on using data from Open Context make sure you check the license for each data set just in case! 

![](Images/noun-camel-4109324-1F294D.png)

For more information about the *Baptizing Spring* site itself, here's an excerpt of the project abstract ([Emery et al. 2018](https://doi.org/10.6078/M7H70CX3)) from the Data Publication page for the site on Open Context:

*"Baptizing Spring (08SU65) is a Spanish mission village archaeological site located in Suwannee County, Florida ... . While the exact founding date is unknown, the site was occupied by at least 1655 and likely abandoned around 1656 during the Timucuan uprising. …*

*The Baptizing Spring site revealed information about the Spanish-Indian interactions in Florida. Artifacts recovered included both aboriginal and Spanish types: lithics, projectile points, Spanish ceramics, lead shot, glass beads, metal objects, aboriginal ceramics (e.g., St. Johns Plain, Miller Plain, and Jefferson Ware), and Colono-Indian Ceramics. Two Spanish structures areas were identified at the site (Structures A and B), as were three aboriginal areas (two of which were named Structures C and D).* 

*Located on a rise, Spanish Structure B, roughly 10m by 8m, was composed of the remains of a 'packed red clay floor, some charred wood, five charred posts, sections of two wall ‘trenches’ and several features including pits' (Loucks 1979: page 130). The building had three walls and was open on the fourth side (Milanich 1999:133). The structure was interpreted as a possible church. Spanish Structure A is reported by Loucks (1979: pg 135\) as approximately 7m by 7.5m, with a dirt floor and probable wattle and daub walls. A large hearth was located inside the structure. Milanich (1999:137) identifies this structure as the 'convento' or residence of the religious personnel. Artifact concentrations in Structure A were larger than in Structure B and included most of the Spanish artifacts. Spanish artifacts included ceramics, nails, and spikes. These Spanish structures did not contain an abundance of faunal remains (as compared to the aboriginal structures              described below).* 

*Three areas of aboriginal structures/areas were located at the site and included 'concentrated features, postholes, and smudge pits (small pits packed with charred wood)' (Loucks 1979: page 138). Based on 5 large postholes, Structure D was likely a 6m circular structure, and recovered in this structure were five 'game pieces' shaped out of gopher tortoise shell. Structure D had 8 features including a 'rectanguloid, deep pit that was probably used for storage and later filled                    with refuse.') ..."* 

This project summary provides background about the site itself and it's pretty useful if you're interested in finding out more about Spanish Mission archaeology in Florida. You can also check out the data and site information included at the [Comparative Mission Archaeology Portal](https://cmap.floridamuseum.ufl.edu/about-the-database/guidelines/) to learn more about the site's non-zooarchaeological remains and data from other mission sites. 

## **FAIR Principles** {#fair-principles}

Here’s a handy reproduction of the FAIR data poster from the game:

![](https://archive.org/download/fairposterv2/fairposterv2.jpg)

*This is the FAIR Principles poster from the game. We license this poster adaptation of the principles (which includes its own credits for the content in the poster) under a [CC BY-SA License](https://creativecommons.org/licenses/by-sa/4.0/). You can get your own copy [here](https://archive.org/download/fairposterv2/fairposterv2.jpg).* 

FAIR is an approach to open data that more and more archaeologists (like your boss in the game) and other data scientists are encouraging people to use. The FAIR principles were first published in 2016 by Wilkinson et al. for scientific data broadly. However, there are reasons, like we found out in the game, that not all archaeological data—or data in general—are FAIR. You can check out more about the principles at [https://www.go-fair.org/fair-principles/](https://www.go-fair.org/fair-principles/) or at [https://eos-rcn.github.io/web/](https://eos-rcn.github.io/web/). However, some of those explanations are still pretty technical so we went over the principles in a more basic and broadly applicable way below. 

### **Findable**

“Findable” means that the data can be located by someone or something. For example, in the game the grey literature reports you found were “findable”. However, findable in the context of FAIR means a bit more than just "can it be found". Making data findable in the FAIR sense means making it findable to humans *and* computers by leveraging methods to make things “machine-readable” or machine findable.

Basically, making it so that someone using a search engine, program, algorithm, or database can locate your data. In addition, "Findable" actually has four sub-principles that define *how* to make data findable for, and through, computers. You can see the principles in an excerpt of the poster included below.

![](Images/FF_poster-Findable.png)

*Here are the sub-principles that support FAIR's approach to findability*.

The first principle means that the data has a name that nothing else has (unique) and that the data's unique name doesn’t change (persistent). Typically, those kinds of names tend towards machine-readable (having a standard structure and format that computers, algorithms, and code can process) rather than human-readable (like a name or a bibliographic citation). In archaeology, we often default to human readable names or codes when we're in the field (e.g. Find 1). However, when things get to the lab, and at field projects with well-developed persistent identifier protocols, machine-readable persistent identifiers get assigned. You can find out more about the interplay between machine and human readable approaches [here](https://control.com/technical-articles/machine-readable-vs-human-readable-data/).

The second principle under *Findable* means that the data has a rich description. This description is in the form of “metadata”. These are “data about data” or “formalized information about data needed to search for, display, and analyze those data” (Przystupa and Lieberman 2023). We’ll explain metadata more in the [*Reusable*](#reusable) section.

The third principle means that the name mentioned in the first principle is included in the “data about data” (aka the metadata) not just as the file name or listed in another identifier location (such as in a caption or title).

The fourth principle translates to the idea that the data, or the data about that data, are connected with a place or way to search or look through for it. It’s like being listed in a phone book or being part of a library catalog. Or having your profile public on social media. AKA something about the data has been shared with the wider data community so they can try to access it as well.

### **Accessible**

"Accessible" implies that someone can *get* the data. You’ve found it, now you want to actually have the ability to get to it. However, like with *Findable* it's not accessible in a general sense but accessible in a digital sense.

![](Images/FF_poster-Accessible.png)

*Here are the sub-principles that support FAIR's approach to accessibility.* 

The first component of this is that using its unique and stable name you can retrieve the data, or the data about that data, in a standardized way that: doesn’t cost money, uses non-proprietary software, allows for authentication (that the thing you *think* you're getting is in fact *what* you are getting or that you are who you say you are) and/or authorization (only people allowed to can access the data and we can confirm that).

The second component is that even if you can’t get to the data itself, the metadata (the data about that or the description of that data) still exists for reference to demonstrate it's existence or that it existed. For example, the citation for the data exists for people to access even if the data may not be accessible.

### **Interoperable**

This is where the FAIR Principles start to become more technical (as if they weren't already!). "Interoperability" has to do with something being able to be combined with other things and continue to work as expected.

![](Images/FF_poster-Interoperable.png)

*Here are the sub-principles that support FAIR's approach to interoperability.*

The first component of interoperability in the FAIR sense is that the data and its metadata "speak" or use a standardized (structured, available, shared, and widely used) language within itself so it can communicate with other data or metadata or things that want to access that data, data set, or metadata.

The second component is that the words used in the data, and the data about data (metadata), also follow the FAIR Principles so that folks can know where those words come from. This helps encourage the broad adoption of the FAIR Principles across disciplines.

The third component is that the data and its metadata reference and connect to other sources to explain attributes of itself. For example, the *Baptizing Spring Zooarchaeological Data*, includes links defining the different animal bones referenced in the project. While the data in the *Baptizing Spring Zooarchaeological Data* focuses on only the specific kinds of animal bones at that site, it references vocabulary for faunal analysis broadly by linking or referencing an external data source that provides widely-accepted definitions of different bones, including those not at that particular site. This aspect of interoperability is a bit like a [Wiki rabbit hole](https://en.wikipedia.org/wiki/Wiki_rabbit_hole) where you can keep clicking on links that explain different parts of the data.

### **Reusable** {#reusable}

"Reusable" under FAIR means that the data can be used again. However, to be reusable in the FAIR sense, requires that the data be in a format that allows reusers to combine it with other data sets as well as the ability to recreate the data themselves or the path of how it was created.

![](Images/FF_poster-Reusable.png)

*Here are the sub-principles that support FAIR's approach to reusability.*

The first principle of this kind of reusability is that the data and/or metadata have a lot of stuff describing them with multiple correct and relevant attributes included. Another definition of metadata is “[f]ormalized information about data needed to search for, display, and analyze those data” (Przystupa and Lieberman 2023). You can find out more about the importance of metadata [here](https://intarch.ac.uk/journal/issue2/wise/toc.html). 

We can break these metadata components down further into descriptive (Who created this data? What is this data about? When was this data created? What is its unique identifier?), administrative (Who owns this data? Who gives access to this data? How can this data be used?), and structural (How is this data set organized? What version of the data is this? What other resources do you need to interpret this data?) (Przystupa and Lieberman 2023). Basically, you want to answer as many of the previously listed questions and have those answers stored with the data in a formal way as metadata.

Within that metadata (data about data) is where you also include the [usage license](https://alexandriaarchive.org/2024/07/05/introduction-to-licenses-and-copyright/). Aka a way for you to explain how other people can use the data. [Creative Commons](#creative-commons-licenses) (which we discuss in the next section) is one kind of license that outlines these uses and there are other license providers if Creative Commons doesn't meet your needs. Licenses can state things like who “owns” the data; who created the data and how those creators or owners should be cited or referenced; whether or not and how you can adapt the data; and then how those adaptations can be shared and under what license.

The next subcomponent for reusability is related to licensing as well. Beyond using the license to specify how to cite, attribute, and use the data, the data and its metadata need to have their creators and/or owners listed and specified. For example, in the game if you picked the “use the data but with heavy citations option” you were listing the provenance of the data by citing the creators of the data you referenced and incorporated. Provenance, in the museum world, has to do with the series of owners of a particular piece and how they came to own it, usually to establish authenticity or legal ownership. We can think of this in relation to data in that we want to trace back from who is currently using the data (possibly you!) all the way back to who originally created the data.

The last subcomponent of reusability is that the data and metadata meet the standards set by your data community regarding vocabulary uses, structures, or attributes. This is typically pretty difficult for archaeology because standards within the discipline and across the subspecialities are hard to nail down but there are some out there. Like we mentioned with faunal data, there are controlled vocabularies that archaeologists use to ensure that features are reusable. And there are [cool projects](https://www.panamericanceramics.org/) out there working to create and encourage the adoption of standards within different archaeology specialities, like ceramics!

## **Creative Commons licenses** {#creative-commons-licenses}

One of the major conflicts in the game was that some of the data you located was under a specific license. These licenses are meant to keep certain kinds of data, information, and products safe for different reasons. You can read more about the purposes of licenses [here](https://creativecommons.org/faq/). While there are legal implications for licenses, they mainly support the idea of giving credit where that credit is due and in the way that the creator of those things wants to be credited.

[Creative Commons](https://creativecommons.org/) (CC) licenses outline how a thing can be used and are one way to license one's work. Licenses [in general](https://alexandriaarchive.org/2024/07/05/introduction-to-licenses-and-copyright/), and [Creative Commons](https://alexandriaarchive.org/2024/08/09/creative-commons-basics/) specifically, grant permissions (under certain conditions and with different requirements) to use copyright-protected works. Or identify if a work lacks restrictions or copyright, such as works under the [public domain](https://alexandriaarchive.org/2024/08/02/basics-of-public-domain/) or those designated CC 0\. We licensed *FAIR Finds: Using Twine In Archaeological Education* the Data Story as "CC BY-SA", noted on the first page of each part of the Data Story. You can even click the link there (or in the references section) to find out more.

![](https://alexandriaarchive.org/wp-content/uploads/2024/08/Foterdotcom_infographic_CC-Cropped-copy.jpg)

*This handy infographic helps you understand how restrictive different CC licenses are. This is a cropped version of “[Foter.com_infographic_CC.jpg](https://foter.com/blog/how-to-attribute-creative-commons-photos/)” by Foter from their piece “How To Attribute Creative Commons Photos” / [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en).* 

Breaking down what a CC BY-SA license actually means, CC is short for Creative Commons, the name of the publisher of the standardized license; BY means the work requires attribution (so, you should list if you adapted the material and how in addition to giving the creators credit), and SA means Share Alike (if you create a new thing based on this Data Story, you need to share it under this same license as the original). Sometimes there will be a number after the alphabetical codes. These indicate the *version* of the license, which is periodically updated for changes to legal code or to clarify wording. Because a CC license can't be changed once established without the consent of all involved parties, it's important to know which version of a CC license a work references to stay up-to-date.

CC licenses are useful because they are standardized and can be easily understood. That's because Creative Commons, the organization, worked to ensure that their licenses used non-technical language in their explanations. They also have detailed legal code explanations for lawyers available through their website. The standardization of licenses (like the codes we just walked through) helps interoperability and reuse by online communities. For example, if data from different places both carry the same Creative Commons Attribution (CC BY) license, you know you can combine them together as long as your use gives appropriate credit to the creators of both data sets.

You'll notice that the *Baptizing Spring Zooarchaeological Data* are licensed [CC BY](https://creativecommons.org/licenses/by/4.0/) while those at the [Comparative Mission Archaeology Portal](https://cmap.floridamuseum.ufl.edu/about-the-database/guidelines/) are under a [CC BY-ND](https://creativecommons.org/licenses/by-nd/3.0/deed.en) [3.0](https://creativecommons.org/licenses/by-nd/3.0/deed.en) license. The second has three codes (minus the numeric at the end) and the first has only two. This difference is your first hint that these data are not *easily* interoperable and reusable together.

![](Images/FairFinds_CCBY.jpg)

*If you scroll down on the* [Baptizing Spring Zooarchaeological Data](https://doi.org/10.6078/M7H70CX3), *Data Publication page on Open Context you'll find this box that specifies how the authors wanted the data licensed.*

Looking at the codes in more detail, the biggest difference is that you can’t make derivatives of the CMAP data. The CC license states that  “[i]f you remix, transform, or build upon the material, you may not distribute the modified material” ([Creative Commons 2024](https://creativecommons.org/licenses/by-nd/3.0/deed.en)). 

However, CMAP states that you can reproduce the data for “research, teaching, and private study” but that for “…commercial products, publication, broadcast, mirroring, reuse on a website, or anything else that does not fall under 'fair use'...we require that you contact the Comparative Mission Archaeology Portal in advance for permission to reproduce the materials” ([Waters 2024](https://cmap.floridamuseum.ufl.edu/about-the-database/guidelines/)).

The reason using the data but with heavy citation potentially caused problems in the game (if you happened to pick that option) is because the report possibly fell under “publication” rather than “research”. However, it looks like if you had more time to contact CMAP directly, incorporating this data might have been allowed.

This example is why it’s important both to find a materials license, understand what it means, and read the *specific guidelines* for a data set. Even licensed material might have flexibility. That's because they’re there to protect the data creator in the way that the data owner wants. That means if the data owner wants something different they might be able to grant                              greater permissions. 

![](Images/FairFinds_BrowserCartoon-01.png)

# **Teaching *FAIR Finds*** {#teaching-fair-finds}

We hope that reviewing the [*Key terms and glossary*](#key-terms-and-glossary) as well as the [*Major concepts*](#major-concepts) helps you consider the many ways you could incorporate *FAIR Finds* into a lesson plan. However, there's also a lot of information in those sections. So, in this section we provide specific suggestions for incorporating this Data Story into classrooms and other educational settings.

First, we suggest using the text-based game and/or its walkthrough as a starting point to explore the concepts highlighted in the [*Major concepts*](#major-concepts) section of this document and to provide an example of [making your own piece of interactive fiction](https://doi.org/10.6078/M7RV0KTM). Other potential topics include using the game as a way to:

- Understand open data (archaeological, mineral, economical, or otherwise) broadly  
- Learn to locate open archaeological data online  
- Explore data-driven stories and narrative based on any open data set  
- Evaluate FAIR approaches through storytelling

Next, we suggest that you ask participants questions through discussions, written reflections, or verbal feedback to engage them with the material on a deeper level.

This could include identifying concepts or terms of confusion, concern, interest, or overlap with the material in your current course or learning setting; questioning the format, looking for biases, structural/procedural issues, within the use of a text-based story or narrative; identifying overarching themes within the material of the game that connects with their experiences or learning styles broadly or what communicative outputs they want to make themselves; or how the Data Stories parts could be applied in other contexts for their future development.

We distilled and organized some of these suggestions into the two subsequent lesson plans. You can also find more general guidance, and some suggested readings, in the [*Educator's Sheet*](https://doi.org/10.6078/M7N29V32) for the Data Story. If you discover more helpful hints, feel free to [reach out](mailto:datastories@opencontext.org) with your recommendations and examples of teaching *FAIR Finds.* We hope that these elements all help you better incorporate *FAIR Finds* into your next class!

## **Example Lesson 1: FAIR Principles and archaeological research** {#example-lesson-1:-fair-principles-and-archaeological-research}

Goals: Define the FAIR Principles, locate sources of open archaeological data sets, determine how FAIR an archaeological data set is, evaluate their users' own and others’ determinations through opportunities for written reflection

1. Have students play, either in class or at home beforehand, *FAIR Finds* and have them consider what kind of archaeological data interests them  
2. Have students write what kind of archaeological data interests them on the board or mark next to data that's interesting to them (or in a digital forum if used in an online course)  
3. Recap the FAIR Principles using the Major concepts section of the Data Story  
4. Group students based on their archaeological data interests  
   1. Have students locate an online source of archaeological data. They may keyword search through a search engine or you can provide them with sources for archaeological information or data such as those cited in *Part Two: Selecting Your Data Set* and *Part Three: Referencing and Teaching* FAIR Finds  
      1. If using this in a regional topics class, you may pre-select online data sources for students appropriate to your region or topic  
   2. Have students individually evaluate the data sources they found in terms of each FAIR principle based on a 1-3 scale using copies of the Major concepts section to help explain the principles. 1 means they found no evidence of that principle, 2 they found unclear evidence of that principle, and 3 being they found clear evidence of the principle.   
   3. Have each student create an average "FAIR" score overall for the data set, and then an average for each principle of FAIR   
   4. Have students who looked at the same data compare their "FAIR" rankings  
      1. Have them note and discuss similarities and differences in ranking and their justification for their ranking   
5. Have students reflect on their understanding of the FAIR Principles in application based on the data they found.  
   1. Ask that their reflections include what they knew before the lesson about FAIR data, what they didn't know, what was interesting to them, and what was still confusing for them.   
6. Have students consider what future questions they have of FAIR data and of the specific data set they located and how they might use FAIR data in the future  
7. For the next class, encourage students to look up the CARE principles for comparison

## **Example Lesson 2: Text-based games for outreach** {#example-lesson-2:-text-based-games-for-outreach}

Goal: Define the FAIR principles, determine how FAIR a collection is; create a narrative incorporating their personal experiences; Evaluate their own and others’ work through play and reflection

1. Have participants play *FAIR Finds* during or near the end of a museum or other data-related educational experience  
   1. Ask participants to reflect on the principles in relation to their surroundings and provide a copy of the explanations if the principles for reference  
2. Introduce the concept of "story" and "narrative" with *FAIR Finds* as an example  
   1. Have participants distill the "story" of the game versus the "narrative"  
   2. Have them consider story and narrative in relation to the exhibit or other educational experience that they just participated in  
   3. Ask them how they might write a similar story-game as *FAIR Finds* but about their experience in the local educational environment  
3. Provide participants *Part Two* and materials to create a paper prototype  
   1. Paper prototype only option (participants may work in groups or alone)  
      1. Allow participants to replay *FAIR FInds* or to play games made by preceding participants with permission (either before or after creating a paper prototype)  
      2. Have participants create a three-act story (with beginning, middle, and ending) that involves at least two different choice points for their character  
      3. Ask for permission if others may "play" their game-story either by retaining the original or copy  
   2. Paper prototype and then Twine option:  
      1. Follow the paper prototype instructions then, on provided computers, have participants read all of *Part Two* and translate their paper prototype into a Twine game  
      2. Allow them to "Build" it for export for themselves (allow for e-mailing of the HTML or later access via Dropbox or other media)  
      3. Ask for permission if others may "play" their game-story either by retaining a digital copy   
4. Have participants fill out a short reflection on the experience (e.g. survey, written, or recorded) focusing on what they learned and which games they liked   
   1. Keep assessment metrics on how many story-games participants created

# **References and Further Reading**  {#references-and-further-reading}

These references are provided in order of appearance in the text, except for references linked to in the [*Key terms and glossary*](#key-terms-and-glossary) section where we include the full website links. If you don't find a link you're looking for from the text, check to see if it's listed in the [*Key terms and glossary*](#key-terms-and-glossary) section. If any links navigate to a broken page please, check the [Wayback Machine](https://web.archive.org/) for an archived copy of the material.

Check out our other Digital Data Stories here: [https://doi.org/10.6078/M74F1NW0](https://doi.org/10.6078/M74F1NW0)

Learn about the Alexandria Archive Institute at their website: [https://alexandriaarchive.org/](https://alexandriaarchive.org/)

Find more sources for open archaeological data at Open Context: [https://opencontext.org](https://opencontext.org)

*FAIR Finds* (the game) by L. Meghan Dennis (2023)  
[https://doi.org/10.6078/M7M906SV](https://doi.org/10.6078/M7M906SV)

Check out *FAIR Finds: Using Twine in Archaeological Education - Part One B: Game Walkthrough* by Paulina F. Przystupa and L. Meghan Dennis (2024)  
[https://doi.org/10.6078/M7HD7SSN](https://doi.org/10.6078/M7HD7SSN)

Check out *FAIR Finds: Using Twine in Archaeological Education - Part Two: Make Your Own Twine Game* by L. Meghan Dennis and Paulina F. Przystupa (2024)  
[https://doi.org/10.6078/M7RV0KTM](https://doi.org/10.6078/M7RV0KTM)

The Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0): [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)

For a quick summary of what interactive fiction is check out their wikipedia page:  [https://en.wikipedia.org/wiki/Interactive_fiction](https://en.wikipedia.org/wiki/Interactive_fiction)

Learn more about Twine: [https://en.wikipedia.org/wiki/Twine_(software)](https://en.wikipedia.org/wiki/Twine_\(software\))

*FAIR Finds: Using Twine in Archaeological Education - Educator's Sheet* by L. Meghan Dennis and Paulina F. Przystupa (2024)  
[https://doi.org/10.6078/M7N29V32](https://doi.org/10.6078/M7N29V32)

Check out the main page for *FAIR Finds: Using Twine in Archaeological Education*: [https://doi.org/10.6078/M7GF0RNT](https://doi.org/10.6078/M7GF0RNT)

You can reach out to the Data Literacy Program at: [datastories@opencontext.org](mailto:datastories@opencontext.org)

Check out New York City Archaeological Repository's glossary here: [https://archaeology.cityofnewyork.us/education/glossary](https://archaeology.cityofnewyork.us/education/glossary)

Learn more archaeology vocabulary from Parks Canada at: [https://parks.canada.ca/culture/arch/page2/doc2](https://parks.canada.ca/culture/arch/page2/doc2)

See how the Archaeology Data Service defines various archaeological terms at this link: [https://archaeologydataservice.ac.uk/help-guidance/glossary/](https://archaeologydataservice.ac.uk/help-guidance/glossary/)

*Living in Data: A Citizen's Guide to a Better Information Future* by Jer Thorp (2021)  
[https://us.macmillan.com/books/9780374720513/livingindata](https://us.macmillan.com/books/9780374720513/livingindata)

*The Brutish Museums: The Benin Bronzes, Colonial Violence and Cultural Restitution* by Dan Hicks (2020)  
[https://www.plutobooks.com/9780745341767/the-brutish-museums/](https://www.plutobooks.com/9780745341767/the-brutish-museums/)

*Of Mycenaean Men: Public Archaeology Book Club - Part Two: The Open Context Keyword Search Tutorial* by Paulina F. Przystupa (2023)  
[https://doi.org/10.6078/M7TX3CHJ](https://doi.org/10.6078/M7TX3CHJ)

*30 Days to An Article: Archaeological Inspiration For Your Writing* by Paulina F. Przystupa and L. Meghan Dennis (2023)  
[https://doi.org/10.6078/M79Z931X](https://doi.org/10.6078/M79Z931X)

“Baptizing Spring Zooarchaeological Data” by Kitty F Emery, L. Jill Loucks, Jerald Milanich, Charles Fairbanks, Cynthia Heath, Arlene Fradkin, Michelle LeFebvre, and Laura Brenskelle (2018) [https://doi.org/10.6078/M7H70CX3](https://doi.org/10.6078/M7H70CX3)

*Political and Economic Interactions between Spaniards and Indians: Ethnohistorical and Archaeological Perspectives of the Mission System in Florida* (1979) by Lana Jill Loucks  
[https://ufl-flvc.primo.exlibrisgroup.com/permalink/01FALSC_UFL/pek2if/cdi_proquest_journals_302896671](https://ufl-flvc.primo.exlibrisgroup.com/permalink/01FALSC_UFL/pek2if/cdi_proquest_journals_302896671)

*Laboring in the Fields of the Lord: Spanish Missions and Southeastern Indians* by Jerald T. Milanich (1999)  
[https://upf.com/book.asp?id=9780813029665](https://upf.com/book.asp?id=9780813029665)

Find out more about the Comparative Mission Archaeology Portal (CMAP): [https://cmap.floridamuseum.ufl.edu/](https://cmap.floridamuseum.ufl.edu/)

You can save a copy of the FAIR principles poster here:  [https://archive.org/download/fairposterv2/fairposterv2.jpg](https://archive.org/download/fairposterv2/fairposterv2.jpg)

"The FAIR Guiding Principles for scientific data management and stewardship" by Mark D. Wilkinson, Michel Dumontier, IJsbrand Jan Aalbersberg, Gabrielle Appleton, Myles Axton, Arie Baak, Niklas Blomberg, Jan-Willem Boiten, Luiz Bonino da Silva Santos, Philip E. Bourne, Jildau Bouwman, Anthony J. Brookes, Tim Clark, Mercè Crosas, Ingrid Dillo, Olivier Dumon, Scott Edmunds, Chris T. Evelo, Richard Finkers, Alejandra Gonzalez-Beltran, Alasdair J.G. Gray, Paul Groth, Carole Goble, Jeffrey S. Grethe, Jaap Heringa, Peter A.C ’t Hoen, Rob Hooft, Tobias Kuhn, Ruben Kok, Joost Kok, Scott J. Lusher, Maryann E. Martone, Albert Mons, Abel L. Packer, Bengt Persson, Philippe Rocca-Serra, Marco Roos, Rene van Schaik, Susanna-Assunta Sansone, Erik Schultes, Thierry Sengstag, Ted Slater, George Strawn, Morris A. Swertz, Mark Thompson, Johan van der Lei, Erik van Mulligen, Jan Velterop, Andra Waagmeester, Peter Wittenburg, Katherine Wolstencroft, Jun Zhao & Barend Mons (2016)  
[https://doi.org/10.1038/sdata.2016.18](https://doi.org/10.1038/sdata.2016.18)

Learn more about the differences between machine and human readable materials at: [https://control.com/technical-articles/machine-readable-vs-human-readable-data/](https://control.com/technical-articles/machine-readable-vs-human-readable-data/)

To learn more about Wiki rabbit holes check out: [https://en.wikipedia.org/wiki/Wiki_rabbit_hole](https://en.wikipedia.org/wiki/Wiki_rabbit_hole)

"Creating Clean Data & Cleaning Messy Data" a presentation for the Networking Archaeological Data and Communities scholars by Paulina F. Przystupa and Leigh A. Lieberman (2023) presentation not publicly available

Learn more about the importance of metadata in archaeology here: [https://intarch.ac.uk/journal/issue2/wise/toc.html](https://intarch.ac.uk/journal/issue2/wise/toc.html)

If you need an introduction to usage licenses (sometimes just called licenses) learn more here: [https://alexandriaarchive.org/2024/07/05/introduction-to-licenses-and-copyright/](https://alexandriaarchive.org/2024/07/05/introduction-to-licenses-and-copyright/)

This is one of the projects working to encourage standardization and sharing amongst archaeological subspecialties: [https://www.panamericanceramics.org/](https://www.panamericanceramics.org/)

If you want to learn about Creative Commons from them directly (including access to all linked licenses, their definitions, uses and so much more) check out: [https://creativecommons.org/](https://creativecommons.org/) 

Check out the Creative Commons FAQ for background on the purpose of licenses: [https://creativecommons.org/faq/](https://creativecommons.org/faq/)

Or, you can see the DLP's short article about the purposes of licenses: [https://alexandriaarchive.org/2024/07/05/introduction-to-licenses-and-copyright/](https://alexandriaarchive.org/2024/07/05/introduction-to-licenses-and-copyright/)

Or check out the DLP's article on the basics of Creative Commons licenses at: [https://alexandriaarchive.org/2024/08/09/creative-commons-basics/](https://alexandriaarchive.org/2024/08/09/creative-commons-basics/)

Find out more about public domain here: [https://alexandriaarchive.org/2024/08/02/basics-of-public-domain/](https://alexandriaarchive.org/2024/08/02/basics-of-public-domain/)

You can look at the original of the *Foter.com_infographic_CC* from “How To Attribute Creative Commons Photos” at: [https://foter.com/blog/how-to-attribute-creative-commons-photos](https://foter.com/blog/how-to-attribute-creative-commons-photos)

The Creative Commons Attribution-ShareAlike 3.0 Unported license (CC BY-SA 3.0): [https://creativecommons.org/licenses/by-sa/3.0/deed.en](https://creativecommons.org/licenses/by-sa/3.0/deed.en)

The Creative Commons Attribution 4.0 license (CC BY): [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)

"Copyright Information" in *Guidelines for Use* from the Comparative Mission Archaeology Portal by Gifford Waters (2024)  
[https://cmap.floridamuseum.ufl.edu/about-the-database/guidelines/](https://cmap.floridamuseum.ufl.edu/about-the-database/guidelines/)

See CMAP's CC BY-ND 3.0 license in its entirety here, it's the same link as Creative Commons 2024: [https://creativecommons.org/licenses/by-nd/3.0/deed.en](https://creativecommons.org/licenses/by-nd/3.0/deed.en)

Gain access to any broken links by getting an archived copy from the Wayback Machine: [https://web.archive.org/](https://web.archive.org/)

# **Credits** {#credits}

Unless otherwise specified in captions, or updated when image provenance is determined, this work and its components are shared under a [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) 4.0 license. To attribute this work, please use our suggested citation:

L. Meghan Dennis and Paulina F. Przystupa, 2024, “FAIR Finds: Using Twine In Archaeological Education”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: [https://doi.org/10.6078/M7GF0RNT](https://doi.org/10.6078/M7GF0RNT).

In order of appearance, this Data Story includes: "FairFinds_controllers-DarkBlue" by Paulina F. Przystupa from the Data Literacy Program (DLP) which is an adaptation (resized, recolored) of "FairFinds_controllers-01" by L. Meghan Dennis from the DLP with attribution information forthcoming. "FairFinds_Shovel-DarkBlue" by Paulina F. Przystupa from the DLP which is an adaptation (resized, recolored) of "FairFinds_Shovel-01" by L. Meghan Dennis from the DLP with attribution information forthcoming. "FairFinds_StickyNote4" by Paulina F. Przystupa from the DLP, which is an adaptation (resized, recolored, with new text) of "DLP Sticky Note" by L. Meghan Dennis from the DLP with attribution information forthcoming. "[Camel](https://thenounproject.com/icon/camel-4109324/)" by Hadi from the Noun Project ([https://thenounproject.com/icon/camel-4109324/](https://thenounproject.com/icon/camel-4109324/)) under a royalty-free license. "[fairposterv2](https://archive.org/download/fairposterv2/fairposterv2.jpg)" by L. Meghan Dennis and Paulina F. Przystupa from the Internet Archive licensed [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) 4.0 and includes an adaptation of an Adobe stock image alongside the text of the FAIR Principles from the "FAIR Principles" licensed [CC BY](https://creativecommons.org/licenses/by/4.0/) by GO FAIR. Cropped versions of "fairposterv2" are included with their corresponding principle and have the same attribution information as "fairposterv2". "FairFinds_BrowserCartoon-01" by L. Meghan Dennis from the Data Literacy Program (DLP) with attribution information forthcoming. All other images included in this text are licensed alongside the work at large, have their attributions cited in their captions, or are logos for our sponsors. 

# **Acknowledgements**  {#acknowledgements}

This Data Story is a collaboration between the producers of the Data Story, our testing audiences, AAI staff, and our open peer reviewers. We are grateful to all the work that people in the Twine creators community have done to inform our assembly of the game related to this Data Story, and are indebted to the work of numerous archaeologists, researchers, and staff in Florida for their continuing contributions to the corpus of archaeological and heritage data and interpretation. 

The Data Stories are part of the overarching Data Literacy Program, with support from  a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.
