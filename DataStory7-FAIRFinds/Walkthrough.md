---
title: 'FAIR Finds: Using Twine in Archaeological Education'
subtitle: 'Part One B: Game Walkthrough'
author: "Paulina F. Przystupa and L. Meghan Dennis"
date: "`r Sys.Date()`"
output:
  html_document: default
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
```{r}
# RELEASE DATE: 10/2024 
# VERSION: 2.0
# LICENSE: CC BY-SA https://creativecommons.org/licenses/by-sa/4.0/deed.en
```
![](Images/fairfindssplash2_modified.png)

# **Overview**

1. [Introduction](#introduction)
2. [Walkthrough of *FAIR Finds*](#walkthrough-of-fair-finds)
3. [Once you've finished the game](#once-you-have-finished-the-game)
4. [References and further reading](#references-and-further-reading)
5. [Credits](#credits)
6. [Acknowledgements](#acknowledgements)

Estimated time to read this guide: 20 Minutes

Estimated play time: User determined

# **Introduction** {#introduction}

Welcome to the [Digital Data Stories](https://doi.org/10.6078/M74F1NW0) of the [Alexandria Archive Institute](https://alexandriaarchive.org/) and [Open Context](https://opencontext.org)! This Data Story uses interactive fiction to understand Findable, Accessible, Interoperable, and Reusable (FAIR) data and to introduce Creative Commons licenses for archaeological and other scientific data. The whole Data Story is included in the game, so all you need to do is play through [*FAIR Finds*](https://doi.org/10.6078/M7M906SV) to experience this archaeological data-driven narrative. To play, click [here](https://doi.org/10.6078/M7M906SV) or travel to [https://doi.org/10.6078/M7M906SV](https://doi.org/10.6078/M7M906SV). 

[*FAIR Finds*](https://doi.org/10.6078/M7M906SV) can be played for fun or help you work through your own data considerations. You can also incorporate it into a larger program of outreach using [*Part Two: Make Your Own Twine Game*](https://doi.org/10.6078/M7RV0KTM). Or, integrate it into a class or learning event, drawing from [*Part Three: Referencing and Teaching* FAIR Finds](https://doi.org/10.6078/M76Q1VC4), which provides guidance on terms used in the game, context for important concepts, and recommendations on how to incorporate this into your pedagogy. 

This walkthrough explains how to play [*FAIR Finds*](https://doi.org/10.6078/M7M906SV), guiding players through the main parts of the game. It also acts as a reference, so that players can examine the game closely without replaying the entire thing. However, we don’t want to give away the whole story, so we only briefly describe what happens in each section. You'll have to play [*FAIR Finds*](https://doi.org/10.6078/M7M906SV) to find out more! 

*If any vocabulary or content in the game or this walkthrough is unfamiliar to you, check out* [Part Three: Referencing and Teaching *FAIR Finds*](https://doi.org/10.6078/M76Q1VC4) *for a glossary and an explanation of key concepts.*

# **Walkthrough of *FAIR Finds*** {#walkthrough-of-fair-finds}

Because [*FAIR Finds*](https://doi.org/10.6078/M7M906SV) is a text-based game, a lot of how to play is straightforward once you know where to click. Specifically, you'll usually want to click [light blue links](#level-zero---game-introduction), or things that arrows point to, so you can progress the story.


If you ever get stuck in the game—such as if you found out your peanut butter crackers are missing from your desk and get caught in a loop looking for them—you can get back to advancing the story by choosing an option, in the game, that focuses on working on your report, rather than grumbling about the crackers. Also, if at any time you want to start over, there's a handy [RESTART](#introduction) button on every page. 

## **Level Zero - Game Introduction** {#level-zero---game-introduction}

[![](https://archive.org/download/fairfindssplash2/fairfindssplash2.jpg)](https://doi.org/10.6078/M7M906SV)

*This is the opening image of* FAIR Finds *and it's where you'll begin your journey. Clicking the image in this walkthrough reveals a hyperlink to the game. Links in subsequent images won't though.*

This part acts as an introduction to the game. It gets you accustomed to clicking in the right spots to advance the story. Mechanics-wise, you'll want to remember that [light blue links](#level-one---how-to-include-fair-data) are how you'll move through the narrative.

Story-wise, this introduction is a pretty accurate portrayal of how most archaeologists feel about their job. We (like many scientists and data analysts) have a lot more spreadsheet stories than adventurous ones, much to our chagrin. In this section, you’ve only got one option on each page, so click through the series of [light blue links](#level-one---how-to-include-fair-data) in the text until you get to the problem.

## **Level One - How to include FAIR data** {#level-one---how-to-include-fair-data}

![](Images/FairFinds_PassageExample-Only.png)

*This is the next major choice point in* FAIR Finds. *Here, you'll need to decide how you want to complete your report, due later today because it's always Friday in* FAIR Finds*.*

It’s Friday and your boss has assigned you a report to finish by the end of the day. Oh, and they want you to incorporate FAIR data into it. Now, you just need to look for some. You can get to work in a number of ways: exploring the grey lit in the office, looking at the existing report, and jumping straight on the internet.

You might meander through a few of these choices and procrastinate by making coffee or notice that the office needs doughnuts. You might even *find* some data but it’s not going to be in an easily accessible format because it’s in a physically printed report. Eventually, you’ll have to go on the internet and begin searching to find FAIR data.

## **Level Two - Exploring the Components of FAIR**

Before you settled down to work, you may have seen a poster that looks like this in your office:

[![](https://archive.org/download/fairposterv2/fairposterv2.jpg)](https://archive.org/details/fairposterv2)

*This poster lists the FAIR principles. The DLP made this poster of the principles and you can get your own copy [here](https://archive.org/download/fairposterv2/fairposterv2.jpg). The poster includes attribution information but you can also find that in the* [References and further reading section](#references-and-further-reading) *of the guide*. *We removed the poster from images later in the walkthrough so they don't take up so much space.*  

Whenever you notice it, the poster is there to ~~haunt~~ help you throughout the day, so keep a copy handy. The poster lists the components of FAIR data that your boss wants you to incorporate. The rest of the game presents you with various options and choices as you look for data that meets all the principles. Each section explores one component of FAIR and you can click the appropriate [hyperlinked blue text](#findable) to make your choices.

### **Findable** {#findable}

The first data set you found might have been grey literature or grey lit. These are unpublished (or not conventionally published) reports produced by private archaeology companies or the government doing legally-mandated compliance archaeology. You can find out more about them [here](https://intarch.ac.uk/journal/issue40/6/toc.html). You might have also looked at your own report (in the game) for clues about what data to include. Or, you found yourself searching the internet.

The fact that you *located* or *found* the data means that data meets the *Findable* criteria …technically. However, if we look at the components under *Findable* on the poster, the non-internet sources don't meet those aspects of *Findable*. But, for now, it was found, so that’s good enough for you. More details about the components of *Findable* data are included in [*Part Three: Referencing and Teaching* FAIR Finds](https://doi.org/10.6078/M76Q1VC4).

The next step, and the hang up with the grey lit and the data within your own report, was that those data did not meet the *Accessible* principle. However, the internet located some data that met both the *Findable* (as you located it) and *Accessible* requirements*.*

### **Accessible**

![](Images/FairFinds_WalkThrough_Accesssible.jpg)

*We took the poster out from this screen capture but here's the part in the game where you locate* Findable *and* Accessible *data!*

You're at "your" desk, on the internet, keyword searching for related data. And, after a few clicks and scrolls, you find some. You found data from the *Baptizing Spring* archaeological site!

Once you *find* this data, you have the chance to dive straight into the work (aka copying and pasting the data) or you can delay the inevitable by looking for your glasses or a pad of paper to take notes on. Regardless of how long you take doing those things, eventually, you must sit yourself down and copy and paste. So click that [blue link](#interoperable) and get to work!

### **Interoperable** {#interoperable}

It’s open data (huzzah!) but only for a *specific kind* of archaeological data, zooarchaeological. Zooarchaeological–or faunal–data are those structured observations (data) about the animal remains (fauna) at a site. Unfortunately, that means there are other kinds of archaeological data that need representation in the report, like lithic and ceramic data.

![](Images/FairFinds_WalkThrough_InteroperableMetadata.jpg)

*Here are some options to find more sources of, hopefully,* Interoperable *archaeological data.*

This leads you to decide whether you want to contact the first author on the *Baptizing Spring Zooarchaeological Data* publication for more leads on data, check out the museum website that houses the archaeological finds from *Baptizing Spring,* or check the metadata links. This continues the search for Findable, Accessible, and now *Interoperable* data that relates to the report.

Looking at the metadata for the *Baptizing Spring Zooarchaeological Data* is one path in the game that you can take to find more (hopefully interoperable) data. In the game, all you have to do is click the light blue link on "metadata" in the "Check links for the metadata" option.

This selection leads to a new page that explains that metadata are data about data and that the data on Open Context clearly used a standard vocabulary, reaffirming that the data on Open Context meet the *Interoperable* principle.

If you want to find more information on both metadata and standard vocabulary outside of what the game provides, check out [*Part Three: Referencing and Teaching* FAIR Finds](https://doi.org/10.6078/M76Q1VC4) as the game alone doesn't explain too much about these concepts. Additionally, on the real *Baptizing Spring Zooarchaeological Data* project page (should you happen to make your way there on your own or through an in-game link), you'll need to scroll down to the section labeled "Metadata" to find out more about the data set. We provide snippets from the Project Abstract for *Baptizing Spring Zooarchaeological Data* in [*Part Three: Referencing and Teaching* FAIR Finds](https://doi.org/10.6078/M76Q1VC4) as a reference for those who want to know more about that project.

![](Images/FairFinds_WalkThrough_CMAP.jpg)

*You've got to search for more open data and this is when you come across another potential source of some related data.*

Regardless of whether you tried to contact the first author or look at the metadata first, you'll end up on a museum website that houses more data for *Baptizing Spring*. It's got a lot of documentation that suggests it will work with your existing data, demonstrating that it's likely going to meet the *Interoperable* principle.

Whether something is interoperable*,* in the FAIR sense, has to do with whether things have metadata (descriptive information about the data) and a standard vocabulary (that words have the same meaning across different data sets). However, after you get to the museum website and look around a bit, the question becomes whether the data meet the *Reusable* principle.

### **Reusable**

![](Images/FairFinds_WalkThrough_Checklist.jpg)

*You've got most of the principles covered but you have to assess for yourself if the data from this website are Reusable in the FAIR sense.*

According to the poster, *Reusability* under FAIR has to do with the attributes used in the data set as well as things like having a “clear and accessible data usage license” and “detailed provenance”. Having a “clear and accessible data usage license” is the part where the Data Story gets a little complicated. It looks like the data you located on the *Comparative Mission Archaeology Portal* (CMAP) have a clear and accessible data usage license…unfortunately it's what’s *in* that license that causes problems. Let's take a look at that in more detail.

#### *Licenses*

![](Images/FairFinds_WalkThrough_Licenses.jpg)

*This part explains more about the Creative Commons license associated with the CMAP data. It might allow for reuse but it's hard to figure out in the few hours you have left in your Friday if you can actually use it in your report.* 

Licenses outline *how* one can use things made by particular people or groups. The license on CMAP, though “clear” is…uh…*clearly* one that limits reuse due to *permissions*. Specifically, the license is “CC BY-ND", which is shorthand for Creative Commons (the place that defined the license), BY, which is shorthand for the need to attribute the work to its original creator aka list who the original work was "by" (under Creative Commons sometimes attribution also requires you to state if changes were made to the original before sharing and redistribution in addition to listing attribution in the desired way), and ND, which means…no derivatives.

As the Creative Commons website states, under ND, “\[i\]f you remix, transform, or build upon the material, you may not distribute the modified material” ([Creative Commons 2024](https://creativecommons.org/licenses/by-nd/2.0/deed.en)). This might mean you can’t use these data in the report you're writing. The inclusion of this license means it's your call to decide whether these data are actually *Reusable*.

You know that the faunal data from Open Context is open, as it has a different license (CC BY) that allows you to use it and remix it freely as long as you attribute the creator. But now it’s up to you to decide how the CMAP data uh…map…to the FAIR principles.

## **Level Three - Open Data Choices**

![](Images/FairFinds_WalkThrough_ToUseOrNotcopy.jpg)

*It's almost the end of the day and you have to decide if you should use the data and, if so, how. There's only so much you can do with the time you're given for this report, so act fast!*

There are only so many ways you can use or not use this data and the clock is ticking on your work day. While you can get caught here too (in non-data distractions), at one point you need to decide if the data you’ve found is FAIR and good enough.

There are three options for ending the story. You can use the data you’ve found whole-hog, use the data and provide heavy citations, or state that the data exist but not incorporate it. These all have different repercussions due to the licensing issues you read about earlier in the game. So take your pick!

## **Level Four - Outcomes**

Once you’ve made your decision you’ll turn in your report to your boss who will be more or less happy with you. Basically the more data you used from the CMAP project, the less happy your boss will be because the data set you found from the CMAP project wasn’t actually FAIR due to its licensing terms. It means that folks could reference it but not do much with it without contacting CMAP to ask for permissions.

Regardless, you got your report turned in, which is great! And you learned about FAIR data along the way. If you want to try redoing the report, go ahead and play through *FAIR Finds* again!

# **Once you've finished the game** {#once-you-have-finished-the-game}

Now that you've finished playing, or reading this walkthrough for, *FAIR Finds* you've learned more about the FAIR principles, Creative Commons licenses, and experienced the power of interactive fiction! You can replay *FAIR Finds* to explore other options or experience the hunt for those peanut butter crackers.

To do that, navigate to the game [here](https://doi.org/10.6078/M7M906SV). If you end up on the final landing page for *FAIR Finds*, click the [RESTART](#introduction) button in the bottom right. You can also clear the cache in your web browser or start a private browsing session and then re-navigate to the game.

If *FAIR Finds* inspires you and you want to create your own piece of interactive fiction, for archaeology or another data-driven or scientific field, check out [*Part Two: Make Your Own Twine Game*](https://doi.org/10.6078/M7RV0KTM)*.* Part Two gives you everything you need to make your own piece of interactive fiction exploring any subject you want!

If you found yourself desiring, or requiring, more information to get the most out of *FAIR Finds,* we suggest checking out [*Part Three: Referencing and Teaching* FAIR Finds](https://doi.org/10.6078/M76Q1VC4)*.* There you'll find an expanded glossary for all parts of *FAIR Finds* (including the game, this walkthrough, the how-to for Twine, as well as Part Three itself) and more information on the key concepts presented in the game. You can also combine Part Three with our [*Educator's Sheet*](https://doi.org/10.6078/M7N29V32) to incorporate *FAIR Finds* into your classroom. We recommend that interested educators start with the [*Educator's Sheet*](https://doi.org/10.6078/M7N29V32) and then use Part Three for additional pedagogical support. It provides this support through two example lesson plans and explaining the major concepts and vocabulary in more detail.

Thanks for checking out this [Digital Data Story](https://doi.org/10.6078/M7GF0RNT) and don't forget to check out our [other](https://doi.org/10.6078/M74F1NW0) Data Stories to keep cultivating your data literacy skills! 

# **References and further reading**  {#references-and-further-reading}

These references are listed in order of appearance in the text or are suggestions for further reading on the topics presented. If any links navigate to a broken page, please check the [Wayback Machine](https://web.archive.org/) for an archived copy of the material.

Check out other Digital Data Stories here: [https://doi.org/10.6078/M74F1NW0](https://doi.org/10.6078/M74F1NW0)

Find out about the work of the Alexandria Archive Institute at: [https://alexandriaarchive.org/](https://alexandriaarchive.org/)

Check out Open Context for yourself here: [https://opencontext.org](https://opencontext.org)

*FAIR Finds* by L. Meghan Dennis (2023)  
[https://doi.org/10.6078/M7M906SV](https://doi.org/10.6078/M7M906SV) 

*FAIR Finds: Using Twine in Archaeological Education \- Part Two: Make Your Own Twine Game* by L. Meghan Dennis and Paulina F. Przystupa (2024) [https://doi.org/10.6078/M7RV0KTM](https://doi.org/10.6078/M7RV0KTM)

*FAIR Finds: Using Twine in Archaeological Education \- Part Three: Referencing and Teaching* FAIR Finds by Paulina F. Przystupa and L. Meghan Dennis (2024) [https://doi.org/10.6078/M76Q1VC4](https://doi.org/10.6078/M76Q1VC4)

The Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0): [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/)

You can save a copy of the FAIR principles poster here: [https://archive.org/download/fairposterv2/fairposterv2.jpg](https://archive.org/download/fairposterv2/fairposterv2.jpg)

To find out more more about the FAIR principles and GO FAIR check out: [https://www.go-fair.org/](https://www.go-fair.org/), [https://www.go-fair.org/fair-principles](https://www.go-fair.org/fair-principles/) or [https://eos-rcn.github.io/web/](https://eos-rcn.github.io/web/)

To learn more about grey literature check out: [https://intarch.ac.uk/journal/issue40/6/toc.html](https://intarch.ac.uk/journal/issue40/6/toc.html)

*Baptizing Spring Zooarchaeological Data* by Kitty F Emery, L. Jill Loucks, Jerald Milanich, Charles Fairbanks, Cynthia Heath, Arlene Fradkin, Michelle LeFebvre, and Laura Brenskelle (2018) [https://doi.org/10.6078/M7H70CX3](https://doi.org/10.6078/M7H70CX3)

Learn more about the importance of metadata in archaeology here: [https://intarch.ac.uk/journal/issue2/wise/toc.html](https://intarch.ac.uk/journal/issue2/wise/toc.html)

Check out the Florida Museum of Natural History: [https://www.floridamuseum.ufl.edu/](https://www.floridamuseum.ufl.edu/)

Find out more about the Comparative Mission Archaeology Portal: [https://cmap.floridamuseum.ufl.edu/](https://cmap.floridamuseum.ufl.edu/)

Check out Creative Commons licenses (including access to all linked licenses, their definitions, uses, and so much more) here: [https://creativecommons.org/](https://creativecommons.org/)

Also listed as Creative Commons 2024 this is the Creative Commons Attribution-NoDerivatives License 2.0 (CC BY-ND): [https://creativecommons.org/licenses/by-nd/2.0/deed.en](https://creativecommons.org/licenses/by-nd/2.0/deed.en) 

The Creative Commons Attribution License 4.0 (CC BY): [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/) 

*FAIR Finds: Using Twine in Archaeological Education \- Educator's Sheet* by L. Meghan Dennis and Paulina F. Przystupa (2024)  
[https://doi.org/10.6078/M7N29V32](https://doi.org/10.6078/M7N29V32)

Check out all the materials for *FAIR Finds: Using Twine In Archaeological Education* over at: [https://doi.org/10.6078/M7GF0RNT](https://doi.org/10.6078/M7GF0RNT)

*Cow-culating Your Data with Spreadsheets and R* by Paulina F. Przystupa and L. Meghan Dennis (2021) [https://doi.org/10.6078/M7ZW1J2X](https://doi.org/10.6078/M7ZW1J2X)

The Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License (CC BY-NC-SA): [https://creativecommons.org/licenses/by-nc-sa/4.0/](https://creativecommons.org/licenses/by-nc-sa/4.0/)

If you missed out on this detour, you can still check out "Pedro Pascal and Puppies": [https://youtu.be/MEMK0y6M6lI?si=LmRKHM3sGhOGVKh1](https://youtu.be/MEMK0y6M6lI?si=LmRKHM3sGhOGVKh1)

Gain access to any broken links by getting an archived copy from the Wayback Machine: [https://web.archive.org/](https://web.archive.org/)

# **Credits** {#credits}

Unless otherwise specified in captions, or updated when image provenance is determined, this work and its components are shared under a [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) 4.0 license. To attribute this work, please use our suggested citation:

L. Meghan Dennis and Paulina F. Przystupa, 2024, “FAIR Finds: Using Twine In Archaeological Education”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: [https://doi.org/10.6078/M7GF0RNT](https://doi.org/10.6078/M7GF0RNT).

In order of appearance, this Data Story includes: "[fairfindssplash2](https://archive.org/details/fairfindssplash2)" by L. Meghan Dennis from the Internet Archive ([https://archive.org/details/fairfindssplash2](https://archive.org/details/fairfindssplash2)) / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) 4.0. Some iterations of this image are cropped to remove the arrow by Paulina F. Przystupa. "Fairfindssplash2" incorporates an Adobe stock image adaptation. "[Fairposterv2](https://archive.org/details/fairposterv2)" by L. Meghan Dennis and Paulina F. Przystupa from the Internet Archive ([https://archive.org/details/fairposterv2](https://archive.org/details/fairposterv2)) / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) 4.0 and includes an adaptation of that same Adobe stock image alongside the text of the FAIR Principles from the "FAIR Principles" webpage licensed [CC BY](https://creativecommons.org/licenses/by/4.0/) by GO FAIR, with links in the preceding references list. All other images included in this text are licensed alongside the work at large, have their attributions cited in their captions, or are logos for our sponsors. 

# **Acknowledgements**  {#acknowledgements}

This Data Story is a collaboration between the producers of the Data Story, our testing audiences, AAI staff, and our open peer reviewers. We are grateful to all the work that people in the Twine creators community have done to inform our assembly of the game that this walkthrough describes, and are indebted to the work of numerous archaeologists, researchers, and staff in Florida for their continuing contributions to the corpus of archaeological and heritage data and interpretation. 

The Data Stories are part of the overarching Data Literacy Program, with support from  a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.