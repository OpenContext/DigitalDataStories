---
title: "Cow-culating Your Data with Spreadsheets and R - Using narrative"
author: "L. Meghan Dennis, PhD. Postdoctoral Researcher for Data Interpretation and Public Engagement"
date: "26 May 2022 - Version 1.0"
output:
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Howdy and welcome back! 

In this third part of the Digital Data Story, we’re going to change focus a bit and look at what we do with data after we’ve completed an analysis. Together, we’ve used a [spreadsheet](https://www.libreoffice.org/discover/calc/), we’ve used [R](https://www.r-project.org/), and now, we’re going to use the data to make a compelling case for the results of our research. This is called creating a ‘data narrative’. 

Learning to create a ‘data narrative’ is important because at some point we’ll want to share our results in a way that the public can understand. Sometimes the public might be a supervisor, other times it might be a funding agency. Often the public is a publication, or a presentation to stakeholders. No matter who the specific public is, it’s crucial that we provide our results in a way that is clear, appropriate to the audience, and engaging.

But hold up there, cowpoke. How is a data narrative different from data, and how is it different from a regular narrative? To answer the first question, a data narrative differs from data because it puts some of the data together to tell a particular story that answers a particular question. And the second question? A data narrative is different from a narrative because where a narrative is the storified version of events, a data narrative is the storified version of data. 

It’s important that at this stage, we think critically about the data we’ve analyzed, and about our analysis. We know that we did our spreadsheet and R exercises ‘right’, or we skipped here directly for a good tail, but it’s still possible to end up with an inaccurate conclusion. An inaccurate conclusion will end up being a ‘data narrative’ that shares those inaccuracies, and the public won’t be served. As researchers and data analysts, our work should always in some way benefit the public and giving them inaccurate information isn’t helping them! So let’s talk about the things that can go ‘wrong’ when we’ve done everything ‘right’. 

## Interpreting data considerations (bias) 

First up, we need to think about bias. Often, the word bias has a pretty negative connotation. In our case, when we’re talking about bias, we’re not necessarily talking about something bad, like racism, or sexism. For our purposes, bias is anything in our personal experience of the world that influences our results in a way that changes those results without the data to back them up. 

So let’s look at the potential kinds of bias we might encounter with our cattle bones data. We’re going to talk briefly about sampling and selection bias, confirmation bias, and outliers. 

### Sampling and selection bias

Sampling and selection bias has to do with the kind of data that are, and are not, included in the data set we’re working with. These kinds of biases can be due to several different things; the kinds of sources that were selected may cause bias, the screen size in the field that specimens were sifted through may cause bias, how we filter our data in a table or spreadsheet may cause bias. Even the questions that drove the data collection team to do the project in the first place may cause bias. 

Often, these are the kinds of decisions that happen even before we open our data set. They’re events out of our control. However, because this is so out of our control, it can be hard to sort out what all of the potential bias factors are, especially if we haven’t heard of these sorts of osteological (bone) remains before. 

A great way to try to approach the issue of selection or sampling bias is by looking at the data… about the data set. This ‘data about data’ is sometimes called metadata. To do this we need to go back to the original source where we got the cattle bones data. Looking there, we’ll find information about the creation of the data set that can help us understand what choices were made when it first was created. So go ahead and open a browser window [here](https://opencontext.org/projects/c89e6a9e-105a-4368-9e90-26940d7bf37a) to the data set on Open Context.

The starting clue to the sort of limitations that might be in this data set comes from its name. While we’ve been just calling this data set ‘cattle bones,’ the actual project was called ‘[Neolithic and Bronze Age Cattle Data from Switzerland.](https://doi.org/10.6078/M7H13049)’ This tells us a couple of important things. 

First, all the bones are from present-day Switzerland, so it means that we’re not dealing with cattle data from all over the world, or even all over Europe. 

Second, it calls this ‘cattle data,’ not cattle bones. This hints that there might be some other kinds of data within the data set. 

Third, it describes the data as ‘Neolithic and Bronze Age’. Now, those terms came up when we were making graph labels related to time, but what do they actually mean? We’re trying to work with data here, but in a pinch we can do a search for it online. (If were interested in using a more data-centric source, we can mosey on over to a site like [PeriodO](https://perio.do/en/) for a quick detour and we’ll wait until the cows come home to continue.) 

Because the terms ‘Neolithic’ and ‘Bronze Age’ refer to time, this means that we only have cattle data from Switzerland that come from those two periods of time. 

Specifically, if we look at the subheading of the project it has particular dates: c4500 - 800 cal BC. The dates refer to a particular method for dating the periods in question, and we’ll explore that in a future tutorial. For now, we just need a general idea of when those periods were. 

The project description also specifies that these are cattle ‘biometrical data’. A quick online search for ‘[biometrical](https://www.thefreedictionary.com/biometrical)’ (https://www.thefreedictionary.com/biometrical) should ensure that we understand what sort of data were collected. The project description also mentions something called [NISP](https://en.wikipedia.org/wiki/Number_of_Identified_Specimens) (https://en.wikipedia.org/wiki/Number_of_Identified_Specimens), though? That’s something we can set aside for now…but it may be important in the future. 

Another part of sampling and selection bias can come from the kinds of data we choose to use to answer our particular questions from a particular data set. For example, if we purposefully, or accidentally, only filtered for a particular kind of bone or for only one period, and then tried to answer the question, ‘What period are these bones from’ we would only be able to answer that in relation to the sample of the data set we filtered for. We wouldn’t be able to answer the question for the whole herd of data. 

#### So, how can we deal with this kind of bias?

The easiest thing to do is to make our question smaller by adding specific information about the data set we’re looking at. This allows us to be explicit about how applicable our conclusions are. It also allows us to be explicit about what populations our narrative and answer applies to. We don’t necessarily want to create long questions that could confuse our reader, so we can separate out these biases in the narrative and state them explicitly. 

### Confirmation bias

The next kind of bias we need to consider is called ‘confirmation bias’. This is the tendency to give more weight to evidence that aligns with our previously existing ideas and thoughts on a subject. This can result in listening to evidence that supports our hypothesis and ignoring evidence that refutes our hypothesis. 

As an example, if we have a hypothesis that Neolithic people consumed more protein derived from cattle sources than modern day people, on average, and we think that Neolithic people didn’t live in big groups, we might choose to compare the potential amount of beef consumed by Neolithic people against the amount of beef consumed by modern people in rural areas, where the population density is low. But that would ignore the infrastructure required for the amount of cattle found in our data set, and infrastructure indicates a potentially high population density. 

#### So, how can we deal with this kind of bias?

Confirmation bias is tricky, and we have to think about where our personal beliefs may be influencing how we analyze our data. There are ways to avoid falling into confirmation bias though. We can use multiple methods and forms of analysis to test our hypotheses. We can make our research reproducible, so that someone else can run the same analysis we did to see if they come to the same conclusions. (Remember, working in R is good for this!) We can avoid ambiguity in our research by making our terms and choices clear. Finally, we can be okay with our results not being what we wanted! 

### Outliers

The final kind of bias we’re going to discuss that can cause our narratives to moove away from accuracy are what are referred to as outliers. These have a particular definition in statistics. For now we’ll operate under the definition that outliers are a kind of ‘extra special’ data. 

It’s important to highlight that ‘extra special’ does not necessarily mean ‘more important’. In fact, the exact opposite is more likely! Sometimes, a particular specimen is so one-of-a-kind, so individual, so unique that it disrupts the story being told by the other specimens within a particular data set. An outlier may be the single human tooth in a data set full of bear femurs. It may be the one artifact that dates to the medieval in a data set of artifacts that all date to the Neolithic. 

#### So, how can we deal with this kind of bias?

While we don’t want to ignore the existence of outliers, it is important to understand that they don’t describe or accurately portray the majority of cases. Which, in archaeology, is typically what we’re going for. So resist the urge to throw out the discussion of all of the data that makes sense together in favor of focusing on the one piece that doesn’t. Exploring the outliers can be a future task, and a chance to ask different questions once our main project is complete!

## Interpreting Data Considerations (Creating a Narrative)

Whew. There’s clearly a lot to consider after we’ve done our analysis before we’re ready to share our work with the public. But having considered all of that...how DO we share our work with the public? Once we’re at this point, it’s time to start creating our data narrative. 

Remember our driving question for this work was ‘What periods are these bones from?’. So we want to create a narrative based on our data that answers that particular question. Some of the ways we could display that narrative include: 

- a list
- a table
- a chart
- a picture
- a presentation
- a podcast 
- a twitter thread
- a cow-drama
- an interpretive dance
- a table-top role-playing game 

While some of our data narratives are ‘traditionally’ academic, such as visualizations like lists, tables, and charts, those traditional methods of sharing results aren’t the only ways to display a data narrative. One big factor that would dictate choosing to make say, an interpretive dance over a podcast, or over a scatter plot, would be the audience. Who are we trying to reach? Why are we trying to reach them? What would be the method of sharing data that would answer both ‘who’ and ‘why’? 

After the ‘who’ and the ‘why’, it’s time to consider the tone we set for our narrative. While we are doing data-driven science, that doesn’t mean that our narrative should be unapproachable. Our intention is to communicate our results and based on our list, there’s lots of ways to do that. 

While we tend to think of scientific writing as objective, if we recall the sections on bias we just considered…that’s not quite right. We can try to be objective, and we can take steps to minimize influences that bias us, but there are always a few things out of our control that make it hard or impossible to be 100% objective. Also, we can try to be honest and open about them by disclosing potential sources of bias. That may help inform others and, over the long term, maybe people can find approaches that reduce the impact of those identified biases. This is the ‘interpretation’ part of archaeology, and it’s critical! 

Additionally, attempts to write in an objective way can also reveal particular biases and do have an amount of subjectivity to them as they imply a kind of authority and create a certain kind of narrative edge to them. Archaeologists refer to these as theoretical perspectives. These drive the kinds of frameworks we use to interpret our data. They also help us give our data meaning (or to try not to give our data meaning) depending on what our goals are. A few of these narrative or theory-driven types include: 

- objective/scientific
- descriptive
- subjective
- perspective (could you share your data narrative from the perspective of a Neolithic cow?)
- question driven

Let’s look at one quick example of a data narrative told via a role-playing game adventure, with an interactive, descriptive format. 



# Fields of Faunal Remains: A Dungeons and Dragons Style Adventure for 3-5 Players

![](Images/FieldsOfFaunalRemains)

Your party is stopped in front of a heavy wood and metal gate. It’s locked. Coils of barbed wire fence stretch off in both directions. Beyond the gate, and through its slats, you see a sweeping green field. 

What does your group do? Do you try to unlock the gate? Do you try to lift the barbed wire and slither through? Do you attempt to find the landowner? 

Once you make it through the gate, you step into the grass. It’s knee-high in places, forcing you to step carefully. As you pick your way through the field, you notice piles of white...something. Bones? Smaller pieces of bone are spread out throughout the grass, making white drifts in the vegetation. 

Are the bones human? Roll your dice and make an investigation or medicine check. 

If you roll up to a 9, failure. Oh no. You’re unsure if these are human bones. They probably are? Maybe. Or not. You don’t know. 

If you roll over a 10, success! You discover that the bones are distinctly non human. They’re much heavier, and appear bleached in the sun. These are definitely cattle bones. 

If you roll a 20, well done! A study of the bones shows that not only are these not human bones, they’re larger than any modern cattle bones you’ve ever seen. These are aurochs bones! 

Once you’ve considered what kinds of bones you’re looking at, the party may wonder…why are there so many bones?! They can spend some time investigating and counting the bones, looking for particular features and sorting them by type. 

Roll your dice and make an investigation check. 

If you roll up to a 9, failure. You discover a well-preserved piece of paper, but it’s unintelligible. You should really do a tutorial on data literacy like that wizard suggested. I heard there are some good ones offered by the Alexandria Archive... 

If you roll over a 10, success! You discover a well-preserved piece of paper, folded under a rock. It’s some sort of inventory of all of the bones in this field. It will take more research to fully understand it though. Lucky for you, you’ve done some work with spreadsheets and with R, so you know how to get started. 

If you roll a 20, well done! A study of the paper reveals it to be a curated data set of cattle bones, based on a project called, ‘Neolithic and Bronze Age Cattle Data from Switzerland.’ The project appears to have been carefully recorded and curated by Elizabeth Wright, Margherita Schäfer, Barbara Stopp, Elisabeth Marti-Grädel, Francesca Ginella, Manar Kerdy, Miki Bopp-Ito, Sabine Deschler-Erb, and Jörg Schibler. Because you’ve worked in spreadsheets and R, you quickly get started querying your data and preparing visualizations.

## Next Steps

It’s time to bring the cows into the barn, as we’ve reached the end of this Digital Data Story. We hope that over the course of the last three pieces of this tutorial we’ve learned a bit about improving our data literacy, and have gotten some practice in with cattle bones data. 

Remember, the methods we discussed in this tutorial aren’t limited to this data set. What we’ve learned can be adapted and used on our own project data, or on any open data that we have access to. We’d love to see how we use these methods and how we’ve created our own data narratives. Please get in touch and share with us! 

-– The Data Literacy Program 

This work has been made possible in part by the National Endowment for the Humanities and The Mellon Foundation. Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or The Mellon Foundation. 

License: [Creative Commons CC-BY](https://creativecommons.org/licenses/by/4.0/)
