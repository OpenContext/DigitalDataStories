![](Images/CowCulatingHeader-1024x555.jpg)

Welcome to this Digital Data Story (DDS) from the Alexandria Archive Institute. We’re happy to provide you with this educational resource that can be used for personal practice or integrated into a course.

The DDS Project promotes an increased focus in archaeological education on digital data literacy. Through the use of open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis. These combined code and theory practicals illustrate the confluence of science and humanities-based investigations in collected data about the past.

This approach promotes multiple levels of engagement with archaeological datasets, linking data-driven narratives with key analytical and interpretive steps used to ethically analyze, visualize, and present research data. In this set of exercises, users will utilize zooarchaeological data.

This exercise is best suited to those with an interest in zooarchaeology, the Neolithic, or Central European archaeology. Users should have a basic understanding of what a spreadsheet is, but little previous experience is required.

This page provides access to the resource in two ways. The first is through a series of PDFs that represent the completed data story. These PDFs include:

1. [Spreadsheet](https://doi.org/10.6078/M7V40SBH)
1. [R](https://doi.org/10.6078/M7QC01M5)
1. [Narrative](https://doi.org/10.6078/M7KK98WW)
1. [Teaching guide](https://doi.org/10.6078/M7FT8J5K)

A single PDF for the combined tutorials is available [here](https://doi.org/10.6078/M7ZW1J2X). Additionally, there is an introductory video available [here](https://doi.org/10.6078/M77942TR) that participants can watch prior to engaging with the tutorial exercises.

This respository is secondary access to the above listed materials. These markdown files represent the source material for the text and primary images within the PDFs, minus additional formatting, and with only a general placement of images. This code represents our commitment to open science and transparency in our process.

These materials, either through the PDFs or the code, are designed to be used in order, beginning with the introductory video and ending with the narrative tutorial. However, any piece of this data story may be used separately or re-ordered according to the requirements of the individual or specific educational goals. Furthermore, the markdown files may be altered and forked from the Codeberg repository for custom use with different data sets. These resources are available free to use under a [Creative Commons Attribution (CC-BY)](https://creativecommons.org/licenses/by/4.0/) license.

Thank you so much for utilizing our educational resources! If participants have the time, please consider contributing thoughts to our ongoing survey, whose data we will use to periodically update the resource. Such updates will be noted here, and will be first available through the code repository prior to PDF updating.

First published: 26 May 2022
Updated: 27 May 2022 - URLs moved to DOIs

This work has been made possible in part by the National Endowment for the Humanities and The Mellon Foundation. Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or The Mellon Foundation. 
