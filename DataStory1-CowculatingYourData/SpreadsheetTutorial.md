---
title: "Cow-Culating Your Data - Using spreadsheets"
author: "Paulina F. Przystupa, data visualization and reproducibility postdoctoral researcher"
date: "08 December 2021"
output:
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
## Tutorial overview

In this tutorial, we will learn to:

1. Download a file from the internet
1. Load data from a file on our computer into a spreadsheet
1. Explore columns in those data
1. Select one column to visualize
1. Make different sorts of text-based visualizations
1. Make a bar chart
1. Save our plots and other outputs

Estimated time to complete this tutorial: 30-60 minutes

## Key concepts used in this tutorial

`Import from .csv format`

`Filter headings`

`Pivot tables`

`GUI-based figure design`

## Introduction

It’s time to mooooove along into this, our tutorial on how to analyze data using a spreadsheet program. This may seem herd at first, but if we calf some patience, it will work out udderly great!

Sorry, sorry. We’ll stop now...moostly.

Luckily for us, [Open Context](https://opencontext.org/) (https://opencontext.org/) has a great [project](https://opencontext.org/projects/c89e6a9e-105a-4368-9e90-26940d7bf37a) (https://opencontext.org/projects/c89e6a9e-105a-4368-9e90-26940d7bf37a) on ancient cattle bones already online. So we can use this existing data set, and don’t have to start from scratch. 

However, before we can do anything we'll need to download the cattle bones data onto our computer. To do so [mozy on over to Open Context](https://opencontext.org/subjects-search/?proj=166-neolithic-and-bronze-age-cattle-data-from-switzerland#8/47.040/8.438/11/any/Google-Satellite) (https://opencontext.org/subjects-search/?proj=166-neolithic-and-bronze-age-cattle-data-from-switzerland#8/47.040/8.438/11/any/Google-Satellite) and save the file by clicking the the hyperlink. 

If we look at our download, we’ll see that the file we just saved ends in ".csv". This stands for `Comma Separated Values`, and means that if we were to open it in a rich-text document reader, like LibreOffice Writer, Google Docs, Microsoft Word, or Wordpad, or a text editor like notepad, all of the values of the file would be separated by a series of commas (,) and new lines. While that pattern can be hard for humans to process, it's super easy for computers. They just search for that symbol and break the data up at those intervals. (We don’t know how hard it is for cows. Yet.)

Thankfully, sorting this kind of data doesn't require a fancy algorithm, and most spreadsheet programs can do it automatically. Of course, we CAN cheese it out using fancy algorithm but maybe we'll explore that in the future...Like, maybe in the next tutorial?  In general, a spreadsheet program can be a great way to explore our data. It allows us to sort and filter visually, seeing things be added or taken away. That interaction allows us to get a feel for our data. 

There are a lot of options out there for spreadsheet programs, so this tutorial will focus on common commands that should get us what we need regardless of the program we use. However, button placement and the names of particular tasks may vary, so don't worry if the output or directions in this don't exactly match what we're working with.

Specifically, this tutorial will be using screen captures (screen caps) and screen shots from LibreOffice Calc. We’re highlighting this program because it’s free, offered open access, and the development team shares our interest in making computing simpler and more accessible for everyone.

## Where are our data located on the computer?

The first thing we'll need to explore the cattle bones data in this environment is to locate the data on our computer. It probably didn’t wander off, but we know how cows are. I mean data. We’re talking about data, not cows.

From Open Context, we should have selected a location to download our table to. If it's something like a `Downloads` folder, consider moving it to `Documents` or another good folder so that any additional work we do with the data is saved in a place we'll remember. Maybe make a specific folder just for Open Context Data Stories and tutorials?

![](Images/FileNamingStructure.JPG) 

<i> An example of a good naming structure that will associate all the parts of this project together. </i>

If we aren't in the habit of organizing things into folders, this tutorial is a good time to start. This way if we forget what something is called, or misspelled something, we can still find it based on knowing we're looking for the work we did for this tutorial.

So consider herding it into the same place that we’ve put this tutorial sheet. If we don't have this tutorial saved on our own computer, we might want to, just in case we lose internet access. It’s also helpful to have all of the information for this tutorial and project close to each other, as that will cut down on organizational issues in the future.

## Getting the data into our spreadsheet

Now that we've located where we put that file, we want to open it with our spreadsheet program. There are a couple of different ways to do this depending on the spreadsheet program that we choose to use for this exercise. 

![](Images/OppositeClick.jpg)

<i>After opposite clicking, these options will show up within a Microsoft OS environment set to dark mode.</i>

Opposite clicking (clicking like we would to copy and paste) and choosing `open with` will typically give us the option of selecting a spreadsheet program that we have on our computer. This is because .csv is a common document type that can be read by a variety of programs.

This will open the CSV directly into that program, detecting the separations between the values in each line and detecting that each new line of information should be in its own row. While we've probably seen tables before, usually we've got lines or boxes that separate out the information. In this case, the commas do the separating for us, and if we look at CSVs in a text editor (like notepad) we'll see that each line is reaaaaally long. Also, were we to count the number of items between each comma we'd see that each row had the exact same number.

For the rest of this tutorial, the information in each unique line may be called an entry, a record or a row. The values separated by the commas may be referred to as columns, attributes, or fields, as they are the pieces that describe the specimen represented by the row. Everyone has a different familiarity with these concepts so we want to make sure that we are all in the same pasture. 

![](Images/LibOffCalc_FileOpen.jpg)

<i>In LibreOffice Calc, the open screen will pop up after we click `Open Template.` In this window, we'll navigate to our cattle bones file and click `Open` to start the process.</i>

Otherwise, we'll need to navigate to that location within our spreadsheet program. Typically this will involve heading to a `data` or `import` tab, or an `open files` section within the program. We know the programs on our individual computers best so take a look around. We'll want to select the version that includes importing from .txt or .csv if given the option.

![](Images/LibOffCalc_TextImport.jpg)

<i>Once we open the file, this window will pop up asking how the information in the file is organized. This is where we'll be asked if the information is at a fixed width or delimited/separated by a particular character.</i>

Some programs will detect that the CSV has an established pattern, others will ask us to define how the data are organized. These are usually divided into categories like "fixed width" or "delimited". Sometimes, we may see them called "separated by". Fixed width means that the data are separated based on the number of characters within each column. For example, if there were no spaces within the file but we knew that every 3 characters and then every 5 characters started a new section we could define those typically by clicking on the scale at particular intervals.

A CSV, as the name implies, will use the delimited or separated type. `Delimited` or `Separated by` means that each column or attribute is separated by a specific character that should not appear elsewhere. In this case, it's a comma (,) but it could also be a forward slash (/) or a semicolon (;). As long as it's used consistently we can find that character until the cows come home.

Additionally, the program may ask us to define the `Character set.` Unicode (UTF-8) is a good choice because it can encode characters in most written languages, including Korean and Japanese. Once we've selected those options, the spreadsheet program will break the data into separate columns. The first row will describe the attributes accounted for in the following rows and our program may ask us if these are headings or headers. Say yes.

With that, we've successfully opened our CSV in a spreadsheet program and can start exploring the data! 

## What kinds of questions can we ask?

Moooooovelous, we've successfully added CSV data into a spreadsheet and can see the thousands of entries in this table. With these set, we can explore the attributes available for each row in the table. However, many of our questions depend on what we know about the data. Specifically, lots of the measurements are unique to a kind of bone and might require additional background information to utilize effectively and ask the right questions.

A question about this data set that does NOT require so much other information though is,  "What periods are these bones from?". While an exploration of the Open Context website gives us a basic visualization of [that](https://opencontext.org/subjects-search/?proj=166-neolithic-and-bronze-age-cattle-data-from-switzerland&prop=oc-gen-cat-animal-bone#8/47.040/8.438/11/any/Google-Satellite) (https://opencontext.org/subjects-search/?proj=166-neolithic-and-bronze-age-cattle-data-from-switzerland&prop=oc-gen-cat-animal-bone#8/47.040/8.438/11/any/Google-Satellite) under the image labeled "Chronological Distribution (Years BCE/CE),"  we can look at this questions in other ways.

To do this, we'll need to look at the columns for headings that say something about time. Some keywords might be things like "period," "chronology," "temporal", "time", "date", "early", "late" and other similar words. We'll scroll to the right along the first row to identify those columns specifically.

To focus on the columns related to time, we can choose to "hide" all columns that don't include those words by opposite clicking on the column and choosing the "hide" option. Or, we can delete these columns by opposite clicking and selecting delete.

Should we choose to go the delete route, make sure to locate our `undo` button within our spreadsheet program or the keyboard command for that. Ctrl+Z (or Command Z) will quickly become our best friend. Additionally, deleting columns will alter the data set, so we should probably save our file under a new name so that we don't need to re-download the original file. But if we need to re-download it, no problem, just go back to the head of the herd for the tutorial to follow those instructions.

While we scroll through the headings, how many attributes does each row in this table have? What's the easiest way to tell? Well, we can count how many columns there are, but most spreadsheet programs also label the columns, even if they don't have names. Unfortunately, these are often letters, to offset the numbers that identify the rows. This means after 26 we start getting into double letters like `AA` as names, so we'll need to do a little bit of addition anyways.

Knowing how many attributes each row has can help us understand how many different ways we can separate and explore our data. It can also give us an understanding of what attributes the creator of this data set thought were important to record, which defines what questions we ask. But why is that?

Well, we can't ask questions about information that we don't have data for. So we need to consider our questions within the scope of the data we're given. For example, we might have some questions about the order that these sites appeared in time. This is a really important question because it might relate to larger issues about cattle domestication or hunting practices! However, not all attributes about time allow us to answer that question in the same way or at the same level of detail. Hold that thought though, we’ll come back to that later... 

Now that we've explored how many columns there are and identified the ones that have to do with time, let's pick a specific column to explore in detail. We can select any we'd like and still follow this tutorial, but the displayed results may look slightly different. As a check, we're going to use the `Period` column to explore the data and answer our question. 

## How do we visualize our question as a table?

Awesome, we looked through the various table headings and found the `Period ` column, now we can figure out a few ways to explore what periods are included in this data set. If we're unfamiliar with archaeological terminology, period generally refers to a particular chunk of time in the past. We may see the term "time period" but often that's flagged as redundant and just period or time will suffice. Periods are generally named chunks of time. However, around the world periods can be tricky, but we'll explore that in the supplementary material for this, and in the R based tutorial we’ll be trying after this one.

Even focusing on this one column, the table is pretty intimidating. A list of over 10000 entries is a lot, but we can use filters within a spreadsheet to summarize that data in a variety of ways. Filters allow us to explore a portion of the table at a time. In our case, only the rows that have a particular period associated with them. 

To do this though, we'll first need to filter our table. Filtering will focus on the headings. Headings are typically the first row of a column and describe the kind of information stored in that particular column. When we filter, the program finds all the unique values underneath it and allows us to select a few of these to decide what rows we want to look at. 

![](Images/LibreOfficeCalc_filter.jpg)

<i>In LibreOffice Calc, we can autofilter by clicking on the "Data" tab and scrolling down to "AutoFilter" or we can click the autofilter icon in the main ribbon.</i>

To filter our data, we'll need to find that option. This can be a few places in a spreadsheet program, such as under the `Data` tab or in the main ribbon as an icon. However, if we have trouble finding it we can use our program's help function or the internet to figure out where this feature might be. It may have a symbol like a funnel or an inverted triangle with horizontal lines on it. It may also be an automatic feature that adds a little caret or inverted triangle to the box where the attribute name box is without us doing anything.

Take a few minutes to figure out where this feature is before moving on, as switching between spreadsheet programs (something the author of this tutorial does do with some regularity) can be disorienting.

In the meantime, we're doing great! Using spreadsheets effectively is a skill, and searching for tools like this helps us build these skills to answer questions in our own research in the future. We'll be wrangling this data in no time!

![](Images/LibreOfficeCalc_Carrot-update.jpg)

<i>In the Period box, column V, a caret appears that can be clicked and will open this menu.</i>

Once we've located the filter option, we can click on the caret it adds to the name box to see how many periods there are within that particular column. This helps us understand the diversity of what things can be called within this column. 

Based on this, there are seven (7) periods in this particular table. Looking at them we might not be familiar with exactly what they mean, but they let us know that we can sort all, or at least many of the rows, into these particular categories.

Because we decided to explore the periods that these specimens are from, we're probably interested in knowing how many specimens are from each period. There are a couple of ways that spreadsheets allow us to do this. The first is to do things by hand. However, we don't really want to count and tabulate each row, so what we can do is go through and filter by each period and then note the number of entries. Using these, we can create a new table, by adding a sheet to our spreadsheet, and call it Specimen by Period. Faaaancy!

![](Images/LibreOfficeCalc_newSheet.jpg)

<i>We'll need to click the + button at the bottom of the screen and a new sheet will appear. This image already has new column names we are going to fill out.</i>

In that new sheet, we can create two columns. The first will be `Period` and the second will be `Frequency`. This means that for each period, that is how many specimens are assigned that period. We'll now go back to our original table and start noting down information. We'll want to click the filter caret in the period column and then deselect all of the periods we're not interested in by clicking the check boxes. Un-selecting them all would return us no entries.

![](Images/LibreOfficeCalc_byPeriod-update.jpg)

<i>In this, we limited the number of rows to those that had Bronze Age as their Period. At the bottom, we found the number of rows that were labeled that way</i>

![](Images/LibreOfficeCalc_FillingTable.jpg)

<i>Once filtered, I added the Bronze Age as a row under period and then the number of rows associated with it to the table in the new sheet</i>

Then, we'll want to select one period at a time, clicking the check box, and counting the number of rows. (If we’re lucky, our spreadsheet programs will state at the bottom of the window how many records were returned.) We'll then put the number for each period into the table under frequency. Once we've finished this, we can check that we counted or recorded the correct numbers by adding all the numbers under frequency together. We should get the same number as the amount of records in the original table.

Great! We've made one version of this table. Or we skipped this, which is totally fine, because it's tedious and is prone to errors. To skip the potential for errors we can create this table in a different fashion.

To do this, we'll explore another tool within spreadsheets after we turn off any previous filters. Specifically, we'll create what is called a `Pivot Table`. Pivot tables allow us to summarize information from a more complicated table (like the original cattle bones one) based on parameters we establish. In this case, we want to know how many specimens are from each period. We'll create a pivot table that counts all of those for us, and lets us know how many specimens are from each period. 

To do this, we'll need to highlight the column with the period information, either by clicking the heading above it or manually highlighting all the values in that column. Then we'll want to look for the pivot table option in our spreadsheet program. This might be under data, insert, or elsewhere depending on our program. Take a few moments to explore this before moving forward.

![](Images/LibOffCalc_PivotTable.jpg)

<i>To add a pivot table, we'll once again navigate to the "Data" tab and select pivot table or click the icon in the ribbon.</i>

Once we've found where to make a pivot table, go ahead and click `Create new` or whatever variant options we have and make sure to save it to a new worksheet, sheet, or similar so that we don't replace anything we've already made. This will create a blank pivot table space that should give us options of what data we're using, possibly represented as the columns and rows by an alphanumeric designator within a range such as (V2:V11199), or just by the name of the column. There should be options for filters, columns, rows, and values. 

We will want our rows to be the period column. We'll either drag that selection to that area or check mark it. That should then generate a column with just the period labels we identified previously. We'll then put that information in values, by either dragging or selecting.

Once we do this, the space might automatically populate with a particular function. This function will be a variation of `COUNTA` or `COUNTUNIQUE`. What this does is it adds all the rows together that have that label. Since this column only has alphanumeric values most other functions won't work here. However, doing this will put those frequency values in a column by their appropriate label.

![](Images/LibOffCalc_PivotTableDetails.jpg)

<i>Once we've selected add or edit pivot table we'll need to specify some information in the design window.</i>

If we did these by hand, how do these values compare? Did we cownt them correctly? Hoof we made a mistake? Are they the same?

They should be because the only difference is that we (humans) counted the values versus the computer doing it for us. Hopefully, they look pretty similar. If they don't, take a second to retrace our steps and see what might have happened.

![](Images/Spreadsheets_PivotTableComparison.jpg)

<i>Here's an example of what a completed pivot table should look like in LibreOffice Calc on the left and Excel on the right.</i>

## How do we visualize these specimens as a chart?

However, if they do look similar, congratulations! We made a pivot table by hand and used the automatic functions in our spreadsheet program as well. Using this table, we can look at the distribution of specimens by the period. Specifically, it looks like there are many bones from what's called the `Neolithic`. 

While this table is nice, it doesn't really convey how different these values are. The Neolithic clearly has a lot more values than the other periods, but looking at the numbers doesn't display that magnitude of difference as well as a chart might.

So let's make one.

The most basic way to symbolize the magnitude of difference visually without a table is to create a bar plot, sometimes called a bar chart. Bar plots use the frequency of a particular value to stack up and compare these values visually. Bar charts are a good visualization in this case because we can use the frequencies of the data directly rather than summarizing them using percentages. Additionally, humans are really good at comparing things across straight lines so it plays to our interpretive strengths. To do this we'll utilize the pivot table version of the data to create a bar plot. First, we need to create a chart within our spreadsheet program.

![](Images/LibOffCalc_InsertChart.jpg)

<i>We'll go to insert, or click the create chart button in the ribbon, to start working with the bar chart.</i>

We'll want to look for the area in our spreadsheet program where we can create a chart. This will typically be under `Insert` and will have different options. We'll want to select the bar chart option when we are on the sheet for the pivot table with the label frequencies.

If we're already in there, the automatically generated table will probably pop up populated with the values from the pivot table, named in the appropriate fashion. It might include a "grand total" column but we can ignore that, or we can remove it by changing the data range that the chart includes. However, if it has frequencies charted and labeled in a way that makes logical sense in relation to the numbers in the original table, congrats and we can moove to the next section. (We’re really resisting making a cow pun here, but failed.)

![](Images/LibOffCalc_selectData.jpg)

<i>Once we've clicked the button with the correct information highlighted, we'll need to confirm the automatically generated styles. We need to confirm these before the chart is created, so click next when this or a similar window appears</i>

If it doesn't have frequencies charted and labeled, we'll need to select the values we want to include. To do this, we'll want to select the column with the `Period` labels as the x-axis (or horizontal categories) then the column with the frequencies (or counts or COUNTA or whatever our program called it) as the "series," "y-axis", or whatever is opposite the x-axis. This will then auto fill our table.

And there we have it, a basic chart that visualizes the huge numerical differences between the values in this particular data set when we look at periods. 

## How do we make our visualization a nicer to look at?

Yay! We've got a chart, and while it does show us what we wanted, there are a few things that aren't quite right. It uses complex jargon that we don't need like possibly referring to the frequencies as "COUNTA OF" rather than just frequency or "number of" which make a lot more logical sense.

Additionally, this extends into the y-axis as well. If we wanted to show this chart off, we'll probably want to fix some of these issues. Most spreadsheet programs will now just allow us to click on the area to edit it directly, so why don't we do that.

![](Images/LibOffCalc_insertTitle.jpg)

<i>Use our opposite click to open up this window and get the option to edit the titles in the chart.</i>

Specifically, let's tackle that title for the chart first and replace whatever is there with something like "Number of specimens per period". We may also have noticed that the x- and y- axes could be labeled in a clearer manner. If we have labels there already, just click on the y-axis label and replace it with "Number of specimens".

If we don't have labels for either axis we'll need to look for a section under design or a similar concept that allows us to add a chart element. Then we'll find a section for axis titles that will add these to the chart. For the horizontal or x-axis, we'll want to put "Period" and for the vertical or y-axis, we'll want to put "Number of specimens".

![](Images/LibOffCalc_labelsAdded.jpg)

<i>We can click elsewhere on the chart to add the titles by typing them into a box like this.</i>

We can also play around with these features to change the colors of the bars should we desire. (And let’s be honest, playing with colors on a chart is sometimes the most fun part of the process.) Additionally, we can use the graphic user interface (GUI or gooey) to stretch, pull, and rearrange or center elements. This allows us to resize the chart or chart elements to the specifications that we want. Play around with these features until we're happy with the appearance of the plot.

![](Images/LibOffCalc_labeledChart.jpg)

<i>Here's an example of a chart with the correct labels.</i>

When we're done, hooray cowpoke, we used our spreadsheet program to make a cool looking bar chart from over 10,000 rows of cow bones! Now that it looks nice, based on these charts and the numbers in the tables, what can we say about when cattle bones appear in this data set? What other questions can we ask? What conclusions might we come to?    

Once we've considered these, consider another column that looks at time, such as "Temporal.Coverage.Label." to see if we get similar results. This column is another way that the specimens were assigned time. Do the patterns look the same? Do the names in that column sound familiar?

These should have similar patterns and the names should be familiar because the categories assign the specimens into similar time chunks. However, this column allows for specimens that could not be assigned a specific period between Neolithic and Bronze Age to be assigned generally to the period that includes Neolithic through the Bronze age. These previously would have had "empty" assignments in the period column.  

## What to do now that we've finished

Congratulations! We've successfully used a spreadsheet program to find a file on our computer, loaded that file into a spreadsheet, opened it as a table, explored what columns were in the data, picked one column to visualize, looked at that table in a couple of formats, and played with creating a bar chart based on that data! We did a lot, y'all.

We may have even tried this tutorial again with a different column heading. Now, we might want to save some of our progress. The charts will of course save when we save our spreadsheet, but if we'd like to save them separately we can either copy and paste them into a new document or utilize options that pop up when we opposite click to export the chart. We may need to click on a set of vertical dots (a vertical version of an ellipsis `...`) and have the options pop up there. 

![](Images/LibOffCalc_SaveImage.jpg)

<i>We can opposite click on another part of the chart to export or save it as its own image.</i>

Pick whatever format works best for us and make sure to save it with a good name so we remember what this beautiful chart was for. Or, just save our spreadsheet and come back to it later for comparison.

## Do it again?

Now that we've gone through this process, once, or possibly twice if we explored a different column, could we easily do these steps again? If the data changed, would it be easy to retrace our steps? Or if we wanted someone else to do it could we guarantee that they'd get the same results?

If we answered no to any of these questions it may be worthwhile to explore these data in a different statistical context. Specifically, utilizing a statistical package or coding language to quickly adapt or edit the process that we took. 

If we're not too cow-herdly, we can take the next step with these data and try our R-based tutorial. Or skip ahead to that if we know spreadsheets just aren't for us.

## Key concepts used in this tutorial

`Import from .csv` Learned how to import a different file type into a spreadsheet program to read the table more easily.

`Filter headings` Used the first row/record/entry of information to filter according to other values found in that column/attribute/field.

`Pivot tables` Took the information in one column and turned it into a pivot table that summarizes data from a more expansive table.

`GUI-based figure design` Created a variety of charts to visualize the data from the pivot table in a different fashion.