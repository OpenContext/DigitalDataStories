---
title: "Cow-culating Your Data with Spreadsheets and R - Educator Resource"
author: L. Meghan Dennis, PhD. Postdoctoral Researcher for Data Interpretation and
  Public Engagement
date: "26 May 2022 - Version 1.0"
output:
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

![](Images/CowCulatingHeader-1024x555.jpg)

## contact the team

L. Meghan Dennis  
@gingerygamer (she/her)

Paulina F. Przystupa  
@punuckish (she/their/none)

datastories@opencontext.org

# Exercise Description and Aims

The [Digital Data Stories Project](https://alexandriaarchive.org/data-literacy-program/) promotes an increased focus in archaeological education on digital data literacy. Through the use of open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis. These combined code and theory practicals illustrate the confluence of science and humanities-based investigations in collected data on the past.

This approach promotes multiple levels of engagement with archaeological data sets, linking data-driven narratives with the key analytical and interpretive steps used to ethically analyze, visualize, and present research data.

In this [Digital Data Story](https://alexandriaarchive.org/tag/datastories/), participants will learn to:

> Ask and answer questions with data using spreadsheets

> Ask and answer questions with data using R

> Create data visualizations

> Locate common problems and biases in datasets

> Create narratives for public engagement

## additional texts

The following texts may be helpful in providing context on ancient cattle.

Ajmone-Marsan, P., Garcia, J.F., and Lenstra, J.A., 2010. On the Origin of Cattle: How Aurochs Became Cattle and Colonized the World. Evolutionary Anthropology, 19, pp.148-157.

Schmölcke, U. and Groß, D., 2021. Cattle husbandry and aurochs hunting in the Neolithic of northern Central Europe and southern Scandinavia. A statistical approach to distinguish between domestic and wild forms. International Journal of Osteoarchaeology, 31(1), pp.108-118.

## the data set

This data set allows researchers to select geographically and chronologically suitable comparative data sets when attempting to distinguish wild and domestic cattle remains on sites across Europe. However, it may also be used for many other purposes relating to the study of European cattle from the Middle Pleistocene through to the Medieval period.

These data were collected and analyzed during a PhD undertaken by Elizabeth Wright at the University of Sheffield (UK).

## assessment and scaffolding options

For those students who require (or desire!) a more challenging data exercise, tutorials offered at GLAM Workbench can be utilized as scaffolding.

For those students who require (or desire!) more support, splitting into pairs to complete the Digital Data Story offers the opportunity to work together, switching off tasks and comparing hypotheses and results.

As there are opportunities during the Digital Data Story for students to select tabular data for analysis based on their own hypotheses, the end result visualizations produced may vary. One potential assessment option is to ask students to re-run the analysis with related data sets.

## Technical Requirements

If engaging via their own computer, participants should have a basic knowledge of how to operate a computer, how to visit a website, and how to download and install software.

If engaging via lab computers, participants should have a basic knowledge of how to operate a computer and how to visit a website. Lab computers should have the following programs installed:

- A spreadsheet program such as LibreOffice Calc, Google Sheets, or Microsoft Excel
- R
- R Studio
- A text editor (like TextEdit or Notepad)
- A PDF reader

The data set that participants will be using in this exercise is available at: 

https://doi.org/10.6078/M7TX3C9V

## Duration

The Digital Data Story is given in three parts. Each part should take approximately 1 hour. Each part can be completed separately, though they are additive when completed in total.

If participants are completing a portion of the tutorial in more than one computer session, please direct them to pay close attention to the in-text directions on how to save their work!

## get in touch!

If you or your students have questions, comments, or concerns, please get in touch. We love feedback, especially from educators!

License: [Creative Commons CC-BY](https://creativecommons.org/licenses/by/4.0/)