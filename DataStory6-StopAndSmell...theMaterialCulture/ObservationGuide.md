---
title: "Stop and Smell the...Material Culture?: An Object-Oriented Observation Guide"
subtitle: "Observation Guide"
author: "Paulina F. Przystupa and L. Meghan Dennis"
date: "`r Sys.Date()`"
output:
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

![](Images/SASTMC_TrowelFlowersImage_cropped.jpg)

# Guide Overview
<newline>

1. Introduction
  + What is slow-observation?
  + How does this relate to archaeological data literacy?
1. Selecting your belonging
1. The exercise
1. Concluding the exercise 
1. References and further reading  
1. Credits
1. Acknowledgements
1. Observation form

Estimated time to read this guide: 15 minutes

Estimated time to complete the exercise: User determined

# Introduction

Welcome (back) to the [Digital Data Stories](https://doi.org/10.6078/M74F1NW0) of the Alexandria Archive Institute and Open Context! This Data Story focuses on how learning to observe a specific belonging can cultivate archaeological data literacy. To get started, this guide provides an introduction to a solo-practice observation exercise by exploring concepts from slow-looking, which is the movement that underlies what we call slow-observation. We do this to help participants understand the practice of slow-looking and use it in other contexts, such as in group exercises to teach archaeological data literacy-relevant skills.

This Data Story will help you conduct a series of data literacy-focused archaeological observations of material culture. In addition, you can use the guide as a series of optional elements to integrate into a class exercise or for personal enjoyment. You can observe the same piece of archaeological material culture or try it out with your own belonging multiple times---each time in a new way! All of the suggestions and exercises in this guide have been tested by the [Data Literacy Program](https://doi.org/10.6078/M70P0X5P) (DLP) team or are adapted from existing slow-observation exercises in other fields, such as the work done by [Project Zero](https://web.archive.org/web/20240530171836/https://pz.harvard.edu/search/site/slow%20looking). As a supplement to these exercises, you can explore further slow-looking resources via [Thinking Museum](https://thinkingmuseum.com/2020/12/02/simple-ways-to-practise-slow-looking-every-day/), [Project Zero Guide](http://www.pz.harvard.edu/sites/default/files/Slow%20Looking%20-%207.11.2018.pdf), and [Slow Art Day](https://www.slowartday.com/about/tools/)---some of whose works are the basis and inspiration for this exercise!

For this [*Solo Series*](https://web.archive.org/web/20230525170050/https://alexandriaarchive.org/2023/05/25/solo-steady-and-structured-data-literacy/) Data Story, you will conduct eight observation sessions with the same object of your choosing. Each session provides a specific method to use or attribute to focus your observation of that same object. To practice formalizing these observation sessions, at the end of each round take notes about what you observe. To formalize this practice, you can fill in the boxes provided after each observation round within this Data Story, complete the Data Story's observation form (included at the end of this guide) or or record your observations in a personal notebook or journal or via a recording device, such as your phone.

If you don’t use the included observation boxes or form try to capture the same kinds of observations each time. You could keep a written journal, an audio journal, a vlog, write or perform a poem, or use illustration. You can even use different note taking styles each round! Should you vary your note-taking style, consider how different kinds of note-taking influenced what you noticed about your belonging. For example, what recording technique made you feel like you knew the object well? Regardless of format, make sure you can revisit these notes and observations at the end of this Data Story to compare what you noticed each time. This is a great way to see how you exercised your observational skills to better understand your belonging and its relationship to yourself and others.

After completing the eighth exciting event, you can compare your notes to see how these practices helped hone your slow-observation skills with your belonging. Slow and careful observation of archaeological materials is an important skill for archaeological analyses because it’s how we create data to say something about the past. Furthermore, the process of standardized recording is an archaeological data literacy skill on its own.

To expand your archaeological data literacy practice, you can turn this exercise into a structured data set using your notes as an adaptation of the [*Gabbing about Gabii: Notes to Data to Narrative*](https://doi.org/10.6078/M7DV1H1R) Data Story. Use your collection of observation forms, or your notes, instead of the Gabii forms that the original Data Story instructs you to use. 

*If using this as part of classroom exercise, the notes from multiple participants can be combined to generate the data set instead.*

## What is slow-observation?

For this Data Story, we define slow-observation as taking the time to focus on observing a particular thing and exploring various ways to observe it so that we can take in as much as possible about the focus of our attention. Similar to other calls for slowing down, such as the [slow food movement](https://www.slowfood.com/), [slow science](https://en.wikipedia.org/wiki/Slow_science), and [slow journalism](https://www.nationalgeographic.org/projects/out-of-eden-walk/your-own-walk/), this guide encourages participants to slow their pace and take time with a particular thing. This guide relies heavily on slow-looking-related and adapted practices. However, slow-looking, and our use of its related terms, more accurately expands the idea of looking mindfully or with intention, and includes using all our senses, rather than focusing on sight alone.

Furthermore, the process does not actually need to be slow in and of itself. Instead the practice is meant to encourage people to draw on their senses and observe something *deeply*. This can take time, as in the exercise conducted by Eva Mol with Herbert the pot (Mol 2021 or watch her presentation about the activity [here](https://web.archive.org/web/20240308200548/https://www.youtube.com/watch?t=4027&v=ziuma1eFLf4&feature=youtu.be)), or it can be brief and timed, like in the [Ten times Two (10 x 2) exercise ](http://pzartfulthinking.org/?p=90) (Tishman 2017). Regardless of the pace, the intention is to select something and focus on specifically observing it.

In some ways, this can be considered a meditative practice, wherein people take time out to do something that’s good for them (Harper 2017). Sitting intently with a particular focus is often a nice break from the demands of multitasking, and meditative and mindful practices have great health benefits (Harper 2017). This is also an important aspect of archaeology in many realms and can be used as a grounding exercise outside of archaeology as well.

## How does this relate to archaeological data literacy?

The ability to observe as much as we can about something is one way to cultivate archaeological [data literacy](https://doi.org/10.6078/M73F4MRM). To create archaeological data, we often begin by observing just one belonging, artifact, ecofact, or piece of material collected at a site. This practice is what has generated many of the open data sets on Open Context!

But how do we know what to observe? What you’ll do in this exercise is explore some observation principles that teach you what to observe, why, and how evaluating what you notice may help or hinder future researchers interested in re-examining or evaluating the subject of your observation. This series of observations can act as their own data set that considers how different ways of observing material culture generates various understandings about that particular belonging. It’s also an exercise in embracing and acknowledging subjectivity in archaeological observation. Some of the ways we ask you to examine your own belongings may seem weird, but it’s important for you to remember that every observer brings with them their own life histories. These influence what and how we observe the world and its contents around us—this diversity is a good thing!

An example of this exists in something as precise as measurement. How do we choose to [round](https://en.wikipedia.org/wiki/Rounding) values? Particularly, how do we choose to round a value that is exactly *halfway* between two other values? Is this a choice made by the observers, who may round up or down at their discretion? Is it a rule, set by the project at some level with a particular rationale? Or, is it a learned rule, that some people follow but that others don’t? Maybe there’s a scientific standard that the project follows for this? All of these possibilities exist, sometimes even within the same observational cohort. Therefore, being aware of this range of potential differences and knowing it exists helps us to understand how our unique observation skills influence archaeological data and aid us in improving our literacy.

# Selecting your belonging

For this archaeological data-inspired practice, we want you to take the guess-work out of what portable piece you’ll be observing. This way, you can focus on honing the observational skill that you can use for almost any kind of archaeological material. The first step in this process is to select which belonging you would like to use for this exercise. It can be anything that you have in your possession. It doesn’t have to be exciting or unique, maybe it’s just the first object that you can think of that you have in front of you, or the first thing your hand touches when you reach out after closing your eyes.

We think an element of randomness is important to this choice because when working with a real collection of archaeological data, or uncovering archaeological remains or [belongings](https://doi.org/10.6078/M7639MVB), we don’t get to choose what we look at. Sometimes these things are very exciting, like whole ceramic vessels or preserved pieces of fabric, but sometimes...they’re a pile of less than 1 cm lithic debris...and there’s kilos of them to process.

In addition, don’t overthink it, because at some point you just need to pick something and explore it. The process of practicing observation is less about the object itself and more about cultivating the ability to use the senses to read that piece.

*Note: If using this in a group setting, educators may provide objects or ask participants to use an object that they've brought with them.*

# The exercise 

Because you are already familiar with this belonging (it’s yours!) it might seem difficult to approach it in a new way. To guide you, we’ve laid out eight observation rounds with different questions, goals, or foci to get you observing your belonging in different ways.  We adapted portions of “[‘Trying to Hear with the Eyes’: Slow Looking and Ontological Difference in Archaeological Object Analysis](https://www.tandfonline.com/doi/full/10.1080/00293652.2021.1951830)” (Mol 2021), incorporated other slow-observation practices, and made up a few of our own.

Although slow-observation often takes the form of a group exercise---in a classroom (Mol 2021), on a tour (Tishman 2017), or with a partner (Horowitz 2013)---we designed this Data Story as a solo practice. Therefore, we separated and reorganized existing slow-observation exercises and provided specific questions for people to answer. This acts in lieu of the conversational experience we might have with another human person or non-human person who’s observing alongside us. Instead, the Data Story is our partner!

*Note: We provide group alternates for each round to aid in classroom adaptations of this Data Story should educators desire (or require) them.*

Experienced archaeology students will note that some observation rounds are similar to standard scientific archaeological observations. Other rounds diverge from this standard to remind us that “objective description” (such as those that focus on measurable quantities or qualities) is only one part of archaeological analysis and one based in Western perspectives on knowledge. It's always great to remember that descriptions are a starting point that allow us to build narratives that say something about the past.

Other students, archaeology and non-archaeology alike, may also notice that our suggested observation methods are similar to mental health grounding techniques (Harper 2017). Specifically, these rounds will ask us to mindfully or meditatively observe something in our present physical environment. This overlap is a great reminder that skills we practice in everyday life can help us be good archaeologists. And also that techniques we apply in archaeology can also help us in our everyday lives.

## 1. The first formal observation

For the first round, use the ten by two exercise (aka Ten times Two or 10 x 2, you can get a guide [here](http://pzartfulthinking.org/?p=90)). Begin by setting a timer for yourself for 30-60 seconds. If you require (or desire) more time, you can set the timer for longer but don’t give yourself more than 5 minutes. Too long and you might start to drift from the purpose of this exercise.

Within that time, list ten things you observe about your belonging (size, shape, weight, color, material, inscriptions, designs, textures, etc. are examples). Take note of those observations in a journal, voice recorder, in the boxes below, or on the *Stop and Smell the…Material Culture?* observation form. Once you’ve noted ten observations or your timer has gone off, take a bit of a break to stretch your body and your mind. Then set the timer again, for the same amount of time, and note *another* ten things you observe about your belonging that you had not previously noticed.

![](Images/SASTMC_Obs1a.jpg)

![](Images/SASTMC_Obs1b.jpg)

*To adapt this to an in-person group setting, you can do this exercise verbally, with a single note-taker, while passing the object around and then make copies of the group’s collected 20 observations.* 

## 2. Sensing the second session

Once you have a list of 20 observations, take a few minutes to sort those observations by what sense you used to observe that quality. Focus on sight, sound, touch, and smell as your main categories and see which sense comes out as the most used. Once you’ve identified your most used sense, do another observational round where you purposefully explore qualities or attributes of the belonging using your other senses.

So that you don’t lose interest or steam, set a timer or goal for yourself to guide you as you finish this round. It could be taking 10 minutes to identify more qualities or pushing yourself to find 20 more attributes. Either way, take the time to meet this goal and explore what more there is to sense about your belonging. In your notes, identify these observations as Observation 2 and add them to your record.

![](Images/SASTMC_Obs2.jpg)

*To do this in a group, break the group up into different ‘sense teams’ for sound, touch, and smell. Then have them take 10 minutes to notice new qualities about the belonging or note 20 more things as a group.*

## 3. Turning the tables for your tertiary take

For this round, because this is *your* belonging (or at least you had access to it), how or why can you call this piece that, a belonging? During this observation round answer these questions as part of your notes about the belonging:

+ What role does this belonging play (or not) in your life and in the assemblage that reflects you as a person? What is your relationship to this object? How is it in relation to you (in space, in time, in story)?

+ How might you consider (or not) this piece part of your family and who in your family does it remind you of? If not a part of your family, who is in this belonging’s family? Who are its ancestors, descendants, and siblings?

+ What perspectives are you using to define “belonging” generally and in relation to the piece you’re observing for this exercise? How does it relate to ownership? How do definitions of belonging, relationship, and ownership change in terms of different frameworks or cultural perspectives?

+ Is there any quality you observed on your belonging during the preceding rounds that could indicate what kind of belonging, relationship, or ownership framework is appropriate for it? What would make the object no longer your, or someone else’s, belonging?

While these might seem rather intimate questions (don’t worry we won’t check your work), they remind us that our belongings have their own lives with and without us and that material culture is a product of lived experiences and part of lived experiences. We can consider this an assessment of the relationship the piece has to you or whoever it belongs with or to.

It can also be a consideration of who “owns” the object in the sense of legal ownership or property rights. These are important to consider in archaeology both in terms of understanding the use of the belonging in the past as well as a consideration of who would be the modern steward of it after excavation and potential removal from a piece’s resting place.

You can add these answers to your own notes as Observation 3, write them on the observation form, or take drawn your answers in the expanded space included on the next page. 

![](Images/SASTMC_Obs3copy.jpg)

*To adapt this to a group setting, have participants first answer these questions on their own on a piece of paper or note-taking device. Then have them discuss their responses with a partner and follow that with a group discussion. Take notes of the group’s answers and consensus on the group note sheet for the whole exercise that will be given to all participants.*

## 4. The fourth finding forage

For this round, leverage a different note-taking system to record your observations. Have you been doing different perspective sketches that capture specific traits? Try writing a poem from the perspective of your belonging. Been composing little ditties to capture its size and shape? Try writing some notes as if you were a scientist. Have you been dutifully observing everything in a table, like you think an archaeologist should? Write a few limericks capturing its personality or take a selfie to capture it for posterity.

This round reminds us that *how* we choose to record our observations (using text, images, music, 3D models, etc.) can both guide us to record certain things consistently and can constrain what we think is important to observe. Knowing when to use different kinds of recording techniques is an important skill to ensure we record as much as possible about our belongings and anything we find in the archaeological record.

![](Images/SASTMC_Obs4.jpg)

*If adapting this to a group setting, have all participants draw the object on their own so that each has a visual representation of what the piece looked like for themselves.*

## 5. The fifth fictitious function

For this round, you’re going to role play and observe this belonging in another context, as someone else, or as *something* else. You can either place the belonging in different contexts around the room, observe the belonging through the perspective of someone else (such as a child, an artist, or a barista), or imagine yourself as something else in your room observing your belonging (such as a chair, a piece of art, or lamp).

Note at least five things you observe about the belonging, considering what the belonging looks, sounds, smells, or feels like in that new place or as this new observer. Exploring various perspectives reminds us how our worldview shapes what we notice about the things in our lives and how context, the relationship between this piece and others in the physical world, may alter how we understand its purpose.

![](Images/SASTMC_Obs5.jpg)

*If working in a group, generate a list of five different roles or perspectives that they think would be interesting to explore or use those suggested by Mol (2021:91): a child, a tree, a scientist, a factory worker, a landscape painter, an art buyer, and the table the object was standing on. Note at least 15 observations made from these new vantage points on the communal notes sheet.*

## 6. The sixth sensory story

Now that you’ve had a chance to take a different perspective (and still observe your object), it’s time to put yourself in the shoes of the belonging you’ve been working with.

What can you see, feel, hear, sense, etc.? What does it feel like to inhabit your shape? What does your condition feel like on the inside? What does the world around you feel like and how do you feel or sense it? How do you feel about this human (you) spending so much time with you? 

Write a paragraph, draw a few sketches, or record a couple of minutes of voice commentary to capture this impression. This practice reminds us that as humans we often interact with our objects as if they are alive or have agency to influence our lives.

![](Images/SASTMC_Obs6.jpg)

*If conducting this as a group exercise, have each person state one thing that they think the object thinks about the group as a whole as if the group is an assemblage of materials. Note down each observation the group makes from the perspective of observed belonging.*

## 7. The seventh subjective survey

Now that you’ve recorded so many things about this belonging, take this round to combine those together into a narrative in your records about the people related to the belonging. Assume that this was the only piece of material culture left of your (or whomever’s piece this was) existence.

As you write your narrative you can consider questions such as what does the belonging say about you as a person or about the group(s) you identify with? What questions might future archaeologists pose based on discovering this belonging? What sort of emotion do you hope this belonging might inspire by those who find it? Or for a bit of fun, why might people think the piece haunted and were you to haunt this object, why this one? 

![](Images/SASTMC_Obs7.jpg)

*If conducting this exercise in a group, consider this belonging as emblematic of the whole group. Discuss as a group what narrative the piece tells about them together and include that in the group notes.*

## 8. The eighth exciting event

To see how your observational skills have expanded over the course of this Data Story, you’ll repeat the ten by two exercise from the first round for this last round. Alongside all the previous rounds records, this will conclude your detailed description of this belonging.

![](Images/SASTMC_Obs8a.jpg)

![](Images/SASTMC_Obs8b.jpg)

*For an in-person group, do this verbally and add these final 20 observations to the group’s notes for sharing after concluding the Data Story.* 

# Concluding your observation session

Now that you’ve exercised your observation skills a number of times, whether you completed this within a day, a week, a few weeks, a month, or a few months; you have information from multiple observation sessions and perspectives exploring different attributes of the same belonging. Review all of your observation entries and consider what they tell you about your belonging. Then answer the following questions and include it with your other notes.

+ Were there similarities between the ways you observed your belonging? What did you notice that was different?

+ How did you observe the same belonging in different ways to notice different features or attributes of it? Which perspectives or context provided the most insight to the piece?

+ What kind of data did you create based on your observations? Would you consider these quantitative or qualitative assessments? If you wanted someone to note the same qualities you observed for another piece, what would you ask them to record and how would you structure that to ensure similarity between your record and theirs?

Take the time to review what you observed and remember that these are the same skills you’d use as an archaeological analyst, just in a different context. (And remember that archaeologists are all about noticing different contexts!)

If you’d like to turn the descriptions from this practice into structured data (expanding your archaeological analysis practice), you can use the methods from the [*Gabbing About Gabii: Going from Notes to Data to Narrative*](https://doi.org/10.6078/M7DV1H1R) Data Story to turn your notes into a table. *Gabbing About Gabii* explains how to turn field forms (which are similar to the observation form included with this guide) into a table. If you chose to record your observations for this Data Story in a different fashion, such as using voice, *Gabbing About Gabii* provides guidance on identifying data types, selecting appropriate attribute headings, and how to record observations for specific attributes which you can use to separate your observations and create structured data.  

Once you've got these observations organized into a structured table, you can use that to repeat *Stop and Smell…the Material Culture?* with another belonging to practice your observation skills and build your own data set. After doing that, consider trying the *Gabbing about Gabii* narrative guide to write about your belonging(s)!

Even if you don’t turn your records from this Data Story into structured data, you can repeat this exercise to practice your observation skills on new materials whenever you travel to a new place or pick up a new piece. Or whenever you come across your belonging again in the future!

# References and further reading
These references are listed in order of appearance in the text or are suggestions for further reading on the topics presented. If any URLs navigate to a broken page please check the[ Wayback Machine](https://web.archive.org/) for an archived copy of the material.

The Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0): <https://creativecommons.org/licenses/by-sa/4.0/>

Check out other Digital Data Stories here: <https://doi.org/10.6078/M74F1NW0>

Find out about the other work that the Data Literacy Program does here: <https://doi.org/10.6078/M70P0X5P>

Explore to explore Project Zero and it's resources begin with: [http://www.pz.harvard.edu/search/site/slow%20looking](https://web.archive.org/web/20240530171836/https://pz.harvard.edu/search/site/slow%20looking)

Consider how you can incorporate slow looking into your daily life through the Thinking Museum: <https://thinkingmuseum.com/2020/12/02/simple-ways-to-practise-slow-looking-every-day/>

Learn more about Slow Art day here: <https://www.slowartday.com/about/tools/>

Learn about Data Stories in the Solo Series here: <https://web.archive.org/web/20230525170050/https://alexandriaarchive.org/2023/05/25/solo-steady-and-structured-data-literacy/>

*Gabbing about Gabii: Notes to Data to Narrative* by Paulina F. Przystupa and L. Meghan Dennis (2022)
<https://doi.org/10.6078/M7DV1H1R>

Find more information about the slow food movement here: <https://www.slowfood.com/>

Consider the benefits and costs of slow science by learning more here: <https://en.wikipedia.org/wiki/Slow_science>

Read about some examples of slow journalism here: <https://www.nationalgeographic.org/projects/out-of-eden-walk/your-own-walk/>

"‘Trying to Hear with the Eyes’: Slow Looking and Ontological Difference in Archaeological Object Analysis" by Eva Mol (2021)
<https://www.tandfonline.com/doi/full/10.1080/00293652.2021.1951830>

Check out Eva Mol's presentation on Herbert the pot here (it should be about 1:07:07 into the video): <https://web.archive.org/web/20240308200548/https://www.youtube.com/watch?t=4027&v=ziuma1eFLf4&feature=youtu.be>

Find a guide for the Ten times Two (10 x 2) exercise here: <http://pzartfulthinking.org/?p=90>

*Slow Looking: The Art and Practice of Learning Through Observation* by Shari Tishman (2017)
<http://www.pz.harvard.edu/resources/slow-looking-the-art-and-practice-of-learning-through-observation>

*Unfuck Your Brain: Using Science to Get Over Anxiety, Depression, Anger, Freak-outs, and Triggers* by Faith G. Harper (2017)
<https://www.faithgharper.com/>

Explore our definition of data literacy here: <https://doi.org/10.6078/M73F4MRM>

Learn more about rounding in general on Wikipedia: <https://en.wikipedia.org/wiki/Rounding>

Explore why we use the word belongings instead of artifacts in our work: <https://doi.org/10.6078/M7639MVB>

*On Looking: Eleven Walks with Expert Eyes* by Alexandra Horowitz (2013)
<https://alexandrahorowitz.net/On-Looking>

*How to See* by Thích Nhất Hạnh (2019) 
<https://www.parallax.org/product/how-to-see/>

Gain access to any broken links by getting an archived copy from the Wayback Machine: <https://web.archive.org/>

# Credits

Unless otherwise specified this work and its components are shared under a [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license. To attribute this work, please use our suggested citation:

Paulina F. Przystupa and L. Meghan Dennis, 2024, “Stop and Smell the...Material Culture?: An Object-Oriented Observation Guide”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: <https://doi.org/10.6078/M7FJ2DXR>.

In order of appearance, this Data Story includes: "SASTMC_TrowelFlowers" and "Small black trowel" by L. Meghan Dennis from the Data Literacy Program adapted from vector images with provenance and attribution information forthcoming. All other images included in this text are logos for our sponsors or our own logo. 

# Acknowledgements

This Data Story is a collaboration between the producers of the Data Story, our testing audiences, and our open peer reviewers. We are grateful to all the work that people in the existing slow-looking movement have done to inform our assembly of this exercise, starting with Dr. Eva Mol’s short presentation on the topic during the Teaching and Learning conference hosted by Dr. Karina Croucher and Dr. Hannah Cobb.

The Data Stories are part of the overarching Data Literacy Program, with support from a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.

![](Images/StopAndSmellTheMaterialCulture_ObservationForm_Page_1.jpg)

![](Images/StopAndSmellTheMaterialCulture_ObservationForm_Pg_2.jpg)