![](Images/StopAndSmellTheMaterialCulture_CodebergHeader.jpg)

*This* Solo Series *Data Story is now published.*


This exercise is best suited to those with an interest in belonging or [material culture](https://doi.org/10.6078/M7639MVB) analysis, archaeological data collection, or using mindful observation skills for archaeology and daily life. Users should have a basic understanding of observation and recording methods, such as handwritten, audio, or video recording, but little previous experience with archaeology or data collection is required.

This page provides access to the resource in two ways. The first is through a series of digital documents that represent the completed Data Story. The documents include the:

1. [Observation Guide](https://doi.org/10.6078/M73B5X8D)
1. An optional [observation form](https://doi.org/10.6078/M7M043JW)
1. [Teaching Guide](https://doi.org/10.6078/M7ZK5DTJ)

A single PDF for the combined Data Story materials is available [here](https://doi.org/10.6078/M7QR4V8X). This repository is secondary access to the above listed materials and the files in this repository may be altered and forked for custom use in different contexts. These markdown files represent the source material for the text and primary images within the PDF and digital documents, minus additional formatting and with only a general placement of images. Acess to this code reflects our commitment to open science and transparency in our process.

These materials, either through this code, the single PDF publication of record, or the dynamic digital documents, are designed to be used synergistically. However, any piece of this Data Story may be used separately or re-ordered according to the requirements of the individual or for specific educational goals. In addition, this Data Story references [*Gabbing about Gabii: Going from Notes to Data to Narrative*](https://doi.org/10.6078/M7DV1H1R), specifically,*[Part One: Notes to Data](https://doi.org/10.6078/M7T43R64)*. *Stop and Smell...the Material Culture* resources are available free to use under a Creative Commons Attribution-ShareAlike ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)) license. The resources for *Gabbing About Gabii* are under a Creative Commons Attribution-ShareAlike ([CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)) license. 

Thank you so much for utilizing our educational resources! Although this Data Story is *published*, if you or your participants have the time, please consider contributing an [open peer review](https://doi.org/10.6078/M72B8W5G) for our ongoing review process. Such updates will be noted below and first available through the digital documents and Codeberg repository prior to PDF updating.

To attribute this Data Story or any of it's parts, please use our suggested citation:

    Paulina F. Przystupa and L. Meghan Dennis, 2024, “Stop and Smell the…Material Culture?: An Object-Oriented Observation Guide”, Data Stories Program, Alexandria Archive Institute / Open Context, Published Online, DOI: https://doi.org/10.6078/M7FJ2DXR

***Page first published:*** *November 2023*

***Page Updated:*** *5 June 2024 – This Digital Data Story completed peer review in 2023 and, after revision, is now published. Major updates included reordering the observation rounds and adding more spaces for interaction with the document. 11 June 2024 – URL shortened due to slight name change for the Data Story and updated images. 6 November 2024 – Added suggested citation and updated the wording in this README to reflect textual updates to the webpage entry of this Data Story such as to include the image credits. 25 February 2025 - Funding acknowledgement altered.*

***Image credit:*** *“[AAI-OC_DataStoryPublished_Header-StopAndSmellTheMaterialCulture](https://alexandriaarchive.org/wp-content/uploads/2024/06/AAI-OC_DataStoryPublished_Header-StopAndSmellTheMaterialCulture-1.jpg)” by Paulina F. Przystupa from the Data Literacy Program (DLP) / [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). It includes an adaptation of “SASTMC_TrowelFlowers” by L. Meghan Dennis from the DLP from vector images with attribution and provenance information forthcoming.*

The Data Stories are part of the overarching Data Literacy Program, with support from a National Endowment for the Humanities Challenge Award and a grant from the Mellon Foundation (2021-2025). Any views, findings, conclusions, or recommendations expressed in this work do not necessarily represent those of the National Endowment for the Humanities or the Mellon Foundation.