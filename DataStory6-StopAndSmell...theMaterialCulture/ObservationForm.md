---
title: Stop and Smell the...Material culture? - An Archaeological Data-centric Slow
  Observation Exercise - Observation Form
author: "L. Meghan Dennis, Postdoctoral Researcher"
date: "`r Sys.Date()`"
output:
  html_document:
    df_print: paged
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Observation 1A  

1.
1.
1.
1.  
1.
1.
1.
1.  
1.
1.

Observation 1B 

1. 
1. 
1. 
1.  
1. 
1. 
1. 
1.   
1. 
1. 

|Observation 2|
|-------------|
||
||
||
||

|Observation 3|
|-------------|
||
||
||
||

|Observation 4|
|-------------|
|| 
||
||
||

|Observation 5|
|-------------|
||  
||
||
||

|Observation 6|
|-------------|
||
||
||
||

|Observation 7|
|-------------|
||  
||
||
||

Observation 8A

1.
1.
1.
1.  
1.
1.
1.
1.  
1.
1.

Observation 8B  

1.
1.
1.
1.  
1.
1.
1.
1.  
1.
1.



