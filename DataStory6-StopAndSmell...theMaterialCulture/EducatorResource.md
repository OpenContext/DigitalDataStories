---
title: "Stop and Smell the...Material Culture?: An Object-Oriented Observation Guide"
subtitle: "Educator's Resource"
author: "L. Meghan Dennis, Postdoctoral Researcher"
date: "`r Sys.Date()`"
output:
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

![](Images/StopAndSmellTheMaterialCulture_CodebergHeader.jpg)

# Exercise Description and Aims

The [Digital Data Stories Project](https://doi.org/10.6078/M74F1NW0) promotes an increased focus in archaeological education on digital data literacy. Through the use of open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis. These practicals illustrate the confluence of science and humanities-based investigations in collected data on the past.

This approach promotes multiple levels of engagement with archaeological data sets, linking data-driven narratives with the key analytical and interpretive steps used to ethically analyze, visualize, and present research data.

![](Images/SASTMC-WordCloud.png)

In this [Digital Data Story](https://doi.org/10.6078/M7FJ2DXR), participants will learn to:

+ Understand belongings as artifacts in various contexts
+ Examine artifacts from the anthropocene
+ Document archaeological observations
+ Categorize observations into groups through multiple media
+ Synthesize multiple observations into description

## additional texts

Eva Mol (2021) 'Trying to Hear with the Eyes': Slow Looking and Ontological Difference in Archaeological Object Analysis, *Norwegian Archaeological Review*, 54:1-2, 80-99, DOI: <https://doi.org/10.1080/00293652.2021.1951830>

Paul Graves-Brown (2011) Touching from a Distance: Alienation, Abjection, Estrangement and Archaeology, *Norwegian Archaeological Review*, 44:2, 131-144, DOI: <https://doi.org/10.1080/00293652.2011.629808>

## the data set

In lieu of a specific data set, in this [Digital Data Story](https://doi.org/10.6078/M7FJ2DXR) we encourage participants to use their own [belongings](https://doi.org/10.6078/M7639MVB) to create a bridge between archaeological method and personal understanding.

This breadth of options allows researchers to select objects that line up taxonomically with their research interests. However, this method may also be used for many other purposes relating to the study of archaeology and heritage in general.

## assessment and scaffolding options

For those students who require (or desire!) more support, splitting into pairs to complete the [Digital Data Story](https://doi.org/10.6078/M7FJ2DXR) offers the opportunity to work together, switching off tasks and comparing observations and associations.

As there are opportunities during the [Digital Data Story](https://doi.org/10.6078/M7FJ2DXR) for personalization, the end results produced may vary. One potential assessment option is to ask students to re-attempt the Data Story with different belongings, or to swap belongings between students and attempt observations on the belongings of others, with comparative discussion to follow.

## Technical Requirements

This exercise is about as low-tech as they come. Participants should have a notebook, journal, or some other method of recording observations—voice recorders or laptops are fine, as long as their technical operation doesn’t distract from the process of focused observation.

## Duty of Care

Because students or participants may be working with items of personal significance, it is important to ensure a safe and emotionally secure environment. Introspective observation has the potential to be revelatory and engaging, but can also tap into memories or experiences that have more negative connotations.

Being aware of how students are responding to their observations is key to maintaining the duty of care inherent as an educator.

## Duration

The Digital Data Story is variable in duration. Some participants may choose to do the exercise within a 30 minute session, while others may embrace the “slow” aspect of the process and take longer with some portions of the exercise.

If students are completing a portion of their observations via more than one session, please direct them to pay close attention to where they left off, and the specific guidance related to the step they ended on.

### get in touch by contacting the team

Please reach out if you or your students have questions, comments, or concerns, please get in touch. We love feedback, especially from educators!

Paulina F. Przystupa  
@punuckish (she/their/none)

L. Meghan Dennis  
@archaeoethicist (she/her)

<datastories@opencontext.org>

**Licensing:** "Stop and Smell…the Material Culture Educators Sheet Text" and "SASTMC-WordCloud" by L. Meghan Dennis are original works from the Data Literacy Program licensed [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). "AAI-OC_DataStoryPublished_Header-StopAndSmellTheMaterialCulture-Educators" by Paulina F. Przystupa from the Data Literacy Program is licensed [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) and includes an adaptation of “SASTMC_TrowelFlowers” by L. Meghan Dennis from the Data Literacy Program adapted from vector images with attribution and provenance information forthcoming. 
