---
title: "Gabbing about Gabii: Going from Notes to Data to Narrative"
subtitle: "Notes to Data"
author: Paulina F. Przystupa, data visualization and reproducibility postdoctoral
  researcher
date: "10 June 2022 - Version 1.0"
output:
  word_document: default
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Tutorial overview

In this tutorial we will learn to:

1. Search Open Context (OC) for a specific data source and download multiple PDF files
1. Open one context sheet and evaluate how the observations might be transposed
1. Create a table based on observations from the context sheet
1. Fill in the table based on observations from the context sheet
1. Select a different context sheet and add those observations to the table

## Key concepts used in this tutorial

- data structure
- paperwork or documentation
- context sheets
- columns, attributes, headings
- entries, rows, observations
- data types

## Introduction

Welcome (back) to our digital data stories! In this tutorial, we’re going to look at something a bit more unstructured than our usual tutorials. Rather than utilizing a ready-to-go data set, we’re going to build one ourselves. Building data sets can be a complicated task. We’ll walk through all the steps together before trying it on our own.

You may be wondering, why would we do this? With so many archaeological data sets out there, why bother creating our own? Well, as much as we like the well-curated collection of data sets that OC shares, data sets like that are...not that common.

What’s more likely is that whenever we start a project, we’ll need to decide everything about that project. We’ll need to decide how we collect our data, how we observe things, as well as other logistics. These will typically be driven by our research questions but may also be driven by other needs, like when excavation is necessary to preserve something from the site that might be otherwise completely destroyed.

We don’t want to mimic a whole excavation through tutorials (at least not yet) so we’re skipping some steps to focus on an undervalued, but essential, part of archaeology: paperwork.

After all, we’re all here because we love sorting through mounds...of dirt stained pages of notes.

## Picking a project

Leaving aside decisions about *data collection*, we're going to jump to the act of *data collation*. With that, we can go from Notes (aka paperwork) to Data (aka something to be analyzed) and then from Analyzed Data to Narrative (the story of a site). To do this we’ll be using context sheets, a particular form of structured note-taking, from the site of Gabii, which is located in Italy.

But before we do this, we’ll need to Rome around OC for this particular project. We’ll do this by going to the [website first](www.opencontext.org) (www.opencontext.org) and putting Gabii into the search bar at the top of the page.

![](Images/OpenContextMainPage_arrow.jpg)

<i>This is where we can find the search bar on the OC website.</i>

Once we put Gabii into the search bar, and click search, we should be navigated to a page that looks a bit like this:

![](Images/Gabii.jpg)

<i>Here’s what it should look like once we’ve searched for Gabii. The image may be more or less zoomed out depending on screen resolution and window size and that’s ok!</i>

Here, we’ll scroll down to a section that is called “Filtering Options” and select from several choices.

![](Images/FilteringOptions_arrow.jpg)

<i>This is what the Filtering Options look like.</i>

Specifically, we’ll want to open the "Project" option and select or click [The Gabii Project](https://opencontext.org/search/?proj=104-the-gabii-project&q=Gabii#21/41.88796/12.71981/20/any/Google-Satellite).

https://opencontext.org/search/?proj=104-the-gabii-project&q=Gabii#21/41.88796/12.71981/20/any/Google-Satellite

This will take us to another page just for The Gabii Project, what we were looking for.

On this page:

1. Scroll down to the filtering options again.
1. Open the "Descriptions(Project Defined)" option. 
1. Select or click "File Attachment Type" to filter the data.


![](Images/Filtering2.JPG)

<i>Under Filtering Options, we'll want to click on "Descriptions(Project Defined)" to open a list of options in that field. We'll then click or select "File Attachment Type" to actually filter for data that have that description.</i>

This will, once again, bring us to a new page. On this new page, repeat the previous steps but look for “File Attachment Type” as the option for how to filter. While it might seem tedious to click and scroll, click and scroll, these different layers introduce the many different ways that data in OC are associated, which is part of the power of the tool. 

![](Images/Filtering3_arrow.jpg)

<i>The Filtering Option for “Context Sheet” will look like this before we click or select it (minus the blue oval, which is just to show you where to look).</i>

Under the “File Attachment Type” filtering option, we’ll select the specific kind of file or notes that we want to look at. In this case it’s the Context Sheet. We’ll click or select this to get to our final selection.

This last choice will take us to a page with a list of context sheets for The Gabii Project. This time, when we scroll down the page the only data will be the context sheets for the project.

![](Images/ContextSheetsList.jpg)

<i>This is what the list of context sheets will look like.</i>

Also, if we want to skip ahead or accidentally mess up the paths or we go down an OC rabbit hole, we can go straight to the context sheets from [here](https://opencontext.org/search/?proj=104-the-gabii-project&prop=104-file-attachment-type---104-context-sheet&prop=oc-gen-image&q=Gabii&rows=20&type=media#18/41.88878/12.71939/20/any/Google-Satellite).

If we need the full URL for ease of use, the context sheets can be found at:

https://opencontext.org/search/?proj=104-the-gabii-project&prop=104-file-attachment-type---104-context-sheet&prop=oc-gen-image&q=Gabii&rows=20&type=media#18/41.88878/12.71939/20/any/Google-Satellite

Now that we’re here, we want to click on one of the hyperlinked "Item Label" numbers. We suggest selecting the one numbered "13675". If we don’t see it on that list immediately, we will need to click the set of ">>" arrows to look at more context sheets. Or we can click the Sort button and select "Item (type, provenance, label), ascending". This will bring number "13675" to the first set of sheets.

Clicking the “13675” link, will take us to that context sheet’s unique page. That page will have a button that says “Download File”. We’ll click that button and it will take us to a view of the PDF. This is when we can download the context sheet to a safe location. Make sure to note where it downloads and under what name it’s saved. 

![](Images/DownloadButton.JPG)

<i>This is what the download button will look like on the new page.</i>

Once that’s all done, we will have downloaded our first context sheet that we can use to build a data set. For reference, the images for this tutorial are from 13675. However, we can pick any context sheet in the list to start with and will be looking at multiple sheets. So, have fun picking different PDFs to explore!

### Kinds of data

Awesome! We’ve downloaded a context sheet. This is an example of one way that The Gabii Project decided to structure their note-taking, by creating a form. It looks like they also took photos, scanned drawings, and collected other data based on the options available under "File Attachment Type" that we saw while we were filtering.

Most projects will choose how to structure their note-taking, and therefore their forms, photos, and drawings, before the project starts. The best projects, though, also think about how those forms will translate into another common structured and organized format, the table, which we may be familiar with if this isn’t the first data story we’ve completed together.

While The Gabii Project already has these data entered in a table or database, the process of turning notes into data still happens regularly in archaeology. This is because many older archaeological projects, sometimes called legacy projects, have not converted their notes to tables. Furthermore, this is also common for projects that don’t have access to digital infrastructure in the field.  

So, the practice of understanding how to turn a form into a structured table is always important, particularly because context sheets like these include a lot of critical observations and data in a short two-page form. Some of these notes will be very easy to turn into a table and some will be harder. However, many of the values that are more difficult to organize are some of the most important parts of archaeological note-taking.

## The structure of the context sheet

When we open up our PDF, we’ll see that it looks like a...form. It has pencil and pen marks denoting observations about the context (how things are related) and of belongings (also known as artifacts) at the site.

Most archaeological forms have similar styles and what’s cool about forms is that rather than just recording whatever the excavator happens to notice, forms guide our observations and formalize what observations we want to make sure we take note of. So take some time to examine the PDF of the context sheet that we downloaded.

- How many different sections are there in this particular form?
- Are they related to each other in any particular way?
- Are some portions not filled out?
- Are some portions crossed out?
- How many of these sections provide options, like lists, checkboxes, or yes/no boxes?
- How many look like they are numeric, in that people only put numbers in the box?
- How many look like someone just wrote whatever?
- Are there any drawings or referenced photographs?

When we look at a form, it’s important to understand the limits of what it contains. No matter what questions we have, we can’t ask them if the excavators didn’t note that information. So it’s really important to get to know what is there before we move forward. Beyond that, it’s important to look at a couple of different sheets before committing to a particular structure for our table.

![](Images/MultipleContextSheets.jpg)

<i>Here are snippets of a few context sheets for comparison. Note similarities and differences in what is filled out.</i>

This is because how individuals fill out the forms can influence how we structure our tables and make sure that all the important elements are captured. Furthermore, there are regional differences in handwriting that are important to know. For example, people from European countries might write the number “1” in a way that looks more like how folks in North America write the number “7”, while a European “7” will look like a “1”, but have a dash mark through it. 

However, as we look at these two pages of the 13675 context sheet, there is a lot going on in this form.

…and this is only a tutorial.

We’re creating this data set for practice right now, so we don’t need to copy every heading because we want to practice organizing the data, not just repeating data entry that’s already been done. So instead, we’ll focus on a few headings and use these to learn how to turn these notes into structured data, which are often considered data that can be read by a computer or filtered!

### Parts of a table

There are typically two axes of information that we use when we turn forms into data structured in a table. We can think of each space on the form as a heading, field, column, or attribute for the tabular data we are going to build. These are the specific observations we want to take note of.

The information within that box is the specific entry, row, or observation for the context sheet we’re working with. These refer to the specific thing we are observing and the particular instance of the attributes it had. These are what we will enter into our table from each context sheet to start building our data set.

### Selecting our headings

After we have a feeling for how the form is organized, we’ll need to select our headings. We can keep a list of these headings anywhere, but for the sake of organization it might be a good time to open up our trusted (or not trusted, as all corporations are evil) spreadsheet program to type these up.

*Or* get some lined or graph paper and make this into a physical table. Some of our other evaluations might be more difficult to do by hand, for example rewriting descriptions, but transposing data into a physical table can be a great exercise in practicing the mechanics of these transpositions.

As we let our eyes Rome the form, it looks like the major headings are the **WORDS IN ALL CAPS AND BOLD**, associated with particular boxes on the form. However, some boxes also have ***Words In Camel Caps That Are Italicized And Bold***, as well. These are probably subheadings and we’ll want those to be in their own columns. We may also notice that some boxes have some other things going on, like check-boxes.

![](Images/SoilMatrix_Gabii_104-13675.JPG)

<i>Here's what the **SOIL/MATRIX** box looks like.</i>

If we take a look at the **SOIL/MATRIX** box, which may or may not be filled in on our context sheet, the section has more than one heading we’ll create from it. That’s ok!

What we’ll want to do, as we’re listing out all the headings, is know that there will be multiple headings that will start with soil/matrix. Then they’ll have something that denotes specifically what information we’re entering. We’ll want to make sure we enter these subheadings using a standard pattern as well, so we can quickly see which headings are related.

Good practice in archaeology is to use more column names or headings rather than less. Why? Well, we can always recombine data if we need to. Going in the opposite direction, separating data into multiple columns, is possible BUT typically causes more errors.

This is because the more we type, the more likely we are to cause errors due to spelling or differences in the number of spaces or capitalization.

![](Images/FirstHeadings_13675.JPG)

<i>These are the headings at the top of the context sheet that we'll be using.</i>

Now that we’ve looked at the various possible boxes we can use as headings, we’re going to focus on the ones at the top of the context sheet, and then the **DESCRIPTION** section on the first page, which should have at least two subheadings. Once we’ve got our list of headings, with more headings probably than the number of boxes we had, we can put them into a table.

![](Images/Description_Gabii_104-13675.JPG)

<i>This is what the description box looks like on the first page of our context sheet.</i>

## Making our table 

Once we have a new spreadsheet document open, or we’ve located some appropriate paper products, we’ll type or write the headings that we decided on into the first row. Some of us might be interested in entering these into a database program, which has more organizational options. However, databases can be complicated, so we’ll explore those in the future and stick with spreadsheets or hand-written tables. 

### Adding our headings

So we’ll put the first box from the form, **SITE**, into the first column, first row; followed by **YEAR** in the next column, same row; then **AREA**, and then **SECTOR**.

However, when we get to **ELEVATION** we'll need to pause.

Within **ELEVATION** we have two pieces of information, Min: and Max:. As these both have to do with elevation, it'll be better to have these as two columns. We'll call these **ELEVATION_MIN** and **ELEVATION_MAX**. 

Then we get to **STRATIGRAPHICAL UNIT**. This also has two pieces of information, but they are structured differently. Rather than both being numeric, it looks like there’s a section that’s usually a number and then a section that has specific options.

We’ll also note that we have to break this into two boxes. The first is **STRATIGRAPHICAL UNIT NUMERIC** (we’ll call it that for now, but we may want to alter that title after looking at other context sheets) and **STRATIGRAPHICAL UNIT TYPE**, which will capture the checkboxes within that section. 

As we’ve already seen, **DESCRIPTION** will probably need to be broken into separate boxes as well because it has spaces for both “Position within sector” area and a “Shape” area.

### Deciding each headings data type

Congratulations, we’ve made some important decisions regarding how we’re going to structure the data collation for this site! Deciding to separate boxes in the form into different column headings can be tough, because on the one hand, fewer columns might make it faster to enter things. On the other hand, more detail can be important for seeing patterns. 

Once we’ve decided what headings we want, we’ll need to name them in the table. Typically, we’ll use the names that match the form. But for additional headings we’ll want some consistency. So first write or type out the heading names into the place where we’ll be collecting tabular data.

![](Images/tableHeadings.jpg)

<i>The first row of our table should look something like this as we add our headings to it.</i>

For the headings we’re adding, we should make sure each version references the main box that the heading comes from, such as including “Description” as part of the title. For completely new headings, we’ll just want to keep their formatting similar. Furthermore, because we’re not using a database, we’ll need to be careful and explicit about the kinds of information we’ll want in the table.

To do this, we will add a README sheet that explains what should be entered under each column. These can be very useful if multiple people are doing data entry or if there is likely to be large gaps between data entry events.

![](Images/ReadMe_example.jpg)

<i>A README sheet will look something like this if typed up in LibreOffice Calc.</i>

However, this isn’t exactly what we’re working on right now, so we’ll explore the importance of READMEs in another tutorial. However, if we can't wait to find out more, we can check out this [page](https://en.wikipedia.org/wiki/README) for a brief history of what READMEs are.

Ok, back to the note-able parts. Once we’ve selected our headings, to remain consistent with how we enter data, we should think about something called “data types”. These are how those data are stored in the computer. To decide our data types we first need to consider whether the observation is a number or not a number. Is it 2, B (in separate boxes, a number and not a number) or not 2B (in one box, not a number).

Once we’ve decided that, there are different directions we’ll need to go. Let’s use the **ELEVATION** box as an example.

1. Is the information contained in that box a number?
    1. If no, it will be text, for now.
    1. If yes, is it a whole number or one with a decimal point?
        1. If it’s a whole number, it’s an integer.
        1. If it has a decimal point, it’s a float. (We’ll explain these in a minute!)

There are other data types but we’ll stick to these so that if we want to do some exploratory data analysis in our spreadsheet, all the data types within a heading or column are the same. Otherwise, we might try to find the average of 1 and fish and that just won’t go anywhere.

A basic rule is that if an observation is a number, use a numeral to symbolize it. For example, in the **STRATIGRAPHICAL UNIT** box, although we might not know exactly what that is, by taking a look at other context sheets we realized that this is always recorded as a number.

If they’re whole numbers, they’ll be integers. If they aren’t whole numbers, meaning there’s a decimal point or comma, then we’ll call them “floats” for now. Floats are a more complicated kind of number that take up more storage in our computer, but for our purposes in this tutorial, it just means there’s a decimal point or a comma.

![](Images/SU_Gabii_104-13675.JPG)

<i>This is what the **STRATIGRAPHICAL UNIT** box looks like.</i>

Looking at the **STRATIGRAPHICAL UNIT** box, we might want to be more strict than typing up the observations as text or numerals. In this box there are a couple of words with little boxes next to them. Specifically, those are check-boxes and they suggest that there are a limited number of options for what can be recorded.

So we’ll want to standardize how we enter those two options into our table. Some programs allow us to use check-boxes in our tables. More commonly though this is done by making sure we only use certain values during data entry. The first step is to decide how many options there actually are.

This will depend on if the options are mutually exclusive (meaning that only one state or the other can exist), if it’s possible for both states to be true simultaneously, or if the possibility exists for neither state to occur.

If all of these options are possible, then a box with two check-box options on a form will actually have four possible ways to be entered. Those options are check-box 1 checked, check-box 2 checked, both check-box 1 and check-box 2 left unchecked, or both check-box 1 and check-box 2 are checked. 

#### Extra Strict

So now that we have an idea of how to decide data types, and what data types we’ll be using, we can actually have our spreadsheet program enforce these rules. More complicated programs can enforce all kinds of data type rules but for today we’ll just give the most basic example. As most of our data types will either be text or numeric, we’ll talk about setting our headings to those.

The first way to do this is to go to the column name above our particular heading and opposite click to bring up a menu. Within that menu we'll want to find something that's called "Format Cells" and regular click on that option. This will bring us to a new window that defines how we want to format our cells. Formatting cells refers to how they appear to us in the spreadsheet, including data types, but also if the information appears on new lines within the cell or just disappears at the restricted width. 

![](Images/LibreOfficeMenu.jpg)

<i>This is what the menu looks like after opposite-clicking on the letter above our column.</i>

This will appear with a box labeled "Format cells** and will have a tab labeled **Number” that will refer to the data, regardless of whether it’s actually numeric or not, within a particular cell. 

![](Images/LibreOfficeMenu_FormatCells.jpg)

<i>This is what the **Format Cells** box looks like.</i>

Within that, there will be different options such as “Number” or “Text” or “Boolean Value”. These refer to the data types we discussed in the last section. Here we can set a rule for the number of decimal places or if we want things to be stored as “Text” in the column. If we’d like, we can set the columns according to these formatting options within our spreadsheet so that these rules will be enforced for every piece of data entered in that column. Doing this will standardize our column.

![](Images/LibreOfficeMenu_FormatCellsDecimal-Example.jpg)

<i>This is how the **Format Cells** type enforces rules on our data. Since Format has two zeros after the decimal place, that column now puts two zeros after the decimal place in that column.</i>

We can play around with this, for example in the **ELEVATION_MAX** box ,and set it to two decimal points by selecting that option under “Format”. As we can see, this means that if we enter any value, in the image “0” or zero, it adds two extra zeros after the decimal point to match the formatting requirement. However, if this seems more complicated than we’d like for this exercise, we can skip this part and just make sure we’re standardized as we enter our data. 

### Specific data headings

So we know what headings we're entering:

- **SITE**
- **YEAR**
- **AREA**
- **SECTOR**
- **ELEVATION_MIN**
- **ELEVATION_MAX**
- **STRATIGRAPHICAL UNIT NUMERIC**
- **STRATIGRAPHICAL UNIT TYPE**
- **DESCRIPTION_PositionWithinSector** 
- **DESCRIPTION_Shape**

Based on our context sheet, what data types make sense for each of these? It may be good to look at a couple of sheets before deciding, but think about the questions we had for each box on the context sheets. Also since we're thinking about these and started a "README" sheet, we can put our thoughts into that table as well to keep ourselves organized. 

Here are some of our explorations of the headings we’re using for the exercise. Some of these may have multiple options but for now stick with these as we develop these skills.

**SITE** looks like it's printed directly onto the sheet and is always "GPR". As this is alphabetical, we'll want this to always be a text value. However, more importantly, this value doesn't change. We'll talk about how do deal with that in the next section.

**YEAR** looks like it is the space where we put the year of the project. If we look at the context sheets, some have two digits and others have four digits. However, they’re always whole numbers so these are probably integers. As we enter these data, we should standardize these to either two digits or four digits. For now, go with four digits, even if the paperwork might only have two. While we’re taking some liberties, as this isn’t **exactly** what is in the form, it will ensure there isn’t confusion over whether excavations happened in 2010 or 1910 or even 10AD depending on how long they’ve been excavating that site.

**AREA** looks like it is the space where we are putting what named part of the site the context sheet is from. However, it is not the area, as in the extent of it, such as by multiplying the width times the height. Instead, this “area” is a named region of the site. It often seems to be just a single letter, so we’ll make sure it only accepts text values and choose to always enter those as capital letters so we don’t confuse lowercase L’s and uppercase I’s. 

**SECTOR** looks like it’s often blank. We won’t make any decisions about that for now as it’s unclear how recorders were capturing that information. We’ll talk about blanks a little later on.

**ELEVATION_MIN** looks like it is the space where we put the minimum elevation for the context. It looks like this is a measurement taken with particular units. It looks like it has a number of decimal points past the original number, but the number past the decimal varies. This should be a numeric value then, and typically one that has decimal places, so...a float. 

**ELEVATION_MAX** looks like it is the space where we put the maximum elevation for the context rather than the minimum. Because it matches with the minimum, we want to capture this just like we did for **ELEVATION_MIN**.

We’ll pause for a second here. It looks like the two values that go into the **ELEVATION** headings are measurements. This means that they should probably have units, for example millimeters (mm) or centimeters (cm), denoting how much of what was measured.

It does not look like these forms specify those. The project took place in Italy though, so it’s likely that the units are the International System of units or SI units. We may consider adding a heading called **ELEVATION_UNITS**, to specify this but without further information we’ll leave our headings as they are.

**STRATIGRAPHICAL_UNIT_NUMERIC** looks like it is the space where we put a number for the stratigraphical unit. These appear to be typically whole numbers, or integers. So we want to make sure that no decimal points are included in this box when we enter it. No floats here!

**STRATIGRAPHICAL_UNIT_TYPE** looks like two check boxes, one for "Natural" and one for "Anthropic". These probably describe something about the stratigraphy and whether it occurred with or without human influence. Based on this, it’s unlikely that a single unit would be both natural and anthropic. However, it is possible that this section could be left blank, so we want to make sure we are aware of that. Because we’re working with basic data types this will be a text field but there should only be three potential values for that heading in the table.

**DESCRIPTION_PositionWithinSector** looks like it describes where in the sector the context recorded was. While they all have something about direction, such as north or south, what this is measured in relation to, changes. Because of that, they are not standardized enough for separate headings. So unfortunately, we’ll leave it as one of the dreaded free-form text options.

**DESCRIPTION_Shape** looks like it describes the shape of the context recorded. These should be one word descriptions. While this will also be a text field, it should be limited to only words associated with potential shapes. However, sometimes there’s a little more to that description therefore, we should add an additional heading.

**DESCRIPTION_Shape_Notes** does not exist as its own box on the form. However, some context sheets have both the named shape and comment on that shape with further observations. This area also sometimes includes observations that are not always obviously relevant to shape but should be captured anyways. While the possible shapes are set, this section should be another free-text option to account for what folks noted regarding the shape of the context and other possible information captured in the **DESCRIPTION** box.

### Headings without a space on the form

Besides adding headings that break up the sections of the form into their own separate fields, it may be important to add headings to the table that fill in information not covered in the form. Most often, these will be headings that describe the process of data entry. However, they can also be things that were left off, like the lack of units regarding the elevation section that we noticed.

An important one, that may have a form-based counterpart, is a **Notes** heading. While in some cases this is the bane of data-entry standardization, **Notes** can be important for capturing observations not described elsewhere. Notes can both exist as a space on a form that the person doing data entry types up, and as notes made by the person doing data entry. These can be separated into headings such as **FieldNotes** for those that are transcribed from paperwork and **LabNotes** to account for observations made during data entry.

Other potential headings might be **Entered By**. A heading like that will allow the person entering the data to note who did the work of entering the data, in case someone wants to know more about the process and decisions the transcriber made for how to fill in the data.

For this tutorial, we’ll add a **LabNotes** heading and use that for exploring what happens when we encounter blanks in the context sheet. 

## Filling in the table

Now that we’ve decided the structure of the table, we can go ahead and put the information that we wanted to gather into it. As a test to see if that structure makes sense, go back to the beginning of the tutorial and download a few more context sheets. That way we can see if how we organized the data makes sense or if we’ll need to edit it in some fashion.

Or, instead of repeating those steps, we can just click this  [link](https://opencontext.org/search/?proj=104-the-gabii-project&prop=104-file-attachment-type---104-context-sheet&prop=oc-gen-image&q=Gabii&type=media#18/41.88878/12.71939/20/any/Google-Satellite) or navigate to: 

https://opencontext.org/search/?proj=104-the-gabii-project&prop=104-file-attachment-type---104-context-sheet&prop=oc-gen-image&q=Gabii&type=media#18/41.88878/12.71939/20/any/Google-Satellite

so that we can download more context sheets to enter.

![](Images/LibreOffice_FilledInData.jpg)

<i>Here's what a partially completed table can look like when entered into LibreOfficeCalc.</i>

![](Images/PenAndPaper_tableFilledIn2.jpg)

<i>Here's what a partially completed table can look like when done on paper. Note that we might not be able to keep all our headings on one page, so we might want to think about how to keep our data organized over multiple pages.</i>

We should do this a number of times so that we evaluate the data structure we’ve created and so that we have enough observations to answer some archaeological questions. For the purposes of this tutorial, let’s try to input at least ten context sheets. It’s a lot to type but ten entries should allow us to test the structure of our table to make sure it does what we need it to. 

However, ten entries is also few enough that if our structure doesn’t work, redoing those won’t feel like a waste of time. Better to change our process or structure at ten entries than at 10,000 entries because something significant was missed or entered incorrectly!

This vetting also reminds us that sometimes our data entry structure just doesn't work in practice. Or something comes up that requires us to re-evaluate that structure, and that's fine. Likely too is that we'll need to revisit our data structure as new questions develop as data entry goes on. However, for any changes we make we should take care to describe them so future researchers understand what changed and why.  

### How to fill in the table

We discussed data types previously and now that we’re actually entering data (yay for data entry, we suggest getting some headphones and listening to [music](https://open.spotify.com/album/5fqlZFKYqvkIe2jdDGt2nl) or [podcasts](https://open.spotify.com/show/4mHQR9Xkhl5ZAOJndFA3uT) during this portion), we want to try to keep what we actually enter as standard as possible.

If we don’t already have a preferred spreadsheet program, LibreOffice Calc, Google Sheets, and Microsoft Excel are common programs for this. However, if we want to recreate the pen and paper experience but in the most minimal way possible we can also enter things into a .txt file using programs like notepad and create columns by adding commas “,” or tabs between the observations. This is, strangely, both a more complicated but also technically tricky way to go, so we suggest a spreadsheet program when we’re just learning how to do this. 

On that note: *copy and paste is our friend*. Copy and Paste might, in fact, be our best friend. The friend who keeps the notes we wrote them in high school and cherishes them forever. Or burns them because they know if those ever got out no one would respect our work as archaeologists. (The authors of this tutorial are definitely not writing from personal experience…)

That aside, for any fields that contain text, using copy and paste to ensure consistency is important.

Similarly, we want to make sure that our numbers are entered the same way, and that we don’t do such things as, um, enter numbers as text. This is something we can set in the formatting section of a column in many spreadsheet programs. If we’d like to know how to do this, we can check out the *Extra Strict* section, right before the *Specific data headings* section on how to do this. However, if we’re just doing this on paper we just want to make sure that we’re consistent. 
 
This is particularly important for the section labeled **ELEVATION**. Based on the forms, this was consistently measured to multiple digits past the decimal place. Unfortunately, sometimes it’s three places and sometimes it’s four. While this was probably a standard that should have been set in the field, we’re going to choose to capture the most detail. Therefore, we might set the formatting to always show us four places past the decimal. This will add an ending zero, possibly implying more precision than was in the field but it also means we’re not rounding when someone in the field put in a specific value in that decimal place. 

### Dealing with "blanks"

One of the great and terrible aspects of archaeological data entry is the blank form or blank box. It’s disheartening to open up, or take a look at, a particular set of paperwork, whether it was written yesterday or 20 years ago, and look at a specific area, which might have the information we really want, and we see that that area has...nothing in it. 

We may think a number of things at this moment. Such as:

- How did I get here? 
- What happened to my beautiful form?
- Is this even my site?
- Why did we ask this particular question?
- Does this blank mean that this didn't exist?
- Did this question not apply to this particular observation?
- And while we hope this isn't the case but sometimes is, did the excavator or note taker simply *forget* to fill in this section of the form?

We are human after all and accidents happen. However, a blank section in a form does not necessarily tell us which of these options is the truth.

Hopefully, there’s someone we can chat with for clarification on this. But sometimes there isn’t, and we need to decide for ourselves what, if any, information we take from the fact that something was *not* recorded.

Someone thought that it was an important box to have on the form. Yet, for this particular context sheet it was crossed out or left blank. So what do *we* choose to put down in this space within our growing table? Did we plan for this possibility in our data structure? And how do we, as the, er, data enter-ers, signal the difference between the previously discussed options?

There are multiple ways to do this and one method may outweigh another depending on the project. For this tutorial though, we’re going to make note of any blank or crossed out fields in the "LabNotes" heading we created earlier

Under that heading, we can put any notes that we thought of while entering the observations, explain why any of the attributes were left blank, and why if we know. It may also be valuable to note either in that heading or in the "README" sheet that any unexplained blanks are data enter-er error.

We can use a similar approach when the box is not **blank** but includes some variation of "N/A". This typically stands for Not Applicable and might look like "na", "NA", or some other clear indication that the recorder most likely **saw** the box and chose not to fill it in or note that it did not apply. Typically, this suggests that the thing or observation being asked for does not apply to the recorded instance. If this is used frequently in our sheets, we should make a note within the "LabNotes" heading that any instances of "NA" reflect the original recorder not the lab transcriber. 

### A tabular data set

After a few observations, take stock and see if we can pick out any particular patterns in the context sheets that we collected. If using a spreadsheet program, we can try filtering the data (one great reason to create structured data) to see if there’s something that we can say about the site based on the information we’ve put into the table. If we’re unfamiliar with filtering in our spreadsheet program, we can check out the spreadsheet tutorial section of [*Cow-culatating Your Data with Spreadsheets and R*](https://drive.google.com/drive/folders/1HAggjkFMRApd2tpqr7eTFLJwAdXlPRl0?usp=sharing).

- Are there any particular words that get repeated in particular fields?
- Do some elevations have more notes about them than others?

## Now what?

Congratulations! We’ve made, or are in the process of, making our very own data set from the resources shared by [The Gabii Project](https://opencontext.org/search/?proj=104-the-gabii-project&q=Gabii#21/41.88796/12.71981/20/any/Google-Satellite). This means that we made some data entry decisions that created a structure for our data. It’s not the largest data set out there, but hopefully we’ve recorded enough observations to feel like we understand how to turn notes into data. And if we aren’t comfortable doing data entry yet, there are 180+ context sheets we can use to keep practicing.

Once a few are entered, we’ve explored what’s there, and looked for patterns, we will probably want to do more with these data. Why? Well this is only the first part of the cycle. We’ve turned our structured notes into data. But we don’t really want data, we want information and, even beyond that, knowledge.

To get to that, we’ll need to move to the next section of this tutorial. Specifically, if we entered in the information from the **DESCRIPTION** section for 13675 there were some belongings, sometimes called artifacts, listed in that section. We’ll focus on the information about those specific belongings and notes from other sections of that context sheet as we transition from data to narrative to say something about the site.

## Key concepts used in this tutorial

- data structure - how we choose to give form or organize our data.
- paperwork or documentation - the notes and ways we choose record our observations of the archaeological record.
- columns, attributes, headings - These are the specific observations we want to take note of.
- entries, rows, observations - These refer to the specific thing we are observing and the particular instance of the attributes it had.
- data types - the ways that information is stored in a digital environment in a standardized fashion.