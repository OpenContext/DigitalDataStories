---
title: "Gabbing about Gabii: Going from Notes to Data to Narrative"
subtitle: "Data to Narrative"
author: L. Meghan Dennis PhD. Data interpretation and public engagement postdoctoral
  researcher
date: "10 June 2022 - Version 1.0"
output:
  html_document: default
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
Welcome to, Data to Narrative! 

## Tutorial overview

In this tutorial we will learn to:

1. Utilize data from tables and context sheets to create data narratives 
2. Analyze existing data narratives for theoretical approach and exclusionary factors 
3. Apply a narrative lens to a data narrative of our your own creation 


## Key concepts used in this tutorial 

- data narrative 
- narrative lens 
- hegemonic storytelling 
- bias 
- tone 
- jargon 
- audience 
- language level 

## Introduction

We hope you’ve taken a few minutes to relax after the last portion of the tutorial. There was a lot to do there. The process of turning notes into data is a crucial part of building knowledge, but most of the time, it’s not the last step. So let’s talk about what we do, when we’re done doing data. 

The goal of every archaeological project is, ultimately, to introduce more knowledge about the past to the people of the present and future. If we don’t find a way to make the data we’ve carefully collected, digitized, and analyzed available and understandable to the public, what we did wasn’t science; it was just digging holes and making spreadsheets. Which brings us to the second part of this tutorial: turning data into narrative. 

Humans are storytellers. We’ve always been storytellers. Every culture, from modern day to those of the earliest paintings in caves, has told stories. People have always looked at the world around them, considered their place in it, and tried to share what they know, value, and regard as important. 

As archaeologists, we occupy a special place in this long chain of storytellers, because not only do we tell the stories of our own lives and cultures, but through our work, we retell the stories of peoples in the past. This allows a connection with the past that is rare, and special, and with that privilege, we have a responsibility to share the stories we learn with others.

Take a moment and sit with that. Think about what that responsibility means. 

![All of these are concepts or issues that archaeologists must consider in their practice.](/Images/)

A data narrative is a particular type of story that is grounded in scientifically derived findings and geared towards an audience. That audience may be very big, or it may only be a few people. It may be adults, or children, or both. 

For the most part though, the audience won’t be other archaeologists. Probably, it won’t be other data scientists either. So while we can look together at our digitized tabular data and see things in it, that’s not the format that most people find engaging when they’re learning about the past. 

We have to go from data back to narrative. We have to tell them a story. 

# What data will we be working with?

The story we’re going to be telling today is about a particular part of a particular archaeological site in a particular part of present-day Italy. We’re going to be using data from the same site that we worked with in the other part of this tutorial, Gabii. To make things a bit easier though, we’ve limited this part of the tutorial to only information that can be reached off of this page. 

Through this curated collection of data, which contains tabular data, images of artifacts, context sheets, and maps, we will tell stories about the people of Gabii. Take a few minutes to look through the files and download any that seem interesting to you. Maybe reorganize them? 

Below is a screenshot of one way we could organize these files, but everyone may have their own way of shuffling things to make sense of them. There’s no one way to organize this information, which brings us to considering point of view. Or, what we call it when designing data narratives, our narrative lens. 

![We chose to download and rename images of the ‘special finds’. They’re organized by association, but could easily be regrouped in many different ways.](/Images/)

# What is a narrative lens?

In an everyday story, the narrative lens is what we use to see, and understand, the story that we’re experiencing. Think of it as a perspective. All stories are written with a perspective, whether their authors intend to do so or not.

As scientists writing data narratives, we have to be very conscious of our perspective, because remember, we’re telling a story that is based on data. The perspective we put on the data narrative we create will influence how that data are perceived, and understood, by our audience.

Let’s look at a few different narrative lenses that we may employ when we’re creating our data narratives. We’re going to look at what makes a narrative standardized, or ‘hegemonic’, at feminist narratives, at queered narratives, and at meta-archaeological narratives. If those four lenses sound familiar, congratulations, there was a good theory teacher involved somewhere along the line!

## Hegemonic narratives

The hegemonic narrative in archaeology is the standard, basic, probably heard it in a million formats, story about the past. A hegemonic narrative is the baseline of archaeological storytelling. In hegemonic narratives, the archaeological story is distilled down. These narratives, though basic and repetitive, can be useful to establish what the tropes are in archaeological narratives of a particular culture, period, and place. It’s important to note that hegemonic narratives aren’t necessarily wrong, sometimes the standard story is the story. We just want to be able to share narratives that consider multiple possibilities.

Common hegemonic narratives in archaeology include: stories about the Roman Empire told from the perspective of a soldier or gladiator, stories about the medieval period told from the perspective of a high ranking member of the nobility, and stories about Native Americans told from the perspective of a shaman. 

## Feminist narratives

An approach from feminist archaeology asks the reader to consider context, the lack of neutrality in data. It asks the reader to rethink hierarchies of gender and access to material goods within the archaeological record. Feminist narratives don’t necessarily have a female central figure, but do more often center peoples of the past who are less typically represented in archaeological narratives, like children, the elderly, the dis/abled, and others who might be marginalized through cultural factors. 

An example of a feminist narrative in archaeology would include: stories about the Roman Empire told from the perspective of a child in an occupied territory, stories about the medieval period told from the perspective of a shepherd’s wife, and stories about Native Americans told from the perspective of a still existing pre-contact oral tradition. 

## Queered narratives

In queered narratives we consider the presence of artifacts from a perspective outside of their past role as commodities and their present role as proxies for the past. We consider the concept of ‘difference’, and ‘indifference’, and illustrate how space and place are fluid, and how power attached to place is fluid. Narratives with this lens may focus on the archaeological unit as a transitional space, a place that once had definition, but is currently in a liminal state.

An example of a queered narrative in archaeology would include: stories about the Roman Empire told from the perspective of a non-Roman living outside of the empire, stories about the medieval period told from the perspective of a Jew in England, and stories about Native Americans told from the perspective of a modern displaced nation. 

## Meta narratives

Finally, the fourth approach we’re presenting is a meta-archaeological narrative. This is where open data sets like this one from Gabii are great to work with. We’re able to interact with digital copies of first-line interactions with artifacts, units, and sites, through scanned copies of site forms, like we looked at before in this tutorial! So we can get details on what the process of excavation was like––linked data lets us look beyond the site form to compare the time of excavation to conditions on the day, and lets us look at the role of a single excavator across a site, or across multiple sites. Meta-narratives take a specific experience and universalize it for the discipline. 

An example of a meta narrative in archaeology would include: stories about the Roman Empire told from the perspective of a student attending their first field school in Italy, stories about the medieval period told from the perspective of a modern potter recreating medieval ceramics, and stories about Native Americans told from the perspective of a Native archaeologist working in cultural resource management in the United States.

# Writing a narrative

So are we ready to try writing a data narrative? We’ll work on this together, so that you don’t have to start from scratch. 

Consider the following narrative. 

> The smell of cooking meat and the stink of other people’s bodies was inescapable. Crouching on the uneven floor, Aemilianus shook the die, blowing on it gently. He rattled it out of his hand. It rolled, and rolled, and rolled -- there was the six, and then, and then, it tipped, coming to a stop. 

> A two. He couldn’t win on a two.

> Reaching into the pouch affixed to the mail of his armor, he pulled out a few thin bronze coins, flipping them to the winner. Someone called his name, and he looked up. Time to go. 

> He left the die. It wasn’t doing him any favors.

Let’s look at each part of this narrative, and see where it came from, and what purpose it served. Here’s the text, again, this time highlighted. Each highlight corresponds to a piece of data we have within our curated data set. 

> **The smell of cooking meat** and the stink of other people’s bodies was inescapable. Crouching on the **uneven floor**, Aemilianus shook **the die**, blowing on it gently. He rattled it out of his hand. It rolled, and rolled, and rolled -- there was the six, and then, and then, it tipped, coming to a stop.

> A two. He couldn’t win on a two.

> Reaching into the pouch affixed to the mail of his armor, he pulled out **a few thin bronze coins**, flipping them to the winner. Someone called his name, and he looked up. Time to go.

> He left the die. It wasn’t doing him any favors.

Have you found the origin of each piece of data? Remember, the data might be an artifact, it might be a unit description, it might be a soil sample, or it might be pure measurements. What kind of narrative, based on the types we talked about before, is this?

The first highlighted section refers to [three](https://opencontext.org/subjects/a3b873e9-7fa0-4cd9-be36-ef4b7add5ccd) [samples](https://opencontext.org/subjects/81f93fa8-0536-44e4-89c0-6981b48b9241) of [faunal materials](https://opencontext.org/subjects/1e328230-f6c6-4cd7-9f00-da45c2f3cb2e).

The second highlighted section refers to the [shape and layer surface](https://opencontext.org/subjects/0281b2b1-930d-41c6-936f-9add56429998).

The third highlighted section refers to a [gaming die](https://opencontext.org/subjects/90c61867-47e9-4046-9f39-f0b251fb15ac) listed as a special find.

The fourth highlighted section refers to a [bronze coin](https://opencontext.org/subjects/5b873b2c-b849-4693-92aa-b664ae6fb9d8) listed as a special find.

This is a hegemonic narrative. This is a typical Roman story. We might see a narrative like this in film or television, in a novel, on a sign at a site, or in a museum booklet. 

In this narrative, the protagonist is a man. He’s in the military. His life is blood, and sweat, and drink, and food, and though he isn’t within the white-robed elite, he has enough money that he can gamble without worrying about a loss. He exists within a larger military structure that tropes in media representation of Romans makes easy to allude to. 

Now it’s your turn. Pick three pieces of evidence from your curated data set. Take about five minutes and write a one-paragraph story, incorporating your three pieces of data. Start with a hegemonic lens, we don’t have to get fancy and show off our theory...yet. 

If you’re worried about how to get started, consider these ideas: 

- Who are you telling this story to?

- What is the most important point you want to make?

- What stories have you heard in the past that might make good foundations for this story?

It’s okay to be outlandish in the first draft. Maybe it would help to just write dialogue, like a conversation? Maybe it would help to draw stick figures or storyboards of what happens? Maybe going online and looking up names from the period will spark an idea about the narrator?

# Analysing your narrative

A critical aspect of creating data narratives is reflection. Gabii wasn’t built in a day, and neither are perfectly crafted narratives! We’re going to look at the narrative you’ve written now, and consider how it’s effective, and where we might want to change things up. 

## Looking for bias

The first thing we should look for is bias. Before getting worried, it’s normal to find some bias in our narratives! This is that thing we talked about before, the writer’s perspective. What we want to do is make sure that our own perspective as archaeologists, data scientists, and writers isn’t leading our readers to incorrect assumptions about our data. 

If you haven’t tried out our  [*Cow-culating Data with Spreadsheets and R*](https://doi.org/10.6078/M73N21HR)  tutorial, you might consider giving it a go after this. In that tutorial, we talk in more detail about types of data bias, and how interpretations can be skewed. 

For now though, let’s look at each piece of data incorporated into the narrative we started with. For each piece of data, answer the following questions.

- Why did I choose this piece of data over another?

- Why is this piece of data important to share with the public?

- Is this piece of data representative of, or contrary to, the time period/site/unit I want to share?

Now have a look at the data you used in the narrative you created. Answer the same questions. If you find that the answers aren’t what you want or what you expected, go ahead and make new choices. 

## Looking for tone

The second thing we should look for is our tone. No one is going to ask us to sing, don’t worry! When we’re looking at our tone in data narratives, we mean we’re looking at how our word choices and sentence structure creates a ‘feeling’ within the narrative. 

In the hegemonic narrative we started with, the tone is intended to reflect the physicality of the main character’s life. Most of what the narrative shares is him ‘doing’ something, or encountering something sensorial. What does he smell? What does he see? What does he do?

Have a look at the narrative we started with.

> The smell of cooking meat and the stink of other people’s bodies was inescapable. Crouching on the uneven floor, Aemilianus shook the die, blowing on it gently. He rattled it out of his hand. It rolled, and rolled, and rolled -- there was the six, and then, and then, it tipped, coming to a stop.

> A two. He couldn’t win on a two.

> Reaching into the pouch affixed to the mail of his armor, he pulled out a few thin bronze coins, flipping them to the winner. Someone called his name, and he looked up. Time to go.

> He left the die. It wasn’t doing him any favors.

- Can you identify the tone?

- What writing choices support this tone?

- Does this intended tone convey the data?

Now have a look at the narrative you created. Answer the same questions.

If the tone isn’t what was intended, take a few minutes to try changing some of the word choices. Give the story a little more attention. Editing is our friend if we want to create an engaging story. 

## Looking for jargon

The third thing we should look for is jargon. Jargon is language or terms used by those within a profession that is so specific to that profession that it’s rarely used or understood by those outside of the field. Archaeologists love jargon. We throw around Munsell colors. We handily discuss areas, units, and features. Sometimes we mention sherds, but other times we mention shards. We can write for pages and pages about stratigraphy. 

In a report, or an article, this jargon can be necessary. In those formats we’re speaking to other archaeologists, and we use the jargon of our discipline to share information with one another, because it’s expected that we’ll all be familiar with the terms. But outside of professional communication, the rest of the world doesn’t necessarily know our lingo.

Have a look at the narrative we started with.

> The smell of cooking meat and the stink of other people’s bodies was inescapable. Crouching on the uneven floor, Aemilianus shook the die, blowing on it gently. He rattled it out of his hand. It rolled, and rolled, and rolled -- there was the six, and then, and then, it tipped, coming to a stop.

> A two. He couldn’t win on a two.

> Reaching into the pouch affixed to the mail of his armor, he pulled out a few thin bronze coins, flipping them to the winner. Someone called his name, and he looked up. Time to go.

> He left the die. It wasn’t doing him any favors.

Are there any terms that are mostly common within archaeology?

Are there any terms that are too specific?

Are there any terms that might pull the reader ‘out’ of the narrative?

Now have a look at the narrative you created. Answer the same questions.

If you find any jargon in your narrative, take a few minutes to replace those terms with more audience-friendly choices. Give your draft another polish. 

## Knowing your audience

And now, here we are, with a solid draft that does what we want it to do, and reads how we want it to read. But oh, we forgot something...we have to consider our audience. 

We should always know, before we write, who our intended audience is. Once we have that information, there are two things to consider to better tailor a data narrative to that audience. 

### *Language proficiency*

The [ACTFL](https://www.actfl.org/), the American Council on the Teaching of Foreign Languages, assess proficiency as what a language user can do regardless of where, when or how a language was acquired. Because the audience for archaeological content can vary so widely, we have to take into account that not all of our readers will have the same level of proficiency. Remember when we talked about jargon? Jargon is extremely difficult for those with lower levels of language proficiency to understand. 

When we’re writing for an audience, we scale our assumed level of language proficiency down. Even if we’re writing for what we assume is a college-level educated public, we scale the language we use down. There are lots of tools and aids available online to help with checking our language levels. Three of the most common formulas for checking language and reading levels are:

- The Flesch Reading Ease

- The Flesch-Kincaid Readability Test

- The Gunning Fog Index

Calculators for these formulas, and others, are available  [online](https://readabilityformulas.com/free-readability-formula-tests.php).

### Length

The other thing we have to consider is the length of what we’ve written. Think about the experience as a whole. Is this piece of writing the only reading the audience will be encountering? If so, we can get away with writing a bit more. If our piece is just one segment of a larger written encounter, we need to consider scaling down what’s presented. 

Readers are more likely to engage with the content if it’s presented in bite-sized segments. Furthermore, they’re more likely to engage with more segments if those segments are themselves short. 

A good best practice is no more than 100 words for anything that’s going up as a sign, or placard, and no more than 250 words if it’s being presented as a hand-held written supplement. If we’re presenting our work as a blog post, or part of a website, 500 words is the max! 

The hegemonic narrative we started with is 107 words. Let’s take a look at the narrative you wrote. 

- How many words long is it?

- How many sentences does it have?

- What’s the reading level?

- Was it possible to stick to, roughly, just one paragraph?

# Rewriting the narrative

Now that we’ve spent time with our hegemonic narrative, and we’ve got it: using solid data, written in an appropriate tone, devoid of jargon, set to our chosen language level, and of a good length...it’s time to apply a different narrative lens! 

For each of the narrative lenses we talked about earlier, we’re going to use the same process that we initially used to write our hegemonic narrative. We’re going to use the same three pieces of data that we used before, too. 

The goal here is to think differently about the same pieces of information. In archaeology, ‘theory’ is the intellectual or philosophical framework that archaeologists apply to make their interpretations of the past. There are lots of different archaeological theories, and theoretical perspectives have changed over time. Here, we’ll consider how several theoretical perspectives, aka, narrative lenses, influence the stories we tell about the past. 

We may find, as we look at each narrative lens, that we’ve naturally already fallen into writing from one of these theoretical perspectives. If so, try to identify what about the data we highlighted led to that narrative lens. 

- Are there other pieces of data within the curated data set that might reinforce that finding?

- Are there pieces of data that refute it?

- How could we rewrite the data narrative to account for the other data sources?

## Rewriting through a lens from feminist archaeology

Remember, when we’re writing from a perspective in feminist archaeology, think gender, hierarchies, power, and access. 

Here’s an example of the narrative we shared earlier, this time written with a feminist archaeological lens. 

> It was her turn, but he kept hoarding the die. He hoarded the thin bronze disc, too, and all of the bits of metal they’d scrounged and hidden here, in their secret place. 

> Helvia and Helvius were meant to be minding the goats, but the goats mostly minded themselves. There was time to play, and this falling down room, with its uneven floor, made the perfect place to hide away and pretend. 

> She toed a broken piece of thick-walled pottery and waited for her brother to remember it was her turn. 

This narrative includes the same artifacts and the same placements within the implied area, but we replace the tropes of hegemonic narration with actors and goals out of feminist archaeological practice.

Instead of a militarized male, an identity associated with access to social and economic power, we challenge unequal power structures by considering a view of the site from the position of children. Specifically, the narrative is from the perspective of a female child, but that perspective is viewed in its context of power alongside a male child of similar age and sociocultural status. 

Looking at your rewrite, consider: 

- Are hegemonic archaeological norms of gender, power, or control subverted in this writing?

- Does this writing reflect a view on the past through an uncommonly represented narrator?

- Does the data used as evidence for the narrative still apply?

## Rewriting through a lens from queer archaeology

Remember, when we’re writing from a perspective in queer archaeology, to think not only about sexuality, but also more broadly about commodification, liminality, and artifacts outside of functioning as proxies for the past. 

Here’s an example of the narrative we shared earlier, written with a queer archaeological lens. 

> He threw a shovelful of dirt on the floor, stamped on it, tamped it down. He threw another shovelful, and again, and again, building the uneven surface to something more usable, something he could live on. If he didn’t get this space sorted, he wouldn’t have anywhere else to go. This space was free and available. 
> Something white stuck out against the dust and soil. Adamo bent down, using the tip of his finger to flick the loose covering (not tamped enough, there was more work to do) off of a bone-colored square. 

> A broken die. Pips where the four should be were gone. Worthless. The past had left behind so much junk here. He dropped the die and threw another shovelful of dirt.

This narrative includes the same artifacts and the same placements within the implied area, but we replace the tropes of hegemonic narration with actors and goals out of queer archaeological practice.Though our protagonist is gendered male, he’s positioned as an outsider, and his socioeconomic power is curtailed. This narrative focuses more on the archaeological unit as a space, instead of as a place in time. 

Looking at your rewrite, consider:

- Are hegemonic narratives of sexuality, power, privilege or space subverted in this narrative?

- Does this writing reflect a view on the past through a queer or gender diverse narrator?

- Does the data used as evidence for the narrative still apply?

## Rewriting through a meta-archaeological lens

Writing a meta-perspective in data narratives can be fun. It gives the public a little peek behind the curtain of how archaeology happens, and it lets us incorporate our own experiences, albeit generalized, into a depiction of our discipline. 

> It was so hot. It was hot all the time. Standing at the edge of their unit -- the ground they had revealed as a small room with an uneven floor, they considered their day’s labor, and they sweated. 

> The whole day, spent sweating and dusty, and it would probably end up in the report as just another post-abandonment surface, if it made it in the report at 
all. Their finds bags had some bones and charcoal, a bronze coin, a few bits of metal, a broken fibula pin, and a compact bone die, the four-side of its matching pips missing. There was a time they’d have had all of these things on their ‘to find’ bucket list.

> Time for a Peroni. They signed their name on the context sheet and stretched. 


Looking at the [context sheet](https://opencontext.org/media/b2fd67d0-dd48-42dd-b66d-f344b49f5098) for the day this unit was excavated, we can cross reference with [historical weather data](https://www.worldweatheronline.com/rome-weather-history/lazio/it.aspx) to find out conditions on the day. The day this unit was excavated was the 13th of July, 2010. The temperature ranged from 28 to 32 degrees Celsius, or 84 to 89 degrees Fahrenheit. It was a hot day in July just outside of Rome.

This context sheet was one of multiple context sheets completed by the same person, and we can infer the amount of effort this unit took by the detail and attention to detail on the form. We’re not judging their enthusiasm or work ethic, we just know what it’s like to be at the end of that day! The narrative we’re telling through this final approach is one that most archaeologists can recognize, regardless of site location, but one that doesn’t frequently occur in mass media––it’s the end of the day, the archaeologist is tired, they’re feeling a little jaded. It’s taking a specific experience and universalizing it for the discipline. 

Looking at your rewrite, consider:

- Are hegemonic narratives of archaeologists as explorers, adventurers, and collectors subverted in this narrative?

- Does this writing reflect a view on the past through the perspective of an archaeologist or scientist as narrator?

- Does the data used as evidence for the narrative still apply?

# The Published Narrative 

Now, having looked at multiple short narratives created by the Digital Data Stories team, and having written your own narratives, it’s worth having a look at the ‘official’ narrative, as presented by the Gabii Project team in their publication on the site, [A Mid-Republican House From Gabii](https://doi.org/10.3998/mpub.9231782). Though this narrative draws on more data than we worked with in this tutorial, hopefully in reading it you can now pick out some of the things we talked about. This text is presented verbatim, thanks to a generous [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/) License. 

As you read the project’s narrative, consider: 

- What data is present in this narrative? 

- What is the narrative’s tone? 

- Can you identify any bias in the narrative? 

- Does the narrative include any jargon? 

- What is the general reading level of the narrative? 

- Who is the narrative’s intended audience? 

- What narrative lens is the narrative told through? 

> The town of Gabii is shaped like the flattened skeleton of a fish, a long curved spine with ribs splayed out, running off either side. It sits on the southern slopes of a low conical mound with a crater at its center; a bit of landscape whose topography was defined by a long-dormant volcano. The town’s main east-west thoroughfare, the Via Gabina, traces the contour of the slopes of the crater, not far from the base of the mound, linking Rome to Praeneste. Between this road and the rim of the crater that defines the northern limit of the town, a series of terraces rise up. South of the road, the town continues across more level terrain. 

> Once upon a time in this town, there was a house. In fact, there were a number of houses, but this story is about a particular house: the Tincu House. This house was built two terraces north of the main road and one block east of a particularly important side street that runs down the slope from the crater’s rim to join up with the Via Gabina. The location of this house, tucked just off this crossroad, gives shape to its story. 

> To tell the story of this house, we must begin before the house was built and even before the streets that defined the limits of the property were laid out or the terraces were cut into the bedrock. At this time, huts, yards, and other small structures, some grouped into compounds and ringed with their own walls, were scattered across the slope of the crater. In the place where our house will be built, someone is living or working. Later events largely obscure the actions and lives of these early occupants, but we know they were there. We see a scrap of a floor—a compact clay surface, disconnected bits of stone-built architecture, and a few cuts in the bedrock that may have served as drains or as foundation trenches for walls. A thorough reorganization of the town in the late 5th or early 4th c. BCE disrupted whatever these people were doing. The decision was taken to lay out a network of streets that would divide the slope and the plain below into a series of long narrow blocks. Within each block, terraces delimited by rock-cut roads provided relatively flat areas on which new structures could be built. Our house is built in a space defined by a street on the west and terracing cuts on the north and south. The eastern limit of the property ends roughly halfway to the next street over, but it is not defined by the skeleton of streets and terraces that provide the physical frame for the town.

> After a pause when the block may have stood empty, the building of the house begins in the early 3rd c. BCE—roughly 280-260 BCE—with the serious business of creating a level and well-drained surface. We see a large cut into the bedrock to construct a level terrace, as well as the installation of channels cut into the bedrock, designed to funnel water south down the slope. Soil and rubble are spread across the building site where the house will appear. The initial outlines of the house are simple enough, a large rectangular courtyard flanked on its eastern side by four rooms. The courtyard is entered from the west through a doorway onto the north-south side street. The walled courtyard space was built open to the sky, and a well was set into the ground in the northern part. The rooms themselves are entered from the courtyard, and some may have connected to one another. The roofs slope outward, dumping rainwater into drains running along the exterior walls of the house. The house is built almost exclusively of local stone—the lapis Gabinus quarried just up the hill. 

> What happened in the house when it was in its first, simplest form? The usual things, we must suppose. People lived there, cooked and ate, argued and misplaced things, swept and trod down the floors. 

> Sooner or later, as often happens with houses and their occupants, the domestic arrangements proved unsatisfactory—not enough space/light or just things wearing down a bit. The floors in the eastern rooms are redone—scraped, leveled, re-packed, and resurfaced. A series of new rooms are added along the southern edge of the courtyard, providing more places to keep things, work, or have a bit of privacy. The house is still a house—just a nicer one with some extra rooms and new floors. Then the change comes. 

> What makes a family leave their home or allow it to be transformed into something that is not a home, even if they still own it? We can’t say, but we know it happened here. The house stops being a house, and a complete transformation into a place with another purpose begins. Two large rooms are added on the northern end of the house, taking over part of what had been an open courtyard. The door through the wall on the western side of the courtyard is blocked up, and a new entrance to the courtyard is built from the south, fronting on a drive that branches off the north-south road that runs up from the Praenestina. A small vestibule is built between the new doorway and the courtyard. Further rooms, small and confining, are added on the southern side of the courtyard, and a large basin is installed in the courtyard, taking up more than its fair share of space. A new wall is added in the courtyard, running parallel to the line of the original rooms, blocking off the formerly easy access to them. The kinds of stones used to build the walls are changed, with the local lapis Gabinus swapped out for the lighter if less durable tufo lionato. The sewers and drains that used to collect water running off the eaves outside the house are closed off. The roofs are rearranged, and instead of water dumping outward and running away from the property, it is gathered inward, pouring down the sloping tiles into the courtyard, perhaps spouted into the new central basin. 

> Why do all this? Why invert the roofs, block the drains, install a basin, add walls, and close and open doorways. It’s clear at this point that the house no longer works as a house. But it looks like a house, at least as seen from the interior of the courtyard or from the road looking across the threshold. There would be roofs, a basin to catch water, and some surrounding rooms—all the things one might expect. Like a stage set of a house, there’s no home behind it, but the look is good. What’s behind the facade?

> For a time, the rooms behind the courtyard remain in use, serving some unknown, backstage purpose. Offices, storage, or something along those lines seem likely. The spaces certainly aren’t well lit, easily accessed, or prominently positioned. This transformation, from a house into a something else, seems to be associated with the arrival of new neighbors. The installation of a large public complex one block to the west doubtless affected the story of this house. Its presence seems the most likely explanation for the radical change witnessed here: the transformation from a house into an annex. This conversion from private domestic to “other use” space requires some explanation. To propose an answer, we must stretch the limits of imagination. Maybe this public complex needed an administrative area. Maybe the house lot was bought and transformed, effectively split into two parts. In the former courtyard, the new walls, rearranged roofs, and basin create the illusion of standing in the interior court of a modern house. Hiding behind the domestic facade are spaces that might work for official business and storage. Together, these spaces create the illusion of the public parts of a house, with functional, if inconvenient, spaces hidden from view. 

> Something like this must have happened. We can be certain there is a transformation from a place that acts as a house, with fairly modest and uncomplicated spatial arrangements and a look as much rural farmhouse as contemporary urban dwelling, into a place that looks like a more modern “Roman” house but doesn’t act as one.

> The activities that required the illusion of a house wind down or shift elsewhere. The rooms hidden behind the facade are repurposed again, as quarrying in this part of the city creeps south down the slopes. The northern wall of the house comes down, and dumps accumulate within some of the rooms, while others remain used for something that required a clear space in reasonable repair. Eventually, the whole place is given up. 


> The basin in the courtyard is removed, and rubble and dirt are dumped in fits and starts. Over time, this space fills with debris. The walls of the house come down; whether of their own accord or through intentional removal is uncertain. The useful material is carted off elsewhere, and more dirt accumulates. In the end, there is no longer a house, a facade of a house, or a rough repurposed industrial space. There is just an empty lot once more. Then the next thing happens. 

# Concluding Thoughts

In this tutorial, you have learned to define a data narrative, to choose data to use in your narrative, to write a basic data narrative, and to apply a narrative lens (aka, a theoretical perspective) to your interpretation. You have considered how bias, tone, jargon, and audience can influence the choices you make in narrative construction, and you have analyzed both a provided narrative and a narrative of your own creation.

You now have the foundation to go from examining a site record to creating tabulated data and sharing that data through a data narrative.

We’ve reached the end of this Digital Data Story. We hope that over the course of the two sections of this tutorial we’ve helped to build your understanding on the importance of data literacy, and have provided the chance to practice with going from forms to notes to tables to narratives. 

Remember, the methods in this tutorial aren’t limited to this data set. What we’ve discussed can be adapted and used on your own project data, or on any open data. (Like that at Open Context, hint, hint!) We’d love to see how these methods are used outside of this tutorial, and any data narratives you’ve created out in the world. Please get in touch and share! 

# Acknowledgements 

The Digital Data Stories Team would like to acknowledge the work and help of the following people and organizations in the production of this resource. 


Rachel Opitz, Sara Perry, Kevin Garstki, Jenn Zovar, Ian Ray, Joshua Goode, and the anonymous testers and reviewers amongst our colleagues. 


Some images and text within this tutorial were originally published by the Gabii Project in their publication, [A Mid-Republican House From Gabii](https://doi.org/10.3998/mpub.9231782). This material is available thanks to a generous [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license. 

Frontis image modified by AAI from[ original](https://commons.wikimedia.org/wiki/File:Terracotta_acroterial_statue_of_a_Harpy_Siren,_from_Gabii,_end_of_6th_century_BC_-_beginning_5th_century_BC,_Monsters._Fantastic_Creatures_of_Fear_and_Myth_Exhibition,_Palazzo_Massimo_alle_Terme,_Rome_(12817668965).jpg) by [Carole Raddato](https://www.flickr.com/people/41523983@N08). This material is available thanks to a generous [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.en) license. 

