---
title: "Gabbing about Gabii: Going from Notes to Data to Narrative - Educator resource"
author: "L. Meghan Dennis, PhD. Postdoctoral Researcher for Data Interpretation and Public Engagement"
date: "10 June 2022 - Version 1.0"
output:
  word_document: default
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

![](Images/GabbinAboutGabii-1024x562.jpg)

## Contact the Team

If you or your students have questions, comments, or concerns, please get in touch. We love feedback, especially from educators!

L. Meghan Dennis  
@gingerygamer (she/her)

Paulina F. Przystupa  
@punuckish (she/their/none)

datastories@opencontext.org

# Exercise Description and Aims

The Digital Data Stories Project promotes an increased focus in archaeological education on digital data literacy. Through the use of open data sets, these exercises teach the principles of digital data literacy alongside methods in archaeological analysis. These combined code and theory practicals illustrate the confluence of science and humanities-based investigations in collected data on the past.

This approach promotes multiple levels of engagement with archaeological data sets, linking data-driven narratives with the key analytical and interpretive steps used to ethically analyze, visualize, and present research data.

In this Digital Data Story, participants will learn to:

> Evaluate and transpose data from context sheets

> Create tables based on context sheet data

> Add data to existing tables

> Utilize tabular data to create data narratives

> Analyze existing data narratives

> Apply narrative lenses to different data narratives

## additional texts

Gustavo Sandoval (2021) Single-Context Recording, Field Interpretation and Reflexivity: An Analysis of Primary Data In Context Sheets, Journal of Field Archaeology, 46:7, 496-512, DOI: 10.1080/00934690.2021.1926700

Johnston, A. C. and Mogetta, M. (2020) Debating Early Republican Urban-ism in Latium Vetus: The Town Planning of Gabii, between Archaeology and History, Journal of Roman Studies. Cambridge University Press, 110, pp. 91–121. DOI: 10.1017/S0075435820001070.

\newpage

## the data set

This data set allows researchers to select site data in the form of tables, texts, media, and maps. However, it may also be used for many other purposes relating to the study of Roman life during the  Republic and Imperial periods, and across the Italian peninsula and beyond.

These data were collected and analysed by the Gabii Project, an archaeological initiative led by Nicola Terrenato and the University of Michigan, with international partners.

## assessment and scaffolding options

For those students who require (or desire!) a more challenging data exercise, tutorials offered at GLAM Workbench can be utilized as scaffolding.

For those students who require (or desire!) more support,  splitting into pairs to complete the Digital Data Story offers the opportunity to work together, switching off tasks and comparing hypotheses and results.

As there are opportunities during the Digital Data Story for students to select tabular data for analysis based on their own interests, the end result tables produced may vary. One potential assessment option is to ask students to re-run the analysis with different context sheets.

## Technical Requirements

If engaging via their own computers, students should have a basic knowledge of how to operate a computer, how to visit a website, and how to download and install software.

If engaging via lab computers, students should have a basic knowledge of how to operate a computer and how to visit a website. Lab computers should have the following programs installed:


- A browser
- A text editor (Like TextEdit or Notepad)
- A spreadsheet program (Like Excel or Google Sheets)

The dataset that students will be using for this exercise is available at: 

https://doi.org/10.6078/M7N014GK

## Duration

The Digital Data Story is given in two parts. Each part should take approximately 1 hour. Each part can be completed separately, though they are additive when completed in total.

If students are completing a portion of the tutorial in more than one session, please direct them to pay close attention to how to save their work!

### get in touch!

If you or your students have questions, comments, or concerns. We love feedback, especially from educators!

License: [Creative Commons CC-BY](https://creativecommons.org/licenses/by/4.0/)